package com.kiosk.android;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by SeyyedMojtaba on 3/11/2016.
 */
public class HistoryService extends Service {

    private int start = 0, amount = Config.DEFAULT_HISTORY_BUFFER_ITEMS;
    public static long timestamp = 0;

    private List<String> history_ids = new ArrayList<String>();
    private List<String> history_authors = new ArrayList<String>();
    private List<String> history_author_names = new ArrayList<String>();
    private List<String> history_author_profile_images = new ArrayList<String>();
    private List<String> history_actions = new ArrayList<String>();
    private List<String> history_timestamps = new ArrayList<String>();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {

            if (getApplicationContext() != null) {


                Timer timer = new Timer();
                TimerTask task = new TimerTask() {
                    public void run() {

                        timestamp = Functions.getLong(getApplicationContext(), "last_check_timestamp", 0);

                        if (Functions.getBoolean(getApplicationContext(), "logged in", false) && Functions.isOnline(getApplicationContext()) && timestamp > 0 && Functions.getBoolean(getApplicationContext(), "show_notifications", true) && Functions.getInt(getApplicationContext(), "HISTORY_REFRESH_INTERVAL", Config.DEFAULT_HISTORY_REFRESH_INTERVAL) > 0) {
                            amount = Functions.getInt(getApplicationContext(), "HISTORY_BUFFER_ITEMS", Config.DEFAULT_HISTORY_BUFFER_ITEMS);

                            new AsyncTask<String, String, String>() {

                                @Override
                                protected void onPreExecute() {
                                    Log.d("history service", "getting history...");
                                    super.onPreExecute();
                                }

                                @Override
                                protected String doInBackground(String... strings) {
                                    return Fetcher.FetchURL(APIManager.getHistory(Functions.getString(getApplicationContext(), "id", ""), amount, String.valueOf(timestamp)));
                                }

                                @Override
                                protected void onPostExecute(String result) {

                                    Functions.putLong(getApplicationContext(), "last_check_timestamp", System.currentTimeMillis());

                                    if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "history").equals("-1")) {

                                        if (Functions.getBoolean(getApplicationContext(), "notification_sound", true)) {
                                            Functions.playSound(getApplicationContext(), R.raw.notification, 2000, AudioManager.STREAM_NOTIFICATION);
                                        }

                                        if (Functions.getBoolean(getApplicationContext(), "notification_vibration", true)) {
                                            Functions.Vibrate(getApplicationContext(), 500);
                                        }

                                        history_ids.addAll(JSONParser.getListFromString(result, "history", "id"));
                                        history_authors.addAll(JSONParser.getListFromString(result, "history", "author"));
                                        history_author_names.addAll(JSONParser.getListFromString(result, "history", "author_name"));
                                        history_author_profile_images.addAll(JSONParser.getListFromString(result, "history", "author_profile_image"));
                                        history_actions.addAll(JSONParser.getListFromString(result, "history", "action"));
                                        history_timestamps.addAll(JSONParser.getListFromString(result, "history", "timestamp"));


                                        for (int i = 0; i < history_ids.size(); i++) {
                                            String message = "";
                                            int id = 0;

                                            if (history_actions.get(i).equals("follow_request")) {
                                                message = "میخواهد شما را دنبال کند.";
                                                id = Config.NOTIFICATION_ID_FOLLOW_REQUEST;
                                            } else if (history_actions.get(i).equals("follow_accept")) {
                                                message = "درخواست شما را قبول کرد.";
                                                id = Config.NOTIFICATION_ID_FOLLOW_ACCEPT;
                                            } else if (history_actions.get(i).equals("follow")) {
                                                message = "شما را دنبال کرد.";
                                                id = Config.NOTIFICATION_ID_FOLLOW;
                                            } else if (history_actions.get(i).equals("like")) {
                                                message = "مطلب شما را پسندید.";
                                                id = Config.NOTIFICATION_ID_LIKE;
                                            } else if (history_actions.get(i).equals("comment")) {
                                                message = "برای مطلب شما نظر گذاشت.";
                                                id = Config.NOTIFICATION_ID_COMMENT;
                                            } else if (history_actions.get(i).equals("tag_post")) {
                                                message = "در مطلبی از شما یاد کرد.";
                                                id = Config.NOTIFICATION_ID_TAG_POST;
                                            } else if (history_actions.get(i).equals("tag_comment")) {
                                                message = "در نظری از شما یاد کرد.";
                                                id = Config.NOTIFICATION_ID_TAG_COMMENT;
                                            }


                                            if (getApplicationContext() != null) {
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                intent.putExtra("action", "show_history");

                                                Notification notification = Functions.createNotification(getApplicationContext(), R.drawable.ic_notification, R.drawable.ic_launcher, message, EmojiMapUtil.replaceCheatSheetEmojis(history_author_names.get(i)), message, intent, true, System.currentTimeMillis(), false, 0, false, true, Functions.getInt(getApplicationContext(), "notification_color", getApplicationContext().getResources().getColor(R.color.basicPrimary)), 1000, 1000);
                                                Functions.showNotification(getApplicationContext(), notification, Integer.parseInt(String.valueOf(id) + history_ids.get(i)));
                                            }

                                        }
                                    }


                                    super.onPostExecute(result);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }


                };

                timer.schedule(task, 0, Functions.getInt(getApplicationContext(), "HISTORY_REFRESH_INTERVAL", Config.DEFAULT_HISTORY_REFRESH_INTERVAL));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }
}
