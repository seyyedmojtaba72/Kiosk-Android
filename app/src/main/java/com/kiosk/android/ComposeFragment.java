package com.kiosk.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.Information;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by SeyyedMojtaba on 2/25/2016.
 */
public class ComposeFragment extends Fragment {
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private static final int CROP = 3;

    public static boolean toCrop = false;


    public static Activity activity;
    public static Context context;
    public static View view;

    private String temp_path, uploaded_image_address = "";
    public static Uri mImageUri, mOutputImageUri;
    private boolean uploaded = false;

    private EditText nameText, descriptionText, priceText, downloadLinkText;
    private ImageButton imageButton;
    private ProgressBar loading;


    private ActionProcessButton submitButton;
    private boolean isProcessing = false;


    public ComposeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_compose, container, false);
        this.view = rootView;

        temp_path = Information.TEMP_PATH_PREFIX + "post.jpg";
        File tempFile = new File(temp_path);
        if (tempFile.exists()){
            tempFile.delete();
        }

        nameText = (EditText) rootView.findViewById(R.id.name);
        descriptionText = (EditText) rootView.findViewById(R.id.description);
        imageButton = (ImageButton) rootView.findViewById(R.id.image);
        loading = (ProgressBar) rootView.findViewById(R.id.progress);
        downloadLinkText = (EditText) rootView.findViewById(R.id.download_link);
        priceText = (EditText) rootView.findViewById(R.id.price);

        downloadLinkText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (downloadLinkText.getText().toString().isEmpty()) {
                    priceText.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_out));
                    priceText.setVisibility(View.GONE);
                    priceText.setText("");
                } else {
                    priceText.setVisibility(View.VISIBLE);
                    priceText.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in));
                }
            }
        });

        submitButton = (ActionProcessButton) rootView.findViewById(R.id.submit);
        submitButton.setMode(ActionProcessButton.Mode.ENDLESS);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                ArrayList<String> localArrayList = new ArrayList<String>();
                localArrayList.add("تصویر جدید بگیرید");
                localArrayList.add("انتخاب از گالری");
                if (mOutputImageUri != null) {
                    localArrayList.add("حذف");
                }
                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                        switch (paramAnonymous2Int) {
                            default:
                                return;
                            case 0:
                                try {
                                    File tempFile = new File(temp_path);
                                    if (tempFile.exists()){
                                        tempFile.delete();
                                    }
                                    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                                    localIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(temp_path)));
                                    startActivityForResult(localIntent, PICK_FROM_CAMERA);
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 1:
                                try {
                                    File tempFile = new File(temp_path);
                                    if (tempFile.exists()){
                                        tempFile.delete();
                                    }
                                    Intent localIntent1 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(localIntent1, PICK_FROM_FILE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 2:
                                imageButton.setImageResource(R.drawable.ic_no_image);
                                mOutputImageUri = null;
                                break;
                        }

                    }
                });
                localBuilder.create().show();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOutputImageUri == null) {
                    Toast.makeText(activity, "لطفاً موارد الزامی را پر کنید.", Toast.LENGTH_LONG).show();
                    return;
                }

                if ((!Functions.isOnline(context))) {
                    Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                    return;
                }

                uploadImage();
            }
        });

        return rootView;
    }

    private void submitPost() {

        final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

        new AsyncTask<String, String, String>() {
            String name = "", description = "", price = "", download_link = "";


            @Override
            protected void onPreExecute() {
                submitButton.setProgress(50);
                if (priceText.getText().toString().equals("")) {
                    priceText.setText("0");
                }

                name = nameText.getText().toString();
                description = descriptionText.getText().toString();
                price = priceText.getText().toString();
                download_link = downloadLinkText.getText().toString();

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0) {
                return Fetcher.FetchURL(APIManager.setMyPost(Functions.getString(context, "id", ""), name, description, uploaded_image_address, price, download_link));
            }

            protected void onPostExecute(final String result) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        if (result.equals("invalid-link")) {
                            sDialog.setTitleText("").setContentText("لینک دانلود معتبر نیست").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        } else if (result.equals("success")) {
                            uploaded_image_address = "";
                            mImageUri = null;
                            mOutputImageUri = null;
                            uploaded = false;
                            nameText.setText("");
                            descriptionText.setText("");
                            priceText.setText("");
                            downloadLinkText.setText("");
                            imageButton.setImageResource(R.drawable.ic_no_image);

                            sDialog.setTitleText("").setContentText("مطلب شما منتشر شد.").setConfirmText("باشه").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    // RELOAD POSTS
                                    MainActivity.viewPager.setCurrentItem(0, true);
                                    MainActivity.tabLayout.setScrollPosition(0, 0, true);
                                    HomeFragment.refreshPosts();
                                    sDialog.hide();
                                }
                            }).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        } else {
                            sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        }

                        submitButton.setProgress(0);
                        isProcessing = false;
                    }
                });

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        sDialog.show();

    }


    /*
    @Override
    public void onActivityResult(int requestCode, int result, final Intent data) {
        if (result != -1) return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:


                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {
                        imageButton.setImageURI(null);
                        imageButton.setImageResource(R.drawable.ic_no_image);
                        loading.setVisibility(View.VISIBLE);
                        loading.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.fade_in));

                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        mImageUri = data.getData();
                        Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {

                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                mImageUri = null;

                                // Rotate picture if necessary
                                Bitmap fileBitmap = Functions.getBitmapFromFile(temp_path);
                                fileBitmap = Functions.rotateBitmap(fileBitmap, Functions.getOrientationOfFile(temp_path));
                                Functions.saveBitmapToFile(fileBitmap, temp_path);
                                return null;

                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                loading.setVisibility(View.INVISIBLE);

                                                mImageUri = Uri.fromFile(new File(temp_path));
                                                imageButton.setImageURI(mImageUri);
                                            }
                                        });
                                    }
                                }, 0);

                                super.onPostExecute(aVoid);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
            case PICK_FROM_FILE:

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {
                        imageButton.setImageURI(null);
                        imageButton.setImageResource(R.drawable.ic_no_image);
                        loading.setVisibility(View.VISIBLE);

                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        mImageUri = data.getData();
                        Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                mImageUri = null;

                                // Rotate picture if necessary
                                Bitmap fileBitmap2 = Functions.getBitmapFromFile(temp_path);
                                fileBitmap2 = Functions.rotateBitmap(fileBitmap2, Functions.getOrientationOfFile(temp_path));
                                Functions.saveBitmapToFile(fileBitmap2, temp_path);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                loading.setVisibility(View.INVISIBLE);

                                                mImageUri = Uri.fromFile(new File(temp_path));
                                                imageButton.setImageURI(mImageUri);
                                            }
                                        });
                                    }
                                }, 0);
                                super.onPostExecute(aVoid);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
        }
    }*/


    private void doCrop() {
        mOutputImageUri = null;
        try {

            File file = new File(temp_path + "_cropped");
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            Uri fileUri = Functions.getUriFromFile(activity, temp_path);
            Uri croppedFileUri = Functions.getUriFromFile(activity, temp_path + "_cropped");


            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image/*");
            cropIntent.putExtra("output", croppedFileUri);
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            //cropIntent.putExtra("outputX", 1024);
            //cropIntent.putExtra("outputY", 1024);


            List<ResolveInfo> resInfoList = activity.getPackageManager().queryIntentActivities(cropIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                if (packageName != null && fileUri != null) {
                    activity.grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                if (packageName != null && croppedFileUri != null) {
                    activity.grantUriPermission(packageName, croppedFileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }





            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, croppedFileUri);
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // retrieve data on return

            //cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, CROP);
            // start the activity - we handle returning in onActivityResult


        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException | IOException e) {
            Log.d("error", "no activity found for cropping image.");
            e.printStackTrace();
            mOutputImageUri = Uri.fromFile(new File(temp_path));
            imageButton.setImageURI(mOutputImageUri);
        }
    }


    @SuppressWarnings("unused")
    private void displayGallery() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) && !Environment.getExternalStorageState().equals(Environment.MEDIA_CHECKING)) {
            Intent intent = new Intent();
            intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, PICK_FROM_FILE);
        } else {
            //Toaster.make(getApplicationContext(), R.string.no_media);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if (resultCode != -1) return;

        if (requestCode == PICK_FROM_CAMERA) {
            try {
                if (resultCode == Activity.RESULT_OK) {
                    //mImageUri = data.getData();
                    //Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                    Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                    Functions.rotateImageIfNeed(temp_path);
                    mImageUri = Uri.fromFile(new File(temp_path));
                    displayPhotoActivity(1);
                } else {
                    //UriToUrl.deleteUri(context, mImageUri);
                }
            } catch (Exception e) {
                //Toaster.make(getApplicationContext(), R.string.error_img_not_found);
            }
        } else if (resultCode == -1 && requestCode == PICK_FROM_FILE) {
            try {
                mImageUri = data.getData();
                Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                Functions.rotateImageIfNeed(temp_path);
                mImageUri = Uri.fromFile(new File(temp_path));
                displayPhotoActivity(2);
            } catch (Exception e) {
                //Toaster.make(getApplicationContext(), R.string.error_img_not_found);
            }
        } else if (resultCode == -1 && requestCode == CROP) {
            Functions.copyFile(new File(temp_path + "_cropped"), new File(temp_path), true);
            mOutputImageUri = Uri.fromFile(new File(temp_path));
            imageButton.setImageURI(mOutputImageUri);
        }
    }

    @SuppressWarnings("unused")
    private void displayCamera() {
        mImageUri = getOutputMediaFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    private Uri getOutputMediaFile() {
        ContentValues values = new ContentValues();
        return activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    private void displayPhotoActivity(int source_id) {
        Intent intent = new Intent(context, PhotoActivity.class);
        intent.putExtra("image_source", source_id);
        intent.setData(mImageUri);
        startActivity(intent);
        //overridePendingTransition(0, 0);
        //finish();
    }


    private void uploadImage() {
        if (isProcessing) {
            return;
        }

        if (mOutputImageUri == null) {
            submitPost();
            return;
        }

        isProcessing = true;

        String fileAddress = Information.TEMP_PATH_PREFIX;
        final String fileName = "post.jpg";
        final String file = fileAddress + fileName;


        final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("در حال آپلود فایل...").setContentText("").showCancelButton(false);


        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                submitButton.setProgress(50);

                super.onPreExecute();
            }

            protected String doInBackground(String... paramArrayOfString) {
                Functions.reduceImageFileSize(file, 1024 * Functions.getInt(context, "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                String result = Functions.uploadFile(temp_path, APIManager.uploadFile("user_uploads/" + Functions.getString(context, "id", ""), false, true));

                if (result.equals("fail")) {
                    uploaded = false;
                } else {
                    uploaded = true;
                }
                return result;
            }

            protected void onPostExecute(String result) {
                if (uploaded) {
                    uploaded_image_address = Functions.getString(context, "USERS_UPLOADS_URL", Information.DEFAULT_USERS_UPLOADS_URL) + Functions.getString(context, "id", "") + "/" + result;
                    submitPost();
                    sDialog.hide();
                } else {
                    sDialog.setTitleText("").setContentText("خطا در آپلود فایل!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    submitButton.setProgress(0);
                    isProcessing = false;
                }
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        sDialog.show();
    }

    @Override
    public void onResume() {

        super.onResume();

        if (mOutputImageUri == null || !toCrop) {
            loading.setVisibility(View.INVISIBLE);
            return;
        }

        if (toCrop) {
            toCrop = false;
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                imageButton.setImageURI(null);
                imageButton.setImageResource(R.drawable.ic_no_image);
                loading.setVisibility(View.VISIBLE);

                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                Functions.copyFile(new File(Functions.getPathFromURI(context, mOutputImageUri)), new File(temp_path), true);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);

                        return null;

                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loading.setVisibility(View.INVISIBLE);

                                        doCrop();

                                    }
                                });
                            }
                        }, 0);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        } else {
            this.context = context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}