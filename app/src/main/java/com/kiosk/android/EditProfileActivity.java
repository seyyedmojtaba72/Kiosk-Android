package com.kiosk.android;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.info.APIManager;

import java.util.Timer;
import java.util.TimerTask;


public class EditProfileActivity extends AppCompatActivity {
    private int trans_time = 10 * 1000;
    private int item_time = 10 * 1000;
    private int trans_num = 0;
    private int trans_num2 = 1;
    private int[] trans_drawables = new int[]{R.drawable.gradient_bg1, R.drawable.gradient_bg2, R.drawable.gradient_bg3, R.drawable.gradient_bg4, R.drawable.gradient_bg5, R.drawable.gradient_bg6};

    private boolean isProcessing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        animateBackground();

        final TextView hintText = (TextView) findViewById(R.id.hint);

        final EditText usernameText = (EditText) findViewById(R.id.username);
        final EditText first_nameText = (EditText) findViewById(R.id.first_name);
        final EditText last_nameText = (EditText) findViewById(R.id.last_name);
        final EditText emailText = (EditText) findViewById(R.id.email);
        final EditText mobileNumberText = (EditText) findViewById(R.id.mobile_number);
        final EditText shabaText = (EditText) findViewById(R.id.shaba);
        final EditText aboutText = (EditText) findViewById(R.id.about);
        final CheckBox privateCheckBox = (CheckBox) findViewById(R.id.private_check);

        usernameText.setText(Functions.getString(getApplicationContext(), "username", ""));
        first_nameText.setText(Functions.getString(getApplicationContext(), "first_name", ""));
        last_nameText.setText(Functions.getString(getApplicationContext(), "last_name", ""));
        emailText.setText(Functions.getString(getApplicationContext(), "email", ""));
        mobileNumberText.setText(Functions.getString(getApplicationContext(), "mobile_number", ""));
        shabaText.setText(Functions.getString(getApplicationContext(), "shaba", ""));
        aboutText.setText(Functions.getString(getApplicationContext(), "about", ""));
        if (Functions.getString(getApplicationContext(), "extras", "").contains("private;")) {
            privateCheckBox.setChecked(true);
        }

        final Button updateButton = (Button) findViewById(R.id.update);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isProcessing) {
                    return;
                }

                if (!Functions.isOnline(getApplicationContext())) {
                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.alert_connect_internet), Toast.LENGTH_LONG).show();
                    startActivity(new Intent("android.settings.WIFI_SETTINGS"));
                    return;
                }

                if ((usernameText.getText().toString().isEmpty() | emailText.getText().toString().isEmpty())) {
                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.alert_enter_required_fields), Toast.LENGTH_LONG).show();
                    return;
                }

                new AsyncTask<String, String, String>() {
                    String username = "", first_name = "", last_name = "", mobile_number = "", shaba = "", email = "", about = "", extras = "";

                    @Override
                    protected void onPreExecute() {
                        username = usernameText.getText().toString();
                        first_name = first_nameText.getText().toString();
                        last_name = last_nameText.getText().toString();
                        mobile_number = mobileNumberText.getText().toString();
                        shaba = shabaText.getText().toString();
                        email = emailText.getText().toString();
                        about = aboutText.getText().toString();

                        if (privateCheckBox.isChecked()) {
                            extras += "private;";
                        }


                        updateButton.setBackgroundResource(R.drawable.selector_button_glass_blue);
                        updateButton.setText("در حال بروزرسانی...");
                        isProcessing = true;

                        super.onPreExecute();
                    }

                    @Override
                    protected String doInBackground(String... strings) {

                        return Fetcher.FetchURL(APIManager.updateProfile(username, first_name, last_name, mobile_number, shaba, Functions.getString(getApplicationContext(), "email", ""), email, Functions.getString(getApplicationContext(), "profile_image", ""), about, extras));
                    }

                    @Override
                    protected void onPostExecute(String result) {
                        isProcessing = false;
                        if (result.equals("invalid-username")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("نام کاربری باید شامل حروف انگلیسی و اعداد باشد.");
                        } else if (result.equals("username-exists")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("کاربری با این نام کاربری موجود است.");
                        } else if (result.equals("max-username-length")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("طول نام کاربری باید حداکثر 50 کاراکتر باشد.");
                        } else if (result.equals("mobile_number-length")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("طول شماره موبایل باید 10 کاراکتر باشد.");
                        } else if (result.equals("mobile_number-exists")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("کاربری با این شماره موبایل موجود است.");
                        } else if (result.equals("invalid-email")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("ایمیل معتبر نیست.");
                        } else if (result.equals("email-exists")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("کاربری با این ایمیل موجود است.");
                        } else if (result.equals("max-email-length")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("طول آدرس ایمیل باید حداکثر 50 کاراکتر باشد!");
                        } else if (result.equals("min-length")) {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("طول نام کاربری باید حداقل 6 کاراکتر باشد.");
                        } else if (result.equals("success")) {
                            isProcessing = true;
                            ProfileFragment.toRefresh = true;

                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_green);
                            updateButton.setText("اطلاعات شما بروز شد!");

                            Functions.putString(getApplicationContext(), "username", usernameText.getText().toString());
                            Functions.putString(getApplicationContext(), "email", emailText.getText().toString());
                            Functions.putString(getApplicationContext(), "first_name", first_nameText.getText().toString());
                            Functions.putString(getApplicationContext(), "last_name", last_nameText.getText().toString());
                            Functions.putString(getApplicationContext(), "mobile_number", mobileNumberText.getText().toString());
                            Functions.putString(getApplicationContext(), "shaba", shabaText.getText().toString());
                            Functions.putString(getApplicationContext(), "about", aboutText.getText().toString());
                            Functions.putString(getApplicationContext(), "extras", extras);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 2000);
                        } else {
                            updateButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                            updateButton.setText("خطای غیرمنتظره‌ای پیش آمده است!");
                        }

                        super.onPostExecute(result);
                    }
                }.execute();

            }
        });

    }


    private void animateBackground() {
        final RelativeLayout bgLayout = (RelativeLayout) findViewById(R.id.bg_layout);

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TransitionDrawable trans = new TransitionDrawable(new Drawable[]{getResources().getDrawable(trans_drawables[trans_num]), getResources().getDrawable(trans_drawables[trans_num2])});
                        bgLayout.setBackgroundDrawable(trans);
                        trans.startTransition(trans_time);
                        trans_num++;
                        trans_num2 = trans_num + 1;
                        if (trans_num > trans_drawables.length - 1) {
                            trans_num = 0;
                            trans_num2 = 1;
                        }
                        if (trans_num2 > trans_drawables.length - 1) {
                            trans_num2 = 0;
                        }
                    }
                });
            }
        };

        timer.schedule(task, 0, item_time);
    }

}