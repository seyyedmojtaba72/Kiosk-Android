package com.kiosk.android;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.LinkManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatActivity {

    private SettingsFragment mSettingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("تنظیمات");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (savedInstanceState == null) {
            mSettingsFragment = new SettingsFragment();
            replaceFragment(R.id.settings_container, mSettingsFragment);
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void replaceFragment(int viewId, android.app.Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(viewId, fragment).commit();
    }

    /**
     * A placeholder fragment containing a settings view.
     */
    public static class SettingsFragment extends PreferenceFragment {
        private Activity activity;
        private Context context;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);


            Preference editProfile = (Preference) findPreference("edit_profile");
            editProfile.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    startActivity(new Intent(context, EditProfileActivity.class));

                    return true;
                }
            });

            Preference changePassword = (Preference) findPreference("change_password");
            changePassword.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                    View view = activity.getLayoutInflater().inflate(R.layout.dialog_change_password, null);
                    final EditText formerPasswordText = (EditText) view.findViewById(R.id.former_password);
                    final EditText passwordText = (EditText) view.findViewById(R.id.password);
                    final EditText passwordConfirmText = (EditText) view.findViewById(R.id.password_confirm);

                    final EditText amountText = (EditText) view.findViewById(R.id.amount);
                    localBuilder.setView(view).setPositiveButton("تغییر", null).setNegativeButton("انصراف", null);
                    final AlertDialog dialog = localBuilder.create();
                    dialog.show();
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            if ((!Functions.isOnline(context))) {
                                Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                                return;
                            }

                            if (formerPasswordText.getText().toString().isEmpty() || passwordText.getText().toString().isEmpty() || passwordConfirmText.getText().toString().isEmpty()) {
                                Toast.makeText(activity, "موارد خواسته شده را پر کنید.", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (!passwordText.getText().toString().equals(passwordConfirmText.getText().toString())) {
                                Toast.makeText(activity, "گذرواژه و تکرار آن مطابقت ندارد!", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                            new AsyncTask<String, String, String>() {
                                String former_password = "", password = "";

                                @Override
                                protected void onPreExecute() {
                                    sDialog.show();
                                    sDialog.setCancelable(false);

                                    former_password = formerPasswordText.getText().toString();
                                    password = passwordText.getText().toString();

                                    super.onPreExecute();
                                }

                                @Override
                                protected String doInBackground(String... arg0) {
                                    return Fetcher.FetchURL(APIManager.changePassword(Functions.getString(context, "username", ""), former_password, password));
                                }

                                protected void onPostExecute(final String result) {
                                    activity.runOnUiThread(new Runnable() {
                                        public void run() {
                                            if (result.contains("incorrect")) {
                                                sDialog.setTitleText("").setContentText("گذرواژه فعلی شما صحیح نیست!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                            } else if (result.equals("min-length")) {
                                                sDialog.setTitleText("").setContentText("طول گذرواژه باید حداقل 6 کاراکتر باشد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                            } else if (result.equals("success")) {
                                                sDialog.setTitleText("").setContentText("گذرواژه شما با موفقیت تغییر کرد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                Functions.putString(context, "password", password);
                                                dialog.dismiss();
                                            } else {
                                                sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                            }


                                        }
                                    });
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    });


                    return true;
                }
            });

            Preference manageAccount = (Preference) findPreference("manage_account");
            manageAccount.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Functions.browseWebViaIntent(context, getString(R.string.browse_web), LinkManager.manageAccount(Functions.getString(context, "username", ""), Functions.getString(context, "password", "")));
                    return true;
                }
            });

            Preference logout = (Preference) findPreference("logout");
            logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Functions.putBoolean(context, "logged in", false);
                    Functions.putString(context, "username", "");
                    Functions.putString(context, "id", "");
                    Functions.putString(context, "password", "");
                    Functions.putString(context, "email", "");
                    Functions.putString(context, "profile_image", "");
                    Functions.putString(context, "first_name", "");
                    Functions.putString(context, "last_name", "");
                    Functions.putString(context, "status", "");
                    Functions.putString(context, "mobile_number", "");
                    Functions.putString(context, "level", "");
                    Functions.putString(context, "balance", "");
                    Functions.putString(context, "shaba", "");
                    Functions.putString(context, "about", "");
                    Functions.putString(context, "extras", "");
                    Functions.putString(context, "timestamp", "");
                    Functions.putString(context, "posts", "");
                    Functions.putString(context, "followers", "");
                    Functions.putString(context, "followeds", "");
                    Functions.putString(activity, "gcm_token", "");
                    if (activity != null) {
                        activity.finish();
                    }
                    if (MainActivity.activity != null) {
                        MainActivity.activity.finish();
                    }
                    startActivity(new Intent(context, LoginActivity.class));
                    return true;
                }
            });

            Preference report = (Preference) findPreference("report");
            report.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                int position = 0;

                public boolean onPreferenceClick(Preference preference) {
                    final String[] reportItems = {"bug_android", "feedback"};
                    final String[] reportItemsTitles = {"باگ نرم افزاری", "بازخورد"};

                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                    View view = activity.getLayoutInflater().inflate(R.layout.dialog_report, null);
                    final Button subjectButton = (Button) view.findViewById(R.id.subject);
                    localBuilder.setView(view);
                    subjectButton.setText(reportItemsTitles[0]);

                    subjectButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(activity);
                            localBuilder2.setItems(reportItemsTitles, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, final int i) {
                                    position = i;
                                    subjectButton.setText(reportItemsTitles[i]);
                                }
                            });

                            localBuilder2.create().show();
                        }
                    });


                    final EditText reportText = (EditText) view.findViewById(R.id.report);
                    localBuilder.setPositiveButton("گزارش", null).setNegativeButton("انصراف", null);
                    final AlertDialog dialog = localBuilder.create();
                    dialog.show();
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            if ((!Functions.isOnline(context))) {
                                Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                                return;
                            }

                            if (reportText.getText().toString().isEmpty()) {
                                Toast.makeText(activity, "موارد خواسته شده را پر کنید.", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                            new AsyncTask<String, String, String>() {
                                String report = "";

                                @Override
                                protected void onPreExecute() {
                                    sDialog.show();
                                    sDialog.setCancelable(false);

                                    report = reportText.getText().toString();

                                    super.onPreExecute();
                                }

                                @Override
                                protected String doInBackground(String... arg0) {

                                    return Fetcher.FetchURL(APIManager.Report(Functions.getString(context, "id", ""), "", reportItems[position], report));
                                }

                                protected void onPostExecute(final String result) {
                                    activity.runOnUiThread(new Runnable() {
                                        public void run() {
                                            if (result.equals("success")) {
                                                sDialog.setTitleText("").setContentText("گزارش شما با موفقیت ثبت شد. نتیجه بررسی برایتان ایمیل خواهد شد. با تشکر.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                dialog.dismiss();
                                            } else {
                                                sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                            }


                                        }
                                    });
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    });

                    return true;
                }
            });

            Preference viewTerms = (Preference) findPreference("view_terms");
            viewTerms.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Functions.browseWebViaIntent(context, getString(R.string.browse_web), LinkManager.viewTerms());
                    return true;
                }
            });

            Preference viewHelp = (Preference) findPreference("view_help");
            viewHelp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Functions.browseWebViaIntent(context, getString(R.string.browse_web), LinkManager.viewHelp());
                    return true;
                }
            });

            Preference openWebsite = (Preference) findPreference("open_website");
            openWebsite.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Functions.browseWebViaIntent(context, getString(R.string.browse_web), Information.WEBSITE_HOME_URL);
                    return true;
                }
            });
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            this.activity = activity;
            context = activity.getApplicationContext();
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof Activity) {
                activity = (Activity) context;
            } else {
                this.context = context;
            }
        }
    }

}