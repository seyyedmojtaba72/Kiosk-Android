/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kiosk.android.fcm;

import android.app.Notification;
import android.content.Intent;
import android.media.AudioManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kiosk.android.ChatActivity;
import com.kiosk.android.MainActivity;
import com.kiosk.android.R;
import com.kiosk.android.info.Config;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.kiosk.android.util.NotificationUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private DatabaseHelper dbHelper;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


        String flag = remoteMessage.getData().get("flag");
        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");
        String data = remoteMessage.getData().get("data");
        String timestamp = remoteMessage.getData().get("timestamp");


        switch (Integer.parseInt(flag)) {
            case Config.FLAG_ACTIVITY:
                processActivityPush(data, timestamp);
                break;
            case Config.FLAG_CHAT:
                processChatPush(data, timestamp);
                break;
            default:
                processPush(title, timestamp);
                break;
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    private void processActivityPush(String data, String timestamp) {

        if (Functions.getBoolean(getApplicationContext(), "show_notifications", true)) {

            String id = JSONParser.getFromString(data, "id");
            String author = JSONParser.getFromString(data, "author");
            String action = JSONParser.getFromString(data, "action");
            String message = JSONParser.getFromString(data, "message");

            if (Functions.getBoolean(getApplicationContext(), "notification_sound", true)) {
                Functions.playSound(getApplication(), R.raw.notification, 1000, AudioManager.STREAM_NOTIFICATION);
            }

            if (Functions.getBoolean(getApplicationContext(), "notification_vibration", true)) {
                Functions.Vibrate(getApplicationContext(), 500);
            }

            int notification_id = Config.NOTIFICATION_ID_DEFAULT;
            if (action.equals("follow_request")) {
                message = "میخواهد شما را دنبال کند.";
                notification_id = Config.NOTIFICATION_ID_FOLLOW_REQUEST;
            } else if (action.equals("follow_accept")) {
                message = "درخواست شما را قبول کرد.";
                notification_id = Config.NOTIFICATION_ID_FOLLOW_ACCEPT;
            } else if (action.equals("follow")) {
                message = "شما را دنبال کرد.";
                notification_id = Config.NOTIFICATION_ID_FOLLOW;
            } else if (action.equals("like")) {
                message = "مطلب شما را پسندید.";
                notification_id = Config.NOTIFICATION_ID_LIKE;
            } else if (action.equals("comment")) {
                message = "برای مطلب شما نظر گذاشت.";
                notification_id = Config.NOTIFICATION_ID_COMMENT;
            } else if (action.equals("tag_post")) {
                message = "در مطلبی از شما یاد کرد.";
                notification_id = Config.NOTIFICATION_ID_TAG_POST;
            } else if (action.equals("tag_comment")) {
                message = "در نظری از شما یاد کرد.";
                notification_id = Config.NOTIFICATION_ID_TAG_COMMENT;
            }

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.INTENT_PUSH_NOTIFICATION);
                pushNotification.putExtra("type", Config.FLAG_ACTIVITY);
                pushNotification.putExtra("message", author + " " + message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                MainActivity.new_activity = true;

            } else {

                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                resultIntent.putExtra("action", "show_history");

                Notification notification = Functions.createNotification(getApplicationContext(), R.drawable.ic_notification, R.drawable.ic_launcher, message, EmojiMapUtil.replaceCheatSheetEmojis(author), message, resultIntent, true, Long.parseLong(Functions.normalLength(timestamp, 13, "0", false)), false, 0, false, true, Functions.getInt(getApplicationContext(), "notification_color", getApplicationContext().getResources().getColor(R.color.basicPrimary)), 1000, 2000);
                Functions.showNotification(getApplicationContext(), notification, notification_id);

            }
        }
    }


    private void processChatPush(String data, String timestamp) {

        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getApplicationContext());
        }


        String id = JSONParser.getFromString(data, "id");
        String chat_id = JSONParser.getFromString(data, "chat");
        String sender = JSONParser.getFromString(data, "sender");
        String sender_name = JSONParser.getFromString(data, "sender_name");
        String sender_profile_image = JSONParser.getFromString(data, "sender_profile_image");
        String action = JSONParser.getFromString(data, "action");
        String message = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getFromString(data, "message"));
        String extras = JSONParser.getFromString(data, "extras");
        timestamp = JSONParser.getFromString(data, "timestamp");

        ChatsMessage chats_message = new ChatsMessage();
        chats_message.id = Integer.parseInt(id);
        chats_message.chat = chat_id;
        chats_message.sender = sender;
        chats_message.sender_name = sender_name;
        chats_message.sender_profile_image = sender_profile_image;
        chats_message.action = action;
        chats_message.message = message;
        chats_message.extras = extras;
        chats_message.timestamp = timestamp;

        chats_message.type = "action";
        if (chats_message.action.equals("message") || chats_message.action.equals("mime-image")) {
            if (chats_message.sender.equals(Functions.getString(getApplicationContext(), "id", ""))) {
                chats_message.type = "me";
            } else {
                chats_message.type = "participant";
            }
        }

        if (dbHelper.checkChat(Integer.parseInt(chats_message.chat))) {
            if (dbHelper.getChat(Integer.parseInt(chats_message.chat)).type.equals("deleted")) {

                ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> filter = new HashMap<String, String>();
                filter.put("name", "chat");
                filter.put("method", "equal");
                filter.put("value", chats_message.chat);
                filters.add(filter);
                List<ChatsMessage> chats_messages = dbHelper.searchChatsMessages(filters, "id", 0, 1, true, true);

                if (chats_messages.size() > 0) {
                    Log.d("tag", "former id: " + chats_messages.get(0).id + ", new id: " + chats_message.id);
                    if (chats_message.id > chats_messages.get(0).id) {
                        Chat chatt = dbHelper.getChat(Integer.parseInt(chats_message.chat));
                        chatt.type = "single";
                        dbHelper.updateChat(chatt);
                    } else {
                        return;
                    }
                }
            }
        }


        String title = sender_name;
        if (chats_message.action.equals("mime-image")) {
            message = "تصویر";
        } else if (chats_message.action.equals("message")) {
        } else {
            String actionText = sender_name + " ";
            if (chats_message.action.equals("create_group")) {
                actionText += "گروه را ایجاد کرد.";
            } else if (chats_message.action.equals("change_name")) {
                actionText += "نام گروه را به «" + message + "» تغییر داد.";
            } else if (chats_message.action.equals("change_image")) {
                actionText += "تصویر گروه را عوض کرد.";
            } else if (chats_message.action.equals("join_group")) {
                actionText += "به گروه ملحق شد.";
            } else if (chats_message.action.equals("leave_group")) {
                actionText += "گروه را ترک کرد.";
            } else if (chats_message.action.equals("add")) {
                actionText += message + " را به گروه اضافه کرد.";
            } else if (chats_message.action.equals("remove")) {
                actionText += message + " را از گروه حذف کرد.";
            }

            message = actionText;
        }


        if (dbHelper.checkChatsMessage(chats_message.id)) {
            chats_message.status = dbHelper.getChatsMessage(Integer.parseInt(id)).status;
            dbHelper.updateChatsMessage(chats_message);
            return;
        } else {
            dbHelper.createChatsMessage(chats_message);
        }

        Chat chat = dbHelper.getChat(Integer.parseInt(chat_id));
        if (dbHelper.checkChat(Integer.parseInt(chat_id))) {

            if (chat.type.equals("group")) {
                title += " در " + chat.name;
            }
        }


        ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
        HashMap filter = new HashMap<>();
        filter.put("name", "status");
        filter.put("method", "equal");
        filter.put("value", "not-read");
        filters.add(filter);
        filter = new HashMap<>();
        filter.put("name", "chat");
        filter.put("method", "equal");
        filter.put("value", chat_id);
        filters.add(filter);

        List<ChatsMessage> messages = dbHelper.searchChatsMessages(filters, "id", 0, 0, true, true);
        if (messages.size() > 1) {
            title = chat.name;
            message = messages.size() + " پیام جدید";
        }


        if (Functions.getBoolean(getApplicationContext(), "chat_show_notifications_" + chat_id, true)) {

            if (chats_message.sender.equals(Functions.getString(getApplicationContext(), "id", ""))) {
                return;
            }

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // IF IS NOT PRESENT
                if (ChatActivity.activity != null) {

                    if (ChatActivity.chat_id.equals(chat_id)) {
                        Functions.playSound(getApplication(), R.raw.notify, 1000, AudioManager.STREAM_NOTIFICATION);
                    } else {
                        if (Functions.getBoolean(getApplicationContext(), "chat_notification_sound_" + chat_id, true)) {
                            Functions.playSound(getApplication(), R.raw.notification, 1000, AudioManager.STREAM_NOTIFICATION);
                        }

                        if (Functions.getBoolean(getApplicationContext(), "chat_notification_vibration_" + chat_id, true)) {
                            Functions.Vibrate(getApplicationContext(), 500);
                        }
                    }
                } else {
                    if (Functions.getBoolean(getApplicationContext(), "chat_notification_sound_" + chat_id, true)) {
                        Functions.playSound(getApplication(), R.raw.notification, 1000, AudioManager.STREAM_NOTIFICATION);
                    }

                    if (Functions.getBoolean(getApplicationContext(), "chat_notification_vibration_" + chat_id, true)) {
                        Functions.Vibrate(getApplicationContext(), 500);
                    }
                }

                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.INTENT_PUSH_NOTIFICATION);
                pushNotification.putExtra("type", Config.FLAG_CHAT);
                pushNotification.putExtra("chats_message", chats_message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                MainActivity.new_message = true;

            } else {

                if (Functions.getBoolean(getApplicationContext(), "chat_notification_sound_" + chat_id, true)) {
                    Functions.playSound(getApplication(), R.raw.notification, 1000, AudioManager.STREAM_NOTIFICATION);
                }

                if (Functions.getBoolean(getApplicationContext(), "chat_notification_vibration_" + chat_id, true)) {
                    Functions.Vibrate(getApplicationContext(), 500);
                }

                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                resultIntent.putExtra("action", "show_chats");

                Notification notification = Functions.createNotification(getApplicationContext(), R.drawable.ic_notification, R.drawable.ic_launcher, message, EmojiMapUtil.replaceCheatSheetEmojis(title), message, resultIntent, true, Long.parseLong(Functions.normalLength(timestamp, 13, "0", false)), false, 0, false, true, Functions.getInt(getApplicationContext(), "notification_color", getApplicationContext().getResources().getColor(R.color.basicPrimary)), 1000, 2000);
                Functions.showNotification(getApplicationContext(), notification, Integer.parseInt(chat_id));

            }
        }


        CustomFunctions.setBadge(getApplicationContext(), dbHelper);


    }

    private void processPush(String title, String timestamp) {

        if (Functions.getBoolean(getApplicationContext(), "show_notifications", true)) {

            if (Functions.getBoolean(getApplicationContext(), "notification_sound", true)) {
                Functions.playSound(getApplication(), R.raw.notification, 1000, AudioManager.STREAM_NOTIFICATION);
            }

            if (Functions.getBoolean(getApplicationContext(), "notification_vibration", true)) {
                Functions.Vibrate(getApplicationContext(), 500);
            }

            int notification_id = Config.NOTIFICATION_ID_DEFAULT;


            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);

            Notification notification = Functions.createNotification(getApplicationContext(), R.drawable.ic_notification, R.drawable.ic_launcher, "", EmojiMapUtil.replaceCheatSheetEmojis(title), "", resultIntent, true, Long.parseLong(Functions.normalLength(timestamp, 13, "0", false)), false, 0, false, true, Functions.getInt(getApplicationContext(), "notification_color", getApplicationContext().getResources().getColor(R.color.basicPrimary)), 1000, 2000);
            Functions.showNotification(getApplicationContext(), notification, Integer.parseInt(String.valueOf(notification_id)));
        }

    }

}