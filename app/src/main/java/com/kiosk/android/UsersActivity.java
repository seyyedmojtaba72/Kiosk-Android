package com.kiosk.android;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.UsersAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;

import java.util.ArrayList;
import java.util.List;


public class UsersActivity extends AppCompatActivity {

    private String method, argument;

    private UsersAdapter adapter;
    private ListView list;

    private List<String> users_ids = new ArrayList<String>();
    private List<String> users_names = new ArrayList<String>();
    private List<String> users_profile_images = new ArrayList<String>();
    private List<String> users_timestamps = new ArrayList<String>();
    private List<String> users_follows = new ArrayList<String>();

    private int start = 0, amount = Config.DEFAULT_USERS_BUFFER_ITEMS;

    private ProgressBar loading;

    private boolean isProcessing = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        amount = Functions.getInt(getApplicationContext(), "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);

        if ((!Functions.isOnline(getApplicationContext()))) {
            Toast.makeText(UsersActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }


        method = getIntent().getStringExtra("method");
        argument = getIntent().getStringExtra("argument");

        list = (ListView) findViewById(R.id.list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (users_ids.get(i).equals(Functions.getString(getApplicationContext(), "id", ""))) {
                    Intent intent = new Intent(UsersActivity.this, MainActivity.class);
                    intent.putExtra("action", "open_profile");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent localIntent = new Intent(UsersActivity.this, ProfileActivity.class);
                    localIntent.putExtra("id", users_ids.get(i));
                    startActivity(localIntent);
                }
            }

        });

        adapter = new UsersAdapter(UsersActivity.this, null, true);

        adapter.users = users_ids;
        adapter.user_profile_images = users_profile_images;
        adapter.user_timestamps = users_timestamps;
        adapter.user_names = users_names;
        adapter.follows = users_follows;

        list.setAdapter(adapter);
        list.setSmoothScrollbarEnabled(true);

        loading = (ProgressBar) findViewById(R.id.loading);

        loadUsers();

        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        if (method.equals("liked")) {
            titleText.setText("کسانی که پسندیده اند");
        } else if (method.equals("follows")){
            titleText.setText("دنبال کننده ها");
        } else if (method.equals("followed")){
            titleText.setText("دنبال شده ها");
        } else {
            titleText.setText("کاربران");
        }

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }


    void loadUsers() {

        if (isProcessing) {
            return;
        }

        isProcessing = true;


        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                loading.setVisibility(View.VISIBLE);

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.getUsers(method, argument, start, amount, Functions.getString(getApplicationContext(), "id", "")));
            }

            @Override
            protected void onPostExecute(String result) {

                loading.setVisibility(View.INVISIBLE);

                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "users").equals("-1")) {

                    users_ids.addAll(JSONParser.getListFromString(result, "users", "id"));
                    users_profile_images.addAll(JSONParser.getListFromString(result, "users", "profile_image"));
                    users_timestamps.addAll(JSONParser.getListFromString(result, "users", "timestamp"));
                    users_names.addAll(JSONParser.getListFromString(result, "users", "name"));
                    users_follows.addAll(JSONParser.getListFromString(result, "users", "follow"));

                    start += amount;

                    adapter.users = users_ids;
                    adapter.user_profile_images = users_profile_images;
                    adapter.user_timestamps = users_timestamps;
                    adapter.user_names = users_names;
                    adapter.follows = users_follows;

                    if (start == amount) { // IF IS FIRST LOAD

                        list.setOnScrollListener(new AbsListView.OnScrollListener() {
                            public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == users_ids.size()))
                                    loadUsers();
                            }

                            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                            }
                        });
                    }

                    adapter.notifyDataSetChanged();

                } else {
                    list.setOnScrollListener(null);
                }

                isProcessing = false;

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }


}