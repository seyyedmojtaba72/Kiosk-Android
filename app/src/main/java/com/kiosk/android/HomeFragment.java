package com.kiosk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.adapter.PostsAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by SeyyedMojtaba on 2/25/2016.
 */
public class HomeFragment extends Fragment {

    public static Activity activity;
    public static Context context;
    public static DatabaseHelper dbHelper;
    public static int start = 0, amount = Config.DEFAULT_POSTS_BUFFER_ITEMS;

    public static SwipeRefreshLayout swipeRefreshLayout;
    public static ListView list;
    public static PostsAdapter adapter;

    public static List<String> posts_ids = new ArrayList<String>();
    public static List<String> posts_authors = new ArrayList<String>();
    public static List<String> posts_author_names = new ArrayList<String>();
    public static List<String> posts_author_profile_images = new ArrayList<String>();
    public static List<String> posts_author_timestamps = new ArrayList<String>();
    public static List<String> posts_names = new ArrayList<String>();
    public static List<String> posts_descriptions = new ArrayList<String>();
    public static List<String> posts_images = new ArrayList<String>();
    public static List<String> posts_prices = new ArrayList<String>();
    public static List<String> posts_download_counts = new ArrayList<String>();
    public static List<String> posts_times = new ArrayList<String>();
    public static List<String> posts_timestamps = new ArrayList<String>();
    public static List<String> posts_likes = new ArrayList<String>();
    public static List<String> posts_user_likes = new ArrayList<String>();
    public static List<String> posts_comments = new ArrayList<String>();
    public static List<String> posts_downloadables = new ArrayList<String>();

    public static boolean isProcessing = false;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        amount = Functions.getInt(context, "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

        dbHelper = new DatabaseHelper(activity);

        start = 0;
        amount = Functions.getInt(context, "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_view);
        swipeRefreshLayout.setColorSchemeResources(R.color.generalDarkRed, R.color.generalDarkGreen, R.color.generalDarkBlue);
        list = (ListView) rootView.findViewById(R.id.list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent localIntent = new Intent(activity, PostActivity.class);
                localIntent.putExtra("id", posts_ids.get(i));
                startActivity(localIntent);
            }

        });

        adapter = new PostsAdapter(activity, null);

        list.setSmoothScrollbarEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                start = 0;
                amount = Functions.getInt(context, "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

                posts_ids.clear();
                posts_authors.clear();
                posts_author_names.clear();
                posts_author_profile_images.clear();
                posts_author_timestamps.clear();
                posts_names.clear();
                posts_descriptions.clear();
                posts_images.clear();
                posts_prices.clear();
                posts_download_counts.clear();
                posts_times.clear();
                posts_timestamps.clear();
                posts_likes.clear();
                posts_user_likes.clear();
                posts_comments.clear();
                posts_downloadables.clear();

                list.setOnScrollListener(null);
                list.setAdapter(adapter);

                loadData();
            }
        });



        refreshPosts();


        // Inflate the layout for this fragment return rootView;
        return rootView;
    }

    public static void refreshPosts() {
        if (swipeRefreshLayout == null) {
            return;
        }

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);

                start = 0;
                amount = Functions.getInt(context, "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

                posts_ids.clear();
                posts_authors.clear();
                posts_author_names.clear();
                posts_author_profile_images.clear();
                posts_author_timestamps.clear();
                posts_names.clear();
                posts_descriptions.clear();
                posts_images.clear();
                posts_prices.clear();
                posts_download_counts.clear();
                posts_times.clear();
                posts_timestamps.clear();
                posts_likes.clear();
                posts_user_likes.clear();
                posts_comments.clear();
                posts_downloadables.clear();

                adapter.ids = posts_ids;
                adapter.author_profile_images = posts_author_profile_images;
                adapter.author_timestamps = posts_author_timestamps;
                adapter.authors = posts_authors;
                adapter.author_names = posts_author_names;
                adapter.times = posts_times;
                adapter.images = posts_images;
                adapter.prices = posts_prices;
                adapter.user_likes = posts_user_likes;
                adapter.likes = posts_likes;
                adapter.names = posts_names;
                adapter.descriptions = posts_descriptions;
                adapter.comments = posts_comments;

                list.setOnScrollListener(null);
                list.setAdapter(adapter);

                loadData();
            }
        });
    }

    public static void loadOffline() {
        if (isProcessing) {
            return;
        }

        isProcessing = true;
        ArrayList<HashMap<String, String>> filters = new ArrayList<>();
        HashMap<String, String> filter = new HashMap<>();
        filter.put("name", "saved");
        filter.put("method", "equal");
        filter.put("value", "1");
        filters.add(filter);
        List<Post> posts = dbHelper.searchPosts(filters, "id", start, amount, true, true);

        swipeRefreshLayout.setRefreshing(true);

        if (posts.size() == 0) {
            list.setOnScrollListener(null);
        }

        for (Post post : posts) {
            posts_ids.add(String.valueOf(post.id));
            posts_authors.add(post.author);
            posts_author_names.add(post.author_name);
            posts_author_profile_images.add(post.author_profile_image);
            posts_author_timestamps.add(post.author_timestamp);
            posts_names.add(post.name);
            posts_descriptions.add(post.description);
            posts_images.add(post.image);
            posts_prices.add(post.price);
            posts_download_counts.add(post.download_count);
            posts_timestamps.add(post.timestamp);

            // GET PAST TIME FROM TIMESTAMP
            posts_times.add(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(post.timestamp))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(post.timestamp), "hh:mm:ss")));

            posts_likes.add(post.likes);
            posts_user_likes.add(post.user_like);
            posts_comments.add(post.comments);
            posts_downloadables.add(post.downloadable);
        }


        start += amount;


        adapter.ids = posts_ids;
        adapter.author_profile_images = posts_author_profile_images;
        adapter.author_timestamps = posts_author_timestamps;
        adapter.authors = posts_authors;
        adapter.author_names = posts_author_names;
        adapter.times = posts_times;
        adapter.images = posts_images;
        adapter.prices = posts_prices;
        adapter.user_likes = posts_user_likes;
        adapter.likes = posts_likes;
        adapter.names = posts_names;
        adapter.descriptions = posts_descriptions;
        adapter.comments = posts_comments;


        if (start == amount) { // IF IS FIRST LOAD

            list.setAdapter(adapter);
            list.setSmoothScrollbarEnabled(true);

            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                    if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == posts_ids.size()))
                        loadData();
                }

                public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                }
            });
        }


        adapter.notifyDataSetChanged();

        isProcessing = false;
        swipeRefreshLayout.setRefreshing(false);
    }

    public static void loadData() {
        if (!Functions.isOnline(context)) {
            loadOffline();

        } else {

            if (isProcessing) {
                return;
            }

            isProcessing = true;
            swipeRefreshLayout.setRefreshing(true);

            new AsyncTask<String, String, String>() {

                @Override
                protected String doInBackground(String... strings) {
                    return Fetcher.FetchURL(APIManager.getPosts("", start, amount, Functions.getString(context, "id", "")));
                }

                @Override
                protected void onPostExecute(String result) {
                    String posts_json = result;

                    if (!posts_json.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(posts_json, "posts").equals("-1")) {
                        int offset = posts_ids.size();

                        posts_ids.addAll(JSONParser.getListFromString(posts_json, "posts", "id"));
                        posts_authors.addAll(JSONParser.getListFromString(posts_json, "posts", "author"));
                        posts_author_names.addAll(JSONParser.getListFromString(posts_json, "posts", "author_name"));
                        posts_author_profile_images.addAll(JSONParser.getListFromString(posts_json, "posts", "author_profile_image"));
                        posts_author_timestamps.addAll(JSONParser.getListFromString(posts_json, "posts", "author_timestamp"));
                        posts_names.addAll(JSONParser.getListFromString(posts_json, "posts", "name"));
                        posts_descriptions.addAll(JSONParser.getListFromString(posts_json, "posts", "description"));
                        posts_images.addAll(JSONParser.getListFromString(posts_json, "posts", "image"));
                        posts_prices.addAll(JSONParser.getListFromString(posts_json, "posts", "price"));
                        posts_download_counts.addAll(JSONParser.getListFromString(posts_json, "posts", "download_count"));
                        posts_timestamps.addAll(JSONParser.getListFromString(posts_json, "posts", "timestamp"));
                        posts_likes.addAll(JSONParser.getListFromString(posts_json, "posts", "likes"));
                        posts_user_likes.addAll(JSONParser.getListFromString(posts_json, "posts", "user_like"));
                        posts_comments.addAll(JSONParser.getListFromString(posts_json, "posts", "comments"));
                        posts_downloadables.addAll(JSONParser.getListFromString(posts_json, "posts", "downloadable"));

                        for (int i = offset; i < posts_ids.size(); i++) {
                            Post post;
                            if (dbHelper.checkPost(Integer.parseInt(posts_ids.get(i)))) {
                                post = dbHelper.getPost(Integer.parseInt(posts_ids.get(i)));
                            } else {
                                post = new Post();
                            }

                            post.id = Integer.parseInt(posts_ids.get(i));
                            post.author = posts_authors.get(i);
                            post.author_name = EmojiMapUtil.replaceCheatSheetEmojis(posts_author_names.get(i));
                            post.author_profile_image = posts_author_profile_images.get(i);
                            post.author_timestamp = posts_author_timestamps.get(i);
                            post.name = EmojiMapUtil.replaceCheatSheetEmojis(posts_names.get(i));
                            post.description = EmojiMapUtil.replaceCheatSheetEmojis(posts_descriptions.get(i));
                            post.image = posts_images.get(i);
                            post.price = posts_prices.get(i);
                            post.download_count = posts_download_counts.get(i);
                            post.timestamp = posts_timestamps.get(i);

                            // GET PAST TIME FROM TIMESTAMP
                            posts_times.add(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(posts_timestamps.get(i)))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(posts_timestamps.get(i)), "hh:mm:ss")));

                            post.likes = posts_likes.get(i);
                            post.user_like = posts_user_likes.get(i);
                            post.comments = posts_comments.get(i);
                            post.downloadable = posts_downloadables.get(i);
                            post.saved = "1";

                            if (dbHelper.checkPost(Integer.parseInt(posts_ids.get(i)))) {
                                post.reading = (dbHelper.getPost(post.id).reading);
                                dbHelper.updatePost(post);
                            } else {
                                post.reading = "0";
                                dbHelper.createPost(post);
                            }

                        }


                        start += amount;

                        adapter.ids = posts_ids;
                        adapter.author_profile_images = posts_author_profile_images;
                        adapter.author_timestamps = posts_author_timestamps;
                        adapter.authors = posts_authors;
                        adapter.author_names = posts_author_names;
                        adapter.times = posts_times;
                        adapter.images = posts_images;
                        adapter.prices = posts_prices;
                        adapter.user_likes = posts_user_likes;
                        adapter.likes = posts_likes;
                        adapter.names = posts_names;
                        adapter.descriptions = posts_descriptions;
                        adapter.comments = posts_comments;

                        if (start == amount) { // IF IS FIRST LOAD

                            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                    if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == posts_ids.size()))
                                        loadData();
                                }

                                public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                }
                            });
                        }


                    } else {
                        list.setOnScrollListener(null);
                    }

                    adapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                    isProcessing = false;

                    super.onPostExecute(result);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        } else {
            this.context = context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}