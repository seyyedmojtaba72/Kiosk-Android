package com.kiosk.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.kiosk.android.model.Chat;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends AppCompatActivity {
    private String id = "", username = "";

    private String follow = "0", request = "0", extras = "";

    private CircleImageView profileImageView;
    private TextView PostsNumberText, followersNumberText, followedsNumberText, nameText, aboutText;
    private Button followButton;
    private LinearLayout postsLayout, followersLayout, followedsLayout;
    private ImageView trustedImage;

    private ProgressBar loadingFollow;
    private SwipeRefreshLayout swipeRefreshLayout;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        dbHelper = new DatabaseHelper(getApplicationContext());


        if ((!Functions.isOnline(getApplicationContext()))) {
            Toast.makeText(ProfileActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (getIntent().getStringExtra("id") != null) {
            id = getIntent().getStringExtra("id");
        } else {
            Uri data = getIntent().getData();
            if (data.getQueryParameters("id").size() > 0) {
                id = data.getQueryParameters("id").get(0);
            } else {
                username = data.getQueryParameters("username").get(0);
            }
        }

        if (id.equals(Functions.getString(getApplicationContext(), "id", "")) || username.equals(Functions.getString(getApplicationContext(), "username", ""))) {
            Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
            intent.putExtra("action", "open_profile");
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }

        profileImageView = (CircleImageView) findViewById(R.id.profile_image);
        postsLayout = (LinearLayout) findViewById(R.id.posts);
        PostsNumberText = (TextView) findViewById(R.id.posts_number);
        followersLayout = (LinearLayout) findViewById(R.id.followers);
        followersNumberText = (TextView) findViewById(R.id.followers_number);
        followedsLayout = (LinearLayout) findViewById(R.id.followeds);
        followedsNumberText = (TextView) findViewById(R.id.followeds_number);
        followButton = (Button) findViewById(R.id.follow);
        nameText = (TextView) findViewById(R.id.name);
        trustedImage = (ImageView) findViewById(R.id.trusted);
        aboutText = (TextView) findViewById(R.id.about);


        //loading = (ProgressBar) findViewById(R.id.loading);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_view);
        swipeRefreshLayout.setColorSchemeResources(R.color.generalDarkRed, R.color.generalDarkGreen, R.color.generalDarkBlue);
        loadingFollow = (ProgressBar) findViewById(R.id.loading_follow);

        postsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (extras.contains("private;") && !follow.equals("accepted")) {
                } else {
                    Intent intent = new Intent(ProfileActivity.this, PostsActivity.class);
                    intent.putExtra("author", id);
                    startActivity(intent);
                }
            }
        });

        followersLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (extras.contains("private;") && !follow.equals("accepted")) {
                } else {
                    Intent intent = new Intent(ProfileActivity.this, UsersActivity.class);
                    intent.putExtra("method", "follows");
                    intent.putExtra("argument", id);
                    startActivity(intent);
                }
            }
        });

        followedsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (extras.contains("private;") && !follow.equals("accepted")) {
                } else {
                    Intent intent = new Intent(ProfileActivity.this, UsersActivity.class);
                    intent.putExtra("method", "followed");
                    intent.putExtra("argument", id);
                    startActivity(intent);
                }
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProfile();
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);

                loadProfile();
            }
        });


        // ACTIONBAR BUTTONS

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton actionRightButton = (ImageButton) findViewById(R.id.btn_right);
        actionRightButton.setImageResource(R.drawable.ic_action_overflow);
        actionRightButton.setVisibility(View.VISIBLE);
        actionRightButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(ProfileActivity.this);
                ArrayList<String> localArrayList = new ArrayList<String>();
                if (request.equals("blocked")) {
                    localArrayList.add("حذف از لیست سیاه");
                } else {
                    localArrayList.add("افزودن به لیست سیاه");
                }
                localArrayList.add("گزارش");
                localArrayList.add("گفتگو");


                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                switch (paramAnonymous2Int) {
                                    default:
                                        return;
                                    case 0:
                                        try {
                                            final SweetAlertDialog sDialog = new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                                            new AsyncTask<String, String, String>() {

                                                @Override
                                                protected void onPreExecute() {
                                                    sDialog.show();
                                                    sDialog.setCancelable(false);

                                                    super.onPreExecute();
                                                }

                                                @Override
                                                protected String doInBackground(String... arg0) {
                                                    return Fetcher.FetchURL(APIManager.blockUser(id, Functions.getString(getApplicationContext(), "id", "")));
                                                }

                                                protected void onPostExecute(final String result) {
                                                    runOnUiThread(new Runnable() {
                                                        public void run() {

                                                            if (result.contains("success")) {
                                                                if (request.equals("blocked")) {
                                                                    request = "0";
                                                                    sDialog.setTitleText("").setContentText("کاربر از لیست سیاه حذف شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                                } else {
                                                                    request = "blocked";
                                                                    sDialog.setTitleText("").setContentText("کاربر به لیست سیاه افزوده شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                                }


                                                            } else {
                                                                sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                            }
                                                        }
                                                    });

                                                }
                                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                            sDialog.show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    case 1:
                                        final String[] reportItems = {"user_fake", "user_content", "user_abuse"};
                                        final String[] reportItemsTitles = {"حساب جعلی", "محتوای نامناسب", "سوء رفتار"};

                                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(ProfileActivity.this);
                                        localBuilder.setItems(reportItemsTitles, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, final int i) {
                                                final SweetAlertDialog sDialog = new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                                                new AsyncTask<String, String, String>() {

                                                    @Override
                                                    protected void onPreExecute() {
                                                        sDialog.show();
                                                        sDialog.setCancelable(false);

                                                        super.onPreExecute();
                                                    }

                                                    @Override
                                                    protected String doInBackground(String... arg0) {
                                                        return Fetcher.FetchURL(APIManager.Report(Functions.getString(getApplicationContext(), "id", ""), id, reportItems[i], ""));
                                                    }

                                                    protected void onPostExecute(final String result) {
                                                        runOnUiThread(new Runnable() {
                                                            public void run() {

                                                                if (result.contains("success")) {
                                                                    sDialog.setTitleText("").setContentText("گزارش شما ثبت شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                                                } else {
                                                                    sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                                }
                                                            }
                                                        });

                                                    }
                                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                                sDialog.show();
                                            }
                                        });
                                        localBuilder.create().show();

                                        break;

                                    case 2:
                                        final SweetAlertDialog sDialog = new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);
                                        new AsyncTask<String, String, String>() {

                                            @Override
                                            protected void onPreExecute() {
                                                sDialog.show();
                                                sDialog.setCancelable(false);

                                                super.onPreExecute();
                                            }

                                            @Override
                                            protected String doInBackground(String... strings) {
                                                return Fetcher.FetchURL(APIManager.startChat(Functions.getString(getApplicationContext(), "id", ""), username + "!new_user!", "", ""));
                                            }

                                            @Override
                                            protected void onPostExecute(String result) {
                                                if (result.contains("success")) {
                                                    Intent intent = new Intent(ProfileActivity.this, ChatActivity.class);
                                                    intent.putExtra("id", result.replaceFirst("success;", ""));

                                                    Chat chat = new Chat();

                                                    chat.id = Integer.parseInt(result.replaceFirst("success;", ""));
                                                    chat.creator = Functions.getString(getApplicationContext(), "id", "");
                                                    chat.type = "single";

                                                    chat.name = username;
                                                    chat.image = "";


                                                    chat.participants = "";
                                                    chat.timestamp = String.valueOf(System.currentTimeMillis());

                                                    if (dbHelper.checkChat(chat.id)) {
                                                        dbHelper.updateChat(chat);
                                                    } else {
                                                        dbHelper.createChat(chat);
                                                    }


                                                    startActivity(intent);
                                                    sDialog.dismiss();
                                                } else if (result.equals("no-user"))

                                                {
                                                    sDialog.setTitleText("").setContentText("چنین کاربری وجود ندارد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                } else

                                                {
                                                    sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                }

                                                super.

                                                        onPostExecute(result);
                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        break;
                                }

                            }
                        }

                );
                localBuilder.create().

                        show();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void loadProfile() {
        if (!Functions.isOnline(getApplicationContext())) {
            return;
        }

        new AsyncTask<String, String, String>() {

            @Override
            protected String doInBackground(String... strings) {
                String method = "id";
                String author = id;

                if (id.isEmpty()) {
                    method = "username";
                    author = username;
                }

                return Fetcher.FetchURL(APIManager.getProfile(author, Functions.getString(getApplicationContext(), "id", ""), method));
            }

            @Override
            protected void onPostExecute(String result) {

                LinearLayout layout = (LinearLayout) findViewById(R.id.layout);
                layout.setVisibility(View.VISIBLE);
                layout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));

                final LinearLayout requestLayout = (LinearLayout) findViewById(R.id.request_layout);

                if (result.isEmpty() || result.equals("-1") || JSONParser.getFromString(result, "profile").equals("-1")) {
                    finish();
                    return;
                }

                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "profile").equals("-1")) {
                    id = JSONParser.getListFromString(result, "profile", "id").get(0);
                    final String profile_image = JSONParser.getListFromString(result, "profile", "profile_image").get(0);
                    String Posts = JSONParser.getListFromString(result, "profile", "posts").get(0);
                    String followers = JSONParser.getListFromString(result, "profile", "followers").get(0);
                    String followeds = JSONParser.getListFromString(result, "profile", "followeds").get(0);
                    follow = JSONParser.getListFromString(result, "profile", "follow").get(0);
                    request = JSONParser.getListFromString(result, "profile", "request").get(0);
                    username = JSONParser.getListFromString(result, "profile", "username").get(0);
                    String first_name = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "profile", "first_name").get(0));
                    String last_name = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "profile", "last_name").get(0));
                    String about = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "profile", "about").get(0));
                    extras = JSONParser.getListFromString(result, "profile", "extras").get(0);
                    final String timestamp = JSONParser.getListFromString(result, "profile", "timestamp").get(0);

                    if (request.equals("waiting")) {
                        requestLayout.setVisibility(View.VISIBLE);
                        requestLayout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
                    } else {
                        requestLayout.setVisibility(View.GONE);
                    }

                    final ImageButton acceptButton = (ImageButton) findViewById(R.id.accept);
                    final ImageButton rejectButton = (ImageButton) findViewById(R.id.reject);

                    acceptButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new AsyncTask<String, String, String>() {

                                @Override
                                protected String doInBackground(String... strings) {
                                    return Fetcher.FetchURL(APIManager.respondRequest(Functions.getString(getApplicationContext(), "id", ""), id, "accept"));
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    ViewGroup.LayoutParams params = requestLayout.getLayoutParams();
                                    params.height = 0;
                                    requestLayout.setLayoutParams(params);
                                    requestLayout.setVisibility(View.GONE);
                                    acceptButton.setVisibility(View.GONE);
                                    rejectButton.setVisibility(View.GONE);
                                    requestLayout.setBackgroundColor(Color.TRANSPARENT);
                                    request = "accepted";

                                    super.onPostExecute(s);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    });

                    rejectButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new AsyncTask<String, String, String>() {

                                @Override
                                protected String doInBackground(String... strings) {
                                    return Fetcher.FetchURL(APIManager.respondRequest(Functions.getString(getApplicationContext(), "id", ""), id, "reject"));
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    ViewGroup.LayoutParams params = requestLayout.getLayoutParams();
                                    params.height = 0;
                                    requestLayout.setLayoutParams(params);
                                    requestLayout.setVisibility(View.GONE);
                                    acceptButton.setVisibility(View.GONE);
                                    rejectButton.setVisibility(View.GONE);
                                    requestLayout.setBackgroundColor(Color.TRANSPARENT);
                                    request = "0";

                                    super.onPostExecute(s);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    });


                    if (!profile_image.isEmpty()) {
                        Picasso.with(getApplicationContext()).load(Functions.getString(getApplicationContext(), "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + profile_image + "_small.jpg" + "?" + timestamp).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().into(profileImageView);
                        //Functions.loadCachedImage(ProfileActivity.this, profileImageView, Functions.getString(getApplicationContext(), "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + profile_image, Information.PROFILE_IMAGES_PATH_PREFIX + profile_image);
                    } else {
                        profileImageView.setImageResource(R.drawable.ic_profile);
                    }


                    profileImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!profile_image.isEmpty()) {
                                AlertDialog.Builder localBuilder = new AlertDialog.Builder(ProfileActivity.this);
                                View dialogView = getLayoutInflater().inflate(R.layout.dialog_profile_image, (ViewGroup) findViewById(R.id.dialog_profile_image));
                                localBuilder.setView(dialogView);
                                ImageView profileImage = (ImageView) dialogView.findViewById(R.id.profile_image);
                                Picasso.with(getApplicationContext()).load(Functions.getString(getApplicationContext(), "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + profile_image + "?" + timestamp).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().into(profileImage);
                                //Functions.loadCachedImage(ProfileActivity.this, profileImage, Functions.getString(getApplicationContext(), "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + profile_image, Information.PROFILE_IMAGES_PATH_PREFIX + profile_image);
                                ImageButton rightButton = (ImageButton) dialogView.findViewById(R.id.btn_right);
                                rightButton.setOnClickListener(new View.OnClickListener() {

                                                                   @Override
                                                                   public void onClick(View view) {
                                                                       AlertDialog.Builder localBuilder = new AlertDialog.Builder(ProfileActivity.this);
                                                                       ArrayList<String> localArrayList = new ArrayList<String>();
                                                                       localArrayList.add("دریافت");


                                                                       String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                                                                       localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                                                                                   public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                                                                       switch (paramAnonymous2Int) {
                                                                                           default:
                                                                                               return;
                                                                                           case 0:

                                                                                               DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                                                                               downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{Information.DEFAULT_PROFILE_IMAGES_URL + profile_image + "?" + timestamp, Information.IMAGES_PATH, username + ".jpg"});
                                                                                               downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                                                                                   @Override
                                                                                                   public void onProgressUpdate(int progress) {

                                                                                                   }

                                                                                                   @Override
                                                                                                   public void onPostExecute(boolean result) {
                                                                                                       if (result) {
                                                                                                           Toast.makeText(ProfileActivity.this, "تصویر کاربر درون حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                                                                                       }
                                                                                                   }
                                                                                               });


                                                                                               break;
                                                                                       }

                                                                                   }
                                                                               }

                                                                       );
                                                                       AlertDialog dialog = localBuilder.create();
                                                                       dialog.show();

                                                                   }
                                                               }

                                );

                                AlertDialog dialog = localBuilder.create();
                                dialog.show();
                            }
                        }
                    });


                    PostsNumberText.setText(Posts);
                    followersNumberText.setText(followers);
                    followedsNumberText.setText(followeds);

                    if (follow.equals("accepted")) {
                        followButton.setBackgroundResource(R.drawable.selector_button_primary);
                        followButton.setTextColor(Color.WHITE);
                        followButton.setText("دنبال شده");
                    } else if (follow.equals("waiting")) {
                        followButton.setBackgroundResource(R.drawable.selector_button_third);
                        followButton.setTextColor(Color.WHITE);
                        followButton.setText("در انتظار");
                    } else {
                        followButton.setBackgroundResource(R.drawable.selector_button_secondary);
                        followButton.setTextColor(Color.BLACK);
                        followButton.setText("دنبال کردن");
                    }

                    followButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new AsyncTask<String, String, String>() {

                                @Override
                                protected void onPreExecute() {
                                    loadingFollow.setVisibility(View.VISIBLE);
                                    followButton.setBackgroundResource(R.drawable.selector_button_third);
                                    followButton.setText("");

                                    super.onPreExecute();
                                }

                                @Override
                                protected String doInBackground(String... strings) {
                                    return Fetcher.FetchURL(APIManager.followUser(Functions.getString(getApplicationContext(), "id", ""), id));
                                }

                                @Override
                                protected void onPostExecute(String result) {
                                    loadingFollow.setVisibility(View.INVISIBLE);


                                    if (result.equals("success")) {
                                        if (follow.equals("accepted")) {
                                            follow = "0";
                                        } else if (follow.equals("waiting")) {
                                            follow = "0";
                                        } else {
                                            follow = "accepted";
                                        }

                                    } else if (result.equals("requested")) {
                                        follow = "waiting";
                                    }


                                    if (follow.equals("accepted")) {
                                        followButton.setBackgroundResource(R.drawable.selector_button_primary);
                                        followButton.setTextColor(Color.WHITE);
                                        followButton.setText("دنبال شده");
                                    } else if (follow.equals("waiting")) {
                                        followButton.setBackgroundResource(R.drawable.selector_button_third);
                                        followButton.setTextColor(Color.WHITE);
                                        followButton.setText("در انتظار");
                                    } else {
                                        followButton.setBackgroundResource(R.drawable.selector_button_secondary);
                                        followButton.setTextColor(Color.BLACK);
                                        followButton.setText("دنبال کردن");
                                    }

                                    loadProfile();


                                    super.onPostExecute(result);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    });

                    String name = first_name + " " + last_name;
                    if (name.equals(" ")) {
                        name = username;
                    } else {
                        name += " (@" + username + ")";
                    }

                    if (extras.contains("trusted;")) {
                        trustedImage.setVisibility(View.VISIBLE);
                    } else {
                        trustedImage.setVisibility(View.GONE);
                    }
                    nameText.setText(name);
                    aboutText.setText(CustomFunctions.getHtml(getApplicationContext(), about));
                    aboutText.setMovementMethod(LinkMovementMethod.getInstance());
                    Functions.stripUnderlines(aboutText);

                    ImageView lockedImage = (ImageView) findViewById(R.id.locked);
                    if (extras.contains("private;") && !follow.equals("accepted")) {
                        lockedImage.setVisibility(View.VISIBLE);
                    } else {
                        lockedImage.setVisibility(View.INVISIBLE);
                    }

                    swipeRefreshLayout.setRefreshing(false);


                }

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


}