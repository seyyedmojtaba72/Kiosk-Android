package com.kiosk.android;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.BackgroundAdapter;
import com.kiosk.android.adapter.UsersAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.Information;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class ChatSettingsActivity extends AppCompatActivity {
    public static String chat_id = "";

    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private static final int CROP = 3;
    private static final int PICK_BACKGROUND = 4;

    public static String temp_path;
    public static Uri mImageUri;
    public static boolean uploaded = false;
    public static boolean isProcessing = false;

    public static String name = "", image = "", about = "";
    public static CircleImageView imageView;
    public static String former_name = "", former_image = "";

    private SettingsFragment mSettingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        chat_id = getIntent().getStringExtra("id");


        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("تنظیمات");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (savedInstanceState == null) {
            mSettingsFragment = new SettingsFragment();
            replaceFragment(R.id.settings_container, mSettingsFragment);
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void replaceFragment(int viewId, android.app.Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(viewId, fragment).commit();
    }

    /**
     * A placeholder fragment containing a settings view.
     */
    public static class SettingsFragment extends PreferenceFragment {
        private Activity activity;
        private Context context;
        private DatabaseHelper dbHelper;
        private Chat chat;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.chat_preferences);

            temp_path = Information.TEMP_PATH_PREFIX + chat_id + ".jpg";
            File tempFile = new File(temp_path);
            if (tempFile.exists()) {
                tempFile.delete();
            }

            dbHelper = new DatabaseHelper(context);
            chat = dbHelper.getChat(Integer.parseInt(chat_id));
            name = chat.name;
            about = chat.about;
            former_name = chat.name;
            image = chat.image;
            former_image = chat.image;

            PreferenceScreen myPreferenceScreen = getPreferenceScreen();

            Preference aboutPref = (Preference) findPreference("about");
            if (chat.about.isEmpty()) {
                myPreferenceScreen.removePreference(aboutPref);
            } else {
                aboutPref.setSummary(EmojiMapUtil.replaceCheatSheetEmojis(chat.about));
            }


            PreferenceCategory generalCategory = (PreferenceCategory) findPreference("general");
            final Preference chooseBG = (Preference) findPreference("choose_bg");
            chooseBG.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                    ArrayList<String> localArrayList = new ArrayList<String>();
                    localArrayList.add("انتخاب از کتابخانه");
                    localArrayList.add("انتخاب از گالری");
                    String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                    localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                        AlertDialog dialog;

                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            switch (paramAnonymous2Int) {
                                default:
                                    return;
                                case 0:
                                    try {
                                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                                        View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_choose_bg, null);
                                        localBuilder.setView(dialogView);

                                        final GridView gridView = (GridView) dialogView.findViewById(R.id.grid);
                                        final BackgroundAdapter adapter = new BackgroundAdapter(activity, null);
                                        adapter.images = new int[]{R.drawable.bg1, R.drawable.bg2, R.drawable.bg3, R.drawable.bg4, R.drawable.bg5, R.drawable.bg6, R.drawable.bg7, R.drawable.bg8, R.drawable.bg9, R.drawable.bg10};
                                        gridView.setAdapter(adapter);


                                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                Functions.putString(context, "chat_bg_type_" + chat.id, "resource");
                                                Functions.putString(context, "chat_bg_" + chat.id, String.valueOf(adapter.images[i]));
                                                dialog.dismiss();
                                            }
                                        });

                                        dialog = localBuilder.create();
                                        dialog.show();

                                        return;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case 1:
                                    try {
                                        File tempFile = new File(temp_path);
                                        if (tempFile.exists()) {
                                            tempFile.delete();
                                        }
                                        Intent localIntent1 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(localIntent1, PICK_BACKGROUND);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    break;
                            }

                        }
                    });

                    localBuilder.create().show();
                    return true;
                }
            });


            final Preference editGroup = (Preference) findPreference("edit_group");
            if (!chat.creator.equals(Functions.getString(context, "id", "")) || !chat.type.equals("group")) {
                generalCategory.removePreference(editGroup);
            } else {
                editGroup.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {

                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                        View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_edit_group, null);
                        localBuilder.setView(dialogView);

                        final EditText nameText = (EditText) dialogView.findViewById(R.id.name);
                        ImageButton editButton = (ImageButton) dialogView.findViewById(R.id.edit);
                        final EditText aboutText = (EditText) dialogView.findViewById(R.id.about);
                        imageView = (CircleImageView) dialogView.findViewById(R.id.image);

                        nameText.setText(EmojiMapUtil.replaceCheatSheetEmojis(chat.name));
                        aboutText.setText(EmojiMapUtil.replaceCheatSheetEmojis(chat.about));

                        if (!chat.image.isEmpty()) {
                            Picasso.with(activity).load(Functions.getString(activity, "GROUP_IMAGES_URL", Information.DEFAULT_GROUP_IMAGES_URL) + chat.image + "?" + chat.timestamp).error(R.drawable.ic_no_image).placeholder(R.drawable.ic_no_image).noFade().into(imageView);
                        } else {
                            Picasso.with(activity).load(R.drawable.ic_no_image).noFade().into(imageView);
                        }

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                                ArrayList<String> localArrayList = new ArrayList<String>();
                                localArrayList.add("تصویر جدید بگیرید");
                                localArrayList.add("انتخاب از گالری");
                                if (!chat.image.isEmpty()) {
                                    localArrayList.add("حذف");
                                }
                                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                        switch (paramAnonymous2Int) {
                                            default:
                                                return;
                                            case 0:
                                                try {
                                                    File tempFile = new File(temp_path);
                                                    if (tempFile.exists()) {
                                                        tempFile.delete();
                                                    }
                                                    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                                                    localIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(temp_path)));
                                                    startActivityForResult(localIntent, PICK_FROM_CAMERA);
                                                    return;
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                break;
                                            case 1:
                                                try {
                                                    File tempFile = new File(temp_path);
                                                    if (tempFile.exists()) {
                                                        tempFile.delete();
                                                    }
                                                    Intent localIntent1 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                    startActivityForResult(localIntent1, PICK_FROM_FILE);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                break;
                                            case 2:
                                                mImageUri = null;
                                                imageView.setImageResource(R.drawable.ic_no_image);
                                                image = "";
                                                chat.image = "";
                                                dbHelper.updateChat(chat);
                                                editGroup();
                                                break;
                                        }

                                    }
                                });
                                localBuilder.create().show();
                            }
                        });

                        final AlertDialog dialog = localBuilder.create();
                        dialog.show();

                        editButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if ((!Functions.isOnline(context))) {
                                    Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                if (nameText.getText().toString().isEmpty()) {
                                    Toast.makeText(activity, "لطفاً نام گروه را وارد کنید.", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                name = nameText.getText().toString();
                                about = aboutText.getText().toString();
                                editGroup();

                                dialog.dismiss();
                            }
                        });


                        return true;
                    }
                });
            }

            Preference manageMembers = (Preference) findPreference("manage_members");
            if (!chat.type.equals("group")) {
                manageMembers.setTitle("پروفایل کاربر");
                manageMembers.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        String profile_id = "";
                        if (!chat.creator.equals(Functions.getString(context, "id", ""))) {
                            profile_id = chat.creator;
                        } else {
                            profile_id = chat.participants.split("!new_user!")[0];
                        }

                        Intent intent = new Intent(activity, ProfileActivity.class);
                        intent.putExtra("id", profile_id);
                        startActivity(intent);
                        return true;
                    }
                });
            } else {

                manageMembers.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    private String chat_method, chat_argument;

                    private UsersAdapter chat_adapter;
                    private ListView chat_list;
                    private EditText chat_userText;
                    private ImageButton chat_addButton;

                    private List<String> chat_users_ids = new ArrayList<String>();
                    private List<String> chat_users_usernames = new ArrayList<String>();
                    private List<String> chat_users_names = new ArrayList<String>();
                    private List<String> chat_users_profile_images = new ArrayList<String>();
                    private List<String> chat_users_timestamps = new ArrayList<String>();
                    private List<String> chat_users_extras = new ArrayList<String>();
                    private List<String> chat_users_follows = new ArrayList<String>();

                    private int chat_start = 0, chat_amount = Config.DEFAULT_USERS_BUFFER_ITEMS;

                    private ProgressBar chat_loading;
                    private AlertDialog dialog;

                    public boolean onPreferenceClick(Preference preference) {


                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                        View view = activity.getLayoutInflater().inflate(R.layout.dialog_group_members, null);
                        RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout);
                        chat_list = (ListView) view.findViewById(R.id.list);
                        chat_addButton = (ImageButton) view.findViewById(R.id.add);
                        chat_loading = (ProgressBar) view.findViewById(R.id.loading);
                        chat_userText = (EditText) view.findViewById(R.id.user);
                        chat_loading.setVisibility(View.VISIBLE);
                        localBuilder.setView(view);


                        chat_amount = Functions.getInt(context, "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);

                        if ((!Functions.isOnline(context))) {
                            Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                            return false;
                        }

                        chat_method = "participate";
                        chat_argument = chat_id;

                        if (!chat.creator.equals(Functions.getString(context, "id", ""))) {
                            layout.setVisibility(View.GONE);
                        } else {

                            chat_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                                    if (chat_users_ids.get(i).equals(Functions.getString(context, "id", ""))) {
                                        return;
                                    }
                                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                                    ArrayList<String> localArrayList = new ArrayList<String>();
                                    localArrayList.add("حذف از گروه");


                                    String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                                    localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface paramAnonymous2DialogInterface, final int paramAnonymous2Int) {
                                            switch (paramAnonymous2Int) {
                                                default:
                                                    return;
                                                case 0:
                                                    final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);
                                                    new AsyncTask<String, String, String>() {
                                                        String username = chat_userText.getText().toString();

                                                        @Override
                                                        protected void onPreExecute() {
                                                            sDialog.show();
                                                            sDialog.setCancelable(false);

                                                            super.onPreExecute();
                                                        }

                                                        @Override
                                                        protected String doInBackground(String... strings) {
                                                            return Fetcher.FetchURL(APIManager.removeFromChat(chat_id, chat_users_usernames.get(i)));
                                                        }

                                                        @Override
                                                        protected void onPostExecute(String result) {
                                                            if (result.contains("success")) {
                                                                sDialog.setTitleText("").setContentText("کاربر از گروه حذف شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                                dialog.dismiss();

                                                                ChatsActivity.chats_adapter.chats = ChatsActivity.chats;

                                                                ChatsActivity.sortItems();
                                                                ChatsActivity.chats_adapter.notifyDataSetChanged();

                                                                ChatActivity.refreshLoad();


                                                                chat_users_ids.remove(i);
                                                                chat_users_usernames.remove(i);
                                                                chat_users_profile_images.remove(i);
                                                                chat_users_timestamps.remove(i);
                                                                chat_users_names.remove(i);
                                                                chat_users_follows.remove(i);

                                                                chat_adapter.users = chat_users_ids;
                                                                chat_adapter.user_profile_images = chat_users_profile_images;
                                                                chat_adapter.user_timestamps = chat_users_timestamps;
                                                                chat_adapter.user_names = chat_users_names;
                                                                chat_adapter.follows = chat_users_follows;


                                                                chat_adapter.notifyDataSetChanged();
                                                            } else if (result.equals("no-user")) {
                                                                sDialog.setTitleText("").setContentText("چنین کاربری وجود ندارد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                            } else {
                                                                sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                            }

                                                            super.onPostExecute(result);
                                                        }
                                                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                                    break;
                                            }
                                        }
                                    });

                                    dialog = localBuilder.create();
                                    dialog.show();


                                }

                            });
                        }


                        final AlertDialog dialog = localBuilder.create();
                        dialog.show();

                        chat_addButton.setOnClickListener(new View.OnClickListener() {
                            private String method, argument;

                            private UsersAdapter adapter;
                            private ListView list;
                            private AutoCompleteTextView userText;
                            private ImageButton selectButton, createButton;

                            private List<String> users_ids = new ArrayList<String>();
                            private List<String> users_usernames = new ArrayList<String>();
                            private List<String> users_names = new ArrayList<String>();
                            private List<String> users_profile_images = new ArrayList<String>();
                            private List<String> users_timestamps = new ArrayList<String>();
                            private List<String> users_follows = new ArrayList<String>();

                            private int start = 0, amount = Config.DEFAULT_USERS_BUFFER_ITEMS;

                            private ProgressBar loading;

                            private boolean isProcessing = false;


                            @Override
                            public void onClick(View view) {
                                if (chat_userText.getText().toString().isEmpty()) {

                                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                                    View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_select_user, null);
                                    localBuilder.setView(dialogView);

                                    list = (ListView) dialogView.findViewById(R.id.list);
                                    userText = (AutoCompleteTextView) dialogView.findViewById(R.id.user);
                                    selectButton = (ImageButton) dialogView.findViewById(R.id.select);


                                    amount = Functions.getInt(context, "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);
                                    loading = (ProgressBar) dialogView.findViewById(R.id.loading);

                                    if ((!Functions.isOnline(context))) {
                                        Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                                        return;
                                    }

                                    method = "followed";
                                    argument = Functions.getString(context, "id", "");


                                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            userText.setText(users_usernames.get(i));
                                        }

                                    });

                                    final AlertDialog dialog = localBuilder.create();
                                    dialog.show();

                                    selectButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (userText.getText().toString().isEmpty()) {
                                                return;
                                            }

                                            chat_userText.setText(userText.getText().toString());
                                            dialog.dismiss();
                                        }
                                    });


                                    adapter = new UsersAdapter(activity, null, false);

                                    adapter.users = users_ids;
                                    adapter.user_profile_images = users_profile_images;
                                    adapter.user_timestamps = users_timestamps;
                                    adapter.user_names = users_names;
                                    adapter.follows = users_follows;

                                    list.setAdapter(adapter);
                                    list.setSmoothScrollbarEnabled(true);


                                    loadUsers();

                                    ArrayAdapter<String> userAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, users_names);
                                    userText.setAdapter(userAdapter);

                                    return;
                                }


                                final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);
                                new AsyncTask<String, String, String>() {
                                    String username = chat_userText.getText().toString();

                                    @Override
                                    protected void onPreExecute() {
                                        sDialog.show();
                                        sDialog.setCancelable(false);

                                        super.onPreExecute();
                                    }

                                    @Override
                                    protected String doInBackground(String... strings) {
                                        return Fetcher.FetchURL(APIManager.addToChat(chat_id, username));
                                    }

                                    @Override
                                    protected void onPostExecute(String result) {
                                        if (result.contains("success")) {

                                            ChatsActivity.chats_adapter.chats = ChatsActivity.chats;

                                            ChatsActivity.sortItems();
                                            ChatsActivity.chats_adapter.notifyDataSetChanged();

                                            ChatActivity.refreshLoad();

                                            sDialog.setTitleText("").setContentText("کاربر به گروه اضافه شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                            dialog.dismiss();
                                        } else if (result.equals("no-user")) {
                                            sDialog.setTitleText("").setContentText("چنین کاربری وجود ندارد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                        } else {
                                            sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                        }

                                        super.onPostExecute(result);
                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }


                            void loadUsers() {

                                if (isProcessing) {
                                    return;
                                }

                                isProcessing = true;


                                new AsyncTask<String, String, String>() {

                                    @Override
                                    protected void onPreExecute() {
                                        loading.setVisibility(View.VISIBLE);

                                        super.onPreExecute();
                                    }

                                    @Override
                                    protected String doInBackground(String... strings) {
                                        return Fetcher.FetchURL(APIManager.getUsers(method, argument, start, amount, Functions.getString(context, "id", "")));
                                    }

                                    @Override
                                    protected void onPostExecute(String result) {

                                        loading.setVisibility(View.INVISIBLE);

                                        if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "users").equals("-1")) {

                                            users_ids.addAll(JSONParser.getListFromString(result, "users", "id"));
                                            users_usernames.addAll(JSONParser.getListFromString(result, "users", "username"));
                                            users_profile_images.addAll(JSONParser.getListFromString(result, "users", "profile_image"));
                                            users_timestamps.addAll(JSONParser.getListFromString(result, "users", "timestamp"));
                                            users_names.addAll(JSONParser.getListFromString(result, "users", "name"));
                                            users_follows.addAll(JSONParser.getListFromString(result, "users", "follow"));

                                            start += amount;

                                            adapter.users = users_ids;
                                            adapter.user_profile_images = users_profile_images;
                                            adapter.user_timestamps = users_timestamps;
                                            adapter.user_names = users_names;
                                            adapter.follows = users_follows;

                                            ArrayAdapter<String> userAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, users_names);
                                            userText.setAdapter(userAdapter);

                                            if (start == amount) { // IF IS FIRST LOAD

                                                list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                                    public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                                        if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == users_ids.size()))
                                                            loadUsers();
                                                    }

                                                    public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                                    }
                                                });
                                            }

                                            adapter.notifyDataSetChanged();

                                        } else {
                                            list.setOnScrollListener(null);
                                        }

                                        isProcessing = false;

                                        super.onPostExecute(result);
                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                            }


                        });


                        chat_adapter = new UsersAdapter(activity, null, false);
                        chat_adapter.showExtras = true;

                        chat_users_ids.clear();
                        chat_users_usernames.clear();
                        chat_users_profile_images.clear();
                        chat_users_timestamps.clear();
                        chat_users_extras.clear();
                        chat_users_names.clear();
                        chat_users_follows.clear();

                        chat_adapter.users = chat_users_ids;
                        chat_adapter.user_profile_images = chat_users_profile_images;
                        chat_adapter.user_timestamps = chat_users_timestamps;
                        chat_adapter.extras = chat_users_extras;
                        chat_adapter.user_names = chat_users_names;
                        chat_adapter.follows = chat_users_follows;

                        chat_list.setAdapter(chat_adapter);
                        chat_list.setSmoothScrollbarEnabled(true);


                        loadChatUsers();
                        return true;

                    }


                    void loadChatUsers() {

                        if (isProcessing) {
                            return;
                        }

                        isProcessing = true;


                        new AsyncTask<String, String, String>() {

                            @Override
                            protected void onPreExecute() {
                                chat_loading.setVisibility(View.VISIBLE);

                                super.onPreExecute();
                            }

                            @Override
                            protected String doInBackground(String... strings) {
                                return Fetcher.FetchURL(APIManager.getUsers(chat_method, chat_argument, chat_start, chat_amount, Functions.getString(context, "id", "")));
                            }

                            @Override
                            protected void onPostExecute(String result) {

                                chat_loading.setVisibility(View.INVISIBLE);

                                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "users").equals("-1")) {

                                    chat_users_ids.addAll(JSONParser.getListFromString(result, "users", "id"));
                                    chat_users_usernames.addAll(JSONParser.getListFromString(result, "users", "username"));
                                    chat_users_profile_images.addAll(JSONParser.getListFromString(result, "users", "profile_image"));
                                    chat_users_timestamps.addAll(JSONParser.getListFromString(result, "users", "timestamp"));
                                    chat_users_extras.addAll(JSONParser.getListFromString(result, "users", "extras"));
                                    chat_users_names.addAll(JSONParser.getListFromString(result, "users", "name"));
                                    chat_users_follows.addAll(JSONParser.getListFromString(result, "users", "follow"));

                                    chat_start += chat_amount;

                                    chat_adapter.users = chat_users_ids;
                                    chat_adapter.user_profile_images = chat_users_profile_images;
                                    chat_adapter.user_timestamps = chat_users_timestamps;
                                    chat_adapter.extras = chat_users_extras;
                                    chat_adapter.user_names = chat_users_names;
                                    chat_adapter.follows = chat_users_follows;

                                    if (chat_start == chat_amount) { // IF IS FIRST LOAD

                                        chat_list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                            public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                                if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 >= chat_start))
                                                    loadChatUsers();
                                            }

                                            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                            }
                                        });
                                    }

                                    chat_adapter.notifyDataSetChanged();

                                } else {
                                    chat_list.setOnScrollListener(null);
                                }

                                isProcessing = false;

                                super.onPostExecute(result);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    }


                });


            }

            Preference leaveChat = (Preference) findPreference("leave_chat");
            leaveChat.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
                    sDialog.setTitleText("").setContentText("آیا مطمئنید؟").showCancelButton(true).setConfirmText("بله").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            new AsyncTask<String, String, String>() {

                                @Override
                                protected void onPreExecute() {
                                    sDialog.setCancelable(false);

                                    super.onPreExecute();
                                }

                                @Override
                                protected String doInBackground(String... strings) {
                                    return Fetcher.FetchURL(APIManager.leaveChat(chat_id, Functions.getString(context, "id", "")));
                                }

                                @Override
                                protected void onPostExecute(String result) {
                                    if (result.contains("success")) {
                                        sDialog.dismiss();


                                        if (ChatActivity.activity != null) {
                                            ChatActivity.activity.finish();
                                        }


                                        try {
                                            List<Chat> chats = ChatsActivity.chats;
                                            for (Chat chat2 : chats) {
                                                if (chat2.id == chat.id) {
                                                    chat2.type = "deleted";
                                                    dbHelper.updateChat(chat2);
                                                    chats.remove(chat2);
                                                }
                                            }


                                            ChatsActivity.chats = chats;
                                            ChatsActivity.sortItems();
                                            ChatsActivity.chats_adapter.chats = ChatsActivity.chats;
                                            ChatsActivity.chats_adapter.notifyDataSetChanged();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                        ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
                                        HashMap<String, String> filter = new HashMap<String, String>();
                                        filter.put("name", "chat");
                                        filter.put("method", "equal");
                                        filter.put("value", chat_id);
                                        filters.add(filter);
                                        List<ChatsMessage> chats_messages = dbHelper.searchChatsMessages(filters, "id", 0, 0, true, true);


                                        for (ChatsMessage chats_message : chats_messages) {
                                            chats_message.type = "deleted";
                                            dbHelper.updateChatsMessage(chats_message);
                                        }


                                        activity.finish();
                                    } else {
                                        sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).showCancelButton(false).setCancelText("").setCancelClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                        sDialog.show();
                                    }

                                    super.onPostExecute(result);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    }).setCancelText("انصراف").setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            sDialog.dismiss();
                        }
                    });
                    sDialog.show();

                    return true;
                }
            });


            CheckBoxPreference showNotification = (CheckBoxPreference) findPreference("chat_show_notifications_");
            showNotification.setKey("chat_show_notifications_" + chat_id);
            showNotification.setChecked(Functions.getBoolean(context, "chat_show_notifications_" + chat_id, true));

            CheckBoxPreference notificationSound = (CheckBoxPreference) findPreference("chat_notification_sound_");
            notificationSound.setKey("chat_notification_sound_" + chat_id);
            notificationSound.setChecked(Functions.getBoolean(context, "chat_notification_sound_" + chat_id, true));

            CheckBoxPreference notificationVibrate = (CheckBoxPreference) findPreference("chat_notification_vibration_");
            notificationVibrate.setKey("chat_notification_vibration_" + chat_id);
            notificationVibrate.setChecked(Functions.getBoolean(context, "chat_notification_vibration_" + chat_id, true));


        }


        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            this.activity = activity;
            context = activity.getApplicationContext();
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof Activity) {
                activity = (Activity) context;
            } else {
                this.context = context;
            }
        }

        private void doCrop() {
            try {
                // call the standard crop action intent (the user device may not
                // support it)
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                // indicate image type and Uri
                cropIntent.setDataAndType(Uri.fromFile(new File(temp_path)), "image/*");
                // set crop properties
                cropIntent.putExtra("crop", "true");
                // indicate aspect of desired crop
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                // indicate output X and Y
                cropIntent.putExtra("outputX", 512);
                cropIntent.putExtra("outputY", 512);

                File f = new File(temp_path + "_cropped");
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }


                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                cropIntent.putExtra("output", Uri.fromFile(f));
                // retrieve data on return

                // cropIntent.putExtra("return-data", true);
                startActivityForResult(cropIntent, CROP);
                // start the activity - we handle returning in onActivityResult

            }
            // respond to users whose devices do not support the crop action
            catch (ActivityNotFoundException | IOException e) {
                Log.d("error", "no activity found for cropping image.");
                e.printStackTrace();
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, final Intent data) {
            //if (resultCode != -1) return;
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            switch (requestCode) {
                case PICK_FROM_CAMERA:
                    //doCrop();

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {

                            super.onPreExecute();
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            //mImageUri = data.getData();
                            //Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {

                            new AsyncTask<Void, Void, Void>() {

                                @Override
                                protected Void doInBackground(Void... voids) {
                                    mImageUri = null;
                                    Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_PROFILE_IMAGE_SIZE", Config.DEFAULT_MAX_PROFILE_IMAGE_SIZE), 75, 5);
                                    Functions.rotateImageIfNeed(temp_path);
                                    return null;

                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    mImageUri = Uri.fromFile(new File(temp_path));
                                                    doCrop();
                                                }
                                            });
                                        }
                                    }, 0);

                                    super.onPostExecute(aVoid);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                            super.onPostExecute(aVoid);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    break;
                case PICK_FROM_FILE:

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... voids) {
                            mImageUri = data.getData();
                            Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            new AsyncTask<Void, Void, Void>() {

                                @Override
                                protected Void doInBackground(Void... voids) {
                                    mImageUri = null;
                                    Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_PROFILE_IMAGE_SIZE", Config.DEFAULT_MAX_PROFILE_IMAGE_SIZE), 75, 5);
                                    Functions.rotateImageIfNeed(temp_path);
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mImageUri = Uri.fromFile(new File(temp_path));
                                                    doCrop();
                                                }
                                            });
                                        }
                                    }, 0);
                                    super.onPostExecute(aVoid);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                            super.onPostExecute(aVoid);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    break;
                case CROP:


                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {
                            imageView.setImageURI(null);
                            imageView.setImageResource(R.drawable.ic_no_image);

                            super.onPreExecute();
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            Functions.copyFile(new File(temp_path + "_cropped"), new File(temp_path), true);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            new AsyncTask<Void, Void, Void>() {

                                @Override
                                protected Void doInBackground(Void... voids) {
                                    mImageUri = null;

                                    // Rotate picture if necessary
                                    Bitmap fileBitmap2 = Functions.getBitmapFromFile(temp_path);
                                    fileBitmap2 = Functions.rotateBitmap(fileBitmap2, Functions.getOrientationOfFile(temp_path));
                                    Functions.saveBitmapToFile(fileBitmap2, temp_path);
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mImageUri = Uri.fromFile(new File(temp_path));
                                                    imageView.setImageURI(mImageUri);
                                                    uploadImage();
                                                }
                                            });
                                        }
                                    }, 0);
                                    super.onPostExecute(aVoid);
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                            super.onPostExecute(aVoid);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    break;
                case PICK_BACKGROUND:
                    Functions.putString(context, "chat_bg_type_" + chat.id, "file");
                    Functions.putString(context, "chat_bg_" + chat.id, Functions.getPathFromURI(context, data.getData()));

                    Log.d("tag", Functions.getPathFromURI(context, data.getData()));

                    break;
            }
        }


        private void uploadImage() {
            if (isProcessing) {
                return;
            }

            if (mImageUri == null) {
                return;
            }

            isProcessing = true;

            String fileAddress = Information.TEMP_PATH_PREFIX;
            final String fileName = chat_id + ".jpg";
            final String file = fileAddress + fileName;


            final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("در حال آپلود فایل...").setContentText("").showCancelButton(false);


            new AsyncTask<String, String, String>() {

                @Override
                protected void onPreExecute() {

                    super.onPreExecute();
                }

                protected String doInBackground(String... paramArrayOfString) {
                    Functions.reduceImageFileSize(file, 1024 * Functions.getInt(context, "MAX_PROFILE_IMAGE_SIZE", Config.DEFAULT_MAX_PROFILE_IMAGE_SIZE), 75, 5);
                    String result = Functions.uploadFile(temp_path, APIManager.uploadFile("group_images", true, false));

                    if (result.equals("fail")) {
                        uploaded = false;
                    } else {
                        uploaded = true;
                    }
                    return result;
                }

                protected void onPostExecute(String result) {
                    if (uploaded) {
                        mImageUri = null;
                        image = result;
                        chat.image = image;
                        dbHelper.updateChat(chat);
                        editGroup();
                        sDialog.hide();
                        isProcessing = false;
                    } else {
                        sDialog.setTitleText("").setContentText("خطا در آپلود فایل!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        isProcessing = false;
                    }
                }

            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            sDialog.show();
        }


        private void editGroup() {


            final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);
            new AsyncTask<String, String, String>() {

                @Override
                protected void onPreExecute() {
                    sDialog.show();
                    sDialog.setCancelable(false);

                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(String... strings) {
                    return Fetcher.FetchURL(APIManager.updateChat(chat_id, name, image, about, uploaded));
                }

                @Override
                protected void onPostExecute(String result) {
                    if (result.contains("success")) {
                        uploaded = false;

                        sDialog.setTitleText("").setContentText("تغییرات ثبت شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        chat.name = name;
                        chat.about = about;

                        dbHelper.updateChat(chat);


                        ChatActivity.refreshLoad();

                        try {
                            List<Chat> chats = ChatsActivity.chats;
                            for (Chat chat2 : chats) {
                                if (chat2.id == chat.id) {
                                    chat2.name = name;
                                    chat2.image = image;
                                    chat2.about = about;

                                }
                            }


                            ChatsActivity.chats = chats;
                            ChatsActivity.chats_adapter.chats = ChatsActivity.chats;

                            ChatsActivity.sortItems();
                            ChatsActivity.chats_adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }




                        /*
                        if (!former_name.equals(chat.name)){
                            String name = Functions.getString(context, "first_name", "") + " " + Functions.getString(getApplicationContext(), "last_name", "");
                            if (name.equals(" ")) {
                                name = Functions.getString(context, "username", "");
                            }

                            ChatsMessage chats_message = new ChatsMessage();
                            chats_message.chat = chat_id;
                            chats_message.sender = Functions.getString(context, "id", "");
                            chats_message.sender_name = name;
                            chats_message.sender_profile_image = Functions.getString(context, "profile_image", "");
                            chats_message.action = "change_name";
                            chats_message.message = "";
                            chats_message.timestamp = String.valueOf(System.currentTimeMillis());
                            chats_message.type = "action";
                            chats_message.status = "read";

                        }


                        if (uploaded || !former_image.equals(chat.image)){

                        }*/


                    } else {
                        sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    }

                    super.onPostExecute(result);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            sDialog.show();

        }

    }

}