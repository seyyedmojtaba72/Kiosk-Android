package com.kiosk.android;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.PostsGridAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;

import java.util.ArrayList;
import java.util.List;


public class SearchActivity extends AppCompatActivity {
    private String tag = "";

    private int start = 0, amount = Config.DEFAULT_POSTS_BUFFER_ITEMS;

    private PostsGridAdapter adapter;
    private GridView grid;

    private List<String> posts_ids = new ArrayList<String>();
    private List<String> posts_images = new ArrayList<String>();
    private List<String> posts_prices = new ArrayList<String>();

    private boolean isProcessing = false;

    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        amount = Functions.getInt(getApplicationContext(), "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

        if ((!Functions.isOnline(getApplicationContext()))) {
            Toast.makeText(SearchActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        Uri data = getIntent().getData();
        tag = data.getQueryParameters("tag").get(0);


        grid = (GridView) findViewById(R.id.grid);

        loading = (ProgressBar) findViewById(R.id.loading);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent localIntent = new Intent(SearchActivity.this, PostActivity.class);
                localIntent.putExtra("id", posts_ids.get(i));
                startActivity(localIntent);
            }

        });


        adapter = new PostsGridAdapter(SearchActivity.this, null);

        adapter.ids = posts_ids;
        adapter.images = posts_images;
        adapter.prices = posts_prices;

        grid.setAdapter(adapter);
        grid.setSmoothScrollbarEnabled(true);

        loadPosts();


        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("نتایج جستجو");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }


    private void loadPosts() {

        if (isProcessing) {
            return;
        }

        isProcessing = true;

        new AsyncTask<String, String, String>() {

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.searchPosts(tag, start, amount, Functions.getString(getApplicationContext(), "id", "")));
            }

            @Override
            protected void onPostExecute(String result) {
                String posts_json = result;

                grid.setVisibility(View.VISIBLE);
                grid.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));

                loading.setVisibility(View.INVISIBLE);


                if (!posts_json.isEmpty() && !posts_json.equals("-1") && !JSONParser.getFromString(posts_json, "posts").equals("-1")) {

                    posts_ids.addAll(JSONParser.getListFromString(posts_json, "posts", "id"));
                    posts_images.addAll(JSONParser.getListFromString(posts_json, "posts", "image"));
                    posts_prices.addAll(JSONParser.getListFromString(posts_json, "posts", "price"));

                    start += amount;

                    adapter.ids = posts_ids;
                    adapter.images = posts_images;
                    adapter.prices = posts_prices;


                    if (start == amount) { // IF IS FIRST LOAD

                        grid.setOnScrollListener(new AbsListView.OnScrollListener() {
                            public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == posts_ids.size()))
                                    loadPosts();
                            }

                            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                            }
                        });
                    }

                    adapter.notifyDataSetChanged();

                } else {
                    grid.setOnScrollListener(null);
                }

                isProcessing = false;


                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

}