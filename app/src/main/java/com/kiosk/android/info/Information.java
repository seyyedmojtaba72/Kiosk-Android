package com.kiosk.android.info;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;

public class Information {

    public static String DB_NAME = "Kiosk.sqlite";

    public static String WEBSITE_HOME_URL = "http://www.kioskapp.ir/";
    public static String EMAIL_ADDRESS = "info@kioskapp.ir";
    public static String SMS_NUMBER = "";

    //public static String PRESENTER = "";

    public static String DEFAULT_SYSTEM_HOME_URL = "http://app.kioskapp.ir/";

    //public static String DEFAULT_API_HOME_URL = "http://api.kioskapp.ir/";
    public static String DEFAULT_API_HOME_URL = "http://192.168.2.1/kioskapp.ir/apadana/api/";
    //public static String DEFAULT_BACKUP_API_HOME_URL = "http://api.kioskapp.ir/";
    public static String DEFAULT_BACKUP_API_HOME_URL = "http://192.168.2.1/kioskapp.ir/apadana/api/";

    //public static String DEFAULT_PROFILE_IMAGES_URL = "http://app.kioskapp.ir/files/profile_images/";
    public static String DEFAULT_PROFILE_IMAGES_URL = "http://192.168.2.1/kioskapp.ir/apadana/files/profile_images/";
    //public static String DEFAULT_GROUP_IMAGES_URL = "http://app.kioskapp.ir/files/group_images/";
    public static String DEFAULT_GROUP_IMAGES_URL = "http://192.168.2.1/kioskapp.ir/apadana/files/group_images/";
    //public static String DEFAULT_USERS_UPLOADS_URL = "http://app.kioskapp.ir/files/user_uploads/";
    public static String DEFAULT_USERS_UPLOADS_URL = "http://192.168.2.1/kioskapp.ir/apadana/files/user_uploads/";

    public static String BASE_NAME = "Kiosk";
    public static String BASE_NAME_FA = "کیوسک";
    public static String DEFAULT_FOOTER_DESCRIPTION = "KioskApp.ir - کیوسک";

    public static String PATH_PREFIX, TEMP_PATH_PREFIX, PROFILE_IMAGES_PATH_PREFIX, DOWNLOADS_PATH, IMAGES_PATH;

    public static String WHATS_NEWS = "";// + "\n" + "- رفع برخی مشکلات جزئی";

    public Information(final Context context) {

        //PRESENTER = Fetcher.FetchAsset(context, "Presenter");

        PATH_PREFIX = context.getExternalFilesDir(BASE_NAME) + "/";
        String EXTERNAL_PATH_PREFIX = Environment.getExternalStorageDirectory() + "/" + BASE_NAME + "/";

        Log.d("debug", "PATH_PREFIX: " + PATH_PREFIX);

        TEMP_PATH_PREFIX = PATH_PREFIX + "/Temp/";
        PROFILE_IMAGES_PATH_PREFIX = PATH_PREFIX + "/Profile Images/";
        DOWNLOADS_PATH = EXTERNAL_PATH_PREFIX + "/Downloads/";
        IMAGES_PATH = EXTERNAL_PATH_PREFIX + "/Images/";

        String[] paths = new String[]{PATH_PREFIX, TEMP_PATH_PREFIX, PROFILE_IMAGES_PATH_PREFIX, DOWNLOADS_PATH, IMAGES_PATH};
        for (String path : paths) {
            (new File(path)).mkdirs();
        }

    }
}
