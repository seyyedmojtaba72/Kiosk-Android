package com.kiosk.android.info;

/**
 * Created by SeyyedMojtaba on 3/22/2016.
 */

public class Config {

    public static final boolean appendNotificationMessages = false;

    // broadcast receiver intent filters
    public static final String INTENT_REGISTRATION_COMPLETE = "registrationComplete";
    public static final String INTENT_PUSH_NOTIFICATION = "pushNotification";

    public static final int DEFAULT_POSTS_BUFFER_ITEMS = 20;
    public static final int DEFAULT_COMMENTS_BUFFER_ITEMS = 20;
    public static final int DEFAULT_USERS_BUFFER_ITEMS = 100;
    public static final int DEFAULT_HISTORY_BUFFER_ITEMS = 100;
    public static final int DEFAULT_CHATS_MESSAGES_BUFFER_ITEMS = 100;

    public static final int DEFAULT_HISTORY_REFRESH_INTERVAL = 2   * 60 * 1000;

    public static final int DEFAULT_MAX_PROFILE_IMAGE_SIZE = 100;
    public static final int DEFAULT_MAX_UPLOAD_FILE_SIZE = 300;


    public static final int FLAG_DEFAULT = 100;
    public static final int FLAG_ACTIVITY = 101;
    public static final int FLAG_CHAT = 102;


    public static final int NOTIFICATION_ID_DEFAULT = 100;
    public static final int NOTIFICATION_ID_FOLLOW_REQUEST = 101;
    public static final int NOTIFICATION_ID_FOLLOW_ACCEPT = 102;
    public static final int NOTIFICATION_ID_FOLLOW = 103;
    public static final int NOTIFICATION_ID_LIKE = 104;
    public static final int NOTIFICATION_ID_COMMENT = 105;
    public static final int NOTIFICATION_ID_TAG_POST = 106;
    public static final int NOTIFICATION_ID_TAG_COMMENT = 107;

    public static final int IN_APP_BILLING_API_VERSION = 3;
    public static final String IN_APP_BILLING_TYPE = "inapp";
    public static final String IRANAPPS_IN_APP_API_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCihxniEKAf5/nyerGXp6uvzstcSwT2oU3Yvr4Oq1QBxL9aS5qyl6C6McDmpr+lzPYloKJBGi4LRkBdSjdYM1nkV+0tYqOul58B5YtW00YRASSo58zgSAE5ZofDYLFnt7rJ/QQmsgGBLx3Qf4TwitCvRfH0NhE2B+jRMOmTTodbEwIDAQAB";
    public static final String CAFEBAZAAR_IN_APP_API_KEY = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCcYnNAzdajSZgiUX3z6nZRsLpKW1caXrTQ7EV1paTVGVnhka5KtfvAEKa0IGBTtwSle5hCsrUZLTOWLOV9peJlrE8fkBLMClnGWDA+tQQ8cnkrGu2A70oOsyIXPxO7BBExY8+t++RznK2An4nzF7AIYv+2qdlixdeQZPDB8elXMstZjcfRs9OtNgeNbsm4xbEJsfQe3l23S1Jawaw4dmedAaxjQZQJ51dS4QvGAm8CAwEAAQ==";

    public static final String GCM_ANDROID_API_KEY = "AIzaSyAiezxPJW_qzsCIwFVHk0PKfVxsEXIRVK0";
}