package com.kiosk.android.info;

import android.content.Context;

import com.github.seyyedmojtaba72.android_utils.Functions;

public class LinkManager {
    public static Context context;
    private static String SYSTEM_HOME_URL = Functions.getString(context, "SYSTEM_HOME_URL", Information.DEFAULT_SYSTEM_HOME_URL);

    public static String viewPost(int Post) {
        return SYSTEM_HOME_URL + "post_view.php?id=" + Post;
    }

    public static String chargeAccount(String username, String password, String amount) {
        return SYSTEM_HOME_URL + "inc/login.php?username=" + username + "&password=" + password + "&redirect=" + Functions.URLEncode("../online_payment.php?action=charge_balance&amount=" + amount);
    }

    public static String manageAccount(String username, String password) {
        return SYSTEM_HOME_URL + "inc/login.php?username=" + username + "&password=" + password + "&redirect=../profile.php";
    }

    public static String viewTerms() {
        return SYSTEM_HOME_URL + "terms.php";
    }

    public static String viewHelp() {
        return SYSTEM_HOME_URL + "help.php";
    }
}