package com.kiosk.android.info;

import android.content.Context;

import com.github.seyyedmojtaba72.android_utils.Functions;

/**
 * Created by SeyyedMojtaba on 3/24/2016.
 */
public class EndPoints {
    public static Context context;
    private static String API_HOME_URL = Functions.getString(context, "API_HOME_URL", Information.DEFAULT_API_HOME_URL);

    public static final String BASE_URL = API_HOME_URL + "fcm/";
    public static final String CHATS_GET = BASE_URL + "chats/get";
    public static final String CHAT_GET = BASE_URL + "chat/get";
    public static final String CHAT_SEND = BASE_URL + "chat/send";
}