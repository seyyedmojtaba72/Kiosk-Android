package com.kiosk.android.info;

import android.content.Context;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.MainActivity;

public class APIManager {
    public static Context context;
    private static String API_HOME_URL = Functions.getString(context, "API_HOME_URL", Information.DEFAULT_API_HOME_URL);
    private static String BACKUP_API_HOME_URL = Functions.getString(context, "BACKUP_API_HOME_URL", Information.DEFAULT_BACKUP_API_HOME_URL);

    public static String getInfo(String user, String deviceID, String version, String platform) {
        return API_HOME_URL + "get_info.php?user=" + user + "&device_id=" + deviceID + "&version=" + version + "&platform=" + platform;
    }

    public static String Register(String username, String email, String password) {
        return API_HOME_URL + "register.php?username=" + Functions.URLEncode(username) + "&email=" + Functions.URLEncode(email) + "&password=" + Functions.URLEncode(password);
    }
    public static String Login(String username, String password, String gcmToken) {
        return API_HOME_URL + "login.php?username=" + Functions.URLEncode(username) + "&password=" + Functions.URLEncode(password) + "&gcm_token=" + Functions.URLEncode(gcmToken);
    }

    public static String updateProfile(String username, String first_name, String last_name, String mobile_number, String shaba, String former_email, String email, String profile_image, String about, String extras) {
        MainActivity.toRefresh = true;
        return API_HOME_URL + "update_profile.php?username=" + Functions.URLEncode(username) + "&first_name=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(first_name)) + "&last_name=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(last_name)) + "&mobile_number=" + Functions.URLEncode(mobile_number) + "&shaba=" + Functions.URLEncode(shaba) + "&former_email=" + Functions.URLEncode(former_email) + "&email=" + Functions.URLEncode(email) + "&profile_image=" + Functions.URLEncode(profile_image) + "&about=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(about)) + "&extras=" + extras;
    }

    public static String uploadFile(String directory, boolean replace, boolean backup) {
        String address = "";
        if (backup) {
            address += BACKUP_API_HOME_URL;
        } else {
            address += API_HOME_URL;
        }
        address += "upload_file.php?directory=" + Functions.URLEncode(directory);
        if (replace) {
            address += "&replace=TRUE";
        } else {
            address += "&replace=FALSE";
        }
        return address;
    }

    public static String deleteFile(String file, String directory, boolean backup) {
        String address = "";
        if (backup) {
            address += BACKUP_API_HOME_URL;
        } else {
            address += API_HOME_URL;
        }
        address += "delete_file.php?file=" + file + "&directory=" + Functions.URLEncode(directory);
        return address;
    }

    public static String getFileSize(String urlAddress, boolean backup) {
        String address = "";
        if (backup) {
            address += BACKUP_API_HOME_URL;
        } else {
            address += API_HOME_URL;
        }
        address += "get_file_size.php?url=" + Functions.URLEncode(urlAddress);
        return address;
    }

    public static String getUpdate() {
        return API_HOME_URL + "get_update.php";
    }


    // CUSTOM API

    public static String getPosts(String author, int start, int amount, String user) {
        return API_HOME_URL + "get_posts.php?author=" + author + "&start=" + start + "&amount=" + amount + "&user=" + user;
    }

    public static String getPost(String post, String user) {
        return API_HOME_URL + "get_post.php?post=" + post + "&user=" + Functions.URLEncode(user);
    }

    public static String getPostComments(String id, String user, String start, String amount) {
        return API_HOME_URL + "get_post_comments.php?id=" + id + "&user=" + user + "&start=" + start + "&amount=" + amount;
    }

    public static String setPostMyComment(String id, String author, String comment) {
        return API_HOME_URL + "set_post_my_comment.php?id=" + id + "&author=" + Functions.URLEncode(author) + "&comment=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(comment));
    }

    public static String setPostMyReport(String id, String author, String report) {
        return API_HOME_URL + "set_post_my_report.php?id=" + id + "&author=" + Functions.URLEncode(author) + "&report=" + Functions.URLEncode(report);
    }

    public static String setMyPost(String author, String name, String description, String image, String price, String download_link) {
        MainActivity.toRefresh = true;
        return API_HOME_URL + "set_my_post.php?author=" + Functions.URLEncode(author) + "&name=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(name)) + "&description=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(description)) + "&image=" + Functions.URLEncode(image) + "&price=" + Functions.URLEncode(price) + "&download_link=" + Functions.URLEncode(download_link);
    }

    public static String editPost(String id, String name, String description) {
        MainActivity.toRefresh = true;
        return API_HOME_URL + "edit_post.php?id=" + Functions.URLEncode(id) + "&name=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(name)) + "&description=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(description));
    }

    public static String likePost(String user, String post) {
        return API_HOME_URL + "like_post.php?user=" + Functions.URLEncode(user) + "&post=" + post;
    }

    public static String purchasePost(String customer, String post) {
        return API_HOME_URL + "purchase_post.php?customer=" + Functions.URLEncode(customer) + "&post=" + post;
    }

    public static String deleteMyPost(String post, String author) {
        MainActivity.toRefresh = true;
        return API_HOME_URL + "delete_my_post.php?post=" + post + "&author=" + author;
    }

    public static String getProfile(String author, String user, String method) {
        return API_HOME_URL + "get_profile.php?author=" + Functions.URLEncode(author) + "&user=" + Functions.URLEncode(user) + "&method=" + method;
    }

    public static String followUser(String follower, String followed) {
        MainActivity.toRefresh = true;
        return API_HOME_URL + "follow_user.php?follower=" + Functions.URLEncode(follower) + "&followed=" + Functions.URLEncode(followed);
    }

    public static String getUsers(String method, String argument, int start, int amount, String user) {
        return API_HOME_URL + "get_users.php?method=" + Functions.URLEncode(method) + "&argument=" + Functions.URLEncode(argument) + "&user=" + Functions.URLEncode(user) + "&start=" + start + "&amount=" + amount;
    }

    public static String requestCheckOut(String user) {
        return API_HOME_URL + "request_checkout.php?user=" + Functions.URLEncode(user);
    }

    public static String searchPosts(String tag, int start, int amount, String user) {
        return API_HOME_URL + "search_posts.php?tag=" + Functions.URLEncode(tag) + "&start=" + start + "&amount=" + amount + "&user=" + user;
    }

    public static String searchUsers(String username, int start, int amount, String user) {
        return API_HOME_URL + "search_users.php?username=" + Functions.URLEncode(username) + "&start=" + start + "&amount=" + amount + "&user=" + user;
    }

    public static String getHistory(String user, int amount, String timestamp) {
        return API_HOME_URL + "get_history.php?user=" + Functions.URLEncode(user) + "&amount=" + amount + "&timestamp=" + timestamp;
    }

    public static String respondRequest(String user, String follower, String action) {
        return API_HOME_URL + "respond_request.php?user=" + user + "&follower=" + follower + "&action=" + action;
    }

    public static String blockUser(String user, String id) {
        return API_HOME_URL + "block_user.php?user=" + user + "&id=" + id;
    }

    public static String deleteComment(String user, String comment) {
        return API_HOME_URL + "delete_comment.php?user=" + user + "&comment=" + comment;
    }

    public static String changePassword(String username, String former_password, String password) {
        return API_HOME_URL + "change_password.php?username=" + Functions.URLEncode(username) + "&former_password=" + Functions.URLEncode(former_password) + "&password=" + Functions.URLEncode(password);
    }

    public static String Report(String reporter, String reported, String subject, String report) {
        return API_HOME_URL + "report.php?reporter=" + Functions.URLEncode(reporter) + "&reported=" + Functions.URLEncode(reported) + "&subject=" + Functions.URLEncode(subject) + "&report=" + Functions.URLEncode(report);
    }

    public static String inAppCharge(String user, String sku) {
        return API_HOME_URL + "in_app_charge.php?user=" + Functions.URLEncode(user) + "&sku=" + Functions.URLEncode(sku);
    }

    public static String startChat(String creator, String participants, String name, String image) {
        return API_HOME_URL + "start_chat.php?creator=" + Functions.URLEncode(creator) + "&participants=" + Functions.URLEncode(participants) + "&name=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(name)) + "&image=" + Functions.URLEncode(image);
    }

    public static String getChats(String user, String start, String amount) {
        return API_HOME_URL + "get_chats.php?user=" + Functions.URLEncode(user) + "&start=" + Functions.URLEncode(start) + "&amount=" + Functions.URLEncode(amount);
    }

    public static String updateChat(String chat, String name, String image, String about, boolean uploaded) {
        return API_HOME_URL + "update_chat.php?chat=" + Functions.URLEncode(chat) + "&name=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(name)) + "&image=" + Functions.URLEncode(image) + "&about=" + Functions.URLEncode(EmojiMapUtil.replaceUnicodeEmojis(about)) + "&uploaded=" + uploaded;
    }

    public static String addToChat(String chat, String user) {
        return API_HOME_URL + "add_to_chat.php?chat=" + Functions.URLEncode(chat) + "&user=" + Functions.URLEncode(user);
    }

    public static String removeFromChat(String chat, String user) {
        return API_HOME_URL + "remove_from_chat.php?chat=" + Functions.URLEncode(chat) + "&user=" + Functions.URLEncode(user);
    }

    public static String leaveChat(String chat, String user) {
        return API_HOME_URL + "leave_chat.php?chat=" + Functions.URLEncode(chat) + "&user=" + Functions.URLEncode(user);
    }
}