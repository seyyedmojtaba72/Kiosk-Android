package com.kiosk.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.LinkManager;
import com.kiosk.android.util.CustomFunctions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by SeyyedMojtaba on 2/25/2016.
 */
public class ProfileFragment extends Fragment {
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private static final int CROP = 3;
    private static final int IN_APP_BILLING = 4;

    public static Activity activity;
    public static Context context;
    public static View view;

    public static String temp_path;
    public static Uri mImageUri;
    public static boolean uploaded = false;
    public static String profile_image = "";
    public static CircleImageView profileImageView;
    public static ImageView trustedImage;

    public static boolean isCharging = false;
    public static ActionProcessButton chargeAccountButton;
    public static TextView balanceText, PostsNumberText, followersNumberText, followedsNumberText;

    public static TextView nameText;
    public static TextView aboutText;


    public static boolean toRefresh = false;

    //private ProgressBar loading;
    public static SwipeRefreshLayout swipeRefreshLayout;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        this.view = rootView;


        temp_path = Information.TEMP_PATH_PREFIX + Functions.getString(context, "id", "profile_image") + ".jpg";
        File tempFile = new File(temp_path);
        if (tempFile.exists()) {
            tempFile.delete();
        }

        //loading = (ProgressBar) view.findViewById(R.id.loading);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_view);
        swipeRefreshLayout.setColorSchemeResources(R.color.generalDarkRed, R.color.generalDarkGreen, R.color.generalDarkBlue);

        profileImageView = (CircleImageView) rootView.findViewById(R.id.profile_image);

        profileImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (!Functions.getString(context, "profile_image", "").isEmpty()) {
                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                    View dilogView = activity.getLayoutInflater().inflate(R.layout.dialog_profile_image, (ViewGroup) rootView.findViewById(R.id.dialog_profile_image));
                    localBuilder.setView(dilogView);
                    ImageView profileImage = (ImageView) dilogView.findViewById(R.id.profile_image);
                    Picasso.with(context).load(Functions.getString(context, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + Functions.getString(context, "profile_image", "") + "?" + Functions.getString(context, "timestamp", "")).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().into(profileImage);
                    //Functions.loadCachedImage(activity, profileImage, Functions.getString(context, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + Functions.getString(context, "profile_image", ""), Information.PROFILE_IMAGES_PATH_PREFIX + Functions.getString(context, "profile_image", ""));
                    ImageButton rightButton = (ImageButton) dilogView.findViewById(R.id.btn_right);
                    rightButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                            ArrayList<String> localArrayList = new ArrayList<String>();
                            localArrayList.add("دریافت");


                            String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                            localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                    switch (paramAnonymous2Int) {
                                        default:
                                            return;
                                        case 0:

                                            DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{Information.DEFAULT_PROFILE_IMAGES_URL + Functions.getString(context, "profile_image", "") + "?" + Functions.getString(context, "timestamp", ""), Information.IMAGES_PATH, Functions.getString(context, "username", "") + ".jpg"});
                                            downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                                @Override
                                                public void onProgressUpdate(int progress) {

                                                }

                                                @Override
                                                public void onPostExecute(boolean result) {
                                                    if (result) {
                                                        Toast.makeText(activity, "تصویر کاربر درون حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                            break;
                                    }

                                }
                            });
                            AlertDialog dialog = localBuilder.create();
                            dialog.show();

                        }
                    });

                    AlertDialog dialog = localBuilder.create();
                    dialog.show();
                }

                return true;
            }
        });


        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                ArrayList<String> localArrayList = new ArrayList<String>();
                localArrayList.add("تصویر جدید بگیرید");
                localArrayList.add("انتخاب از گالری");
                if (!Functions.getString(context, "profile_image", "").isEmpty()) {
                    localArrayList.add("حذف");
                }
                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                        switch (paramAnonymous2Int) {
                            default:
                                return;
                            case 0:
                                try {
                                    File tempFile = new File(temp_path);
                                    if (tempFile.exists()) {
                                        tempFile.delete();
                                    }
                                    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                                    localIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(temp_path)));
                                    startActivityForResult(localIntent, PICK_FROM_CAMERA);
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 1:
                                try {
                                    Intent localIntent1 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(localIntent1, PICK_FROM_FILE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 2:
                                profileImageView.setImageResource(R.drawable.ic_profile);
                                profile_image = "";
                                updateProfile();
                                break;
                        }

                    }
                });
                localBuilder.create().show();
            }
        });

        PostsNumberText = (TextView) rootView.findViewById(R.id.posts_number);
        followersNumberText = (TextView) rootView.findViewById(R.id.followers_number);
        followedsNumberText = (TextView) rootView.findViewById(R.id.followeds_number);

        LinearLayout postsLayout = (LinearLayout) rootView.findViewById(R.id.posts);
        postsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PostsActivity.class);
                intent.putExtra("author", Functions.getString(context, "id", ""));
                startActivity(intent);
            }
        });

        LinearLayout followersLayout = (LinearLayout) rootView.findViewById(R.id.followers);
        followersLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, UsersActivity.class);
                intent.putExtra("method", "follows");
                intent.putExtra("argument", Functions.getString(context, "id", ""));
                startActivity(intent);
            }
        });

        LinearLayout followedsLayout = (LinearLayout) rootView.findViewById(R.id.followeds);
        followedsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, UsersActivity.class);
                intent.putExtra("method", "followed");
                intent.putExtra("argument", Functions.getString(context, "id", ""));
                startActivity(intent);
            }
        });

        Button changeProfileButton = (Button) rootView.findViewById(R.id.change_profile);
        changeProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, EditProfileActivity.class));
            }
        });

        nameText = (TextView) rootView.findViewById(R.id.name);
        trustedImage = (ImageView) rootView.findViewById(R.id.trusted);
        aboutText = (TextView) rootView.findViewById(R.id.about);
        balanceText = (TextView) rootView.findViewById(R.id.balance);

        chargeAccountButton = (ActionProcessButton) rootView.findViewById(R.id.charge_account);
        chargeAccountButton.setMode(ActionProcessButton.Mode.ENDLESS);

        chargeAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                ArrayList<String> localArrayList = new ArrayList<String>();
                localArrayList.add("از طریق کارت بانکی");
                localArrayList.add("از طریق ایران اپس");


                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                        switch (paramAnonymous2Int) {
                            default:
                                return;
                            case 0:
                                AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                                View view = activity.getLayoutInflater().inflate(R.layout.dialog_charge_account, (ViewGroup) rootView.findViewById(R.id.dialog_charge_account));
                                final EditText amountText = (EditText) view.findViewById(R.id.amount);
                                localBuilder.setView(view).setPositiveButton("پرداخت", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (!amountText.getText().toString().isEmpty()) {
                                            if (Integer.parseInt(amountText.getText().toString()) < 1000) {
                                                Toast.makeText(activity, "حداقل مقدار باید 1.000 ریال باشد.", Toast.LENGTH_LONG).show();
                                                chargeAccountButton.setProgress(0);
                                                return;
                                            }
                                        } else {
                                            Toast.makeText(activity, "مقداری را وارد کنید.", Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                        chargeAccountButton.setProgress(50);
                                        Functions.browseWebViaIntent(context, getString(R.string.browse_web), LinkManager.chargeAccount(Functions.getString(context, "username", ""), Functions.getString(context, "password", ""), amountText.getText().toString()));
                                        isCharging = true;
                                    }
                                }).setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                    }
                                });


                                AlertDialog dialog = localBuilder.create();
                                dialog.show();
                                break;
                            case 1:


                                /*if (MainActivity.mHelper != null) {
                                    MainActivity.mHelper.dispose();
                                    MainActivity.mHelper = null;
                                }


                                if (MainActivity.mHelper == null) {
                                    MainActivity.mHelper = new IabHelper(activity, Config.IRANAPPS_IN_APP_API_KEY);
                                    MainActivity.setupHelper();
                                    if (MainActivity.mHelper.mAsyncInProgress) {
                                        MainActivity.mHelper.flagEndAsync();
                                    }
                                }*/

                                if (MainActivity.mHelper.mAsyncInProgress) {
                                    MainActivity.mHelper.flagEndAsync();
                                }

                                if (MainActivity.mHelper.mAsyncInProgress) {
                                    Toast.makeText(activity, "پروسه پرداخت هنوز فعال است. لطفاً اندکی صبر کنید و دوباره امتحان کنید.", Toast.LENGTH_LONG).show();
                                    MainActivity.mHelper.flagEndAsync();
                                    return;
                                }

                                AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(activity);
                                ArrayList<String> localArrayList = new ArrayList<String>();


                                String[] arrayOfString2 = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                                localBuilder2.setItems(MainActivity.skuTitles_iranapps, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {

                                        try {
                                            (MainActivity.mHelper).launchPurchaseFlow(activity, MainActivity.skuIDs_iranapps[paramAnonymous2Int], IN_APP_BILLING, MainActivity.mPurchaseFinishedListener, "");
                                        } catch (Exception e) {
                                            Toast.makeText(activity, "خطایی رخ داده. مطمئن شوید که اپلیکیشن ایران اپس روی دستگاه شما نصب شده است و وارد حساب بازار خود شده اید.", Toast.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }

                                    }

                                });
                                AlertDialog dialog2 = localBuilder2.create();
                                dialog2.show();
                                break;
                        }

                    }
                });
                AlertDialog dialog = localBuilder.create();
                dialog.show();


            }
        });

        Button checkOutButton = (Button) rootView.findViewById(R.id.checkout);
        checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((!Functions.isOnline(context))) {
                    Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                    return;
                }

                final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                new AsyncTask<String, String, String>() {

                    @Override
                    protected void onPreExecute() {
                        sDialog.show();
                        sDialog.setCancelable(false);

                        super.onPreExecute();
                    }

                    @Override
                    protected String doInBackground(String... arg0) {
                        return Fetcher.FetchURL(APIManager.requestCheckOut(Functions.getString(context, "id", "")));
                    }

                    protected void onPostExecute(final String result) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (result.contains("not-enough-balance")) {
                                    sDialog.setTitleText("").setContentText("موجودی شما باید حداقل " + Functions.normalPrice(result.replaceFirst("not-enough-balance;", ""), ".") + " ریال باشد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                } else if (result.equals("mobile_number")) {
                                    sDialog.setTitleText("").setContentText("لطفاً شماره موبایل خود را در بخش ویرایش نمایه وارد کنید.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                } else if (result.equals("shaba")) {
                                    sDialog.setTitleText("").setContentText("لطفاً شماره شبا حساب بانکی خود را در بخش ویرایش نمایه وارد کنید.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                } else if (result.equals("success")) {
                                    sDialog.setTitleText("").setContentText("درخواست شما ثبت شد و از طریق پیامک و ایمیل شما را از روند بررسی درخواست شما را مطلع خواهیم کرد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                    swipeRefreshLayout.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            refreshAccount();
                                        }
                                    });

                                } else {
                                    sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                }
                            }
                        });
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshAccount();
            }
        });


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshAccount();
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    private void updateProfile() {
        if ((!Functions.isOnline(context))) {
            Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            startActivity(new Intent("android.settings.WIFI_SETTINGS"));
            return;
        }

        final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                sDialog.show();
                sDialog.setCancelable(false);

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0) {
                return Fetcher.FetchURL(APIManager.updateProfile(Functions.getString(context, "username", ""), Functions.getString(context, "first_name", ""), Functions.getString(context, "last_name", ""), Functions.getString(context, "mobile_number", ""), Functions.getString(context, "shaba", ""), Functions.getString(context, "email", ""), Functions.getString(context, "email", ""), profile_image, Functions.getString(context, "about", ""), Functions.getString(context, "extras", "")));
            }

            protected void onPostExecute(final String result) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {

                        if (result.contains("success")) {
                            sDialog.setTitleText("").setContentText("اطلاعات ثبت شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            Functions.putString(context, "profile_image", profile_image);
                        } else {
                            sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        }
                        uploaded = false;
                    }
                });

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        sDialog.show();

    }

    private void doCrop() {
        try {

            File file = new File(temp_path + "_cropped");
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            Uri fileUri = Functions.getUriFromFile(activity, temp_path);
            Uri croppedFileUri = Functions.getUriFromFile(activity, temp_path + "_cropped");


            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image/*");
            cropIntent.putExtra("output", croppedFileUri);
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            //cropIntent.putExtra("outputX", 1024);
            //cropIntent.putExtra("outputY", 1024);


            List<ResolveInfo> resInfoList = activity.getPackageManager().queryIntentActivities(cropIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                if (packageName != null && fileUri != null) {
                    activity.grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                if (packageName != null && croppedFileUri != null) {
                    activity.grantUriPermission(packageName, croppedFileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }





            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, croppedFileUri);
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // retrieve data on return

            //cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, CROP);
            // start the activity - we handle returning in onActivityResult


        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException | IOException e) {
            Log.d("error", "no activity found for cropping image.");
            e.printStackTrace();
            uploadProfileImage();
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        //if (resultCode != -1) return;
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                //doCrop();

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {

                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        //mImageUri = data.getData();
                        //Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {

                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                mImageUri = null;
                                Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_PROFILE_IMAGE_SIZE", Config.DEFAULT_MAX_PROFILE_IMAGE_SIZE), 75, 5);
                                Functions.rotateImageIfNeed(temp_path);
                                return null;

                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                mImageUri = Uri.fromFile(new File(temp_path));
                                                doCrop();
                                            }
                                        });
                                    }
                                }, 0);

                                super.onPostExecute(aVoid);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
            case PICK_FROM_FILE:

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        mImageUri = data.getData();
                        Functions.copyFile(new File(Functions.getPathFromURI(context, mImageUri)), new File(temp_path), true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                mImageUri = null;
                                Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(context, "MAX_PROFILE_IMAGE_SIZE", Config.DEFAULT_MAX_PROFILE_IMAGE_SIZE), 75, 5);
                                Functions.rotateImageIfNeed(temp_path);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mImageUri = Uri.fromFile(new File(temp_path));
                                                doCrop();
                                            }
                                        });
                                    }
                                }, 0);
                                super.onPostExecute(aVoid);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                break;
            case CROP:
                Functions.copyFile(new File(temp_path + "_cropped"), new File(temp_path), true);
                uploadProfileImage();
                break;

            case IN_APP_BILLING:
                MainActivity.mHelper.handleActivityResult(requestCode, resultCode, data);

                break;
        }
    }


    private void uploadProfileImage() {
        String fileAddress = Information.PROFILE_IMAGES_PATH_PREFIX;
        final String fileName = Functions.getString(context, "id", "") + ".jpg";
        final String file = fileAddress + fileName;


        final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("در حال آپلود فایل...").setContentText("").showCancelButton(false);

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                sDialog.show();
                sDialog.setCancelable(false);

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0) {
                Functions.reduceImageFileSize(temp_path, 1024 * Functions.getInt(activity, "MAX_PROFILE_IMAGE_SIZE", Config.DEFAULT_MAX_PROFILE_IMAGE_SIZE), 75, 5);
                String result = Functions.uploadFile(temp_path, APIManager.uploadFile("profile_images", true, false));
                if (result.equals("fail")) {
                    uploaded = false;
                } else {
                    uploaded = true;
                }
                return result;
            }

            protected void onPostExecute(final String result) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        if (uploaded) {
                            Functions.copyFile(temp_path, file, true);
                            profile_image = fileName;
                            Functions.loadImageFromFile(activity, profileImageView, temp_path);
                            updateProfile();
                            sDialog.hide();
                        } else {
                            sDialog.setTitleText("").setContentText("خطا در آپلود فایل!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        }
                        uploaded = false;
                    }
                });

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        sDialog.show();
    }

    public static void refreshAccount() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setRefreshing(true);
                chargeAccountButton.setProgress(0);

                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                if (Functions.isOnline(context)) {
                    CustomFunctions.Login(activity);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (nameText != null) {
                    swipeRefreshLayout.setRefreshing(false);

                    if (!Functions.getString(context, "profile_image", "").isEmpty()) {
                        Picasso.with(context).load(Functions.getString(context, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + Functions.getString(context, "profile_image", "") + "_small.jpg" + "?" + Functions.getString(context, "timestamp", "")).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().into(profileImageView);
                        //Functions.loadCachedImage(activity, profileImageView, Functions.getString(context, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + Functions.getString(context, "profile_image", ""), Information.PROFILE_IMAGES_PATH_PREFIX + Functions.getString(context, "profile_image", ""));
                    } else {
                        profileImageView.setImageResource(R.drawable.ic_profile);
                    }

                    String name = Functions.getString(context, "first_name", "") + " " + Functions.getString(context, "last_name", "");
                    if (name.equals(" ")) {
                        name = Functions.getString(context, "username", "");
                    } else {
                        name += " (@" + Functions.getString(context, "username", "") + ")";
                    }

                    nameText.setText(name);
                    if (Functions.getString(context, "extras", "").contains("trusted;")) {
                        trustedImage.setVisibility(View.VISIBLE);
                    } else {
                        trustedImage.setVisibility(View.GONE);
                    }

                    aboutText.setText(CustomFunctions.getHtml(context, Functions.getString(context, "about", "")));
                    aboutText.setMovementMethod(LinkMovementMethod.getInstance());
                    Functions.stripUnderlines(aboutText);

                    balanceText.setText("موجودی: " + Functions.normalPrice(Functions.getString(context, "balance", "0"), ".") + " ریال");

                    PostsNumberText.setText(Functions.getString(context, "posts", "0"));

                    followersNumberText.setText(Functions.getString(context, "followers", "0"));

                    followedsNumberText.setText(Functions.getString(context, "followeds", "0"));
                }

                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    @Override
    public void onResume() {
        chargeAccountButton.setProgress(0);
        if (toRefresh) {
            toRefresh = false;

            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    refreshAccount();
                }
            });
        }

        super.onResume();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        context = activity.getApplicationContext();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        } else {
            this.context = context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}