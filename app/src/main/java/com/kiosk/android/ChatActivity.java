package com.kiosk.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.ChatsMessagesAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.EndPoints;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.MyApplication;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.DatabaseHelper;
import com.kiosk.android.util.NotificationUtils;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.EmojiTextView;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardCloseListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class ChatActivity extends AppCompatActivity {
    private static final String TAG = ChatActivity.class.getSimpleName();
    public static Activity activity;
    public static DatabaseHelper dbHelper;
    public static String chat_id;

    public static EmojiEditText messageEditText;

    public static ChatsMessagesAdapter adapter;
    public static ListView list;

    public static final int PICK_FROM_CAMERA = 1;
    public static final int PICK_FROM_FILE = 2;
    public static final int CROP = 3;

    public static String temp_path;
    public static Uri mImageUri;
    public static boolean uploaded = false;
    public static boolean isProcessing = false;

    public static String name = "", image = "";


    public static List<ChatsMessage> chats_messages = new ArrayList<ChatsMessage>();


    public static int start = 0, amount = Config.DEFAULT_CHATS_MESSAGES_BUFFER_ITEMS;
    /*private boolean isProcessing = false;*/

    private long last_timestamp = 0;


    private EmojiPopup emojiPopup;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static ImageButton expandButton;

    public static int firstVisibleItem = 0, visibleItemCount = 0, totalItemCount = 0;

    // TOOLS LAYOUT
    public static RelativeLayout toolsLayout;
    public static EmojiTextView toolsText;
    public static ImageButton toolsIconButton, toolsCloseButton;
    public static String extras = "";

    private String intent_action = "", intent_data = "", intent_extras = "";
    private Uri intent_uri = null;


    // REFRESH CHAT
    private Runnable r;
    private boolean refreshChat = true;
    Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // TOOLS
        extras = "";

        toolsLayout = (RelativeLayout) findViewById(R.id.tools_layout);
        toolsText = (EmojiTextView) findViewById(R.id.tools_text);
        toolsIconButton = (ImageButton) findViewById(R.id.tools_icon);
        toolsCloseButton = (ImageButton) findViewById(R.id.tools_close);
        toolsCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolsText.setText("");
                toolsLayout.setVisibility(View.GONE);
                messageEditText.setEnabled(true);
                extras = "";
                ChatsActivity.intent_action = "";
                ChatsActivity.intent_data = "";
                ChatsActivity.intent_extras = "";
                ChatsActivity.intent_uri = null;
            }
        });


        activity = this;

        new Information(getApplicationContext());
        temp_path = Information.TEMP_PATH_PREFIX + "chat.jpg";
        File tempFile = new File(temp_path);
        if (tempFile.exists()) {
            tempFile.delete();
        }
        expandButton = (ImageButton) findViewById(R.id.expand);
        expandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideExpandButton();
                expandButton.setBackgroundResource(R.drawable.selector_round_corner);
                if (adapter.getCount() > 0) {
                    list.setSelection(chats_messages.size() - 1);
                    //list.smoothScrollToPosition(chats_messages.size() - 1);
                }
            }
        });


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.INTENT_PUSH_NOTIFICATION)) {
                    // new push message is received
                    handlePushNotification(intent);
                }
            }
        };


        amount = Functions.getInt(getApplicationContext(), "CHATS_MESSAGES_BUFFER_ITEMS", Config.DEFAULT_CHATS_MESSAGES_BUFFER_ITEMS);
        //amount = 20;

        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;

        chat_id = getIntent().getStringExtra("id");
        dbHelper = new DatabaseHelper(getApplicationContext());

        list = (ListView) findViewById(R.id.list);
        messageEditText = (EmojiEditText) findViewById(R.id.message_text);
        ImageButton addMessageButton = (ImageButton) findViewById(R.id.add_message);
        final ImageButton emojiKeyboardButton = (ImageButton) findViewById(R.id.emojikeyboard);

        adapter = new ChatsMessagesAdapter(ChatActivity.this, null);


        // INTENT
        if (getIntent().getStringExtra("action") != null) {
            intent_action = getIntent().getStringExtra("action");
        }

        if (getIntent().getStringExtra("data") != null) {
            intent_data = getIntent().getStringExtra("data");
        }

        if (getIntent().getStringExtra("extras") != null) {
            intent_extras = getIntent().getStringExtra("extras");
        }

        if (getIntent().getExtras().get("uri") != null) {
            intent_uri = (Uri) getIntent().getExtras().get("uri");
        }

        if (intent_action.equals("forward")) {
            ChatActivity.toolsLayout.setVisibility(View.VISIBLE);
            ChatActivity.toolsIconButton.setImageResource(R.mipmap.ic_forward);
            ChatActivity.messageEditText.setText("");
            ChatActivity.messageEditText.setEnabled(false);
            ChatActivity.toolsText.setText(intent_data);
            ChatActivity.extras = intent_extras;
        } else if (intent_action.equals("image")) {
            mImageUri = intent_uri;
            ChatsActivity.intent_action = "";
            ChatsActivity.intent_data = "";
            ChatsActivity.intent_extras = "";
            ChatsActivity.intent_uri = null;
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {

                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    Functions.copyFile(new File(Functions.getPathFromURI(getApplicationContext(), mImageUri)), new File(temp_path), true);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... voids) {
                            mImageUri = null;
                            Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(getApplicationContext(), "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                            Functions.rotateImageIfNeed(temp_path);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            mImageUri = Uri.fromFile(new File(temp_path));
                                            doCrop();
                                        }
                                    });
                                }
                            }, 0);
                            super.onPostExecute(aVoid);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    super.onPostExecute(aVoid);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        ///////////////////////

        new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... voids) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chats_messages.clear();
                        readAllMessages();
                        refreshLoad();
                    }
                });

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        ImageButton imageButton = (ImageButton) findViewById(R.id.image);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                ArrayList<String> localArrayList = new ArrayList<String>();
                localArrayList.add("تصویر جدید بگیرید");
                localArrayList.add("انتخاب از گالری");
                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                        switch (paramAnonymous2Int) {
                            default:
                                return;
                            case 0:
                                try {
                                    File tempFile = new File(temp_path);
                                    if (tempFile.exists()) {
                                        tempFile.delete();
                                    }
                                    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                                    localIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(temp_path)));
                                    startActivityForResult(localIntent, PICK_FROM_CAMERA);
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 1:
                                try {
                                    File tempFile = new File(temp_path);
                                    if (tempFile.exists()) {
                                        tempFile.delete();
                                    }
                                    Intent localIntent1 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(localIntent1, PICK_FROM_FILE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                        }

                    }
                });
                localBuilder.create().show();
            }
        });


        emojiPopup = EmojiPopup.Builder.fromRootView(messageEditText).setOnSoftKeyboardCloseListener(new OnSoftKeyboardCloseListener() {
            @Override
            public void onKeyboardClose() {
                if (emojiPopup.isShowing()) {
                    emojiPopup.dismiss();
                }
            }
        }).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
            @Override
            public void onEmojiPopupDismiss() {
                emojiKeyboardButton.setImageResource(R.drawable.selector_emoji);
            }
        }).setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
            @Override
            public void onEmojiPopupShown() {
                emojiKeyboardButton.setImageResource(R.mipmap.ic_emoji_selected);
            }
        }).build(messageEditText);

        emojiKeyboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emojiPopup.toggle();
            }
        });

        messageEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (messageEditText.getText().toString().isEmpty() && extras.isEmpty()) {
                        return true;
                    }

                    if (emojiPopup.isShowing()) {
                        emojiPopup.dismiss();
                    }

                    sendMessage("message", messageEditText.getText().toString(), extras);
                    return true;
                }
                return false;
            }
        });

        addMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (messageEditText.getText().toString().isEmpty() && extras.isEmpty()) {
                    return;
                }

                if (emojiPopup.isShowing()) {
                    emojiPopup.dismiss();
                }

                sendMessage("message", messageEditText.getText().toString(), extras);
            }
        });


        final Chat chat = dbHelper.getChat(Integer.parseInt(chat_id));

        // ACTIONBAR

        CircleImageView imageView = (CircleImageView) findViewById(R.id.title_image);
        if (chat.type.equals("single")) {
            Picasso.with(activity).load(R.drawable.ic_profile).noFade().into(imageView);

            if (!chat.image.isEmpty()) {

                if (last_timestamp == 0) {
                    String id = "";
                    if (Functions.getString(activity, "id", "").equals(chat.creator)) {
                        id = chat.participants.split("!new_user!")[0];
                    } else {
                        id = chat.creator;
                    }

                    if (Functions.getLong(activity, id + "_timestamp", 0) != 0) {
                        last_timestamp = Functions.getLong(activity, id + "_timestamp", 0);
                    } else {
                        if (Functions.isOnline(activity)) {
                            last_timestamp = System.currentTimeMillis();
                        } else {
                            last_timestamp = Long.parseLong(chat.timestamp);
                        }
                        Functions.putLong(activity, id + "_timestamp", last_timestamp);
                    }
                }


                Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + chat.image + "?" + last_timestamp).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(100, 100).into(imageView);
            }
        } else if (chat.type.equals("group")) {
            Picasso.with(activity).load(R.drawable.ic_no_image).noFade().into(imageView);

            if (!chat.image.isEmpty()) {
                Picasso.with(activity).load(Functions.getString(activity, "GROUP_IMAGES_URL", Information.DEFAULT_GROUP_IMAGES_URL) + chat.image + "?" + chat.timestamp).error(R.drawable.ic_no_image).placeholder(R.drawable.ic_no_image).noFade().resize(100, 100).into(imageView);
            }
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatActivity.this, ChatSettingsActivity.class);
                intent.putExtra("id", chat_id);
                startActivity(intent);
            }
        });

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText(EmojiMapUtil.replaceCheatSheetEmojis(chat.name));
        titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatActivity.this, ChatSettingsActivity.class);
                intent.putExtra("id", chat_id);
                startActivity(intent);
            }
        });

        final TextViewPlus subtitleText = (TextViewPlus) findViewById(R.id.subtitle);
        if (chat.type.equals("group")) {
            refreshChat = false;
            List<String> participants = new LinkedList<String>(Arrays.asList(chat.participants.split("!new_user!")));
            participants.removeAll(new LinkedList<String>(Arrays.asList("")));

            subtitleText.setText(String.valueOf(participants.size() + 1 + " عضو"));
        } else {
            refreshChat = true;
            mHandler = new Handler();
            r = new Runnable() {

                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (Functions.getDifferenceOfTimes(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(chat.last_timestamp))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(chat.last_timestamp), "hh:mm:ss")) < 60) {
                                subtitleText.setText("آنلاین");
                            } else {
                                subtitleText.setText(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(chat.last_timestamp))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(chat.last_timestamp), "hh:mm:ss")) + " پیش");
                            }

                            if (refreshChat) {
                                mHandler.postDelayed(r, 60 * 1000);
                            }
                        }
                    });
                }
            };
            if (refreshChat) {
                mHandler.post(r);
            }

        }

        subtitleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatActivity.this, ChatSettingsActivity.class);
                intent.putExtra("id", chat_id);
                startActivity(intent);
            }
        });

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setImageResource(R.drawable.ic_action_back);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    private void doCrop() {
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(Uri.fromFile(new File(temp_path)), "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            //cropIntent.putExtra("aspectX", 1);
            //cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            //cropIntent.putExtra("outputX", 1024);
            //cropIntent.putExtra("outputY", 1024);

            File f = new File(temp_path + "_cropped");
            if (f.exists()) {
                f.delete();
                f.createNewFile();
            }


            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            cropIntent.putExtra("output", Uri.fromFile(f));
            // retrieve data on return

            //cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, CROP);
            // start the activity - we handle returning in onActivityResult


        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException | IOException e) {
            Log.d("error", "no activity found for cropping image.");
            e.printStackTrace();
            mImageUri = Uri.fromFile(new File(temp_path));
        }
    }


    @Override
    public void onActivityResult(int requestCode, int result, final Intent data) {
        if (result != -1) return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:


                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {

                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        //mImageUri = data.getData();
                        //Functions.copyFile(new File(Functions.getPathFromURI(getApplicationContext(), mImageUri)), new File(temp_path), true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {

                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                mImageUri = null;
                                Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(getApplicationContext(), "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                                Functions.rotateImageIfNeed(temp_path);
                                return null;

                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                mImageUri = Uri.fromFile(new File(temp_path));
                                                doCrop();
                                            }
                                        });
                                    }
                                }, 0);

                                super.onPostExecute(aVoid);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
            case PICK_FROM_FILE:

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {

                        super.onPreExecute();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        mImageUri = data.getData();
                        Functions.copyFile(new File(Functions.getPathFromURI(getApplicationContext(), mImageUri)), new File(temp_path), true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                mImageUri = null;
                                Functions.reduceImageFileSize(temp_path, 2 * 1024 * Functions.getInt(getApplicationContext(), "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                                Functions.rotateImageIfNeed(temp_path);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                mImageUri = Uri.fromFile(new File(temp_path));
                                                doCrop();
                                            }
                                        });
                                    }
                                }, 0);
                                super.onPostExecute(aVoid);
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;

            case CROP:
                Functions.copyFile(new File(temp_path + "_cropped"), new File(temp_path), true);
                uploadImage();
                break;
        }
    }

    public static void showExpandButton() {
        if (expandButton.getVisibility() == View.VISIBLE) {
            return;
        }
        expandButton.setVisibility(View.VISIBLE);
        expandButton.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.zoom_in));

    }

    public static void hideExpandButton() {
        if (expandButton.getVisibility() == View.GONE) {
            return;
        }
        expandButton.setBackgroundResource(R.drawable.selector_round_corner);
        expandButton.setVisibility(View.GONE);
        expandButton.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.zoom_out));


    }

    private void uploadImage() {
        if (isProcessing) {
            return;
        }

        if (mImageUri == null) {
            return;
        }

        isProcessing = true;


        final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("در حال آپلود فایل...").setContentText("").showCancelButton(false);


        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();
            }

            protected String doInBackground(String... paramArrayOfString) {
                Functions.reduceImageFileSize(temp_path, 1024 * Functions.getInt(getApplicationContext(), "MAX_UPLOAD_FILE_SIZE", Config.DEFAULT_MAX_UPLOAD_FILE_SIZE), 75, 5);
                String result = Functions.uploadFile(temp_path, APIManager.uploadFile("user_uploads/" + Functions.getString(getApplicationContext(), "id", ""), false, true));

                if (result.equals("fail")) {
                    uploaded = false;
                } else {
                    uploaded = true;
                }
                return result;
            }

            protected void onPostExecute(String result) {
                if (uploaded) {
                    String uploaded_image_address = Functions.getString(getApplicationContext(), "USERS_UPLOADS_URL", Information.DEFAULT_USERS_UPLOADS_URL) + Functions.getString(getApplicationContext(), "id", "") + "/" + result;
                    sendMessage("mime-image", uploaded_image_address, extras);
                    sDialog.hide();
                    isProcessing = false;
                } else {
                    sDialog.setTitleText("").setContentText("خطا در آپلود فایل!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    isProcessing = false;
                }

                mImageUri = null;
                uploaded = false;
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        sDialog.show();
    }

    /*
    void loadMessages() {
        if (!Functions.isOnline(getApplicationContext())) {
            if (isProcessing) {
                return;
            }

            isProcessing = true;

            ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> filter = new HashMap<String, String>();
            filter.put("name", "chat");
            filter.put("method", "equal");
            filter.put("value", id);
            filters.add(filter);

            List<ChatsMessage> chats_messages = dbHelper.searchChatsMessages(filters, "id", start, amount, true, true);


            if (chats_messages.size() == 1) {
                list.setOnScrollListener(null);
            }


            for (ChatsMessage chats_message : chats_messages) {

                chats_messages_ids.add(String.valueOf(chats_message.id));
                chats_messages_senders.add(chats_message.sender);
                chats_messages_sender_names.add(chats_message.sender_name);
                chats_messages_sender_profile_images.add(chats_message.sender_profile_image);
                chats_messages_actions.add(chats_message.action);
                chats_messages_messages.add(chats_message.message);
                chats_messages_timestamps.add(chats_message.timestamp);
                if (!chats_message.action.equals("message")) {
                    chats_messages_types.add("action");
                } else {
                    if (chats_message.sender.equals(Functions.getString(getApplicationContext(), "id", ""))) {
                        chats_messages_types.add("me");
                    } else {
                        chats_messages_types.add("participant");
                    }
                }

            }


            start += amount;

            adapter.ids = chats_messages_ids;
            adapter.senders = chats_messages_senders;
            adapter.sender_names = chats_messages_sender_names;
            adapter.sender_profile_images = chats_messages_sender_names;
            adapter.actions = chats_messages_actions;
            adapter.messages = chats_messages_messages;
            adapter.timestamps = chats_messages_timestamps;
            adapter.types = chats_messages_types;


            if (start == amount) { // IF IS FIRST LOAD

                list.setAdapter(adapter);
                list.setSmoothScrollbarEnabled(true);

                list.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                        if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 >= start)) {
                            loadMessages();
                        }
                    }

                    public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                    }
                });
            }


            adapter.notifyDataSetChanged();

            isProcessing = false;

        } else {
            if (isProcessing) {
                return;
            }

            isProcessing = true;

            new AsyncTask<String, String, String>() {

                @Override
                protected void onPreExecute() {

                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(String... strings) {
                    //return Fetcher.FetchURL(APIManager.getPostsComments(id, String.valueOf(start), String.valueOf(amount)));
                    return "";
                }

                @Override
                protected void onPostExecute(String result) {


                    if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "post_comments").equals("-1")) {
                        int offset = chats_messages_ids.size();

                        chats_messages_ids.addAll(JSONParser.getListFromString(result, "chats_messages", "id"));
                        chats_messages_senders.addAll(JSONParser.getListFromString(result, "chats_messages", "sender"));
                        chats_messages_sender_names.addAll(JSONParser.getListFromString(result, "chats_messages", "sender_name"));
                        chats_messages_sender_profile_images.addAll(JSONParser.getListFromString(result, "chats_messages", "sender_profile_image"));
                        chats_messages_actions.addAll(JSONParser.getListFromString(result, "chats_messages", "action"));
                        chats_messages_messages.addAll(JSONParser.getListFromString(result, "chats_messages", "message"));
                        chats_messages_timestamps.addAll(JSONParser.getListFromString(result, "chats_messages", "timestamp"));


                        for (int i = offset; i < chats_messages_ids.size(); i++) {
                            if (!chats_messages_actions.get(i).equals("message")) {
                                chats_messages_types.add("action");
                            } else {
                                if (chats_messages_senders.get(i).equals(Functions.getString(getApplicationContext(), "id", ""))) {
                                    chats_messages_types.add("me");
                                } else {
                                    chats_messages_types.add("participant");
                                }
                            }
                        }


                        start += amount;

                        adapter.ids = chats_messages_ids;
                        adapter.sender_profile_images = chats_messages_sender_names;
                        adapter.senders = chats_messages_senders;
                        adapter.sender_names = chats_messages_sender_names;
                        adapter.actions = chats_messages_actions;
                        adapter.messages = chats_messages_messages;
                        adapter.timestamps = chats_messages_timestamps;
                        adapter.types = chats_messages_types;

                        if (start == amount) { // IF IS FIRST LOAD

                            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                    if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 >= start))
                                        loadMessages();
                                }

                                public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                }
                            });
                        }

                        adapter.notifyDataSetChanged();

                    } else {
                        list.setOnScrollListener(null);
                    }

                    isProcessing = false;

                    super.onPostExecute(result);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }
    }*/


    /**
     * Handling new push message, will add the message to
     * recycler view and scroll it to bottom
     */
    private void handlePushNotification(Intent intent) {
        int type = intent.getIntExtra("type", -1);

        // if the push is of chat room message
        // simply update the UI unread messages count
        if (type == Config.FLAG_CHAT) {
            ChatsMessage chats_message = (ChatsMessage) intent.getSerializableExtra("chats_message");
            if (chats_message != null) {
                if (!chats_message.sender.equals(Functions.getString(getApplicationContext(), "id", "")) && chats_message.chat.equals(chat_id)) {

                    chats_message.status = "read";
                    dbHelper.updateChatsMessage(chats_message);

                    chats_messages.add(chats_message);

                    sortItems();
                    adapter.chats_messages = chats_messages;

                    dbHelper.getChat(Integer.parseInt(chat_id)).last_timestamp = chats_message.timestamp;

                    adapter.notifyDataSetChanged();

                    if (chats_messages.size() - firstVisibleItem - visibleItemCount < 3) {
                        if (adapter.getCount() > 0) {
                            //Log.d("tab", "count: " + chats_messages.size());
                            //list.setSelection(adapter.getCount() - 1);
                            list.setSelection(chats_messages.size() - 1);
                        }
                    } else {
                        expandButton.setBackgroundResource(R.drawable.selector_round_corner_red);
                        showExpandButton();
                    }


                    /*
                    if (adapter.getCount() > 0) {
                        Log.d("tab", "count: " + chats_messages.size());
                        //list.setSelection(adapter.getCount() - 1);
                        list.smoothScrollToPosition(chats_messages.size() - 1);
                    }*/
                }


            }
        }
    }


    /**
     * Posting a new message in chat room
     * will make an http call to our server. Our server again sends the message
     * to all the devices as push notification
     */
    private void sendMessage(final String action, final String message, final String extras) {

        Log.d("tag", "extras: " + extras);

        this.extras = "";

        /*
        new AsyncTask<String, String, String>() {
            String message = "";

            @Override
            protected void onPreExecute() {

                //InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                message = messageEditText.getText().toString();

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... voids) {
                //return Fetcher.FetchURL(APIManager.setPostMyComment(id, Functions.getString(getApplicationContext(), "id", ""), message));
                return "";
            }

            @Override
            protected void onPostExecute(String result) {


                chats_messages_ids.add("0");


                String name = Functions.getString(getApplicationContext(), "first_name", "") + " " + Functions.getString(getApplicationContext(), "last_name", "");
                if (name.equals(" ")) {
                    name = Functions.getString(getApplicationContext(), "username", "");
                }

                chats_messages_sender_names.add(name);
                chats_messages_sender_profile_images.add(Functions.getString(getApplicationContext(), "profile_image", ""));
                chats_messages_messages.add(messageEditText.getText().toString());
                chats_messages_timestamps.add(String.valueOf(System.currentTimeMillis()));
                if (new Random().nextInt(2) < 1) {
                    chats_messages_senders.add("0");
                    chats_messages_types.add("me");
                } else {
                    chats_messages_senders.add(Functions.getString(getApplicationContext(), "id", ""));
                    chats_messages_types.add("participant");
                }
                chats_messages_actions.add("message");


                adapter.ids = chats_messages_ids;
                adapter.senders = chats_messages_senders;
                adapter.sender_names = chats_messages_sender_names;
                adapter.sender_profile_images = chats_messages_sender_profile_images;
                adapter.actions = chats_messages_actions;
                adapter.messages = chats_messages_messages;
                adapter.types = chats_messages_types;


                start++;
                adapter.notifyDataSetChanged();

                messageEditText.setText("");

                if (adapter.getCount() > 1) {
                    list.smoothScrollToPosition(adapter.getCount() - 1);
                }

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        */


        this.messageEditText.setText("");
        toolsText.setText("");
        toolsLayout.setVisibility(View.GONE);
        messageEditText.setEnabled(true);


        String endPoint = EndPoints.CHAT_SEND;

        String name = Functions.getString(getApplicationContext(), "first_name", "") + " " + Functions.getString(getApplicationContext(), "last_name", "");
        if (name.equals(" ")) {
            name = Functions.getString(getApplicationContext(), "username", "");
        }

        final ChatsMessage chats_message = new ChatsMessage();
        chats_message.id = 999999999;
        chats_message.chat = chat_id;
        chats_message.sender = Functions.getString(getApplicationContext(), "id", "");
        chats_message.sender_name = name;
        chats_message.sender_profile_image = Functions.getString(getApplicationContext(), "profile_image", "");
        chats_message.action = action;
        if (extras.contains("forward")) {
            chats_message.message = intent_data;
        } else {
            chats_message.message = message;
        }
        chats_message.timestamp = String.valueOf(System.currentTimeMillis());
        chats_message.type = "me";
        chats_message.status = "read";
        chats_message.send_status = "waiting";

        dbHelper.createChatsMessage(chats_message);
        chats_messages.add(chats_message);

        sortItems();
        adapter.chats_messages = chats_messages;

        adapter.notifyDataSetChanged();


        if (adapter.getCount() > 0) {
            Log.d("tab", "count: " + chats_messages.size());
            //list.setSelection(adapter.getCount() - 1);
            list.setSelection(chats_messages.size() - 1);
        }


        StringRequest strReq = new StringRequest(Request.Method.POST, endPoint, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {


                    dbHelper.deleteChatsMessage(chats_message.id);
                    chats_messages.remove(chats_messages.indexOf(chats_message));


                    String id = JSONParser.getFromString(response, "id");
                    String extras = JSONParser.getFromString(response, "extras");

                    String name = Functions.getString(getApplicationContext(), "first_name", "") + " " + Functions.getString(getApplicationContext(), "last_name", "");
                    if (name.equals(" ")) {
                        name = Functions.getString(getApplicationContext(), "username", "");
                    }

                    ChatsMessage chats_message = new ChatsMessage();
                    chats_message.id = Integer.parseInt(id);
                    chats_message.chat = chat_id;
                    chats_message.sender = Functions.getString(getApplicationContext(), "id", "");
                    chats_message.sender_name = name;
                    chats_message.sender_profile_image = Functions.getString(getApplicationContext(), "profile_image", "");
                    chats_message.action = action;
                    chats_message.message = message;
                    chats_message.extras = extras;
                    chats_message.timestamp = String.valueOf(System.currentTimeMillis());
                    chats_message.type = "me";
                    chats_message.status = "read";
                    chats_message.send_status = "sent";


                    dbHelper.createChatsMessage(chats_message);

                    chats_messages.add(chats_message);

                    sortItems();
                    adapter.chats_messages = chats_messages;

                    adapter.notifyDataSetChanged();

                    /*
                    if (adapter.getCount() > 0) {
                        Log.d("tab", "count: " + chats_messages.size());
                        //list.setSelection(adapter.getCount() - 1);
                        list.setSelection(chats_messages.size() - 1);
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "خطا در ارسال پیام.", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                messageEditText.setText(message);
                chats_messages.remove(chats_message);
                dbHelper.deleteChatsMessage(chats_message.id);
                adapter.chats_messages = chats_messages;
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("chat", chat_id);
                params.put("sender", Functions.getString(getApplicationContext(), "id", ""));
                params.put("action", action);
                params.put("message", EmojiMapUtil.replaceUnicodeEmojis(chats_message.message));
                params.put("extras", extras);

                return params;
            }

            ;
        };


        // disabling retry policy so that it won't make
        // multiple http calls
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strReq.setRetryPolicy(policy);

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);

    }


    /**
     * Fetching all the messages of a single chat room
     */
    public static void fetchChatThread() {

        if (Functions.isOnline(activity.getApplicationContext())) {
            if (isProcessing) {
                return;
            }

            isProcessing = true;

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    String endPoint = EndPoints.CHAT_GET;

                    StringRequest strReq = new StringRequest(Request.Method.POST, endPoint, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {

                                if (!response.isEmpty() && !response.equals("-1") && !JSONParser.getFromString(response, "chats_messages").equals("-1")) {

                                    if (start == 0) {
                                        chats_messages.clear();
                                    }

                                    int offset = chats_messages.size();

                                    List<String> chats_messages_ids = JSONParser.getListFromString(response, "chats_messages", "id");
                                    //chats_messages_chats.addAll(JSONParser.getListFromString(response, "chats_messages", "chat");
                                    List<String> chats_messages_senders = JSONParser.getListFromString(response, "chats_messages", "sender");
                                    List<String> chats_messages_sender_names = JSONParser.getListFromString(response, "chats_messages", "sender_name");
                                    List<String> chats_messages_sender_profile_images = JSONParser.getListFromString(response, "chats_messages", "sender_profile_image");
                                    List<String> chats_messages_actions = JSONParser.getListFromString(response, "chats_messages", "action");
                                    List<String> chats_messages_messages = JSONParser.getListFromString(response, "chats_messages", "message");
                                    List<String> chats_messages_extras = JSONParser.getListFromString(response, "chats_messages", "extras");
                                    List<String> chats_messages_timestamps = JSONParser.getListFromString(response, "chats_messages", "timestamp");


                                    for (int i = offset; i < chats_messages_ids.size(); i++) {


                                        if (dbHelper.checkChatsMessage(Integer.parseInt(chats_messages_ids.get(i)))) {
                                            if (dbHelper.getChatsMessage(Integer.parseInt(chats_messages_ids.get(i))).type.equals("deleted")) {
                                                continue;
                                            }
                                        }


                                        ChatsMessage chats_message = new ChatsMessage();
                                        chats_message.id = Integer.parseInt(chats_messages_ids.get(i));
                                        chats_message.chat = chat_id;
                                        chats_message.sender = chats_messages_senders.get(i);
                                        chats_message.sender_name = chats_messages_sender_names.get(i);
                                        chats_message.sender_profile_image = chats_messages_sender_profile_images.get(i);
                                        chats_message.action = chats_messages_actions.get(i);
                                        chats_message.message = chats_messages_messages.get(i);
                                        chats_message.extras = chats_messages_extras.get(i);
                                        chats_message.timestamp = chats_messages_timestamps.get(i);
                                        chats_message.status = "read";


                                        chats_messages.add(chats_message);


                                        chats_message.type = "action";
                                        if (chats_messages_actions.get(i).equals("message") || chats_messages_actions.get(i).equals("mime-image")) {
                                            if (chats_messages_senders.get(i).equals(Functions.getString(activity, "id", ""))) {
                                                chats_message.type = "me";
                                            } else {
                                                chats_message.type = "participant";
                                            }
                                        }

                                        if (dbHelper.checkChatsMessage(chats_message.id)) {
                                            dbHelper.updateChatsMessage(chats_message);
                                        } else {
                                            dbHelper.createChatsMessage(chats_message);
                                        }


                                    }

                                    start += amount;
                                    sortItems();


                                    list.setOnScrollListener(new AbsListView.OnScrollListener() {

                                        @Override
                                        public void onScroll(AbsListView listView, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
                                            if (firstVisibleItem == firstVisibleItem2 && visibleItemCount == visibleItemCount2 && totalItemCount == totalItemCount2) {
                                                return;
                                            }

                                            firstVisibleItem = firstVisibleItem2;
                                            visibleItemCount = visibleItemCount2;
                                            totalItemCount = totalItemCount2;

                                        }

                                        @Override
                                        public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                            Log.d("tag", "if " + paramInt + " ==  0");
                                            if (paramInt == 0) fetchChatThread();

                                            Log.d("tag", "if " + firstVisibleItem + "+ " + visibleItemCount + " < " + totalItemCount);

                                            if ((firstVisibleItem + visibleItemCount < totalItemCount)) {
                                                showExpandButton();
                                            } else {
                                                hideExpandButton();
                                            }


                                        }
                                    });

                                    if (start == amount) { // IF IS FIRST LOAD

                                        list.setAdapter(adapter);
                                        //list.setSmoothScrollbarEnabled(true);

                                        adapter.notifyDataSetChanged();

                                        if (adapter.getCount() > 0) {
                                            Log.d("tab", "count: " + chats_messages.size());
                                            //list.setSelection(adapter.getCount() - 1);
                                            list.setSelection(chats_messages.size() - 1);
                                            //list.smoothScrollToPosition(chats_messages.size() - 1);
                                        }

                                    }

                                    adapter.chats_messages = chats_messages;
                                    adapter.notifyDataSetChanged();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            isProcessing = false;


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            //NetworkResponse networkResponse = error.networkResponse;
                            //Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                            //Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("chat", chat_id);
                            params.put("start", String.valueOf(start));
                            params.put("amount", String.valueOf(amount));


                            return params;
                        }
                    };

                    //Adding request to request queue
                    MyApplication.getInstance().addToRequestQueue(strReq);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        } else {

            loadOffline();

        }
    }


    public void readAllMessages() {
        ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> filter = new HashMap<String, String>();
        filter.put("name", "chat");
        filter.put("method", "equal");
        filter.put("value", chat_id);
        filters.add(filter);
        filter = new HashMap<String, String>();
        filter.put("name", "status");
        filter.put("method", "equal");
        filter.put("value", "not-read");
        filters.add(filter);


        List<ChatsMessage> new_messages = dbHelper.searchChatsMessages(filters, "id", 0, 0, true, true);
        for (int i = 0; i < new_messages.size(); i++) {
            String former_status = new_messages.get(i).status;

            ChatsMessage chats_message = new_messages.get(i);
            chats_message.status = "read";


            dbHelper.updateChatsMessage(chats_message);
        }
    }


    public static void refreshLoad() {
        temp_path = Information.TEMP_PATH_PREFIX + "chat.jpg";
        mImageUri = null;
        uploaded = false;
        isProcessing = false;
        start = 0;
        adapter.chats_messages = chats_messages;

        list.setAdapter(adapter);
        list.setSmoothScrollbarEnabled(true);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadOffline();
                    }
                });

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (Functions.isOnline(activity)) {
                                start = 0;
                                fetchChatThread();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 500);


                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static void loadOffline() {

        ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> filter = new HashMap<String, String>();
        filter.put("name", "chat");
        filter.put("method", "equal");
        filter.put("value", chat_id);
        filters.add(filter);
        filter = new HashMap<String, String>();
        filter.put("name", "type");
        filter.put("method", "not-equal");
        filter.put("value", "deleted");
        filters.add(filter);
        filter = new HashMap<String, String>();
        filter.put("name", "message");
        filter.put("method", "not-equal");
        filter.put("value", "null");
        filters.add(filter);

        if (start == 0) {
            chats_messages.clear();
        }

        List<ChatsMessage> new_messages = dbHelper.searchChatsMessages(filters, "id", start, amount, true, true);
        for (int i = 0; i < new_messages.size(); i++) {
            ChatsMessage chats_message = new_messages.get(i);

            chats_message.type = "action";
            if (chats_message.action.equals("message") || chats_message.action.equals("mime-image")) {
                if (chats_message.sender.equals(Functions.getString(activity, "id", ""))) {
                    chats_message.type = "me";
                } else {
                    chats_message.type = "participant";
                }
            }

        }


        chats_messages.addAll(new_messages);

        start += amount;

        sortItems();


        if (new_messages.size() != 0) {

            if (start == amount) { // IF IS FIRST LOAD

                list.setAdapter(adapter);
                //list.setSmoothScrollbarEnabled(true);

                adapter.notifyDataSetChanged();
                if (adapter.getCount() > 0) {
                    Log.d("tab", "count: " + chats_messages.size());
                    //list.setSelection(adapter.getCount() - 1);
                    list.setSelection(chats_messages.size() - 1);
                    //list.smoothScrollToPosition(chats_messages.size() - 1);

                }

            }
        }


        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView listView, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
                if (firstVisibleItem == firstVisibleItem2 && visibleItemCount == visibleItemCount2 && totalItemCount == totalItemCount2) {
                    return;
                }

                firstVisibleItem = firstVisibleItem2;
                visibleItemCount = visibleItemCount2;
                totalItemCount = totalItemCount2;

            }

            @Override
            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                Log.d("tag", "if " + paramInt + " ==  0");
                if (paramInt == 0) fetchChatThread();

                Log.d("tag", "if " + firstVisibleItem + "+ " + visibleItemCount + " < " + totalItemCount);

                if ((firstVisibleItem + visibleItemCount < totalItemCount)) {
                    showExpandButton();
                } else {
                    hideExpandButton();
                }
            }
        });

        sortItems();
        adapter.chats_messages = chats_messages;
        adapter.notifyDataSetChanged();

    }

    public static void sortItems() {
        try {

            Collections.sort(chats_messages, new Comparator<ChatsMessage>() {
                @Override
                public int compare(ChatsMessage chats_message1, ChatsMessage chats_message2) {

                    return Functions.normalLength(String.valueOf(chats_message1.id), 15, "0", true).
                            compareTo(Functions.normalLength(String.valueOf(chats_message2.id), 15, "0", true));
                }
            });

            for (int i = 0; i < chats_messages.size(); i++) {
                //chats_messages.get(i).status = "read";
                //dbHelper.updateChatsMessage(chats_messages.get(i));
                if (i > 0) {
                    if (!chats_messages.get(i - 1).type.equals("date")) {
                        if (!Functions.getTimeFormat(Long.parseLong(chats_messages.get(i - 1).timestamp), "dd/MM/yyyy").equals(Functions.getTimeFormat(Long.parseLong(chats_messages.get(i).timestamp), "dd/MM/yyyy"))) {
                            ChatsMessage chats_message = new ChatsMessage();
                            chats_message.id = chats_messages.get(i).id;
                            chats_message.timestamp = chats_messages.get(i).timestamp;
                            chats_message.type = "date";
                            chats_messages.add(i, chats_message);
                        }
                    } else {
                        if (chats_messages.get(i).type.equals("date")) {
                            chats_messages.remove(i);
                            i--;
                        }
                    }

                } else {
                    if (!chats_messages.get(0).type.equals("date")) {
                        ChatsMessage chats_message = new ChatsMessage();
                        chats_message.id = chats_messages.get(0).id;
                        chats_message.timestamp = chats_messages.get(0).timestamp;
                        chats_message.type = "date";
                        chats_messages.add(0, chats_message);
                    }
                }
            }
            if (chats_messages.size() > 0) {
                if (chats_messages.get(chats_messages.size() - 1).type.equals("date")) {
                    chats_messages.remove(chats_messages.size() - 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void scrollToId(String id, int offset) {
        if (offset >= chats_messages.size()) {
            return;
        }
        int i = offset;
        for (; i < chats_messages.size(); i++) {
            if (chats_messages.get(i).id == Integer.parseInt(id)) {
                list.setSelection(i);
                list.smoothScrollToPosition(i);
                //adapter.selected = i;
                //adapter.notifyDataSetChanged();
                return;
            }
        }

        list.setSelection(0);
        scrollToId(id, i);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (emojiPopup.isShowing()) {
            emojiPopup.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        String chatBGType = Functions.getString(getApplicationContext(), "chat_bg_type_" + chat_id, "resource");
        String chatBG = Functions.getString(getApplicationContext(), "chat_bg_" + chat_id, String.valueOf(R.drawable.bg5));

        ImageView bgImage = (ImageView) findViewById(R.id.bg);
        if (!chatBG.isEmpty()) {
            if (chatBGType.equals("file")) {
                Picasso.with(getApplicationContext()).load(new File(chatBG)).error(R.drawable.bg5).noFade().fit().centerCrop().into(bgImage);
            } else {
                bgImage.setImageResource(Integer.parseInt(chatBG));
            }
        }

        if (refreshChat && mHandler != null && r != null) {
            mHandler.post(r);
        }


        // registering the receiver for new notification
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.INTENT_PUSH_NOTIFICATION));

        NotificationUtils.clearNotifications();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        activity = null;
        if (refreshChat && mHandler != null && r != null) {
            mHandler.removeCallbacks(r);
        }
        refreshChat = false;
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        if (refreshChat && mHandler != null && r != null) {
            mHandler.removeCallbacks(r);
        }
        super.onStop();
    }
}