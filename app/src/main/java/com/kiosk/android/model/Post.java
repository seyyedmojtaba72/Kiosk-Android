package com.kiosk.android.model;

import java.io.Serializable;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class Post implements Serializable {
    public int id = 0;
    public String author = "", author_name = "", author_profile_image = "", author_timestamp="0", name = "", description = "", image = "", price = "", download_count = "", timestamp = "", likes = "", user_like = "", comments = "", downloadable = "0", reading = "0", saved = "0";
}
