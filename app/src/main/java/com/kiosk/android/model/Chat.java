package com.kiosk.android.model;

import java.io.Serializable;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class Chat implements Serializable {
    public int id = 0;
    public String creator = "", type = "single", participants = "", name = "", image = "", about = "", timestamp = "", last_timestamp = "";
}
