package com.kiosk.android.model;

import java.io.Serializable;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class ChatsMessage implements Serializable {
    public int id = 0;
    public String chat = "", sender = "", sender_name = "", sender_profile_image = "", action = "message", message = "", extras = "", timestamp = "", status = "not-read", type = "participant";

    // NOT NECESSARY FOR DB
    public String send_status = "sent";
}
