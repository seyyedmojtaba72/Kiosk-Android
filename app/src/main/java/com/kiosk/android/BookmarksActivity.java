package com.kiosk.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.PostsAdapter;
import com.kiosk.android.info.Config;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class BookmarksActivity extends AppCompatActivity {
    private DatabaseHelper dbHelper;
    private ListView list;
    private PostsAdapter adapter;

    private List<String> posts_ids = new ArrayList<String>();
    private List<String> posts_authors = new ArrayList<String>();
    private List<String> posts_author_names = new ArrayList<String>();
    private List<String> posts_author_profile_images = new ArrayList<String>();
    private List<String> posts_author_timestamps = new ArrayList<String>();
    private List<String> posts_names = new ArrayList<String>();
    private List<String> posts_descriptions = new ArrayList<String>();
    private List<String> posts_images = new ArrayList<String>();
    private List<String> posts_prices = new ArrayList<String>();
    private List<String> posts_download_counts = new ArrayList<String>();
    private List<String> posts_times = new ArrayList<String>();
    private List<String> posts_timestamps = new ArrayList<String>();
    private List<String> posts_likes = new ArrayList<String>();
    private List<String> posts_user_likes = new ArrayList<String>();
    private List<String> posts_comments = new ArrayList<String>();
    private List<String> posts_downloadables = new ArrayList<String>();

    private int start = 0, amount = Config.DEFAULT_POSTS_BUFFER_ITEMS;

    private boolean isProcessing = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);

        amount = Functions.getInt(getApplicationContext(), "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

        dbHelper = new DatabaseHelper(getApplicationContext());

        list = (ListView) findViewById(R.id.list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent localIntent = new Intent(BookmarksActivity.this, PostActivity.class);
                localIntent.putExtra("id", posts_ids.get(i));
                startActivity(localIntent);
            }

        });

        adapter = new PostsAdapter(BookmarksActivity.this, null);

        list.setSmoothScrollbarEnabled(true);

        loadData();

        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("لیست خواندن");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    void loadData() {
        if (isProcessing) {
            return;
        }

        isProcessing = true;


        ArrayList<HashMap<String, String>> filters = new ArrayList<>();
        HashMap<String, String> filter = new HashMap<>();
        filter.put("name", "reading");
        filter.put("method", "equal");
        filter.put("value", "1");
        filters.add(filter);
        filter = new HashMap<>();
        filter.put("name", "saved");
        filter.put("method", "equal");
        filter.put("value", "1");
        filters.add(filter);
        List<Post> posts = dbHelper.searchPosts(filters, "id", start, amount, true, true);

        if (posts.size() == 0) {
            list.setOnScrollListener(null);
        }

        for (Post post : posts) {
            posts_ids.add(String.valueOf(post.id));
            posts_authors.add(post.author);
            posts_author_names.add(post.author_name);
            posts_author_profile_images.add(post.author_profile_image);
            posts_author_timestamps.add(post.author_timestamp);
            posts_names.add(post.name);
            posts_descriptions.add(post.description);
            posts_images.add(post.image);
            posts_prices.add(post.price);
            posts_download_counts.add(post.download_count);
            posts_timestamps.add(post.timestamp);

            // GET PAST TIME FROM TIMESTAMP
            posts_times.add(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(post.timestamp))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(post.timestamp), "hh:mm:ss")));

            posts_likes.add(post.likes);
            posts_user_likes.add(post.user_like);
            posts_comments.add(post.comments);
            posts_downloadables.add(post.downloadable);
        }

        start += amount;


        adapter.ids = posts_ids;
        adapter.author_profile_images = posts_author_profile_images;
        adapter.author_timestamps = posts_author_timestamps;
        adapter.authors = posts_authors;
        adapter.author_names = posts_author_names;
        adapter.times = posts_times;
        adapter.images = posts_images;
        adapter.prices = posts_prices;
        adapter.user_likes = posts_user_likes;
        adapter.likes = posts_likes;
        adapter.names = posts_names;
        adapter.descriptions = posts_descriptions;
        adapter.comments = posts_comments;


        if (start == amount) { // IF IS FIRST LOAD

            list.setAdapter(adapter);

            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                    if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == posts_ids.size())) loadData();
                }

                public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                }
            });
        }

        isProcessing = false;

        adapter.notifyDataSetChanged();

    }


}