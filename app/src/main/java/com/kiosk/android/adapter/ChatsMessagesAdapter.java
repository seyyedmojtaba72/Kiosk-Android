package com.kiosk.android.adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.ChatActivity;
import com.kiosk.android.ChatsActivity;
import com.kiosk.android.MainActivity;
import com.kiosk.android.ProfileActivity;
import com.kiosk.android.R;
import com.kiosk.android.info.Information;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class ChatsMessagesAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;
    private DatabaseHelper dbHelper;
    private long last_timestamp = 0;

    public int selected = -1;

    public List<ChatsMessage> chats_messages;

    public ChatsMessagesAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        this.animation = animation;
        this.dbHelper = new DatabaseHelper(activity);
    }

    class ViewHolder {
        LinearLayout layout;
    }

    class DateViewHolder extends ViewHolder {
        TextView dateText;
    }

    class ActionViewHolder extends ViewHolder {
        TextView actionText;
    }

    class MeViewHolder extends ViewHolder {
        TextView messageText, timeText, replyAuthor, replyMessage;
        ImageView imageView, replyImage;
        LinearLayout replyLayout;
        ImageButton moreButton;
        ImageView sendStatus;
    }

    class ParticipateViewHolder extends ViewHolder {
        CircleImageView profileImageView;
        ImageView imageView, replyImage;
        TextView authorText, messageText, timeText, replyAuthor, replyMessage;
        LinearLayout replyLayout;
        ImageButton moreButton;
        ImageView sendStatus;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {

            if (Functions.getLong(activity, chats_messages.get(position).sender + "_timestamp", 0) != 0) {
                last_timestamp = Functions.getLong(activity, chats_messages.get(position).sender + "_timestamp", 0);
            } else {
                if (Functions.isOnline(activity)) {
                    last_timestamp = System.currentTimeMillis();
                } else {
                    last_timestamp = Long.parseLong(dbHelper.getChat(Integer.parseInt(chats_messages.get(position).chat)).timestamp);
                }
                Functions.putLong(activity, chats_messages.get(position).sender + "_timestamp", last_timestamp);
            }


            ViewHolder holder = new ViewHolder();
            if (chats_messages.get(position).type.equals("date")) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_chat_message_date_item, parent, false);
                holder = new DateViewHolder();
                ((DateViewHolder) holder).dateText = (TextView) convertView.findViewById(R.id.date);
            } else if (chats_messages.get(position).type.equals("action")) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_chat_message_action_item, parent, false);
                holder = new ActionViewHolder();
                ((ActionViewHolder) holder).actionText = (TextView) convertView.findViewById(R.id.action);
            } else if (chats_messages.get(position).type.equals("me")) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_chat_message_me_item, parent, false);
                holder = new MeViewHolder();
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                ((MeViewHolder) holder).imageView = (ImageView) convertView.findViewById(R.id.image);
                ((MeViewHolder) holder).messageText = (TextView) convertView.findViewById(R.id.message);
                ((MeViewHolder) holder).timeText = (TextView) convertView.findViewById(R.id.time);
                ((MeViewHolder) holder).moreButton = (ImageButton) convertView.findViewById(R.id.more);
                ((MeViewHolder) holder).sendStatus = (ImageView) convertView.findViewById(R.id.send_status);
                ((MeViewHolder) holder).replyAuthor = (TextView) convertView.findViewById(R.id.reply_author);
                ((MeViewHolder) holder).replyImage = (ImageView) convertView.findViewById(R.id.reply_image);
                ((MeViewHolder) holder).replyLayout = (LinearLayout) convertView.findViewById(R.id.reply_layout);
                ((MeViewHolder) holder).replyMessage = (TextView) convertView.findViewById(R.id.reply_message);
            } else if (chats_messages.get(position).type.equals("participant")) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_chat_message_participant_item, parent, false);
                holder = new ParticipateViewHolder();
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                ((ParticipateViewHolder) holder).profileImageView = (CircleImageView) convertView.findViewById(R.id.profile_image);
                ((ParticipateViewHolder) holder).authorText = (TextView) convertView.findViewById(R.id.author);
                ((ParticipateViewHolder) holder).imageView = (ImageView) convertView.findViewById(R.id.image);
                ((ParticipateViewHolder) holder).messageText = (TextView) convertView.findViewById(R.id.message);
                ((ParticipateViewHolder) holder).moreButton = (ImageButton) convertView.findViewById(R.id.more);
                ((ParticipateViewHolder) holder).sendStatus = (ImageView) convertView.findViewById(R.id.send_status);
                ((ParticipateViewHolder) holder).timeText = (TextView) convertView.findViewById(R.id.time);
                ((ParticipateViewHolder) holder).replyAuthor = (TextView) convertView.findViewById(R.id.reply_author);
                ((ParticipateViewHolder) holder).replyImage = (ImageView) convertView.findViewById(R.id.reply_image);
                ((ParticipateViewHolder) holder).replyLayout = (LinearLayout) convertView.findViewById(R.id.reply_layout);
                ((ParticipateViewHolder) holder).replyMessage = (TextView) convertView.findViewById(R.id.reply_message);
            }
            ((ViewHolder) holder).layout = (LinearLayout) convertView.findViewById(R.id.layout);


            // PRELOAD IMAGE FOR MORE SPEED

            //Log.d("tag", "caching images of items from " + Math.min(ids.size() - 1, position + 3) + " to " + Math.max(0, position - 3));
            for (int i = Math.min(chats_messages.size() - 1, position + 3); i >= Math.max(0, position - 3); i--) {
                //Log.d("tag", "caching item " + i + " 's image: " + images.get(i));
                if (chats_messages.get(i).action.equals("mime-image")) {

                    if (!chats_messages.get(i).message.isEmpty()) {
                        Picasso.with(activity).load(chats_messages.get(i).message).fetch();
                    }

                }
            }


            if (chats_messages.get(position).type.equals("date")) {
                if (((DateViewHolder) holder).dateText != null) {

                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -1);

                    if (Functions.getTimeFormat(System.currentTimeMillis(), "dd/MM/yyyy").equals(Functions.getTimeFormat(Long.parseLong(chats_messages.get(position).timestamp), "dd/MM/yyyy"))) {
                        ((DateViewHolder) holder).dateText.setText("امروز");
                    } else if (Functions.getTimeFormat(cal.getTimeInMillis(), "dd/MM/yyyy").equals(Functions.getTimeFormat(Long.parseLong(chats_messages.get(position).timestamp), "dd/MM/yyyy"))) {
                        ((DateViewHolder) holder).dateText.setText("دیروز");
                    } else {
                        ((DateViewHolder) holder).dateText.setText(Functions.getDayOfWeekPersianTitle(Functions.getTimeFormat(Long.parseLong(chats_messages.get(position).timestamp), "yyyy/MM/dd")) + " " + PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(chats_messages.get(position).timestamp))));
                    }
                }
            } else if (chats_messages.get(position).type.equals("action")) {
                if (((ActionViewHolder) holder).actionText != null) {
                    String actionText = chats_messages.get(position).sender_name + " ";
                    if (chats_messages.get(position).action.equals("create_group")) {
                        actionText += "گروه را ایجاد کرد.";
                    } else if (chats_messages.get(position).action.equals("change_name")) {
                        actionText += "نام گروه را به «" + chats_messages.get(position).message + "» تغییر داد.";
                    } else if (chats_messages.get(position).action.equals("change_image")) {
                        actionText += "تصویر گروه را عوض کرد.";
                    } else if (chats_messages.get(position).action.equals("join_group")) {
                        actionText += "به گروه ملحق شد.";
                    } else if (chats_messages.get(position).action.equals("leave_group")) {
                        actionText += "گروه را ترک کرد.";
                    } else if (chats_messages.get(position).action.equals("add")) {
                        actionText += chats_messages.get(position).message + " را به گروه اضافه کرد.";
                    } else if (chats_messages.get(position).action.equals("remove")) {
                        actionText += chats_messages.get(position).message + " را از گروه حذف کرد.";
                    }

                    ((ActionViewHolder) holder).actionText.setText(EmojiMapUtil.replaceCheatSheetEmojis(actionText) + "    " + Functions.getTimeFormat(Long.parseLong(chats_messages.get(position).timestamp), "HH:mm"));
                }

            } else {
                if (chats_messages.get(position).type.equals("me")) {

                    //Log.d("chats_messages", "extras : " + chats_messages.get(position).extras);
                    if (chats_messages.get(position).extras.contains("reply")) {
                        ((MeViewHolder) holder).replyLayout.setVisibility(View.VISIBLE);

                        final String reply_id = JSONParser.getFromString(chats_messages.get(position).extras, "reply_id");
                        final String reply_sender = JSONParser.getFromString(chats_messages.get(position).extras, "reply_sender");
                        final String reply_action = JSONParser.getFromString(chats_messages.get(position).extras, "reply_action");
                        final String reply_message = JSONParser.getFromString(chats_messages.get(position).extras, "reply_message");

                        ((MeViewHolder) holder).replyAuthor.setText(EmojiMapUtil.replaceCheatSheetEmojis(reply_sender));
                        if (reply_action.equals("message")) {
                            ((MeViewHolder) holder).replyMessage.setVisibility(View.VISIBLE);
                            ((MeViewHolder) holder).replyImage.setVisibility(View.GONE);
                            ((MeViewHolder) holder).replyMessage.setText(EmojiMapUtil.replaceCheatSheetEmojis(reply_message));
                        } else if (reply_action.equals("mime-image")) {
                            ((MeViewHolder) holder).replyMessage.setVisibility(View.GONE);
                            ((MeViewHolder) holder).replyImage.setVisibility(View.VISIBLE);
                            Picasso.with(activity).load(reply_message + "_small.jpg").error(R.drawable.ic_no_image).placeholder(R.drawable.ic_loading).noFade().into(((MeViewHolder) holder).replyImage);
                        }

                        ((MeViewHolder) holder).replyLayout.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                ChatActivity.scrollToId(reply_id, 0);
                            }
                        });

                    } else if (chats_messages.get(position).extras.contains("forward")) {
                        ((MeViewHolder) holder).replyLayout.setVisibility(View.VISIBLE);
                        ((MeViewHolder) holder).messageText.setVisibility(View.GONE);

                        final String forward_id = JSONParser.getFromString(chats_messages.get(position).extras, "forward_id");
                        final String forward_sender = JSONParser.getFromString(chats_messages.get(position).extras, "forward_sender");
                        final String forward_action = JSONParser.getFromString(chats_messages.get(position).extras, "forward_action");
                        final String forward_message = JSONParser.getFromString(chats_messages.get(position).extras, "forward_message");

                        ((MeViewHolder) holder).replyAuthor.setText(EmojiMapUtil.replaceCheatSheetEmojis(forward_sender));
                        if (forward_action.equals("message")) {
                            ((MeViewHolder) holder).replyMessage.setVisibility(View.VISIBLE);
                            ((MeViewHolder) holder).replyImage.setVisibility(View.GONE);
                            ((MeViewHolder) holder).replyMessage.setText(EmojiMapUtil.replaceCheatSheetEmojis(forward_message));
                        } else if (forward_action.equals("mime-image")) {
                            ((MeViewHolder) holder).replyMessage.setVisibility(View.GONE);
                            ((MeViewHolder) holder).replyImage.setVisibility(View.VISIBLE);
                            Picasso.with(activity).load(forward_message + "_small.jpg").error(R.drawable.ic_no_image).placeholder(R.drawable.ic_loading).noFade().into(((MeViewHolder) holder).replyImage);
                            ((MeViewHolder) holder).replyImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (!new File(Information.IMAGES_PATH + "chat_" + forward_id + ".jpg").exists()) {
                                        DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                        downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{forward_message, Information.IMAGES_PATH, "chat_" + forward_id + ".jpg"});
                                        downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                            @Override
                                            public void onProgressUpdate(int progress) {

                                            }

                                            @Override
                                            public void onPostExecute(boolean result) {
                                                if (result) {
                                                    Intent intent = new Intent();
                                                    intent.setAction(Intent.ACTION_VIEW);
                                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                                    intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + forward_id + ".jpg")), "image/*");
                                                    activity.startActivity(intent);
                                                }
                                            }
                                        });
                                    } else {
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + forward_id + ".jpg")), "image/*");
                                        activity.startActivity(intent);
                                    }
                                }
                            });
                        }

                        ((MeViewHolder) holder).replyLayout.setBackgroundColor(Color.TRANSPARENT);

                    } else {
                        ((MeViewHolder) holder).replyLayout.setVisibility(View.GONE);
                    }

                    if (chats_messages.get(position).action.equals("mime-image")) {

                        if (((MeViewHolder) holder).imageView != null) {

                            ((MeViewHolder) holder).imageView.setVisibility(View.VISIBLE);
                            ((MeViewHolder) holder).messageText.setVisibility(View.GONE);
                            if (!chats_messages.get(position).message.isEmpty()) {
                                Picasso.with(activity).load(chats_messages.get(position).message).error(R.drawable.ic_no_image).placeholder(R.drawable.ic_loading).noFade().into(((MeViewHolder) holder).imageView);

                                ((MeViewHolder) holder).imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (!new File(Information.IMAGES_PATH + "chat_" + chats_messages.get(position).id + ".jpg").exists()) {
                                            DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{chats_messages.get(position).message, Information.IMAGES_PATH, "chat_" + chats_messages.get(position).id + ".jpg"});
                                            downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                                @Override
                                                public void onProgressUpdate(int progress) {

                                                }

                                                @Override
                                                public void onPostExecute(boolean result) {
                                                    if (result) {
                                                        Intent intent = new Intent();
                                                        intent.setAction(Intent.ACTION_VIEW);
                                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                                        intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + chats_messages.get(position).id + ".jpg")), "image/*");
                                                        activity.startActivity(intent);
                                                    }
                                                }
                                            });
                                        } else {
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                                            intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + chats_messages.get(position).id + ".jpg")), "image/*");
                                            activity.startActivity(intent);
                                        }
                                    }
                                });


                                //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
                            } else {
                                Picasso.with(activity).load(R.drawable.ic_no_image).into(((MeViewHolder) holder).imageView);
                            }
                        }

                    } else {
                        ((MeViewHolder) holder).imageView.setVisibility(View.GONE);
                        //((MeViewHolder) holder).messageText.setVisibility(View.VISIBLE);
                        if (((MeViewHolder) holder).messageText != null) {
                            ((MeViewHolder) holder).messageText.setText(CustomFunctions.getHtml(activity, EmojiMapUtil.replaceCheatSheetEmojis(chats_messages.get(position).message)));
                            ((MeViewHolder) holder).messageText.setMovementMethod(LinkMovementMethod.getInstance());
                            Functions.stripUnderlines(((MeViewHolder) holder).messageText);
                        }
                    }


                    if (((MeViewHolder) holder).timeText != null) {
                        ((MeViewHolder) holder).timeText.setText(Functions.getTimeFormat(Long.parseLong(chats_messages.get(position).timestamp), "HH:mm"));
                    }

                    if (chats_messages.get(position).send_status.equals("waiting")) {
                        ((MeViewHolder) holder).sendStatus.setImageResource(R.mipmap.ic_waiting);
                    } else {
                        if (Long.parseLong(dbHelper.getChat(Integer.parseInt(chats_messages.get(position).chat)).last_timestamp) >= Long.parseLong(chats_messages.get(position).timestamp)) {
                            ((MeViewHolder) holder).sendStatus.setImageResource(R.mipmap.ic_read);
                        } else {
                            ((MeViewHolder) holder).sendStatus.setImageResource(R.mipmap.ic_sent);
                        }

                    }

                    ((MeViewHolder) holder).moreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            displayMoreDialog(position);
                        }
                    });


                } else if (chats_messages.get(position).type.equals("participant")) {


                    if (chats_messages.get(position).type.equals("participant") && (!chats_messages.get(position - 1).type.equals("author"))) {
                        if (!chats_messages.get(position - 1).sender.equals(chats_messages.get(position).sender)) {

                            if (((ParticipateViewHolder) holder).authorText != null) {
                                ((ParticipateViewHolder) holder).authorText.setVisibility(View.VISIBLE);
                                ((ParticipateViewHolder) holder).authorText.setText(EmojiMapUtil.replaceCheatSheetEmojis(chats_messages.get(position).sender_name));
                            }
                            if (((ParticipateViewHolder) holder).profileImageView != null) {
                                ((ParticipateViewHolder) holder).profileImageView.setVisibility(View.VISIBLE);
                                Picasso.with(activity).load(R.drawable.ic_profile).noFade().into(((ParticipateViewHolder) holder).profileImageView);
                                if (!chats_messages.get(position).sender_profile_image.isEmpty()) {
                                    Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + chats_messages.get(position).sender_profile_image + "_small.jpg" + "?" + last_timestamp).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(50, 50).into(((ParticipateViewHolder) holder).profileImageView);
                                    //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
                                }

                                ((ParticipateViewHolder) holder).profileImageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (chats_messages.get(position).sender.equals(Functions.getString(activity, "id", ""))) {
                                            Intent intent = new Intent(activity, MainActivity.class);
                                            intent.putExtra("action", "open_profile");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            activity.startActivity(intent);
                                        } else {
                                            Intent localIntent = new Intent(activity, ProfileActivity.class);
                                            localIntent.putExtra("id", chats_messages.get(position).sender);
                                            activity.startActivity(localIntent);
                                        }
                                    }
                                });
                            }

                        } else {
                            ((ParticipateViewHolder) holder).authorText.setVisibility(View.GONE);
                            ((ParticipateViewHolder) holder).profileImageView.setVisibility(View.GONE);
                        }
                    }


                    if (chats_messages.get(position).extras.contains("reply")) {
                        ((ParticipateViewHolder) holder).replyLayout.setVisibility(View.VISIBLE);

                        final String reply_id = JSONParser.getFromString(chats_messages.get(position).extras, "reply_id");
                        final String reply_sender = JSONParser.getFromString(chats_messages.get(position).extras, "reply_sender");
                        final String reply_action = JSONParser.getFromString(chats_messages.get(position).extras, "reply_action");
                        final String reply_message = JSONParser.getFromString(chats_messages.get(position).extras, "reply_message");

                        ((ParticipateViewHolder) holder).replyAuthor.setText(EmojiMapUtil.replaceCheatSheetEmojis(reply_sender));
                        if (reply_action.equals("message")) {
                            ((ParticipateViewHolder) holder).replyMessage.setVisibility(View.VISIBLE);
                            ((ParticipateViewHolder) holder).replyImage.setVisibility(View.GONE);
                            ((ParticipateViewHolder) holder).replyMessage.setText(EmojiMapUtil.replaceCheatSheetEmojis(reply_message));
                        } else if (reply_action.equals("mime-image")) {
                            ((ParticipateViewHolder) holder).replyMessage.setVisibility(View.GONE);
                            ((ParticipateViewHolder) holder).replyImage.setVisibility(View.VISIBLE);
                            Picasso.with(activity).load(reply_message + "_small.jpg").error(R.drawable.ic_no_image).placeholder(R.drawable.ic_loading).noFade().into(((MeViewHolder) holder).replyImage);
                        }

                        ((ParticipateViewHolder) holder).replyLayout.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                ChatActivity.scrollToId(reply_id, 0);
                            }
                        });

                    } else if (chats_messages.get(position).extras.contains("forward")) {
                        ((ParticipateViewHolder) holder).replyLayout.setVisibility(View.VISIBLE);
                        ((ParticipateViewHolder) holder).messageText.setVisibility(View.GONE);

                        final String forward_id = JSONParser.getFromString(chats_messages.get(position).extras, "forward_id");
                        final String forward_sender = JSONParser.getFromString(chats_messages.get(position).extras, "forward_sender");
                        final String forward_action = JSONParser.getFromString(chats_messages.get(position).extras, "forward_action");
                        final String forward_message = JSONParser.getFromString(chats_messages.get(position).extras, "forward_message");

                        ((ParticipateViewHolder) holder).replyAuthor.setText(EmojiMapUtil.replaceCheatSheetEmojis(forward_sender));
                        if (forward_action.equals("message")) {
                            ((ParticipateViewHolder) holder).replyMessage.setVisibility(View.VISIBLE);
                            ((ParticipateViewHolder) holder).replyImage.setVisibility(View.GONE);
                            ((ParticipateViewHolder) holder).replyMessage.setText(EmojiMapUtil.replaceCheatSheetEmojis(forward_message));
                        } else if (forward_action.equals("mime-image")) {
                            ((ParticipateViewHolder) holder).replyMessage.setVisibility(View.GONE);
                            ((ParticipateViewHolder) holder).replyImage.setVisibility(View.VISIBLE);
                            Picasso.with(activity).load(forward_message + "_small.jpg").error(R.drawable.ic_no_image).placeholder(R.drawable.ic_loading).noFade().into(((ParticipateViewHolder) holder).replyImage);
                            ((ParticipateViewHolder) holder).replyImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (!new File(Information.IMAGES_PATH + "chat_" + forward_id + ".jpg").exists()) {
                                        DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                        downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{forward_message, Information.IMAGES_PATH, "chat_" + forward_id + ".jpg"});
                                        downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                            @Override
                                            public void onProgressUpdate(int progress) {

                                            }

                                            @Override
                                            public void onPostExecute(boolean result) {
                                                if (result) {
                                                    Intent intent = new Intent();
                                                    intent.setAction(Intent.ACTION_VIEW);
                                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                                    intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + forward_id + ".jpg")), "image/*");
                                                    activity.startActivity(intent);
                                                }
                                            }
                                        });
                                    } else {
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + forward_id + ".jpg")), "image/*");
                                        activity.startActivity(intent);
                                    }
                                }
                            });

                        }

                        ((ParticipateViewHolder) holder).replyLayout.setBackgroundColor(Color.TRANSPARENT);

                    } else {
                        ((ParticipateViewHolder) holder).replyLayout.setVisibility(View.GONE);
                    }


                    if (chats_messages.get(position).action.equals("mime-image")) {
                        ((ParticipateViewHolder) holder).imageView.setVisibility(View.VISIBLE);
                        ((ParticipateViewHolder) holder).messageText.setVisibility(View.GONE);

                        if (((ParticipateViewHolder) holder).imageView != null) {

                            if (!chats_messages.get(position).message.isEmpty()) {
                                Picasso.with(activity).load(chats_messages.get(position).message).error(R.drawable.ic_no_image).placeholder(R.drawable.ic_loading).noFade().into(((ParticipateViewHolder) holder).imageView);

                                ((ParticipateViewHolder) holder).imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (!new File(Information.IMAGES_PATH + "chat_" + chats_messages.get(position).id + ".jpg").exists()) {
                                            DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{chats_messages.get(position).message, Information.IMAGES_PATH, "chat_" + chats_messages.get(position).id + ".jpg"});
                                            downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                                @Override
                                                public void onProgressUpdate(int progress) {

                                                }

                                                @Override
                                                public void onPostExecute(boolean result) {
                                                    if (result) {
                                                        Intent intent = new Intent();
                                                        intent.setAction(Intent.ACTION_VIEW);
                                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                                        intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + chats_messages.get(position).id + ".jpg")), "image/*");
                                                        activity.startActivity(intent);
                                                    }
                                                }
                                            });
                                        } else {
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                                            intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + "chat_" + chats_messages.get(position).id + ".jpg")), "image/*");
                                            activity.startActivity(intent);
                                        }
                                    }
                                });


                                //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
                            } else {
                                Picasso.with(activity).load(R.drawable.ic_no_image).into(((ParticipateViewHolder) holder).imageView);
                            }
                        }

                    } else {
                        ((ParticipateViewHolder) holder).imageView.setVisibility(View.GONE);
                        //((ParticipateViewHolder) holder).messageText.setVisibility(View.VISIBLE);

                        if (((ParticipateViewHolder) holder).messageText != null) {
                            ((ParticipateViewHolder) holder).messageText.setText(CustomFunctions.getHtml(activity, EmojiMapUtil.replaceCheatSheetEmojis(chats_messages.get(position).message)));
                            ((ParticipateViewHolder) holder).messageText.setMovementMethod(LinkMovementMethod.getInstance());
                            Functions.stripUnderlines(((ParticipateViewHolder) holder).messageText);
                        }
                    }
                    if (((ParticipateViewHolder) holder).timeText != null) {
                        ((ParticipateViewHolder) holder).timeText.setText(Functions.getTimeFormat(Long.parseLong(chats_messages.get(position).timestamp), "HH:mm"));
                    }

                    ((ParticipateViewHolder) holder).moreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            displayMoreDialog(position);
                        }
                    });

                }
            }


            /*if (selected > -1 && position == selected) {
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.blink);
                animation.setDuration(500);
                animation.setRepeatCount(5);
                holder.layout.startAnimation(animation);

                final ViewHolder holder2 = holder;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            holder2.layout.clearAnimation();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 2500);

                selected = -1;
            }*/

            holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    displayMoreDialog(position);
                    return true;
                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private void displayMoreDialog(final int position) {
        if (chats_messages.get(position).type.equals("date") || chats_messages.get(position).type.equals("action")) {
            return;
        }

        AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
        ArrayList<String> localArrayList = new ArrayList<String>();
        localArrayList.add("پاسخ");
        localArrayList.add("هدایت");
        localArrayList.add("حذف");
        if (chats_messages.get(position).action.equals("message")) {
            localArrayList.add("کپی متن");
        }


        String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
        localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                switch (paramAnonymous2Int) {
                    default:
                        return;
                    case 0:
                        ChatActivity.toolsLayout.setVisibility(View.VISIBLE);
                        if (chats_messages.get(position).action.equals("message")) {
                            ChatActivity.toolsText.setText(EmojiMapUtil.replaceCheatSheetEmojis(chats_messages.get(position).message));
                        } else if (chats_messages.get(position).action.equals("mime-image")) {
                            ChatActivity.toolsText.setText("تصویر");
                        }
                        ChatActivity.toolsIconButton.setImageResource(R.drawable.ic_share);
                        ChatActivity.extras = "reply:" + chats_messages.get(position).id;
                        break;
                    case 1:

                        Intent intent = new Intent(activity, ChatsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        ChatsActivity.intent_action = "forward";
                        if (chats_messages.get(position).action.equals("message")) {
                            ChatsActivity.intent_data = EmojiMapUtil.replaceCheatSheetEmojis(chats_messages.get(position).message);
                        } else if (chats_messages.get(position).action.equals("mime-image")) {
                            ChatsActivity.intent_data = "تصویر";
                        }

                        ChatsActivity.intent_extras = "forward:" + chats_messages.get(position).id;
                        activity.startActivity(intent);
                        break;
                    case 2:
                        chats_messages.get(position).type = "deleted";
                        dbHelper.updateChatsMessage(chats_messages.get(position));
                        chats_messages.remove(position);
                        ChatActivity.chats_messages = chats_messages;

                        try {
                            if (chats_messages.get(position).type != null) {
                                if (chats_messages.get(position).type.equals("date")) {
                                    chats_messages.get(position).type = "deleted";
                                    dbHelper.updateChatsMessage(chats_messages.get(position));
                                    chats_messages.remove(position);
                                    ChatActivity.chats_messages = chats_messages;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        notifyDataSetChanged();

                        break;
                    case 3:
                        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(activity.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText(Information.BASE_NAME, EmojiMapUtil.replaceCheatSheetEmojis(chats_messages.get(position).message));
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(activity, "متن در حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        });
        AlertDialog dialog = localBuilder.create();
        dialog.show();

    }

    @Override
    public int getCount() {
        return chats_messages.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}