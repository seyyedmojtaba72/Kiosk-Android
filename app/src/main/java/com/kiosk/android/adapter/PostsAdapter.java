package com.kiosk.android.adapter;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.EditPostActivity;
import com.kiosk.android.MainActivity;
import com.kiosk.android.PostActivity;
import com.kiosk.android.PostCommentsActivity;
import com.kiosk.android.ProfileActivity;
import com.kiosk.android.R;
import com.kiosk.android.UsersActivity;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.LinkManager;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class PostsAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;
    private DatabaseHelper dbHelper;
    private List<ViewHolder> holders = new ArrayList<ViewHolder>();
    //ImageLoader imageLoader = ImageLoader.getInstance();
    //DisplayImageOptions options = new DisplayImageOptions.Builder().delayBeforeLoading(0).cacheInMemory(true).cacheOnDisk(true).build();

    public List<String> ids, authors, author_names, author_profile_images, author_timestamps, times, images, prices, user_likes, likes, names, descriptions, comments;


    public PostsAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        //this.animation = animation;
        dbHelper = new DatabaseHelper(activity);
        //imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
    }


    class ViewHolder {
        RelativeLayout layout;
        CircleImageView profileImageView;
        TextView authorText, timeText, priceText, likesText, nameText, descriptionText, allCommentsText, comment1Text, comment2Text, comment3Text;
        ImageView imageView, likeBig;
        ImageButton likeButton, commentButton, shareButton, moreButton;
        GestureDetector gdt;
        int position = -1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        try {
            final ViewHolder holder;

            if (convertView != null) {
                holder = (ViewHolder) convertView.getTag();
            } else {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_post_item, parent, false);

                holder = new ViewHolder();

                holder.layout = (RelativeLayout) convertView.findViewById(R.id.layout);
                holder.profileImageView = (CircleImageView) convertView.findViewById(R.id.profile_image);
                holder.authorText = (TextView) convertView.findViewById(R.id.author);
                holder.priceText = (TextView) convertView.findViewById(R.id.price);
                holder.timeText = (TextView) convertView.findViewById(R.id.time);
                holder.imageView = (ImageView) convertView.findViewById(R.id.image);
                holder.likeBig = (ImageView) convertView.findViewById(R.id.likeBig);
                //holder.progress = (CircularProgressBar) convertView.findViewById(R.id.progress);
                holder.likeButton = (ImageButton) convertView.findViewById(R.id.like);
                holder.commentButton = (ImageButton) convertView.findViewById(R.id.comment);
                holder.shareButton = (ImageButton) convertView.findViewById(R.id.share);
                holder.moreButton = (ImageButton) convertView.findViewById(R.id.more);
                holder.likesText = (TextView) convertView.findViewById(R.id.likes);
                holder.nameText = (TextView) convertView.findViewById(R.id.name);
                holder.descriptionText = (TextView) convertView.findViewById(R.id.description);
                holder.allCommentsText = (TextView) convertView.findViewById(R.id.all_comments);
                holder.comment1Text = (TextView) convertView.findViewById(R.id.comment1);
                holder.comment2Text = (TextView) convertView.findViewById(R.id.comment2);
                holder.comment3Text = (TextView) convertView.findViewById(R.id.comment3);

                if (!author_timestamps.get(position).equals("null")) {
                    Functions.putLong(activity, authors.get(position) + "_timestamp", Long.parseLong(author_timestamps.get(position)));
                }

                //post = dbHelper.getPost(Integer.parseInt(ids.get(position)));

            /*if (animation != null) {
                animation.setStartOffset(position * 500);
                convertView.startAnimation(animation);
            }*/
                convertView.setTag(holder);
            }


            if (position == holder.position) {
                return convertView;
            }

            holder.position = position;


            if (holders.size() >= position + 1) {
                holders.set(position, holder);
            } else {
                holders.add(position, holder);
            }


            // PRELOAD IMAGE FOR MORE SPEED

            //Log.d("tag", "caching images of items from " + Math.min(ids.size() - 1, position + 3) + " to " + Math.max(0, position - 3));
            for (int i = Math.min(ids.size() - 1, position + 3); i >= Math.max(0, position - 3); i--) {
                //Log.d("tag", "caching item " + i + " 's image: " + images.get(i));

                if (i == position) {
                    Picasso.with(activity).load(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);
                    if (!author_profile_images.get(position).isEmpty()) {
                        Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + author_profile_images.get(position) + "_small.jpg" + "?" + author_timestamps.get(position)).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);
                        //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
                    }

                    if (!images.get(i).isEmpty()) {
                        Picasso.with(activity).load(images.get(i)).error(R.drawable.ic_no_image).noFade().into(holder.imageView);
                    } else {
                        Picasso.with(activity).load(R.drawable.ic_no_image).noFade().into(holder.imageView);
                    }
                } else {
                    if (!author_profile_images.get(position).isEmpty()) {
                        Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + author_profile_images.get(position) + "_small.jpg" + "?" + author_timestamps.get(position)).fetch();
                    }

                    if (!images.get(i).isEmpty()) {
                        Picasso.with(activity).load(images.get(i)).error(R.drawable.ic_no_image).fetch();
                    }
                }
            }


            holder.authorText.setText(author_names.get(position));
            holder.timeText.setText(times.get(position));
            if (prices.get(position).equals("0")) {
                holder.priceText.setVisibility(View.INVISIBLE);
            } else {
                holder.priceText.setVisibility(View.VISIBLE);
                holder.priceText.setText(Functions.normalPrice(prices.get(position), ".") + " ریال");
            }

            if (user_likes.get(position).equals("1")) {
                holder.likeButton.setBackgroundResource(R.drawable.selector_button_unlike);
            } else {
                holder.likeButton.setBackgroundResource(R.drawable.selector_button_like);
            }

            String likesDesc = "";
            if (Integer.parseInt(likes.get(position)) == 0) {
                likesDesc = "بدون پسند";
            } else if (Integer.parseInt(likes.get(position)) == 1) {
                likesDesc = "1 نفر پسندید";
            } else {
                likesDesc = Functions.normalPrice(likes.get(position), ",") + " نفر پسندیدند";
            }
            holder.likesText.setText(likesDesc);
            holder.nameText.setText(EmojiMapUtil.replaceCheatSheetEmojis(names.get(position)));


            if (!descriptions.get(position).isEmpty()) {
                holder.descriptionText.setVisibility(View.VISIBLE);
                holder.descriptionText.setText(CustomFunctions.getDescriptionHtml(activity, ids.get(position), EmojiMapUtil.replaceCheatSheetEmojis(descriptions.get(position))));
                holder.descriptionText.setMovementMethod(LinkMovementMethod.getInstance());
                Functions.stripUnderlines(holder.descriptionText);
            } else {
                holder.descriptionText.setVisibility(View.GONE);
            }

            String[] comments_array = comments.get(position).split("%end_row%");


            if (Integer.parseInt(comments_array[0]) > 3) {
                holder.allCommentsText.setVisibility(View.VISIBLE);
                holder.allCommentsText.setText("مشاهده همه " + Functions.normalPrice(comments_array[0], ",") + " نظر");
            } else {
                holder.allCommentsText.setVisibility(View.GONE);
            }
            holder.allCommentsText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent localIntent = new Intent(activity, PostCommentsActivity.class);
                    localIntent.putExtra("id", ids.get(position));
                    activity.startActivity(localIntent);
                }
            });

            if (Integer.parseInt(comments_array[0]) > 0) {
                holder.comment1Text.setVisibility(View.VISIBLE);
                holder.comment1Text.setText(CustomFunctions.getCommentHtml(activity, comments_array[1].split("%end_record%")[0], EmojiMapUtil.replaceCheatSheetEmojis(comments_array[1].split("%end_record%")[1]), EmojiMapUtil.replaceCheatSheetEmojis(comments_array[1].split("%end_record%")[2])));
                holder.comment1Text.setMovementMethod(LinkMovementMethod.getInstance());
                Functions.stripUnderlines(holder.comment1Text);
            } else {
                holder.comment1Text.setVisibility(View.GONE);
            }

            if (Integer.parseInt(comments_array[0]) > 1) {
                holder.comment2Text.setVisibility(View.VISIBLE);
                holder.comment2Text.setText(CustomFunctions.getCommentHtml(activity, comments_array[2].split("%end_record%")[0], EmojiMapUtil.replaceCheatSheetEmojis(comments_array[2].split("%end_record%")[1]), EmojiMapUtil.replaceCheatSheetEmojis(comments_array[2].split("%end_record%")[2])));
                holder.comment2Text.setMovementMethod(LinkMovementMethod.getInstance());
                Functions.stripUnderlines(holder.comment2Text);
            } else {
                holder.comment2Text.setVisibility(View.GONE);
            }

            if (Integer.parseInt(comments_array[0]) > 2) {
                holder.comment3Text.setVisibility(View.VISIBLE);
                holder.comment3Text.setMovementMethod(LinkMovementMethod.getInstance());
                holder.comment3Text.setText(CustomFunctions.getCommentHtml(activity, comments_array[3].split("%end_record%")[0], EmojiMapUtil.replaceCheatSheetEmojis(comments_array[3].split("%end_record%")[1]), EmojiMapUtil.replaceCheatSheetEmojis(comments_array[3].split("%end_record%")[2])));
                Functions.stripUnderlines(holder.comment3Text);
            } else {
                holder.comment3Text.setVisibility(View.GONE);
            }



        /*
        imageLoader.displayImage(images.get(position), holder.imageView, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                //holder.imageView.setVisibility(View.INVISIBLE);
                Picasso.with(activity).load(R.color.generalWhite).into(holder.imageView);
                //holder.progress.setProgress(5);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                //holder.imageView.setVisibility(View.VISIBLE);
                Picasso.with(activity).load(R.drawable.ic_no_image).into(holder.imageView);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                //holder.imageView.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams params = holder.imageView.getLayoutParams();
                heights.set(position, params.height);

                ViewGroup.LayoutParams params2 = holder.layout.getLayoutParams();
                params2.height = params.height;
                holder.layout.setLayoutParams(params2);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                //holder.imageView.setVisibility(View.VISIBLE);
                Picasso.with(activity).load(R.drawable.ic_no_image).into(holder.imageView);
            }
        });*/


            holder.gdt = new GestureDetector(new GestureListener(position));
            holder.profileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (authors.get(position).equals(Functions.getString(activity, "id", ""))) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("action", "open_profile");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                    } else {
                        Intent localIntent = new Intent(activity, ProfileActivity.class);
                        localIntent.putExtra("id", authors.get(position));
                        activity.startActivity(localIntent);
                    }
                }
            });


            holder.authorText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (authors.get(position).equals(Functions.getString(activity, "id", ""))) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("action", "open_profile");
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        activity.startActivity(intent);
                    } else {
                        Intent localIntent = new Intent(activity, ProfileActivity.class);
                        localIntent.putExtra("id", authors.get(position));
                        activity.startActivity(localIntent);
                    }
                }
            });


            holder.imageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return holder.gdt.onTouchEvent(motionEvent);
                }
            });


            holder.likeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Post post = dbHelper.getPost(Integer.parseInt(ids.get(position)));

                    if (post.user_like.equals("1")) {
                        holder.likeButton.setBackgroundResource(R.drawable.selector_button_like);
                        post.user_like = "0";
                        user_likes.set(position, "0");
                        post.likes = String.valueOf(Integer.parseInt(post.likes) - 1);
                        likes.set(position, String.valueOf(Integer.parseInt(post.likes) - 1));
                        dbHelper.updatePost(post);

                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                Fetcher.FetchURL(APIManager.likePost(Functions.getString(activity, "id", ""), ids.get(position)));
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String likesDesc = "";
                                        if (Integer.parseInt(post.likes) == 0) {
                                            likesDesc = "بدون پسند";
                                        } else if (Integer.parseInt(post.likes) == 1) {
                                            likesDesc = "1 نفر پسندید";
                                        } else {
                                            likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
                                        }
                                        holder.likesText.setText(likesDesc);
                                    }
                                });
                            }
                        });

                    } else {
                        holder.likeButton.setBackgroundResource(R.drawable.selector_button_unlike);
                        post.user_like = "1";
                        user_likes.set(position, "1");
                        likes.set(position, String.valueOf(Integer.parseInt(post.likes) + 1));
                        post.likes = String.valueOf(Integer.parseInt(post.likes) + 1);
                        dbHelper.updatePost(post);

                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                Fetcher.FetchURL(APIManager.likePost(Functions.getString(activity, "id", ""), ids.get(position)));
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String likesDesc = "";
                                        if (Integer.parseInt(post.likes) == 0) {
                                            likesDesc = "بدون پسند";
                                        } else if (Integer.parseInt(post.likes) == 1) {
                                            likesDesc = "1 نفر پسندید";
                                        } else {
                                            likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
                                        }
                                        holder.likesText.setText(likesDesc);
                                    }
                                });
                            }
                        });

                    }

                }
            });


            holder.commentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent localIntent = new Intent(activity, PostCommentsActivity.class);
                    localIntent.putExtra("id", ids.get(position));
                    activity.startActivity(localIntent);
                }
            });

            holder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Functions.shareTextViaIntent(activity, activity.getString(R.string.share), Functions.getString(activity, "USERS_BUFFER_ITEMS", Information.DEFAULT_FOOTER_DESCRIPTION), LinkManager.viewPost(Integer.parseInt(ids.get(position))));
                }
            });


            holder.likesText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, UsersActivity.class);
                    intent.putExtra("method", "liked");
                    intent.putExtra("argument", ids.get(position));
                    activity.startActivity(intent);
                }
            });


            holder.moreButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    final Post post = dbHelper.getPost(Integer.parseInt(ids.get(position)));

                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                    ArrayList<String> localArrayList = new ArrayList<String>();
                    localArrayList.add("گزارش");
                    localArrayList.add("دانلود تصویر");
                    localArrayList.add("کپی لینک اشتراک");
                    if (post.reading.equals("1")) {
                        localArrayList.add("حذف از لیست خواندن");
                    } else {
                        localArrayList.add("افزودن به لیست خواندن");
                    }

                    if (post.author.equals(Functions.getString(activity, "id", ""))) {
                        localArrayList.add("ویرایش");
                        localArrayList.add("حذف");
                    }
                    String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                    localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            switch (paramAnonymous2Int) {
                                default:
                                    return;
                                case 0:
                                    try {
                                        final String[] arrayOfString = {"محتوای نامناسب", "نقض قانون کپی رایت", "خرابی لینک دانلود", "قیمت نامناسب"};
                                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                                        localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, final int i) {
                                                final SweetAlertDialog sDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                                                new AsyncTask<String, String, String>() {

                                                    @Override
                                                    protected void onPreExecute() {
                                                        sDialog.show();
                                                        sDialog.setCancelable(false);

                                                        super.onPreExecute();
                                                    }

                                                    @Override
                                                    protected String doInBackground(String... arg0) {
                                                        return Fetcher.FetchURL(APIManager.setPostMyReport(ids.get(position), Functions.getString(activity, "id", ""), arrayOfString[i]));
                                                    }

                                                    protected void onPostExecute(final String result) {
                                                        activity.runOnUiThread(new Runnable() {
                                                            public void run() {

                                                                if (result.contains("success")) {
                                                                    sDialog.setTitleText("").setContentText("گزارش شما ثبت شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                                                } else {
                                                                    sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                                }
                                                            }
                                                        });

                                                    }
                                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                                sDialog.show();
                                            }
                                        });

                                        localBuilder.create().show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case 1:
                                    if (!new File(Information.IMAGES_PATH + post.id + ".jpg").exists()) {
                                        DownloadManager.DownloadTask downloadTask = new DownloadManager.DownloadTask();
                                        downloadTask = new DownloadManager.DownloadTask();
                                        downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{post.image, Information.IMAGES_PATH, post.id + ".jpg"});
                                        downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                            @Override
                                            public void onProgressUpdate(int progress) {

                                            }

                                            @Override
                                            public void onPostExecute(boolean result) {
                                                if (result) {
                                                    Toast.makeText(activity, "تصویر مطلب درون حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } else {
                                        Toast.makeText(activity, "تصویر مطلب درون حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                    }

                                    break;

                                case 2:
                                    ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(activity.CLIPBOARD_SERVICE);
                                    ClipData clip = ClipData.newPlainText(Information.BASE_NAME, LinkManager.viewPost(post.id));
                                    clipboard.setPrimaryClip(clip);
                                    Toast.makeText(activity, "لینک اشتراک مطلب به حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                    break;
                                case 3:
                                    try {
                                        if (post.reading.equals("1")) {
                                            post.reading = "0";
                                            Toast.makeText(activity, "از لیست خواندن حذف شد.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            post.reading = "1";
                                            Toast.makeText(activity, "به لیست خواندن اضافه شد.", Toast.LENGTH_SHORT).show();
                                        }
                                        dbHelper.updatePost(post);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case 4:
                                    Intent localIntent = new Intent(activity, EditPostActivity.class);
                                    localIntent.putExtra("id", ids.get(position));
                                    activity.startActivity(localIntent);
                                    break;
                                case 5:
                                    new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE).setTitleText("حذف مطلب").setContentText("آیا مطمئنید میخواهید این مطلب را حذف کنید؟").setConfirmText("بله").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(final SweetAlertDialog sDialog) {
                                            new AsyncTask<String, String, String>() {

                                                @Override
                                                protected void onPreExecute() {
                                                    sDialog.setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false).changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                                    sDialog.show();
                                                    sDialog.setCancelable(false);

                                                    super.onPreExecute();
                                                }

                                                @Override
                                                protected String doInBackground(String... arg0) {
                                                    if (Functions.isOnline(activity)) {
                                                        return Fetcher.FetchURL(APIManager.deleteMyPost(ids.get(position), Functions.getString(activity, "id", "")));
                                                    }
                                                    return "fail";
                                                }

                                                protected void onPostExecute(final String result) {
                                                    activity.runOnUiThread(new Runnable() {
                                                        public void run() {

                                                            if (result.contains("success")) {
                                                                dbHelper.deletePost(Integer.parseInt(ids.get(position)));

                                                                sDialog.setTitleText("").setContentText("مطلب شما حذف شد!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                                ids.remove(position);
                                                                authors.remove(position);
                                                                author_names.remove(position);
                                                                author_profile_images.remove(position);
                                                                author_timestamps.remove(position);
                                                                times.remove(position);
                                                                images.remove(position);
                                                                prices.remove(position);
                                                                user_likes.remove(position);
                                                                likes.remove(position);
                                                                names.remove(position);
                                                                descriptions.remove(position);
                                                                comments.remove(position);

                                                                notifyDataSetChanged();
                                                            } else {
                                                                sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                            }
                                                        }
                                                    });

                                                }
                                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        }


                                    }).showCancelButton(true).setCancelText("انصراف").setCancelClickListener(null).show();

                                    break;
                            }

                        }
                    });
                    localBuilder.create().show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class GestureListener implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
        private int position = 0;

        GestureListener(int position) {
            this.position = position;
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            Intent localIntent = new Intent(activity, PostActivity.class);
            localIntent.putExtra("id", ids.get(position));
            activity.startActivity(localIntent);
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent motionEvent) {
            final ViewHolder holder = holders.get(position);

            final Post post = dbHelper.getPost(Integer.parseInt(ids.get(position)));

            holder.likeBig.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.instagram_like));

            if (!post.user_like.equals("1")) {
                holder.likeButton.setBackgroundResource(R.drawable.selector_button_unlike);
                post.user_like = "1";
                user_likes.set(position, "1");
                likes.set(position, String.valueOf(Integer.parseInt(post.likes) + 1));
                post.likes = String.valueOf(Integer.parseInt(post.likes) + 1);
                dbHelper.updatePost(post);

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        Fetcher.FetchURL(APIManager.likePost(Functions.getString(activity, "id", ""), ids.get(position)));
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String likesDesc = "";
                                if (Integer.parseInt(post.likes) == 0) {
                                    likesDesc = "بدون پسند";
                                } else if (Integer.parseInt(post.likes) == 1) {
                                    likesDesc = "1 نفر پسندید";
                                } else {
                                    likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
                                }
                                holder.likesText.setText(likesDesc);
                            }
                        });
                    }
                });
            }
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onDown(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
            return false;
        }
    }

}