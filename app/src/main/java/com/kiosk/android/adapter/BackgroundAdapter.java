package com.kiosk.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.kiosk.android.PostActivity;
import com.kiosk.android.R;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class BackgroundAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;
    private DatabaseHelper dbHelper;
    private int layout = R.layout.adapter_post_grid_item;

    public int[] images;

    private DownloadManager.DownloadTask downloadTask;

    public BackgroundAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        this.animation = animation;
        dbHelper = new DatabaseHelper(activity);

    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    class ViewHolder {
        ImageView imageView, priceImage;
        ProgressBar progress;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(layout, parent, false);

                holder = new ViewHolder();

                holder.imageView = (ImageView) convertView.findViewById(R.id.image);
                holder.priceImage = (ImageView) convertView.findViewById(R.id.price);
                holder.progress = (ProgressBar) convertView.findViewById(R.id.progress);

                /*if (animation != null) {
                    animation.setStartOffset(position * 500);
                    convertView.startAnimation(animation);
                }*/
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(activity).load(images[position]).error(R.drawable.bg1).into(holder.imageView);
            holder.imageView.setClickable(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}