package com.kiosk.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.kiosk.android.PostActivity;
import com.kiosk.android.R;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class PostsGridAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;
    private DatabaseHelper dbHelper;
    private int layout = R.layout.adapter_post_grid_item;

    public List<String> ids, images, prices;

    private DownloadManager.DownloadTask downloadTask;

    public PostsGridAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        this.animation = animation;
        dbHelper = new DatabaseHelper(activity);

    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    class ViewHolder {
        ImageView imageView, priceImage;
        ProgressBar progress;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(layout, parent, false);

                holder = new ViewHolder();

                holder.imageView = (ImageView) convertView.findViewById(R.id.image);
                holder.priceImage = (ImageView) convertView.findViewById(R.id.price);
                holder.progress = (ProgressBar) convertView.findViewById(R.id.progress);

            /*if (animation != null) {
                animation.setStartOffset(position * 500);
                convertView.startAnimation(animation);
            }*/
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            // PRELOAD IMAGE FOR MORE SPEED

            //Log.d("tag", "caching images of items from " + Math.min(ids.size() - 1, position + 3) + " to " + Math.max(0, position - 3));
            for (int i = Math.min(ids.size() - 1, position + 30); i >= Math.max(0, position - 30); i--) {
                //Log.d("tag", "caching item " + i + " 's image: " + images.get(i));

                if (i == position) {
                    if (!images.get(i).isEmpty()) {
                        Picasso.with(activity).load(images.get(i) + "_small.jpg").error(R.drawable.ic_no_image).noFade().resize(150, 150).centerCrop().into(holder.imageView);
                    } else {
                        Picasso.with(activity).load(R.drawable.ic_no_image).noFade().into(holder.imageView);
                    }
                } else {
                    if (!images.get(i).isEmpty()) {
                        Picasso.with(activity).load(images.get(i) + "_small.jpg").error(R.drawable.ic_no_image).resize(150, 150).fetch();
                    }
                }
            }


            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent localIntent = new Intent(activity, PostActivity.class);
                    localIntent.putExtra("id", ids.get(position));
                    activity.startActivity(localIntent);
                }
            });

            if (prices.get(position).equals("0")) {
                holder.priceImage.setVisibility(View.INVISIBLE);
            } else {
                holder.priceImage.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}