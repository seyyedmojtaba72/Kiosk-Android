package com.kiosk.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.MainActivity;
import com.kiosk.android.ProfileActivity;
import com.kiosk.android.R;
import com.kiosk.android.info.Information;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class HistoryAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;

    public List<String> ids, authors, author_names, author_profile_images, author_timestamps, actions, timestamps, times;

    public HistoryAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        this.animation = animation;
    }

    class ViewHolder {
        LinearLayout layout;
        CircleImageView profileImageView;
        TextView nameText, actionText, timeText;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;
            if (convertView != null) {
                holder = (ViewHolder) convertView.getTag();
            } else {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_history_item, parent, false);

                holder = new ViewHolder();
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.profileImageView = (CircleImageView) convertView.findViewById(R.id.profile_image);
                holder.nameText = (TextView) convertView.findViewById(R.id.name);
                holder.actionText = (TextView) convertView.findViewById(R.id.action);
                holder.timeText = (TextView) convertView.findViewById(R.id.time);


            /*if (animation != null) {
                animation.setStartOffset(position * 500);
                convertView.startAnimation(animation);
            }*/

                convertView.setTag(holder);
            }

            Picasso.with(activity).load(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);

            if (!author_profile_images.get(position).isEmpty()) {
                Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + author_profile_images.get(position) + "_small.jpg" + "?" + author_timestamps.get(position)).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);
                //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
            }


            //Log.d("tag", "if "+Long.parseLong(timestamps.get(position))+ " > "+Long.parseLong(String.valueOf(Functions.getLong(activity, "last_check_timestamp", 0L)).substring(0, 10)));
            if (Long.parseLong(timestamps.get(position)) > Long.parseLong(Functions.normalLength(String.valueOf(Functions.getLong(activity, "last_check_timestamp", 0L)), 10, "0", false).substring(0, 10))) {
                holder.layout.setBackgroundColor(activity.getResources().getColor(R.color.generalTransparentGreen));
            } else {
                holder.layout.setBackgroundColor(Color.TRANSPARENT);
            }


            holder.nameText.setText(EmojiMapUtil.replaceCheatSheetEmojis(author_names.get(position)));

            if (actions.get(position).equals("follow_request")) {
                holder.actionText.setText("میخواهد شما را دنبال کند.");
                holder.layout.setBackgroundColor(activity.getResources().getColor(R.color.generalTransparentRed));
            } else if (actions.get(position).equals("follow_accept")) {
                holder.actionText.setText("درخواست شما را قبول کرد.");
            } else if (actions.get(position).equals("follow")) {
                holder.actionText.setText("شما را دنبال کرد.");
            } else if (actions.get(position).equals("like")) {
                holder.actionText.setText("مطلب شما را پسندید.");
            } else if (actions.get(position).equals("comment")) {
                holder.actionText.setText("برای مطلب شما نظر گذاشت.");
            } else if (actions.get(position).equals("tag_post")) {
                holder.actionText.setText("در مطلبی از شما یاد کرد.");
            } else if (actions.get(position).equals("tag_comment")) {
                holder.actionText.setText("در نظری از شما یاد کرد.");
            } else {
                holder.actionText.setText("");
            }

            holder.timeText.setText(times.get(position));


            holder.profileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (authors.get(position).equals(Functions.getString(activity, "id", ""))) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("action", "open_profile");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                    } else {
                        Intent localIntent = new Intent(activity, ProfileActivity.class);
                        localIntent.putExtra("id", authors.get(position));
                        activity.startActivity(localIntent);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}