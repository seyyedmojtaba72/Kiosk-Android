package com.kiosk.android.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.kiosk.android.MainActivity;
import com.kiosk.android.ProfileActivity;
import com.kiosk.android.R;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.CustomFunctions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class PostsCommentsAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;

    public Post post;
    public List<String> ids, authors, author_names, author_profile_images, author_timestamps, comments, times;

    public PostsCommentsAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        this.animation = animation;
    }

    class ViewHolder {
        LinearLayout layout;
        CircleImageView profileImageView;
        TextView authorCommentText, timeText;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_post_comment_item, parent, false);

                holder = new ViewHolder();
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.profileImageView = (CircleImageView) convertView.findViewById(R.id.profile_image);
                holder.authorCommentText = (TextView) convertView.findViewById(R.id.author_comment);
                holder.timeText = (TextView) convertView.findViewById(R.id.time);

            /*if (animation != null) {
                animation.setStartOffset(position * 500);
                convertView.startAnimation(animation);
            }*/

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(activity).load(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);

            if (!author_profile_images.get(position).isEmpty()) {
                Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + author_profile_images.get(position) + "_small.jpg" + "?" + author_timestamps.get(position)).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);
                //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
            }

            holder.authorCommentText.setText(CustomFunctions.getCommentHtml(activity, authors.get(position), EmojiMapUtil.replaceCheatSheetEmojis(author_names.get(position)), EmojiMapUtil.replaceCheatSheetEmojis(comments.get(position))));
            holder.authorCommentText.setMovementMethod(LinkMovementMethod.getInstance());
            Functions.stripUnderlines(holder.authorCommentText);

            holder.timeText.setText(times.get(position));


            holder.profileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (authors.get(position).equals(Functions.getString(activity, "id", ""))) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("action", "open_profile");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                    } else {
                        Intent localIntent = new Intent(activity, ProfileActivity.class);
                        localIntent.putExtra("id", authors.get(position));
                        activity.startActivity(localIntent);
                    }
                }
            });

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (authors.get(position).equals(Functions.getString(activity, "id", "")) || post.author.equals(Functions.getString(activity, "id", ""))) {
                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(activity);
                        ArrayList<String> localArrayList = new ArrayList<String>();
                        localArrayList.add("حذف");

                        String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                        localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                switch (paramAnonymous2Int) {
                                    default:
                                        return;
                                    case 0:
                                        new AsyncTask<String, String, String>() {

                                            @Override
                                            protected String doInBackground(String... voids) {
                                                return Fetcher.FetchURL(APIManager.deleteComment(Functions.getString(activity, "id", ""), ids.get(position)));
                                            }

                                            @Override
                                            protected void onPostExecute(String result) {
                                                if (result.equals("success")) {
                                                    ids.remove(position);
                                                    authors.remove(position);

                                                    author_names.remove(position);
                                                    author_profile_images.remove(position);
                                                    author_timestamps.remove(position);
                                                    comments.remove(position);
                                                    times.remove(position);


                                                    notifyDataSetChanged();
                                                }

                                                super.onPostExecute(result);
                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        break;
                                }

                            }
                        });
                        AlertDialog dialog = localBuilder.create();
                        dialog.show();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}