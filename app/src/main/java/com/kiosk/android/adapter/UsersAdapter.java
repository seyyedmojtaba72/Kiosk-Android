package com.kiosk.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.MainActivity;
import com.kiosk.android.ProfileActivity;
import com.kiosk.android.R;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class UsersAdapter extends BaseAdapter {
    public Activity activity;
    public Animation animation;
    public boolean showButtons = false, showExtras = false;

    public List<String> users, user_names, user_profile_images, user_timestamps, follows, extras;


    public UsersAdapter(Activity activity, Animation animation, boolean showButtons) {
        this.activity = activity;
        this.animation = animation;
        this.showButtons = showButtons;
    }

    class ViewHolder {
        CircleImageView profileImageView;
        TextView nameText, extrasText;
        Button followButton;
        ProgressBar loading;
        RelativeLayout followLayout, extrasLayout;
        ImageView extrasIconView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_user_item, parent, false);

                holder = new ViewHolder();
                holder.profileImageView = (CircleImageView) convertView.findViewById(R.id.profile_image);
                holder.nameText = (TextView) convertView.findViewById(R.id.name);
                holder.followButton = (Button) convertView.findViewById(R.id.follow);
                holder.loading = (ProgressBar) convertView.findViewById(R.id.loading);
                holder.followLayout = (RelativeLayout) convertView.findViewById(R.id.follow_layout);
                holder.extrasLayout = (RelativeLayout) convertView.findViewById(R.id.extras_layout);
                holder.extrasIconView = (ImageView) convertView.findViewById(R.id.extras_icon);
                holder.extrasText = (TextView) convertView.findViewById(R.id.extras_text);

                Functions.putLong(activity, users.get(position) + "_timestamp", Long.parseLong(user_timestamps.get(position)));


            /*if (animation != null) {
                animation.setStartOffset(position * 500);
                convertView.startAnimation(animation);
            }*/
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(activity).load(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);

            if (!user_profile_images.get(position).isEmpty()) {
                Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position) + "_small.jpg" + "?" + user_timestamps.get(position)).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.profileImageView);
                //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_images.get(position), Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_images.get(position));
            }


            if (showButtons) {
                holder.followLayout.setVisibility(View.VISIBLE);
                if (users.get(position).equals(Functions.getString(activity, "id", ""))) {
                    holder.followButton.setVisibility(View.GONE);
                } else {
                    holder.followButton.setVisibility(View.VISIBLE);
                }

                holder.loading.setVisibility(View.INVISIBLE);
                if (follows.get(position).equals("processing")) {
                    holder.loading.setVisibility(View.VISIBLE);
                    holder.followButton.setBackgroundResource(R.drawable.selector_button_third);
                    holder.followButton.setText("");
                } else if (follows.get(position).equals("accepted")) {
                    holder.followButton.setBackgroundResource(R.drawable.selector_button_primary);
                    holder.followButton.setTextColor(Color.WHITE);
                    holder.followButton.setText("دنبال شده");
                } else if (follows.get(position).equals("waiting")) {
                    holder.followButton.setBackgroundResource(R.drawable.selector_button_third);
                    holder.followButton.setTextColor(Color.WHITE);
                    holder.followButton.setText("در انتظار");
                } else {
                    holder.followButton.setBackgroundResource(R.drawable.selector_button_secondary);
                    holder.followButton.setTextColor(Color.BLACK);
                    holder.followButton.setText("دنبال کردن");
                }
            } else {
                holder.followLayout.setVisibility(View.GONE);
                holder.followButton.setVisibility(View.GONE);
            }

            holder.nameText.setText(EmojiMapUtil.replaceCheatSheetEmojis(user_names.get(position)));

            holder.profileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (users.get(position).equals(Functions.getString(activity, "id", ""))) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("action", "open_profile");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                    } else {
                        Intent localIntent = new Intent(activity, ProfileActivity.class);
                        localIntent.putExtra("id", users.get(position));
                        activity.startActivity(localIntent);
                    }
                }
            });

            /*
            holder.nameText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (users.get(position).equals(Functions.getString(activity, "id", ""))) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("action", "open_profile");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                    } else {
                        Intent localIntent = new Intent(activity, ProfileActivity.class);
                        localIntent.putExtra("id", users.get(position));
                        activity.startActivity(localIntent);
                    }
                }
            });*/


            holder.followButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String former_follow = follows.get(position);
                    new AsyncTask<String, String, String>() {

                        @Override
                        protected void onPreExecute() {
                            holder.loading.setVisibility(View.VISIBLE);
                            holder.followButton.setBackgroundResource(R.drawable.selector_button_third);
                            holder.followButton.setText("");
                            follows.set(position, "processing");

                            super.onPreExecute();
                        }

                        @Override
                        protected String doInBackground(String... strings) {
                            return Fetcher.FetchURL(APIManager.followUser(Functions.getString(activity, "id", ""), users.get(position)));
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            holder.loading.setVisibility(View.INVISIBLE);

                            if (result.equals("success")) {
                                if (former_follow.equals("accepted")) {
                                    follows.set(position, "0");
                                } else if (former_follow.equals("waiting")) {
                                    follows.set(position, "0");
                                } else {
                                    follows.set(position, "accepted");
                                }

                            } else if (result.equals("requested")) {
                                follows.set(position, "waiting");
                            }


                            if (follows.get(position).equals("processing")) {
                                holder.loading.setVisibility(View.VISIBLE);
                                holder.followButton.setBackgroundResource(R.drawable.selector_button_third);
                                holder.followButton.setText("");
                            } else if (follows.get(position).equals("accepted")) {
                                holder.followButton.setBackgroundResource(R.drawable.selector_button_primary);
                                holder.followButton.setTextColor(Color.WHITE);
                                holder.followButton.setText("دنبال شده");
                            } else if (follows.get(position).equals("waiting")) {
                                holder.followButton.setBackgroundResource(R.drawable.selector_button_third);
                                holder.followButton.setTextColor(Color.WHITE);
                                holder.followButton.setText("در انتظار");
                            } else {
                                holder.followButton.setBackgroundResource(R.drawable.selector_button_secondary);
                                holder.followButton.setTextColor(Color.BLACK);
                                holder.followButton.setText("دنبال کردن");
                            }

                            notifyDataSetChanged();

                            super.onPostExecute(result);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });


            if (showExtras) {
                if (!extras.get(position).isEmpty()) {
                    holder.extrasLayout.setVisibility(View.VISIBLE);

                    String extras_role = JSONParser.getFromString(extras.get(position), "role");
                    String extras_last_seen = JSONParser.getFromString(extras.get(position), "last_seen");

                    if (extras_role.equals("admin")) {
                        holder.extrasIconView.setVisibility(View.VISIBLE);
                        holder.extrasIconView.setImageResource(R.mipmap.ic_admin);
                    } else {
                        holder.extrasIconView.setVisibility(View.GONE);
                    }

                    if (Functions.getDifferenceOfTimes(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(extras_last_seen))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(extras_last_seen),"hh:mm:ss")) < 60) {
                        holder.extrasText.setText("آنلاین");
                    } else {
                        holder.extrasText.setText(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(extras_last_seen))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(extras_last_seen), "hh:mm:ss")));
                    }

                } else {
                    holder.followLayout.setVisibility(View.GONE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}