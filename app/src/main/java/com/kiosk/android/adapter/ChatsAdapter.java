package com.kiosk.android.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.R;
import com.kiosk.android.info.Information;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SeyyedMojtaba on 3/1/2016.
 */
public class ChatsAdapter extends BaseAdapter {
    private DatabaseHelper dbHelper;
    public Activity activity;
    public Animation animation;
    private long last_timestamp = 0;

    public List<Chat> chats;


    public ChatsAdapter(Activity activity, Animation animation) {
        this.activity = activity;
        this.animation = animation;
        dbHelper = new DatabaseHelper(activity);
    }

    class ViewHolder {
        CircleImageView imageView;
        TextView nameText, messageText, timeText, countText;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(R.layout.adapter_chat_item, parent, false);

                holder = new ViewHolder();
                holder.imageView = (CircleImageView) convertView.findViewById(R.id.image);
                holder.nameText = (TextView) convertView.findViewById(R.id.name);
                holder.messageText = (TextView) convertView.findViewById(R.id.message);
                holder.timeText = (TextView) convertView.findViewById(R.id.time);
                holder.countText = (TextView) convertView.findViewById(R.id.count);

            /*if (animation != null) {
                animation.setStartOffset(position * 500);
                convertView.startAnimation(animation);
            }*/
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (chats.get(position).type.equals("single")) {
                Picasso.with(activity).load(R.drawable.ic_profile).noFade().into(holder.imageView);

                if (!chats.get(position).image.isEmpty()) {

                    String id = "";
                    if (Functions.getString(activity, "id", "").equals(chats.get(position).creator)) {
                        id = chats.get(position).participants.split("!new_user!")[0];
                    } else {
                        id = chats.get(position).creator;
                    }

                    if (Functions.getLong(activity, id + "_timestamp", 0) != 0) {
                        last_timestamp = Functions.getLong(activity, id + "_timestamp", 0);
                    } else {
                        if (Functions.isOnline(activity)) {
                            last_timestamp = System.currentTimeMillis();
                        } else {
                            last_timestamp = Long.parseLong(chats.get(position).timestamp);
                        }
                        Functions.putLong(activity, id + "_timestamp", last_timestamp);
                    }


                    //Picasso.with(activity).invalidate(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + chats.get(position).image);
                    Picasso.with(activity).load(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + chats.get(position).image + "_small.jpg" + "?" + last_timestamp).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).noFade().resize(100, 100).into(holder.imageView);
                    //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_chats.get(position).image, Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_chats.get(position).image);
                }
            } else if (chats.get(position).type.equals("group")) {
                Picasso.with(activity).load(R.drawable.ic_no_image).noFade().into(holder.imageView);

                if (!chats.get(position).image.isEmpty()) {
                    //Picasso.with(activity).invalidate(Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + chats.get(position).image);
                    Picasso.with(activity).load(Functions.getString(activity, "GROUP_IMAGES_URL", Information.DEFAULT_GROUP_IMAGES_URL) + chats.get(position).image + "_small.jpg" + "?" + chats.get(position).timestamp).error(R.drawable.ic_no_image).placeholder(R.drawable.ic_no_image).noFade().resize(100, 100).into(holder.imageView);
                    //Functions.loadCachedImage(activity, holder.profileImageView, Functions.getString(activity, "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + user_profile_chats.get(position).image, Information.PROFILE_IMAGES_PATH_PREFIX + user_profile_chats.get(position).image);
                }
            }

            ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> filter = new HashMap<String, String>();
            filter.put("name", "chat");
            filter.put("method", "equal");
            filter.put("value", String.valueOf(chats.get(position).id));
            filters.add(filter);
            filter = new HashMap<String, String>();
            filter.put("name", "type");
            filter.put("method", "not-equal");
            filter.put("value", "deleted");
            filters.add(filter);

            List<ChatsMessage> messages = dbHelper.searchChatsMessages(filters, "id", 0, 1, true, true);
            if (messages.size() > 0) {
                String message = "";
                if (messages.get(0).action.equals("mime-image")) {
                    if (chats.get(position).type.equals("single")) {
                        if (messages.get(0).sender.equals(Functions.getString(activity, "id", ""))) {
                            message += "شما" + ":  ";
                        }
                    } else if (chats.get(position).type.equals("group")) {
                        if (messages.get(0).sender.equals(Functions.getString(activity, "id", ""))) {
                            message += "شما" + ":  ";
                        } else {
                            message += messages.get(0).sender_name + ":  ";
                        }
                    }
                    message += "تصویر";
                } else if (messages.get(0).action.equals("message")) {
                    if (chats.get(position).type.equals("single")) {
                        if (messages.get(0).sender.equals(Functions.getString(activity, "id", ""))) {
                            message += "شما" + ":  ";
                        }
                    } else if (chats.get(position).type.equals("group")) {
                        if (messages.get(0).sender.equals(Functions.getString(activity, "id", ""))) {
                            message += "شما" + ":  ";
                        } else {
                            message += messages.get(0).sender_name + ":  ";
                        }
                    }
                    message += messages.get(0).message;
                } else {
                    String actionText = messages.get(0).sender_name + " ";
                    if (messages.get(0).action.equals("create_group")) {
                        actionText += "گروه را ایجاد کرد.";
                    } else if (messages.get(0).action.equals("change_name")) {
                        actionText += "نام گروه را به «" + messages.get(0).message + "» تغییر داد.";
                    } else if (messages.get(0).action.equals("change_image")) {
                        actionText += "تصویر گروه را عوض کرد.";
                    } else if (messages.get(0).action.equals("join_group")) {
                        actionText += "به گروه ملحق شد.";
                    } else if (messages.get(0).action.equals("leave_group")) {
                        actionText += "گروه را ترک کرد.";
                    } else if (messages.get(0).action.equals("add")) {
                        actionText += messages.get(0).message + " را به گروه اضافه کرد.";
                    } else if (messages.get(0).action.equals("remove")) {
                        actionText += messages.get(0).message + " را از گروه حذف کرد.";
                    }

                    message = actionText;
                }
                holder.messageText.setText(EmojiMapUtil.replaceCheatSheetEmojis(message));
                holder.timeText.setText(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(messages.get(0).timestamp))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(messages.get(0).timestamp), "hh:mm:ss")));
            } else {
                holder.messageText.setText("");
                holder.timeText.setText("");
            }


            holder.nameText.setText(EmojiMapUtil.replaceCheatSheetEmojis(chats.get(position).name));

            filters.clear();
            filter.put("name", "chat");
            filter.put("method", "equal");
            filter.put("value", String.valueOf(chats.get(position).id));
            filters.add(filter);
            filter = new HashMap<>();
            filter.put("name", "status");
            filter.put("method", "equal");
            filter.put("value", "not-read");
            filters.add(filter);

            messages = dbHelper.searchChatsMessages(filters, "id", 0, 0, true, true);
            if (messages.size() > 0) {
                holder.countText.setVisibility(View.VISIBLE);
                holder.countText.setText(String.valueOf(messages.size()));
            } else {
                holder.countText.setText("");
                holder.countText.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return chats.size();
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}