package com.kiosk.android.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kiosk.android.info.Information;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.model.Post;

import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = Information.DB_NAME;
    public static final int DATABASE_VERSION = 12;
    private static final String TAG = "DatabaseHelper";

    private static final String CREATE_TABLE_POSTS = "CREATE TABLE posts (id INTEGER PRIMARY KEY, author TEXT, author_name TEXT, author_profile_image TEXT, author_timestamp TEXT, name TEXT, description TEXT, image TEXT, price TEXT, download_count TEXT, timestamp TEXT, likes TEXT, user_like TEXT, comments TEXT, downloadable TEXT, reading TEXT, saved TEXT)";
    private static final String DROP_TABLE_POSTS = "DROP TABLE IF EXISTS posts";
    private static final String CREATE_TABLE_CHATS = "CREATE TABLE chats (id INTEGER PRIMARY KEY, creator TEXT, type TEXT, participants TEXT, name TEXT, image TEXT, about TEXT, timestamp TEXT, last_timestamp TEXT)";
    private static final String DROP_TABLE_CHATS = "DROP TABLE IF EXISTS chats";
    private static final String CREATE_TABLE_CHATS_MESSAGES = "CREATE TABLE chats_messages (id INTEGER PRIMARY KEY, chat TEXT, sender TEXT, sender_name TEXT, sender_profile_image TEXT, action TEXT, message TEXT, extras TEXT, timestamp TEXT, status TEXT, type TEXT)";
    private static final String DROP_TABLE_CHATS_MESSAGES = "DROP TABLE IF EXISTS chats_messages";

    private static final String TABLE_POSTS = "posts";
    private static final String TABLE_CHATS = "chats";
    private static final String TABLE_CHATS_MESSAGES = "chats_messages";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void closeDB() {
        SQLiteDatabase localSQLiteDatabase = getReadableDatabase();
        if ((localSQLiteDatabase != null) && (localSQLiteDatabase.isOpen()))
            localSQLiteDatabase.close();
    }

    @Override
    public void onCreate(SQLiteDatabase paramSQLiteDatabase) {
        paramSQLiteDatabase.execSQL(CREATE_TABLE_POSTS);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_CHATS);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_CHATS_MESSAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {
        paramSQLiteDatabase.execSQL(DROP_TABLE_POSTS);
        paramSQLiteDatabase.execSQL(DROP_TABLE_CHATS);
        paramSQLiteDatabase.execSQL(DROP_TABLE_CHATS_MESSAGES);

        onCreate(paramSQLiteDatabase);
    }


    // POSTS

    public boolean checkPost(int id) {
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT * FROM posts WHERE id = " + id, null);

        boolean exists = (localCursor.getCount() > 0);
        localCursor.close();
        Log.d(TAG, "[checkPost] exists? " + exists + " id: " + id);
        return exists;
    }

    public long createPost(Post post) {

        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", post.id);
        localContentValues.put("author", post.author);
        localContentValues.put("author_name", post.author_name);
        localContentValues.put("author_profile_image", post.author_profile_image);
        localContentValues.put("author_timestamp", post.author_timestamp);
        localContentValues.put("name", post.name);
        localContentValues.put("description", post.description);
        localContentValues.put("image", post.image);
        localContentValues.put("price", post.price);
        localContentValues.put("download_count", post.download_count);
        localContentValues.put("timestamp", post.timestamp);
        localContentValues.put("likes", post.likes);
        localContentValues.put("user_like", post.user_like);
        localContentValues.put("comments", post.comments);
        localContentValues.put("downloadable", post.downloadable);
        localContentValues.put("reading", post.reading);
        localContentValues.put("saved", post.saved);

        long l = localSQLiteDatabase.insert(TABLE_POSTS, null, localContentValues);
        Log.d(TAG, "new post created. id = " + String.valueOf(l));
        return l;
    }

    public void deletePost(int id) {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String[] arrayOfString = new String[1];
        arrayOfString[0] = String.valueOf(id);
        localSQLiteDatabase.delete(TABLE_POSTS, "id = ?", arrayOfString);
        Log.d(TAG, "post deleted. id = " + String.valueOf(id));
    }

    public ArrayList<Post> getAllPosts() {
        ArrayList<Post> localArrayList = new ArrayList<Post>();
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT * FROM posts", null);
        if (localCursor.moveToFirst()) do {
            Post post = new Post();

            post.id = localCursor.getInt(localCursor.getColumnIndex("id"));
            post.author = localCursor.getString(localCursor.getColumnIndex("author"));
            post.name = localCursor.getString(localCursor.getColumnIndex("name"));
            post.author_name = localCursor.getString(localCursor.getColumnIndex("author_name"));
            post.author_profile_image = localCursor.getString(localCursor.getColumnIndex("author_profile_image"));
            post.author_timestamp = localCursor.getString(localCursor.getColumnIndex("author_timestamp"));
            post.description = localCursor.getString(localCursor.getColumnIndex("description"));
            post.image = localCursor.getString(localCursor.getColumnIndex("image"));
            post.price = localCursor.getString(localCursor.getColumnIndex("price"));
            post.download_count = localCursor.getString(localCursor.getColumnIndex("download_count"));
            post.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
            post.likes = localCursor.getString(localCursor.getColumnIndex("likes"));
            post.user_like = localCursor.getString(localCursor.getColumnIndex("user_like"));
            post.comments = localCursor.getString(localCursor.getColumnIndex("comments"));
            post.downloadable = localCursor.getString(localCursor.getColumnIndex("downloadable"));
            post.reading = localCursor.getString(localCursor.getColumnIndex("reading"));
            post.saved = localCursor.getString(localCursor.getColumnIndex("saved"));

            localArrayList.add(post);
        } while (localCursor.moveToNext());
        Log.d(TAG, "all posts fetched.");
        return localArrayList;
    }

    public Post getPost(int id) {
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT  * FROM posts WHERE id = " + id, null);
        if (localCursor != null) localCursor.moveToFirst();
        Post post = new Post();

        post.id = localCursor.getInt(localCursor.getColumnIndex("id"));
        post.author = localCursor.getString(localCursor.getColumnIndex("author"));
        post.name = localCursor.getString(localCursor.getColumnIndex("name"));
        post.author_name = localCursor.getString(localCursor.getColumnIndex("author_name"));
        post.author_profile_image = localCursor.getString(localCursor.getColumnIndex("author_profile_image"));
        post.author_timestamp = localCursor.getString(localCursor.getColumnIndex("author_timestamp"));
        post.description = localCursor.getString(localCursor.getColumnIndex("description"));
        post.image = localCursor.getString(localCursor.getColumnIndex("image"));
        post.price = localCursor.getString(localCursor.getColumnIndex("price"));
        post.download_count = localCursor.getString(localCursor.getColumnIndex("download_count"));
        post.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
        post.likes = localCursor.getString(localCursor.getColumnIndex("likes"));
        post.user_like = localCursor.getString(localCursor.getColumnIndex("user_like"));
        post.comments = localCursor.getString(localCursor.getColumnIndex("comments"));
        post.downloadable = localCursor.getString(localCursor.getColumnIndex("downloadable"));
        post.reading = localCursor.getString(localCursor.getColumnIndex("reading"));
        post.saved = localCursor.getString(localCursor.getColumnIndex("saved"));

        Log.d(TAG, "post fetched. name = " + post.name);
        return post;
    }

    public int updatePost(Post post) {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", post.id);
        localContentValues.put("author", post.author);
        localContentValues.put("author_name", post.author_name);
        localContentValues.put("author_profile_image", post.author_profile_image);
        localContentValues.put("author_timestamp", post.author_timestamp);
        localContentValues.put("name", post.name);
        localContentValues.put("description", post.description);
        localContentValues.put("image", post.image);
        localContentValues.put("price", post.price);
        localContentValues.put("download_count", post.download_count);
        localContentValues.put("timestamp", post.timestamp);
        localContentValues.put("likes", post.likes);
        localContentValues.put("user_like", post.user_like);
        localContentValues.put("comments", post.comments);
        localContentValues.put("downloadable", post.downloadable);
        localContentValues.put("reading", post.reading);
        localContentValues.put("saved", post.saved);

        Log.d(TAG, "post updated. id = " + String.valueOf(post.id));
        String[] arrayOfString = new String[1];
        arrayOfString[0] = String.valueOf(post.id);
        return localSQLiteDatabase.update(TABLE_POSTS, localContentValues, "id = ?", arrayOfString);
    }

    public ArrayList<Post> searchPosts(ArrayList<HashMap<String, String>> filters, String sort, int limit_start, int limit_amount, boolean desc, boolean and_method) {
        ArrayList<Post> posts = new ArrayList<Post>();

        String method = "AND";
        if (!and_method) {
            method = "OR";
        }

        String selectQuery = "SELECT * FROM " + TABLE_POSTS;
        if (filters != null) {
            selectQuery += " WHERE";
            for (HashMap<String, String> filter : filters) {
                if (filter.get("method").equals("like")) {
                    selectQuery += " " + filter.get("name") + " LIKE \"%" + filter.get("value") + "%\"";
                } else if (filter.get("method").equals("equal")) {
                    selectQuery += " " + filter.get("name") + " = \"" + filter.get("value") + "\"";
                } else if (filter.get("method").equals("not-equal")) {
                    selectQuery += " " + filter.get("name") + " != \"" + filter.get("value") + "\"";
                }
                selectQuery += " " + method;
            }
            selectQuery = selectQuery.substring(0, selectQuery.length() - method.length());
        }

        if (sort.isEmpty()) {
            sort = "id";
        }
        selectQuery += " ORDER BY " + sort;

        if (desc) {
            selectQuery += " DESC";
        } else {
            selectQuery += " ASC";
        }

        if (limit_amount > 0) {
            selectQuery += " LIMIT " + limit_start + ", " + limit_amount;
        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor localCursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (localCursor.moveToFirst()) {
            do {
                Post post = new Post();

                post.id = localCursor.getInt(localCursor.getColumnIndex("id"));
                post.author = localCursor.getString(localCursor.getColumnIndex("author"));
                post.name = localCursor.getString(localCursor.getColumnIndex("name"));
                post.author_name = localCursor.getString(localCursor.getColumnIndex("author_name"));
                post.author_profile_image = localCursor.getString(localCursor.getColumnIndex("author_profile_image"));
                post.author_timestamp = localCursor.getString(localCursor.getColumnIndex("author_timestamp"));
                post.description = localCursor.getString(localCursor.getColumnIndex("description"));
                post.image = localCursor.getString(localCursor.getColumnIndex("image"));
                post.price = localCursor.getString(localCursor.getColumnIndex("price"));
                post.download_count = localCursor.getString(localCursor.getColumnIndex("download_count"));
                post.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
                post.likes = localCursor.getString(localCursor.getColumnIndex("likes"));
                post.user_like = localCursor.getString(localCursor.getColumnIndex("user_like"));
                post.comments = localCursor.getString(localCursor.getColumnIndex("comments"));
                post.downloadable = localCursor.getString(localCursor.getColumnIndex("downloadable"));
                post.reading = localCursor.getString(localCursor.getColumnIndex("reading"));
                post.saved = localCursor.getString(localCursor.getColumnIndex("saved"));

                // adding to list
                posts.add(post);
            } while (localCursor.moveToNext());
        }

        Log.d(TAG, "searched posts. found " + posts.size() + " items. query = \n" + selectQuery);

        return posts;
    }


    // CHATS

    public boolean checkChat(int id) {
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT * FROM chats WHERE id = " + id, null);

        boolean exists = (localCursor.getCount() > 0);
        localCursor.close();
        Log.d(TAG, "[checkChat] exists? " + exists + " id: " + id);
        return exists;
    }

    public long createChat(Chat chat) {

        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", chat.id);
        localContentValues.put("creator", chat.creator);
        localContentValues.put("type", chat.type);
        localContentValues.put("participants", chat.participants);
        localContentValues.put("name", chat.name);
        localContentValues.put("image", chat.image);
        localContentValues.put("about", chat.about);
        localContentValues.put("timestamp", chat.timestamp);
        localContentValues.put("last_timestamp", chat.last_timestamp);

        long l = localSQLiteDatabase.insert(TABLE_CHATS, null, localContentValues);
        Log.d(TAG, "new chat created. id = " + String.valueOf(l));
        return l;
    }

    public void deleteAllChats() {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        localSQLiteDatabase.delete(TABLE_CHATS, "1", new String[]{});
        Log.d(TAG, "chats deleted.");
    }

    public void deleteChat(int id) {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String[] arrayOfString = new String[1];
        arrayOfString[0] = String.valueOf(id);
        localSQLiteDatabase.delete(TABLE_CHATS, "id = ?", arrayOfString);
        Log.d(TAG, "chat deleted. id = " + String.valueOf(id));
    }

    public ArrayList<Chat> getAllChats() {
        ArrayList<Chat> localArrayList = new ArrayList<Chat>();
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT * FROM chats", null);
        if (localCursor.moveToFirst()) do {
            Chat chat = new Chat();

            chat.id = localCursor.getInt(localCursor.getColumnIndex("id"));
            chat.creator = localCursor.getString(localCursor.getColumnIndex("creator"));
            chat.type = localCursor.getString(localCursor.getColumnIndex("type"));
            chat.participants = localCursor.getString(localCursor.getColumnIndex("participants"));
            chat.name = localCursor.getString(localCursor.getColumnIndex("name"));
            chat.image = localCursor.getString(localCursor.getColumnIndex("image"));
            chat.about = localCursor.getString(localCursor.getColumnIndex("about"));
            chat.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
            chat.last_timestamp = localCursor.getString(localCursor.getColumnIndex("last_timestamp"));

            localArrayList.add(chat);
        } while (localCursor.moveToNext());
        Log.d(TAG, "all chats fetched.");
        return localArrayList;
    }

    public Chat getChat(int id) {
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT  * FROM chats WHERE id = " + id, null);
        if (localCursor != null) localCursor.moveToFirst();
        Chat chat = new Chat();

        chat.id = localCursor.getInt(localCursor.getColumnIndex("id"));
        chat.creator = localCursor.getString(localCursor.getColumnIndex("creator"));
        chat.type = localCursor.getString(localCursor.getColumnIndex("type"));
        chat.participants = localCursor.getString(localCursor.getColumnIndex("participants"));
        chat.name = localCursor.getString(localCursor.getColumnIndex("name"));
        chat.image = localCursor.getString(localCursor.getColumnIndex("image"));
        chat.about = localCursor.getString(localCursor.getColumnIndex("about"));
        chat.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
        chat.last_timestamp = localCursor.getString(localCursor.getColumnIndex("last_timestamp"));

        Log.d(TAG, "chat fetched. name = " + chat.name);
        return chat;
    }

    public int updateChat(Chat chat) {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", chat.id);
        localContentValues.put("creator", chat.creator);
        localContentValues.put("type", chat.type);
        localContentValues.put("participants", chat.participants);
        localContentValues.put("name", chat.name);
        localContentValues.put("image", chat.image);
        localContentValues.put("about", chat.about);
        localContentValues.put("timestamp", chat.timestamp);
        localContentValues.put("last_timestamp", chat.last_timestamp);

        Log.d(TAG, "chat updated. id = " + String.valueOf(chat.id));
        String[] arrayOfString = new String[1];
        arrayOfString[0] = String.valueOf(chat.id);
        return localSQLiteDatabase.update(TABLE_CHATS, localContentValues, "id = ?", arrayOfString);
    }

    public ArrayList<Chat> searchChats(ArrayList<HashMap<String, String>> filters, String sort, int limit_start, int limit_amount, boolean desc, boolean and_method) {
        ArrayList<Chat> chats = new ArrayList<Chat>();

        String method = "AND";
        if (!and_method) {
            method = "OR";
        }

        String selectQuery = "SELECT * FROM " + TABLE_CHATS;
        if (filters != null) {
            selectQuery += " WHERE";
            for (HashMap<String, String> filter : filters) {
                if (filter.get("method").equals("like")) {
                    selectQuery += " " + filter.get("name") + " LIKE \"%" + filter.get("value") + "%\"";
                } else if (filter.get("method").equals("equal")) {
                    selectQuery += " " + filter.get("name") + " = \"" + filter.get("value") + "\"";
                } else if (filter.get("method").equals("not-equal")) {
                    selectQuery += " " + filter.get("name") + " != \"" + filter.get("value") + "\"";
                }
                selectQuery += " " + method;
            }
            selectQuery = selectQuery.substring(0, selectQuery.length() - method.length());
        }

        if (sort.isEmpty()) {
            sort = "id";
        }
        selectQuery += " ORDER BY " + sort;

        if (desc) {
            selectQuery += " DESC";
        } else {
            selectQuery += " ASC";
        }

        if (limit_amount > 0) {
            selectQuery += " LIMIT " + limit_start + ", " + limit_amount;
        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor localCursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (localCursor.moveToFirst()) {
            do {
                Chat chat = new Chat();

                chat.id = localCursor.getInt(localCursor.getColumnIndex("id"));
                chat.creator = localCursor.getString(localCursor.getColumnIndex("creator"));
                chat.type = localCursor.getString(localCursor.getColumnIndex("type"));
                chat.participants = localCursor.getString(localCursor.getColumnIndex("participants"));
                chat.name = localCursor.getString(localCursor.getColumnIndex("name"));
                chat.image = localCursor.getString(localCursor.getColumnIndex("image"));
                chat.about = localCursor.getString(localCursor.getColumnIndex("about"));
                chat.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
                chat.last_timestamp = localCursor.getString(localCursor.getColumnIndex("last_timestamp"));

                // adding to list
                chats.add(chat);
            } while (localCursor.moveToNext());
        }

        Log.d(TAG, "searched chats. found " + chats.size() + " items. query = \n" + selectQuery);

        return chats;
    }

    // CHATS MESSAGES

    public boolean checkChatsMessage(int id) {
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT * FROM chats_messages WHERE id = " + id, null);

        boolean exists = (localCursor.getCount() > 0);
        localCursor.close();
        Log.d(TAG, "[checkChatsMessage] exists? " + exists + " id: " + id);
        return exists;
    }

    public long createChatsMessage(ChatsMessage chats_message) {

        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", chats_message.id);
        localContentValues.put("chat", chats_message.chat);
        localContentValues.put("sender", chats_message.sender);
        localContentValues.put("sender_name", chats_message.sender_name);
        localContentValues.put("sender_profile_image", chats_message.sender_profile_image);
        localContentValues.put("action", chats_message.action);
        localContentValues.put("message", chats_message.message);
        localContentValues.put("extras", chats_message.extras);
        localContentValues.put("timestamp", chats_message.timestamp);
        localContentValues.put("status", chats_message.status);
        localContentValues.put("type", chats_message.type);

        long l = localSQLiteDatabase.insert(TABLE_CHATS_MESSAGES, null, localContentValues);
        Log.d(TAG, "new chats_message created. id = " + String.valueOf(l));
        return l;
    }

    public void deleteChatsMessage(int id) {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String[] arrayOfString = new String[1];
        arrayOfString[0] = String.valueOf(id);
        localSQLiteDatabase.delete(TABLE_CHATS_MESSAGES, "id = ?", arrayOfString);
        Log.d(TAG, "chats_message deleted. id = " + String.valueOf(id));
    }

    public ArrayList<ChatsMessage> getAllChatsMessages() {
        ArrayList<ChatsMessage> localArrayList = new ArrayList<ChatsMessage>();
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT * FROM chats_messages", null);
        if (localCursor.moveToFirst()) do {
            ChatsMessage chats_message = new ChatsMessage();

            chats_message.id = localCursor.getInt(localCursor.getColumnIndex("id"));
            chats_message.chat = localCursor.getString(localCursor.getColumnIndex("chat"));
            chats_message.sender = localCursor.getString(localCursor.getColumnIndex("sender"));
            chats_message.sender_name = localCursor.getString(localCursor.getColumnIndex("sender_name"));
            chats_message.sender_profile_image = localCursor.getString(localCursor.getColumnIndex("sender_profile_image"));
            chats_message.action = localCursor.getString(localCursor.getColumnIndex("action"));
            chats_message.message = localCursor.getString(localCursor.getColumnIndex("message"));
            chats_message.extras = localCursor.getString(localCursor.getColumnIndex("extras"));
            chats_message.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
            chats_message.status = localCursor.getString(localCursor.getColumnIndex("status"));
            chats_message.type = localCursor.getString(localCursor.getColumnIndex("type"));

            localArrayList.add(chats_message);
        } while (localCursor.moveToNext());
        Log.d(TAG, "all chats_messages fetched.");
        return localArrayList;
    }

    public ChatsMessage getChatsMessage(int id) {
        Cursor localCursor = getReadableDatabase().rawQuery("SELECT  * FROM chats_messages WHERE id = " + id, null);
        if (localCursor != null) localCursor.moveToFirst();
        ChatsMessage chats_message = new ChatsMessage();

        chats_message.id = localCursor.getInt(localCursor.getColumnIndex("id"));
        chats_message.chat = localCursor.getString(localCursor.getColumnIndex("chat"));
        chats_message.sender = localCursor.getString(localCursor.getColumnIndex("sender"));
        chats_message.sender_name = localCursor.getString(localCursor.getColumnIndex("sender_name"));
        chats_message.sender_profile_image = localCursor.getString(localCursor.getColumnIndex("sender_profile_image"));
        chats_message.action = localCursor.getString(localCursor.getColumnIndex("action"));
        chats_message.message = localCursor.getString(localCursor.getColumnIndex("message"));
        chats_message.extras = localCursor.getString(localCursor.getColumnIndex("extras"));
        chats_message.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
        chats_message.status = localCursor.getString(localCursor.getColumnIndex("status"));
        chats_message.type = localCursor.getString(localCursor.getColumnIndex("type"));

        Log.d(TAG, "chats_message fetched. message = " + chats_message.message);
        return chats_message;
    }

    public int updateChatsMessage(ChatsMessage chats_message) {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", chats_message.id);
        localContentValues.put("chat", chats_message.chat);
        localContentValues.put("sender", chats_message.sender);
        localContentValues.put("sender_name", chats_message.sender_name);
        localContentValues.put("sender_profile_image", chats_message.sender_profile_image);
        localContentValues.put("action", chats_message.action);
        localContentValues.put("message", chats_message.message);
        localContentValues.put("extras", chats_message.extras);
        localContentValues.put("timestamp", chats_message.timestamp);
        localContentValues.put("status", chats_message.status);
        localContentValues.put("type", chats_message.type);


        Log.d(TAG, "chats_message updated. id = " + String.valueOf(chats_message.id));
        String[] arrayOfString = new String[1];
        arrayOfString[0] = String.valueOf(chats_message.id);
        return localSQLiteDatabase.update(TABLE_CHATS_MESSAGES, localContentValues, "id = ?", arrayOfString);
    }

    public ArrayList<ChatsMessage> searchChatsMessages(ArrayList<HashMap<String, String>> filters, String sort, int limit_start, int limit_amount, boolean desc, boolean and_method) {
        ArrayList<ChatsMessage> chats_messages = new ArrayList<ChatsMessage>();

        String method = "AND";
        if (!and_method) {
            method = "OR";
        }

        String selectQuery = "SELECT * FROM " + TABLE_CHATS_MESSAGES;
        if (filters != null) {
            selectQuery += " WHERE";
            for (HashMap<String, String> filter : filters) {
                if (filter.get("method").equals("like")) {
                    selectQuery += " " + filter.get("name") + " LIKE \"%" + filter.get("value") + "%\"";
                } else if (filter.get("method").equals("equal")) {
                    selectQuery += " " + filter.get("name") + " = \"" + filter.get("value") + "\"";
                } else if (filter.get("method").equals("not-equal")) {
                    selectQuery += " " + filter.get("name") + " != \"" + filter.get("value") + "\"";
                }
                selectQuery += " " + method;
            }
            selectQuery = selectQuery.substring(0, selectQuery.length() - method.length());
        }

        if (sort.isEmpty()) {
            sort = "id";
        }
        selectQuery += " ORDER BY " + sort;

        if (desc) {
            selectQuery += " DESC";
        } else {
            selectQuery += " ASC";
        }

        if (limit_amount > 0) {
            selectQuery += " LIMIT " + limit_start + ", " + limit_amount;
        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor localCursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (localCursor.moveToFirst()) {
            do {
                ChatsMessage chats_message = new ChatsMessage();

                chats_message.id = localCursor.getInt(localCursor.getColumnIndex("id"));
                chats_message.chat = localCursor.getString(localCursor.getColumnIndex("chat"));
                chats_message.sender = localCursor.getString(localCursor.getColumnIndex("sender"));
                chats_message.sender_name = localCursor.getString(localCursor.getColumnIndex("sender_name"));
                chats_message.sender_profile_image = localCursor.getString(localCursor.getColumnIndex("sender_profile_image"));
                chats_message.action = localCursor.getString(localCursor.getColumnIndex("action"));
                chats_message.message = localCursor.getString(localCursor.getColumnIndex("message"));
                chats_message.timestamp = localCursor.getString(localCursor.getColumnIndex("timestamp"));
                chats_message.extras = localCursor.getString(localCursor.getColumnIndex("extras"));
                chats_message.status = localCursor.getString(localCursor.getColumnIndex("status"));
                chats_message.type = localCursor.getString(localCursor.getColumnIndex("type"));

                // adding to list
                chats_messages.add(chats_message);
            } while (localCursor.moveToNext());
        }

        Log.d(TAG, "searched chats_messages. found " + chats_messages.size() + " items. query = \n" + selectQuery);

        return chats_messages;
    }

}