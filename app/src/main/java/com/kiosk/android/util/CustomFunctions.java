package com.kiosk.android.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.kiosk.android.LoginActivity;
import com.kiosk.android.MainActivity;
import com.kiosk.android.R;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.model.ChatsMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by SeyyedMojtaba on 3/4/2016.
 */
public class CustomFunctions {
    public static void Login(final Activity activity) {
        try {
            String result = Fetcher.FetchURL(APIManager.Login(Functions.getString(activity, "username", ""), Functions.getString(activity, "password", ""), Functions.getString(activity, "gcm_token", "")));
            if (result.contains("login")) {
                String id = JSONParser.getListFromString(result, "login", "id").get(0);
                String username = JSONParser.getListFromString(result, "login", "username").get(0);
                String password = JSONParser.getListFromString(result, "login", "password").get(0);
                String email = JSONParser.getListFromString(result, "login", "email").get(0);
                String profile_image = JSONParser.getListFromString(result, "login", "profile_image").get(0);
                String first_name = JSONParser.getListFromString(result, "login", "first_name").get(0);
                String last_name = JSONParser.getListFromString(result, "login", "last_name").get(0);
                String status = JSONParser.getListFromString(result, "login", "status").get(0);
                String mobile_number = JSONParser.getListFromString(result, "login", "mobile_number").get(0);
                String level = JSONParser.getListFromString(result, "login", "level").get(0);
                String balance = JSONParser.getListFromString(result, "login", "balance").get(0);
                String shaba = JSONParser.getListFromString(result, "login", "shaba").get(0);
                String about = JSONParser.getListFromString(result, "login", "about").get(0);
                String extras = JSONParser.getListFromString(result, "login", "extras").get(0);
                String timestamp = JSONParser.getListFromString(result, "login", "timestamp").get(0);
                String posts = JSONParser.getListFromString(result, "login", "posts").get(0);
                String followers = JSONParser.getListFromString(result, "login", "followers").get(0);
                String followeds = JSONParser.getListFromString(result, "login", "followeds").get(0);

                Functions.putBoolean(activity, "logged in", true);
                Functions.putString(activity, "username", username);
                Functions.putString(activity, "id", id);
                Functions.putString(activity, "password", password);
                Functions.putString(activity, "email", email);
                Functions.putString(activity, "profile_image", profile_image);
                Functions.putString(activity, "first_name", EmojiMapUtil.replaceCheatSheetEmojis(first_name));
                Functions.putString(activity, "last_name", last_name);
                Functions.putString(activity, "status", EmojiMapUtil.replaceCheatSheetEmojis(status));
                Functions.putString(activity, "mobile_number", mobile_number);
                Functions.putString(activity, "level", level);
                Functions.putString(activity, "balance", balance);
                Functions.putString(activity, "shaba", shaba);
                Functions.putString(activity, "about", EmojiMapUtil.replaceCheatSheetEmojis(about));
                Functions.putString(activity, "extras", extras);
                Functions.putString(activity, "timestamp", timestamp);
                Functions.putString(activity, "posts", posts);
                Functions.putString(activity, "followers", followers);
                Functions.putString(activity, "followeds", followeds);

            } else if (result.contains("incorrect")) {
                Functions.putBoolean(activity, "logged in", false);
                Functions.putString(activity, "username", "");
                Functions.putString(activity, "id", "");
                Functions.putString(activity, "password", "");
                Functions.putString(activity, "email", "");
                Functions.putString(activity, "profile_image", "");
                Functions.putString(activity, "first_name", "");
                Functions.putString(activity, "last_name", "");
                Functions.putString(activity, "status", "");
                Functions.putString(activity, "mobile_number", "");
                Functions.putString(activity, "level", "");
                Functions.putString(activity, "balance", "");
                Functions.putString(activity, "shaba", "");
                Functions.putString(activity, "about", "");
                Functions.putString(activity, "extras", "");
                Functions.putString(activity, "posts", "");
                Functions.putString(activity, "followers", "");
                Functions.putString(activity, "followeds", "");
                Functions.putString(activity, "gcm_token", "");
                activity.startActivity(new Intent(activity, LoginActivity.class));
                activity.finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Spanned getHtml(Context context, String text) {

        // CHECK LINES
        String html = text.replace("\n", "<br>");


        // CHECK EMAILS
        Matcher matcher = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+").matcher(html);

        while (matcher.find()) {
            //Log.d("tag", "email found: " + matcher.group());
            html = html.replace(matcher.group(), "<a href=\"mailto:" + matcher.group() + "\"><b>" + matcher.group() + "</b></a>");
        }

        // CHECK WEBSITE
        // (https?:\/\/|\@|<a href="|\">)*?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-\?\&]*)*[!\n]?\b
        //"\\www\\.+(\\w)*\\.(\\w)+[^\\<>#]+"

        matcher = Pattern.compile("(<a|@)+.*|(https?:\\/\\/)?([\\da-z-]+)\\.([a-z]{2,6})([\\/\\w\\.-?\\&]*)*\\/?").matcher(html);

        while (matcher.find()) {
            if (matcher.group().startsWith("@") || matcher.group().startsWith("<a")) {
                continue;
            }
            //Log.d("tag", "website found: " + matcher.group());
            html = html.replace(matcher.group(), "<a href=\"http://" + matcher.group().replaceFirst("http://", "").replaceFirst("https://", "") + "\"><b>" + matcher.group().replaceFirst("http://", "").replaceFirst("https://", "") + "</b></a>");
        }

        //Log.d("tag", "former html: " + html);

        for (String word : ((" <br>" + html).replace("<br>", "<br> ")).split(" ")) {
            // USERS
            if (word.startsWith("@")) {
                //Log.d("tag", "user found: " + word.substring(1));
                html = html.replace(word, "<a href=\"" + context.getResources().getString(R.string.app_url_scheme) + "://" + context.getResources().getString(R.string.system_home_url_host) + context.getResources().getString(R.string.profile_view_path_prefix) + "?username=" + word.substring(1) + "\"><b>" + word + "</b></a>");
            }

            // HASHTAGS
            if (word.startsWith("#")) {
                //Log.d("tag", "tag found: " + word.substring(1));
                html = html.replace(word, "<a href=\"" + context.getResources().getString(R.string.app_url_scheme) + "://" + context.getResources().getString(R.string.system_home_url_host) + context.getResources().getString(R.string.tag_search_path_prefix) + "?tag=" + word.substring(1) + "\"><b>" + word + "</b></a>");
            }
        }

        //Log.d("tag", "html: " + html);

        return Html.fromHtml(html);
    }

    public static Spanned getDescriptionHtml(Context context, String id, String description) {
        String html = "";
        String[] splitted = description.split((" "));

        if (splitted.length > 20) {
            for (int i = 0; i < 20; i++) {
                html += splitted[i] + " ";
            }
            html += " ...&ensp;‏‏‏‏<b><a href=\"" + context.getResources().getString(R.string.system_home_url_scheme) + "://" + context.getResources().getString(R.string.system_home_url_host) + context.getResources().getString(R.string.post_view_path_prefix) + "?id=" + id + "\">" + "بیشتر" + "</b></a>";
        } else {
            html = description;
        }

        return getHtml(context, html);
    }

    public static Spanned getCommentHtml(Context context, String author, String author_name, String comment) {
        String html = "\u200F<a href=\"" + context.getResources().getString(R.string.app_url_scheme) + "://" + context.getResources().getString(R.string.system_home_url_host) + context.getResources().getString(R.string.profile_view_path_prefix) + "?id=" + author + "\"><b>" + author_name + "</b></a>&emsp;\u200F " + comment;

        return getHtml(context, html);
    }



    public static void startUpFunctions(Activity activity) {
        String[] permissions = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.MODIFY_AUDIO_SETTINGS
        };
        Functions.verifyPermissions(activity, permissions, true);
    }

    public static void setBadge(Context context, DatabaseHelper dbHelper) {
        if (dbHelper == null)
            return;


        // SET BADGE FOR UNREAD MESSAGES
        ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
        HashMap filter = new HashMap<>();
        filter.put("name", "status");
        filter.put("method", "equal");
        filter.put("value", "not-read");
        filters.add(filter);
        filter = new HashMap<String, String>();
        filter.put("name", "type");
        filter.put("method", "not-equal");
        filter.put("value", "deleted");
        filters.add(filter);

        List<ChatsMessage> messages = dbHelper.searchChatsMessages(filters, "id", 0, 0, true, true);
        ShortcutBadger.applyCount(context, messages.size());
        if (messages.size() > 0) {
            MainActivity.new_message = true;
        } else {
            MainActivity.new_message = false;
        }
    }
}
