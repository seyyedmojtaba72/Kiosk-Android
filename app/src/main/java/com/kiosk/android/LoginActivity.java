package com.kiosk.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.Information;
import com.kiosk.android.util.CustomFunctions;

import java.util.Timer;
import java.util.TimerTask;


public class LoginActivity extends AppCompatActivity {

    private int GOOGLE_SIGN = 1;

    private boolean isLogin = true;

    private int trans_num = 0;
    private int trans_num2 = 1;
    private int[] trans_drawables = new int[]{R.drawable.gradient_bg1, R.drawable.gradient_bg2, R.drawable.gradient_bg3, R.drawable.gradient_bg4, R.drawable.gradient_bg5, R.drawable.gradient_bg6};
    private int trans_time = 10000;
    private int item_time = 10000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private boolean isProcessing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        animateBackground();

        // GCM REGISTRATION
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.INTENT_REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");
                    Functions.putString(getApplicationContext(), "gcm_token", token);

                }
            }
        };


        final TextView hintText = (TextView) findViewById(R.id.hint);

        final EditText usernameText = (EditText) findViewById(R.id.username);
        final EditText emailText = (EditText) findViewById(R.id.email);
        final EditText passwordText = (EditText) findViewById(R.id.password);

        final Button loginButton = (Button) findViewById(R.id.login);
        final Button forgetButton = (Button) findViewById(R.id.forget);


        final Button loginToggle = (Button) findViewById(R.id.loginToggle);
        loginToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.setBackgroundResource(R.drawable.selector_button_glass_white);
                if (isLogin) {
                    emailText.setVisibility(View.VISIBLE);
                    forgetButton.setVisibility(View.GONE);
                    loginButton.setText("ثبت نام");
                    hintText.setText("لطفاً موارد خواسته شده را پر کنید.");
                    loginToggle.setText("ورود به حساب کاربری");
                    isLogin = false;
                } else {
                    emailText.setVisibility(View.GONE);
                    forgetButton.setVisibility(View.VISIBLE);
                    loginButton.setText("ورود");
                    hintText.setText("برای ادامه، لطفاً وارد حساب کاربریتان شوید.");
                    loginToggle.setText("حساب کاربری ندارید؟ ثبت نام کنید.");
                    isLogin = true;
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isProcessing) {
                    return;
                }

                if (isLogin) {
                    if (!Functions.isOnline(getApplicationContext())) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.alert_connect_internet), Toast.LENGTH_LONG).show();
                        startActivity(new Intent("android.settings.WIFI_SETTINGS"));
                        return;
                    }

                    if ((usernameText.getText().toString().isEmpty() | passwordText.getText().toString().isEmpty())) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.alert_enter_fields), Toast.LENGTH_LONG).show();
                        return;
                    }

                    new AsyncTask<String, String, String>() {
                        String username = "", password = "";

                        @Override
                        protected void onPreExecute() {
                            loginButton.setBackgroundResource(R.drawable.selector_button_glass_blue);
                            loginButton.setText("در حال ورود...");
                            isProcessing = true;

                            username = usernameText.getText().toString();
                            password = passwordText.getText().toString();

                            super.onPreExecute();
                        }

                        @Override
                        protected String doInBackground(String... strings) {
                            return Fetcher.FetchURL(APIManager.Login(username, password, ""));
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            isProcessing = false;
                            if (result.contains("incorrect")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("نام کاربری یا گذرواژه اشتباه است.");
                            } else if (result.equals("inactive")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("حساب شما غیرفعال است. لطفاً ایمیلتان را چک کنید.");
                            } else if (result.contains("login")) {
                                isProcessing = true;

                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_green);
                                loginButton.setText("خوش آمدید!");

                                String id = JSONParser.getListFromString(result, "login", "id").get(0);
                                String username = JSONParser.getListFromString(result, "login", "username").get(0);
                                String password = JSONParser.getListFromString(result, "login", "password").get(0);
                                String email = JSONParser.getListFromString(result, "login", "email").get(0);
                                String profile_image = JSONParser.getListFromString(result, "login", "profile_image").get(0);
                                String first_name = JSONParser.getListFromString(result, "login", "first_name").get(0);
                                String last_name = JSONParser.getListFromString(result, "login", "last_name").get(0);
                                String status = JSONParser.getListFromString(result, "login", "status").get(0);
                                String mobile_number = JSONParser.getListFromString(result, "login", "mobile_number").get(0);
                                String level = JSONParser.getListFromString(result, "login", "level").get(0);
                                String balance = JSONParser.getListFromString(result, "login", "balance").get(0);
                                String shaba = JSONParser.getListFromString(result, "login", "shaba").get(0);
                                String about = JSONParser.getListFromString(result, "login", "about").get(0);
                                String extras = JSONParser.getListFromString(result, "login", "extras").get(0);
                                String timestamp = JSONParser.getListFromString(result, "login", "timestamp").get(0);
                                String posts = JSONParser.getListFromString(result, "login", "posts").get(0);
                                String followers = JSONParser.getListFromString(result, "login", "followers").get(0);
                                String followeds = JSONParser.getListFromString(result, "login", "followeds").get(0);

                                Functions.putBoolean(LoginActivity.this, "logged in", true);
                                Functions.putString(LoginActivity.this, "username", username);
                                Functions.putString(LoginActivity.this, "id", id);
                                Functions.putString(LoginActivity.this, "password", password);
                                Functions.putString(LoginActivity.this, "email", email);
                                Functions.putString(LoginActivity.this, "profile_image", profile_image);
                                Functions.putString(LoginActivity.this, "first_name", EmojiMapUtil.replaceCheatSheetEmojis(first_name));
                                Functions.putString(LoginActivity.this, "last_name", EmojiMapUtil.replaceCheatSheetEmojis(last_name));
                                Functions.putString(LoginActivity.this, "status", status);
                                Functions.putString(LoginActivity.this, "mobile_number", mobile_number);
                                Functions.putString(LoginActivity.this, "level", level);
                                Functions.putString(LoginActivity.this, "balance", balance);
                                Functions.putString(LoginActivity.this, "shaba", shaba);
                                Functions.putString(LoginActivity.this, "about", EmojiMapUtil.replaceCheatSheetEmojis(about));
                                Functions.putString(LoginActivity.this, "extras", extras);
                                Functions.putString(LoginActivity.this, "timestamp", timestamp);
                                Functions.putString(LoginActivity.this, "posts", posts);
                                Functions.putString(LoginActivity.this, "followers", followers);
                                Functions.putString(LoginActivity.this, "followeds", followeds);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                        finish();
                                    }
                                }, 2000);
                            } else {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("خطای غیرمنتظره‌ای پیش آمده است!");
                            }

                            super.onPostExecute(result);
                        }
                    }.execute();
                } else {
                    if (!Functions.isOnline(getApplicationContext())) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_connect_internet), Toast.LENGTH_LONG).show();
                        startActivity(new Intent("android.settings.WIFI_SETTINGS"));
                        return;
                    }

                    if ((usernameText.getText().toString().isEmpty() || emailText.getText().toString().isEmpty() || passwordText.getText().toString().isEmpty())) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.alert_enter_fields), Toast.LENGTH_LONG).show();
                        return;
                    }

                    new AsyncTask<String, String, String>() {
                        String username = "", email = "", password = "";

                        @Override
                        protected void onPreExecute() {
                            loginButton.setBackgroundResource(R.drawable.selector_button_glass_blue);
                            loginButton.setText("در حال ثبت نام...");
                            isProcessing = true;

                            username = usernameText.getText().toString();
                            email = emailText.getText().toString();
                            password = passwordText.getText().toString();

                            super.onPreExecute();
                        }

                        @Override
                        protected String doInBackground(String... strings) {
                            return Fetcher.FetchURL(APIManager.Register(username, email, password));
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            isProcessing = false;
                            if (result.equals("invalid-username")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("نام کاربری باید شامل حروف انگلیسی و اعداد باشد.");
                            } else if (result.equals("inactive")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("حساب شما غیرفعال است. لطفاً ایمیلتان را چک کنید.");
                            } else if (result.equals("username-exists")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("کاربری با این نام کاربری موجود است.");
                            } else if (result.equals("max-username-length")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("طول نام کاربری باید حداکثر 50 کاراکتر باشد.");
                            } else if (result.equals("invalid-email")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("ایمیل معتبر نیست.");
                            } else if (result.equals("email-exists")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("کاربری با این ایمیل موجود است.");
                            } else if (result.equals("max-email-length")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("طول آدرس ایمیل باید حداکثر 50 کاراکتر باشد!");
                            } else if (result.equals("min-length")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("طول نام کاربری و گذرواژه باید حداقل 6 کاراکتر باشد.");
                            } else if (result.equals("check-email")) {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_green);
                                loginButton.setText("ایمیلتان را چک کنید.");
                            } else if (result.equals("login")) {
                                isProcessing = true;

                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_green);
                                loginButton.setText("خوش آمدید");

                                Functions.putBoolean(LoginActivity.this, "logged in", true);
                                Functions.putString(LoginActivity.this, "username", usernameText.getText().toString());
                                Functions.putString(LoginActivity.this, "email", emailText.getText().toString());
                                Functions.putString(LoginActivity.this, "password", passwordText.getText().toString());
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        new AsyncTask<Void, Void, Void>() {

                                            @Override
                                            protected void onPreExecute() {
                                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_green);
                                                loginButton.setText("در حال ورود...");

                                                super.onPreExecute();
                                            }

                                            @Override
                                            protected Void doInBackground(Void... voids) {
                                                CustomFunctions.Login(LoginActivity.this);
                                                return null;
                                            }

                                            @Override
                                            protected void onPostExecute(Void result) {

                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                            }
                                                        });
                                                    }
                                                }, 1000);

                                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                finish();


                                                super.onPostExecute(result);
                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }
                                }, 2000);

                            } else {
                                loginButton.setBackgroundResource(R.drawable.selector_button_glass_red);
                                loginButton.setText("خطای غیرمنتظره‌ای پیش آمده است!");
                            }

                            super.onPostExecute(result);
                        }
                    }.execute();
                }
            }
        });


        forgetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.browseWebViaIntent(getApplicationContext(), getString(R.string.browse_web), Functions.getString(getApplicationContext(), "SYSTEM_HOME_URL", Information.DEFAULT_SYSTEM_HOME_URL) + "forget.php");
            }
        });

    }

    private void animateBackground() {
        final RelativeLayout bgLayout = (RelativeLayout) findViewById(R.id.bg_layout);

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TransitionDrawable trans = new TransitionDrawable(new Drawable[]{getResources().getDrawable(trans_drawables[trans_num]), getResources().getDrawable(trans_drawables[trans_num2])});
                        bgLayout.setBackgroundDrawable(trans);
                        trans.startTransition(trans_time);
                        trans_num++;
                        trans_num2 = trans_num + 1;
                        if (trans_num > trans_drawables.length - 1) {
                            trans_num = 0;
                            trans_num2 = 1;
                        }
                        if (trans_num2 > trans_drawables.length - 1) {
                            trans_num2 = 0;
                        }
                    }
                });
            }
        };

        timer.schedule(task, 0, item_time);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    protected void onResume() {
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.INTENT_REGISTRATION_COMPLETE));

        super.onResume();
    }


}