package com.kiosk.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by SeyyedMojtaba on 3/11/2016.
 */
public class HistoryBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        context.startService(new Intent(context, HistoryService.class));
    }
}
