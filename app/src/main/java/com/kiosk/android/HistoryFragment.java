package com.kiosk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.adapter.HistoryAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by SeyyedMojtaba on 2/25/2016.
 */
public class HistoryFragment extends Fragment {
    public static Activity activity;
    public static Context context;
    public static View rootView;

    public static HistoryAdapter adapter;
    public static ListView list;
    public static List<String> history_ids = new ArrayList<String>();
    public static List<String> history_authors = new ArrayList<String>();
    public static List<String> history_author_names = new ArrayList<String>();
    public static List<String> history_author_profile_images = new ArrayList<String>();
    public static List<String> history_author_timestamps = new ArrayList<String>();
    public static List<String> history_actions = new ArrayList<String>();
    public static List<String> history_timestamps = new ArrayList<String>();
    public static List<String> history_times = new ArrayList<String>();

    //private ProgressBar loading;
    public static SwipeRefreshLayout swipeRefreshLayout;

    public static int amount = Config.DEFAULT_HISTORY_BUFFER_ITEMS;

    public static boolean isProcessing = false;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_history, container, false);

        amount = Functions.getInt(context, "HISTORY_BUFFER_ITEMS", Config.DEFAULT_HISTORY_BUFFER_ITEMS);

        list = (ListView) rootView.findViewById(R.id.list);
        //loading = (ProgressBar) rootView.findViewById(R.id.loading);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_view);
        swipeRefreshLayout.setColorSchemeResources(R.color.generalDarkRed, R.color.generalDarkGreen, R.color.generalDarkBlue);


        adapter = new HistoryAdapter(activity, null);
        adapter.ids = history_ids;
        adapter.authors = history_authors;
        adapter.author_names = history_author_names;
        adapter.author_profile_images = history_author_profile_images;
        adapter.author_timestamps = history_author_timestamps;
        adapter.actions = history_actions;
        adapter.times = history_times;
        adapter.timestamps = history_timestamps;

        list.setAdapter(adapter);
        list.setSmoothScrollbarEnabled(true);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (history_actions.get(i).equals("follow_request")) {
                    Intent localIntent = new Intent(activity, ProfileActivity.class);
                    localIntent.putExtra("id", history_authors.get(i));
                    activity.startActivity(localIntent);
                } else if (history_actions.get(i).equals("follow_accept")) {
                    Intent localIntent = new Intent(activity, ProfileActivity.class);
                    localIntent.putExtra("id", history_authors.get(i));
                    activity.startActivity(localIntent);
                } else if (history_actions.get(i).equals("follow")) {
                    Intent localIntent = new Intent(activity, ProfileActivity.class);
                    localIntent.putExtra("id", history_authors.get(i));
                    activity.startActivity(localIntent);
                } else if (history_actions.get(i).equals("like")) {
                    Intent localIntent = new Intent(activity, PostActivity.class);
                    localIntent.putExtra("id", history_ids.get(i));
                    activity.startActivity(localIntent);
                } else if (history_actions.get(i).equals("comment")) {
                    Intent localIntent = new Intent(activity, PostActivity.class);
                    localIntent.putExtra("id", history_ids.get(i));
                    activity.startActivity(localIntent);
                } else if (history_actions.get(i).equals("tag_post")) {
                    Intent localIntent = new Intent(activity, PostActivity.class);
                    localIntent.putExtra("id", history_ids.get(i));
                    activity.startActivity(localIntent);
                } else if (history_actions.get(i).equals("tag_comment")) {
                    Intent localIntent = new Intent(activity, PostActivity.class);
                    localIntent.putExtra("id", history_ids.get(i));
                    activity.startActivity(localIntent);
                }
            }

        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                amount = Functions.getInt(context, "HISTORY_BUFFER_ITEMS", Config.DEFAULT_HISTORY_BUFFER_ITEMS);

                history_ids.clear();
                history_authors.clear();
                history_author_names.clear();
                history_author_profile_images.clear();
                history_author_timestamps.clear();
                history_actions.clear();
                history_timestamps.clear();
                history_times.clear();

                list.setOnScrollListener(null);

                loadHistory();
            }
        });


        refreshHistory();


        // Inflate the layout for this fragment
        return rootView;
    }

    public static void refreshHistory() {
        if (rootView == null) return;

        if (isProcessing) {
            return;
        }

        amount = Functions.getInt(context, "HISTORY_BUFFER_ITEMS", Config.DEFAULT_HISTORY_BUFFER_ITEMS);

        list = (ListView) rootView.findViewById(R.id.list);
        //loading = (ProgressBar) rootView.findViewById(R.id.loading);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_view);


        adapter = new HistoryAdapter(activity, null);
        adapter.ids = history_ids;
        adapter.authors = history_authors;
        adapter.author_names = history_author_names;
        adapter.author_profile_images = history_author_profile_images;
        adapter.author_timestamps = history_author_timestamps;
        adapter.actions = history_actions;
        adapter.times = history_times;
        adapter.timestamps = history_timestamps;

        list.setAdapter(adapter);
        list.setSmoothScrollbarEnabled(true);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);

                amount = Functions.getInt(context, "HISTORY_BUFFER_ITEMS", Config.DEFAULT_HISTORY_BUFFER_ITEMS);

                history_ids.clear();
                history_authors.clear();
                history_author_names.clear();
                history_author_profile_images.clear();
                history_author_timestamps.clear();
                history_actions.clear();
                history_timestamps.clear();
                history_times.clear();

                list.setOnScrollListener(null);

                loadHistory();
            }
        });
    }

    public static void loadHistory() {
        if (!Functions.isOnline(context)) {
            isProcessing = false;
            swipeRefreshLayout.setRefreshing(false);
            return;
        }

        if (isProcessing) {
            return;
        }

        isProcessing = true;

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.getHistory(Functions.getString(context, "id", ""), amount, "0"));
            }

            @Override
            protected void onPostExecute(String result) {


                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "history").equals("-1")) {

                    history_ids.addAll(JSONParser.getListFromString(result, "history", "id"));
                    history_authors.addAll(JSONParser.getListFromString(result, "history", "author"));
                    history_author_names.addAll(JSONParser.getListFromString(result, "history", "author_name"));
                    history_author_profile_images.addAll(JSONParser.getListFromString(result, "history", "author_profile_image"));
                    history_author_timestamps.addAll(JSONParser.getListFromString(result, "history", "author_timestamp"));
                    history_actions.addAll(JSONParser.getListFromString(result, "history", "action"));
                    history_timestamps.addAll(JSONParser.getListFromString(result, "history", "timestamp"));


                    for (int i = 0; i < history_ids.size(); i++) {
                        // GET PAST TIME FROM TIMESTAMP
                        history_times.add(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(history_timestamps.get(i)))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(history_timestamps.get(i)), "hh:mm:ss")));

                    }

                    if (!MainActivity.tabLayout.getTabAt(3).isSelected()) {
                        for (int i = 0; i < history_ids.size(); i++) {
                            //Toast.makeText(context, "time: " + Long.parseLong(String.valueOf(Functions.getLong(activity, "last_check_timestamp", 0L)).substring(0, 10)), Toast.LENGTH_SHORT).show();
                            if (Long.parseLong(history_timestamps.get(i)) > Long.parseLong(Functions.normalLength(String.valueOf(Functions.getLong(activity, "last_check_timestamp", 0L)), 10, "0", false).substring(0, 10))) {
                                MainActivity.tabLayout.getTabAt(3).setIcon(R.mipmap.ic_tab_history_new);
                                break;
                            }
                        }
                    }


                    adapter.ids = history_ids;
                    adapter.authors = history_authors;
                    adapter.author_names = history_author_names;
                    adapter.author_profile_images = history_author_profile_images;
                    adapter.author_timestamps = history_author_timestamps;
                    adapter.actions = history_actions;
                    adapter.timestamps = history_timestamps;
                    adapter.times = history_times;


                    adapter.notifyDataSetChanged();


                } else {
                    list.setOnScrollListener(null);
                }

                Functions.putLong(context, "last_check_timestamp", System.currentTimeMillis());

                isProcessing = false;
                swipeRefreshLayout.setRefreshing(false);

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        } else {
            this.context = context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}