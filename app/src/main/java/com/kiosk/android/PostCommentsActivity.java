package com.kiosk.android;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.PostsCommentsAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.DatabaseHelper;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardCloseListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class PostCommentsActivity extends AppCompatActivity {
    private DatabaseHelper dbHelper;
    private Post post;
    private String id;

    private PostsCommentsAdapter adapter;
    private ListView list;

    private List<String> comments_ids = new ArrayList<String>();
    private List<String> comments_authors = new ArrayList<String>();
    private List<String> comments_author_names = new ArrayList<String>();
    private List<String> comments_author_profile_images = new ArrayList<String>();
    private List<String> comments_author_timestamps = new ArrayList<String>();
    private List<String> comments_comments = new ArrayList<String>();
    private List<String> comments_timestamps = new ArrayList<String>();
    private List<String> comments_times = new ArrayList<String>();

    private int start = 0, amount = Config.DEFAULT_COMMENTS_BUFFER_ITEMS;

    private ProgressBar loading;

    private boolean isProcessing = false;
    private EmojiPopup emojiPopup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_comments);

        amount = Functions.getInt(getApplicationContext(), "COMMENTS_BUFFER_ITEMS", Config.DEFAULT_COMMENTS_BUFFER_ITEMS);

        if ((!Functions.isOnline(getApplicationContext()))) {
            Toast.makeText(PostCommentsActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }


        id = getIntent().getStringExtra("id");
        dbHelper = new DatabaseHelper(getApplicationContext());
        post = dbHelper.getPost(Integer.parseInt(id));

        list = (ListView) findViewById(R.id.list);
        final EmojiEditText CommentEditText = (EmojiEditText) findViewById(R.id.comment_text);
        ImageButton addCommentButton = (ImageButton) findViewById(R.id.add_comment);
        final ImageButton emojiKeyboardButton = (ImageButton) findViewById(R.id.emojikeyboard);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (comments_authors.get(i).equals(Functions.getString(getApplicationContext(), "id", ""))) {
                    Intent intent = new Intent(PostCommentsActivity.this, MainActivity.class);
                    intent.putExtra("action", "open_profile");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent localIntent = new Intent(PostCommentsActivity.this, ProfileActivity.class);
                    localIntent.putExtra("id", comments_authors.get(i));
                    startActivity(localIntent);
                }
            }

        });

        adapter = new PostsCommentsAdapter(PostCommentsActivity.this, null);
        adapter.ids = comments_ids;
        adapter.author_profile_images = comments_author_profile_images;
        adapter.author_timestamps = comments_author_timestamps;
        adapter.authors = comments_authors;
        adapter.author_names = comments_author_names;
        adapter.comments = comments_comments;
        adapter.times = comments_times;
        adapter.post = post;

        list.setAdapter(adapter);
        list.setSmoothScrollbarEnabled(true);


        loading = (ProgressBar) findViewById(R.id.loading);

        loadComments();

        emojiPopup = EmojiPopup.Builder.fromRootView(CommentEditText).setOnSoftKeyboardCloseListener(new OnSoftKeyboardCloseListener() {
            @Override
            public void onKeyboardClose() {
                if (emojiPopup.isShowing()) {
                    emojiPopup.dismiss();
                }
            }
        }).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
            @Override
            public void onEmojiPopupDismiss() {
                emojiKeyboardButton.setImageResource(R.drawable.selector_emoji);
            }
        }).setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
            @Override
            public void onEmojiPopupShown() {
                emojiKeyboardButton.setImageResource(R.mipmap.ic_emoji_selected);
            }
        }).build(CommentEditText);


        emojiKeyboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emojiPopup.toggle();
            }
        });


        addCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emojiPopup.isShowing()) {
                    emojiPopup.dismiss();
                }


                if (CommentEditText.getText().toString().isEmpty()) {
                    return;
                }

                new AsyncTask<String, String, String>() {
                    String comment = "";

                    @Override
                    protected void onPreExecute() {
                        loading.setVisibility(View.VISIBLE);

                        //InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                        comment = CommentEditText.getText().toString();
                        CommentEditText.setText("");

                        super.onPreExecute();
                    }

                    @Override
                    protected String doInBackground(String... voids) {
                        return Fetcher.FetchURL(APIManager.setPostMyComment(id, Functions.getString(getApplicationContext(), "id", ""), comment));
                    }

                    @Override
                    protected void onPostExecute(String result) {

                        loading.setVisibility(View.INVISIBLE);

                        comments_ids.add(0, "0");
                        comments_authors.add(0, Functions.getString(getApplicationContext(), "id", ""));

                        String name = Functions.getString(getApplicationContext(), "first_name", "") + " " + Functions.getString(getApplicationContext(), "last_name", "");
                        if (name.equals(" ")) {
                            name = Functions.getString(getApplicationContext(), "username", "");
                        }

                        comments_author_names.add(0, name);
                        comments_author_profile_images.add(0, Functions.getString(getApplicationContext(), "profile_image", ""));
                        comments_author_timestamps.add(0, Functions.getString(getApplicationContext(), "timestamp", ""));

                        comments_comments.add(0, comment);
                        comments_times.add(0, "همین الآن");

                        adapter.ids = comments_ids;
                        adapter.author_profile_images = comments_author_profile_images;
                        adapter.author_timestamps = comments_author_timestamps;
                        adapter.authors = comments_authors;
                        adapter.author_names = comments_author_names;
                        adapter.comments = comments_comments;
                        adapter.times = comments_times;


                        start++;
                        adapter.notifyDataSetChanged();

                        super.onPostExecute(result);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });


        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("نظرات");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }


    void loadComments() {
        if (isProcessing) {
            return;
        }

        isProcessing = true;

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                loading.setVisibility(View.VISIBLE);

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.getPostComments(id, Functions.getString(getApplicationContext(), "id", ""), String.valueOf(start), String.valueOf(amount)));
            }

            @Override
            protected void onPostExecute(String result) {

                loading.setVisibility(View.INVISIBLE);

                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "post_comments").equals("-1")) {
                    int offset = comments_ids.size();

                    comments_ids.addAll(JSONParser.getListFromString(result, "post_comments", "id"));
                    comments_authors.addAll(JSONParser.getListFromString(result, "post_comments", "author"));
                    comments_author_names.addAll(JSONParser.getListFromString(result, "post_comments", "author_name"));
                    comments_author_profile_images.addAll(JSONParser.getListFromString(result, "post_comments", "author_profile_image"));
                    comments_author_timestamps.addAll(JSONParser.getListFromString(result, "post_comments", "author_timestamp"));
                    comments_comments.addAll(JSONParser.getListFromString(result, "post_comments", "comment"));
                    comments_timestamps.addAll(JSONParser.getListFromString(result, "post_comments", "timestamp"));


                    for (int i = offset; i < comments_ids.size(); i++) {

                        // GET PAST TIME FROM TIMESTAMP
                        comments_times.add(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(comments_timestamps.get(i)))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(comments_timestamps.get(i)), "hh:mm:ss")) + " پیش");

                    }

                    start += amount;

                    adapter.ids = comments_ids;
                    adapter.author_profile_images = comments_author_profile_images;
                    adapter.author_timestamps = comments_author_timestamps;
                    adapter.authors = comments_authors;
                    adapter.author_names = comments_author_names;
                    adapter.comments = comments_comments;
                    adapter.times = comments_times;

                    if (start == amount) { // IF IS FIRST LOAD

                        list.setOnScrollListener(new AbsListView.OnScrollListener() {
                            public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == comments_ids.size()))
                                    loadComments();
                            }

                            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                            }
                        });
                    }

                    adapter.notifyDataSetChanged();

                } else {
                    list.setOnScrollListener(null);
                }

                isProcessing = false;

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (emojiPopup.isShowing()) {
            emojiPopup.dismiss();
        }
    }

}