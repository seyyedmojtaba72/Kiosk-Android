package com.kiosk.android;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.EndPoints;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.LinkManager;
import com.kiosk.android.info.MyApplication;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.kiosk.android.util.iab.IabBroadcastReceiver;
import com.kiosk.android.util.iab.IabHelper;
import com.kiosk.android.util.iab.IabResult;
import com.kiosk.android.util.iab.Inventory;
import com.kiosk.android.util.iab.Purchase;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import it.sephiroth.android.library.tooltip.Tooltip;

public class MainActivity extends AppCompatActivity {
    public static Activity activity;
    private Toolbar toolbar;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    private DatabaseHelper dbHelper;
    private int[] tabIcons = {R.drawable.ic_tab_home, R.drawable.ic_tab_search, R.drawable.ic_tab_kiosk, R.drawable.ic_tab_history, R.drawable.ic_tab_profile};

    public static ImageButton actionLeftButton, actionRightButton;

    private DownloadManager.DownloadTask localDownloadTask = new DownloadManager.DownloadTask();

    // The helper object
    public static IabHelper mHelper;


    //public static String[] skuTitles = new String[]{"10000 ریال", "20000 ریال", "50000 ریال", "100000 ریال", "200000 ریال", "500000 ریال", "1000000 ریال"};
    //public static String[] skuIDs = new String[]{"CHARGE_10000", "CHARGE_20000", "CHARGE_50000", "CHARGE_100000", "CHARGE_200000", "CHARGE_500000", "CHARGE_1000000"};
    //public static String[] skuAmounts = new String[]{"10000", "20000", "50000", "100000", "200000", "500000", "1000000"};


    public static String[] skuTitles_iranapps = new String[]{"10000 ریال", "20000 ریال", "50000 ریال", "100000 ریال", "200000 ریال", "500000 ریال", "1000000 ریال"};
    public static String[] skuIDs_iranapps = new String[]{"CHARGE_10000", "CHARGE_20000", "CHARGE_50000", "CHARGE_100000", "CHARGE_200000", "CHARGE_500000", "CHARGE_1000000"};
    public static String[] skuAmounts_iranapps = new String[]{"6000", "12000", "30000", "60000", "120000", "300000", "600000"};

    // Provides purchase notification while this app is running
    public static IabBroadcastReceiver mBillingBroadcastReceiver;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public static boolean new_activity = false, new_message = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DatabaseHelper(getApplicationContext());

        CustomFunctions.startUpFunctions(MainActivity.this);
        CustomFunctions.setBadge(getApplicationContext(), dbHelper);


        //startActivity(new Intent(MainActivity.this, ChatsActivity.class));
        //finish();

        activity = this;
        APIManager.context = getApplicationContext();
        LinkManager.context = getApplicationContext();
        EndPoints.context = getApplicationContext();

        MyApplication.getInstance().getRequestQueue().getCache().invalidate(EndPoints.CHATS_GET, true);
        MyApplication.getInstance().getRequestQueue().getCache().invalidate(EndPoints.CHAT_GET, true);
        MyApplication.getInstance().getRequestQueue().getCache().invalidate(EndPoints.CHAT_SEND, true);
        MyApplication.getInstance().getRequestQueue().getCache().remove(EndPoints.CHATS_GET);
        MyApplication.getInstance().getRequestQueue().getCache().remove(EndPoints.CHAT_GET);
        MyApplication.getInstance().getRequestQueue().getCache().remove(EndPoints.CHAT_SEND);
        MyApplication.getInstance().getRequestQueue().getCache().clear();


        // LOGIN FIRST!
        if (!Functions.getBoolean(getApplicationContext(), "logged in", false)) {
            startActivity(new Intent(this, TourActivity.class));
            finish();
            return;
        }


        // GCM REGISTRATION
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.INTENT_REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");
                    Functions.putString(getApplicationContext(), "gcm_token", token);

                } else if (intent.getAction().equals(Config.INTENT_PUSH_NOTIFICATION)) {
                    int type = intent.getIntExtra("type", Config.FLAG_DEFAULT);
                    switch (type) {
                        case Config.FLAG_ACTIVITY:
                            new_activity = true;
                            if (!tabLayout.getTabAt(3).isSelected()) {
                                tabLayout.getTabAt(3).setIcon(R.mipmap.ic_tab_history_new);
                            }
                            String message = intent.getStringExtra("message");

                            final ViewGroup root = (ViewGroup) tabLayout.getChildAt(0);
                            final View tab = root.getChildAt(3);
                            Tooltip.make(MainActivity.this, new Tooltip.Builder(101).anchor(tab, Tooltip.Gravity.TOP).closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000).activateDelay(800).showDelay(300).text(message).maxWidth(500).withArrow(true).withOverlay(true)
                                    //.typeface(mYourCustomFont)
                                    .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT).build()).show();
                            HistoryFragment.refreshHistory();
                            // new push notification is received

                            //Toast.makeText(getApplicationContext(), "Push notification is received!", Toast.LENGTH_LONG).show();
                            break;
                        case Config.FLAG_CHAT:
                            new_message = true;
                            if (tabLayout.getTabAt(0).isSelected()) {
                                //tabLayout.getTabAt(3).setIcon(R.mipmap.ic_tab_history_new);
                                actionRightButton.setImageResource(R.mipmap.ic_action_chat_new);

                                Tooltip.make(MainActivity.this, new Tooltip.Builder(101).anchor(actionRightButton, Tooltip.Gravity.LEFT).closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000).activateDelay(800).showDelay(300).text("پیام جدید").maxWidth(500).withArrow(true).withOverlay(true)
                                        //.typeface(mYourCustomFont)
                                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT).build()).show();
                            } else {
                                final ViewGroup root2 = (ViewGroup) tabLayout.getChildAt(0);
                                final View tab2 = root2.getChildAt(0);
                                Tooltip.make(MainActivity.this, new Tooltip.Builder(101).anchor(tab2, Tooltip.Gravity.TOP).closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000).activateDelay(800).showDelay(300).text("پیام جدید").maxWidth(500).withArrow(true).withOverlay(true)
                                        //.typeface(mYourCustomFont)
                                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT).build()).show();
                            }

                            //Toast.makeText(getApplicationContext(), "Push notification is received!", Toast.LENGTH_LONG).show();
                            break;
                        default:
                            break;
                    }
                }
            }
        };



        // IN APP BILLING
        mHelper = new IabHelper(this, Config.IRANAPPS_IN_APP_API_KEY);
        setupHelper();


        // ACTIONBAR BUTTONS

        actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionRightButton = (ImageButton) findViewById(R.id.btn_right);
        actionLeftButton.setImageResource(R.drawable.ic_action_paste);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BookmarksActivity.class));
            }
        });
        actionRightButton.setImageResource(R.drawable.ic_action_chat);
        actionRightButton.setVisibility(View.VISIBLE);
        actionRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new_message = false;
                actionRightButton.setImageResource(R.drawable.ic_action_chat);
                startActivity(new Intent(MainActivity.this, ChatsActivity.class));
            }
        });


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(5);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                actionRightButton.setVisibility(View.INVISIBLE);
                actionLeftButton.setVisibility(View.INVISIBLE);
                switch (position) {
                    case 0:
                        actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
                        actionRightButton = (ImageButton) findViewById(R.id.btn_right);
                        actionLeftButton.setImageResource(R.drawable.ic_action_paste);
                        actionLeftButton.setVisibility(View.VISIBLE);
                        actionLeftButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(MainActivity.this, BookmarksActivity.class));
                            }
                        });

                        if (new_message) {
                            actionRightButton.setImageResource(R.mipmap.ic_action_chat_new);
                            //Tooltip.make(MainActivity.this, new Tooltip.Builder(101).anchor(actionRightButton, Tooltip.Gravity.LEFT).closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000).activateDelay(800).showDelay(300).text("پیام جدید").maxWidth(500).withArrow(true).withOverlay(true)
                                    //.typeface(mYourCustomFont)
                            //        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT).build()).show();
                        } else {
                            actionRightButton.setImageResource(R.drawable.ic_action_chat);
                        }

                        actionRightButton.setVisibility(View.VISIBLE);
                        actionRightButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new_message = false;
                                actionRightButton.setImageResource(R.drawable.ic_action_chat);
                                startActivity(new Intent(MainActivity.this, ChatsActivity.class));
                            }
                        });
                        break;
                    case 3:
                        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
                        new_activity = false;
                        break;
                    case 4:
                        actionRightButton.setImageResource(R.drawable.ic_action_settings);
                        actionRightButton.setVisibility(View.VISIBLE);
                        actionRightButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                            }
                        });
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // INTENT CONTROL
        intentControl();


        // GET INFORMATION
        new Information(MainActivity.this);

        if (Functions.isOnline(getApplicationContext())) {

            // DISCOVERY
            if (Functions.getString(getApplicationContext(), "followeds", "0").equals("0")) {
                final Timer timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SearchFragment.users != null) {
                                    viewPager.setCurrentItem(1, true);
                                    tabLayout.setScrollPosition(1, 0, true);
                                    SearchFragment.searchTopUsers();
                                    timer.cancel();
                                }
                            }
                        });

                    }
                };
                timer.schedule(task, 0, 2000);
            }


            if (DatabaseHelper.DATABASE_VERSION > Functions.getInt(getApplicationContext(), "db_version", 1)) {
                // RESET TIMESTAMPS BECAUSE OF CLEARING DATABASE
                Functions.putLong(getApplicationContext(), "last_check_timestamp", 0);
                Functions.putLong(getApplicationContext(), "last_chats_check_timestamp", 0);

                Functions.putInt(getApplicationContext(), "db_version", DatabaseHelper.DATABASE_VERSION);
            }


            // LOGIN
            /* relogin to check for errors and refresh user data */
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    CustomFunctions.Login(MainActivity.this);
                }
            });

        }

        // DOWNLOAD AND SET PREFERENCES
        if (Functions.isOnline(getApplicationContext())) {
            String deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            String my_version = "Unknown Version";
            try {
                my_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            localDownloadTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new String[]{APIManager.getInfo(Functions.getString(getApplicationContext(), "id", ""), deviceID, my_version, "android"), Information.TEMP_PATH_PREFIX, "info.json"});
        }

        if (new File(Information.TEMP_PATH_PREFIX + "info.json").exists()) {
            try {
                String preferences_json = Fetcher.FetchFile(Information.TEMP_PATH_PREFIX + "info.json");
                List<String> preferences_variables = JSONParser.getListFromString(preferences_json, "preferences", "variable");
                List<String> preferences_values = JSONParser.getListFromString(preferences_json, "preferences", "value");
                Functions.putString(getApplicationContext(), "SYSTEM_HOME_URL", Functions.findValueFromVariable(preferences_variables, preferences_values, "SYSTEM_HOME_URL"));
                Functions.putString(getApplicationContext(), "API_HOME_URL", Functions.findValueFromVariable(preferences_variables, preferences_values, "API_HOME_URL"));
                Functions.putString(getApplicationContext(), "BACKUP_API_HOME_URL", Functions.findValueFromVariable(preferences_variables, preferences_values, "BACKUP_API_HOME_URL"));
                Functions.putString(getApplicationContext(), "PROFILE_IMAGES_URL", Functions.findValueFromVariable(preferences_variables, preferences_values, "PROFILE_IMAGES_URL"));
                Functions.putString(getApplicationContext(), "USERS_UPLOADS_URL", Functions.findValueFromVariable(preferences_variables, preferences_values, "USERS_UPLOADS_URL"));
                Functions.putString(getApplicationContext(), "FOOTER_DESCRIPTION", Functions.findValueFromVariable(preferences_variables, preferences_values, "FOOTER_DESCRIPTION"));
                Functions.putInt(getApplicationContext(), "POSTS_BUFFER_ITEMS", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "POSTS_BUFFER_ITEMS")));
                Functions.putInt(getApplicationContext(), "COMMENTS_BUFFER_ITEMS", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "COMMENTS_BUFFER_ITEMS")));
                Functions.putInt(getApplicationContext(), "USERS_BUFFER_ITEMS", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "USERS_BUFFER_ITEMS")));
                Functions.putInt(getApplicationContext(), "HISTORY_BUFFER_ITEMS", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "HISTORY_BUFFER_ITEMS")));
                Functions.putInt(getApplicationContext(), "CHATS_MESSAGES_BUFFER_ITEMS", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "CHATS_MESSAGES_BUFFER_ITEMS")));
                Functions.putInt(getApplicationContext(), "HISTORY_REFRESH_INTERVAL", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "HISTORY_REFRESH_INTERVAL")));
                Functions.putInt(getApplicationContext(), "MAX_PROFILE_IMAGE_SIZE", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "MAX_PROFILE_IMAGE_SIZE")));
                Functions.putInt(getApplicationContext(), "MAX_UPLOAD_FILE_SIZE", Integer.parseInt("0" + Functions.findValueFromVariable(preferences_variables, preferences_values, "MAX_UPLOAD_FILE_SIZE")));


                // VERSION AND MAINTENANCE CHECK
                String my_version = "1.0.0.0", last_version = "1.0.0.0", min_version = "1.0.0.0", maintenance_mode = "FALSE", maintenance_time = "0";
                List<String> versioning_variables = JSONParser.getListFromString(preferences_json, "versioning_options", "variable");
                List<String> versioning_values = JSONParser.getListFromString(preferences_json, "versioning_options", "value");
                min_version = Functions.findValueFromVariable(versioning_variables, versioning_values, "android_min_version");
                last_version = Functions.findValueFromVariable(versioning_variables, versioning_values, "android_last_version");
                try {
                    my_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                maintenance_mode = Functions.findValueFromVariable(versioning_variables, versioning_values, "maintenance_mode");
                maintenance_time = Functions.findValueFromVariable(versioning_variables, versioning_values, "maintenance_time");


                if (maintenance_mode.equals("TRUE")) {
                    final AlertDialog.Builder localBuilder = new AlertDialog.Builder(MainActivity.this);
                    localBuilder.setMessage("سامانه هم‌اکنون در حال تعمیر است. لطفاً " + Functions.normalPastTime(PersianDate.getCurrentShamsidate(), maintenance_time) + " دیگر امتحان کنید.");
                    localBuilder.setPositiveButton("باشه", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                            finish();
                        }
                    });
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                AlertDialog localAlertDialog = localBuilder.create();
                                localAlertDialog.show();
                                localAlertDialog.setCancelable(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } else {
                    String[] my_version_array = my_version.split(Pattern.quote("."));
                    String[] last_version_array = last_version.split(Pattern.quote("."));
                    String[] min_version_array = min_version.split(Pattern.quote("."));

                    double my_version_value = Functions.getVersionValue(my_version_array, 4, 5);
                    double last_version_value = Functions.getVersionValue(last_version_array, 4, 5);
                    double min_version_value = Functions.getVersionValue(min_version_array, 4, 5);

                    if (last_version_value > my_version_value) {
                        if (min_version_value > my_version_value) {
                            final AlertDialog.Builder localBuilder = new AlertDialog.Builder(MainActivity.this);
                            localBuilder.setMessage("نسخه‌ی جدیدی از " + Information.BASE_NAME_FA + " موجود است. برای ادامه لطفاً آن را به روز کنید.");
                            localBuilder.setPositiveButton("باشه", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                                    finish();
                                }
                            });
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    AlertDialog localAlertDialog = localBuilder.create();
                                    localAlertDialog.show();
                                    localAlertDialog.setCancelable(false);
                                }
                            });

                        }
                    }

                    if (Functions.isFirstTime(getApplicationContext(), my_version + "_whats_news") && !Information.WHATS_NEWS.isEmpty()) {
                        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(MainActivity.this);

                        localBuilder.setTitle("تغییرات نسخه‌ی " + my_version);
                        localBuilder.setMessage(Information.WHATS_NEWS);
                        localBuilder.setNegativeButton("باشه", null);

                        runOnUiThread(new Runnable() {
                            public void run() {
                                localBuilder.create();
                                localBuilder.show();
                                localBuilder.setCancelable(true);
                            }
                        });
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        /*
        // BACKGROUND HISTORY SERVICE
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                Functions.putLong(getApplicationContext(), "last_check_timestamp", System.currentTimeMillis());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Functions.isServiceRunning(getApplicationContext(), HistoryService.class)) {
                            stopService(new Intent(MainActivity.this, HistoryService.class));
                        }
                        startService(new Intent(MainActivity.this, HistoryService.class));
                    }
                }, 2000);

                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        */
    }

    private void fetchChats() {
        if (Functions.isOnline(getApplicationContext())) {

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {


                    String endPoint = EndPoints.CHATS_GET;

                    StringRequest strReq = new StringRequest(Request.Method.POST, endPoint, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {

                                if (!response.isEmpty() && !response.equals("-1") && !JSONParser.getFromString(response, "chats").equals("-1")) {

                                    List<String> chats_ids = JSONParser.getListFromString(response, "chats", "id");
                                    List<String> chats_creators = JSONParser.getListFromString(response, "chats", "creator");
                                    List<String> chats_types = JSONParser.getListFromString(response, "chats", "type");
                                    List<String> chats_participants = JSONParser.getListFromString(response, "chats", "participants");
                                    List<String> chats_names = JSONParser.getListFromString(response, "chats", "name");
                                    List<String> chats_images = JSONParser.getListFromString(response, "chats", "image");
                                    List<String> chats_abouts = JSONParser.getListFromString(response, "chats", "about");
                                    List<String> chats_timestamps = JSONParser.getListFromString(response, "chats", "timestamp");
                                    List<String> chats_last_timestamps = JSONParser.getListFromString(response, "chats", "last_timestamp");
                                    List<String> chats_news = JSONParser.getListFromString(response, "chats", "new");
                                    List<String> chats_last_messages = JSONParser.getListFromString(response, "chats", "last_message");
                                    List<String> chats_last_message_ids = JSONParser.getListFromString(response, "chats", "last_message_id");
                                    List<String> chats_last_message_senders = JSONParser.getListFromString(response, "chats", "last_message_sender");
                                    List<String> chats_last_message_timestamps = JSONParser.getListFromString(response, "chats", "last_message_timestamp");

                                    // DELETE DELETED CHATS!
                                    List<Chat> all_chats = dbHelper.getAllChats();
                                    for (Chat chat : all_chats) {
                                        if (chats_ids.indexOf(String.valueOf(chat.id)) < 0) {
                                            dbHelper.deleteChat(chat.id);
                                        }
                                    }


                                    for (int i = 0; i < chats_ids.size(); i++) {
                                        if (dbHelper.checkChat(Integer.parseInt(chats_ids.get(i)))) {
                                            if (dbHelper.getChat(Integer.parseInt(chats_ids.get(i))).type.equals("deleted")) {
                                                continue;
                                            }
                                        }


                                        Chat chat = new Chat();
                                        chat.id = Integer.parseInt(chats_ids.get(i));
                                        chat.creator = chats_creators.get(i);
                                        chat.type = chats_types.get(i);
                                        chat.participants = chats_participants.get(i);
                                        chat.name = chats_names.get(i);
                                        chat.image = chats_images.get(i);
                                        chat.about = chats_abouts.get(i);
                                        chat.timestamp = chats_timestamps.get(i);
                                        chat.last_timestamp = chats_last_timestamps.get(i);

                                        if (dbHelper.checkChat(chat.id)) {
                                            dbHelper.updateChat(chat);
                                        } else {
                                            dbHelper.createChat(chat);
                                        }


                                        ChatsMessage last_message = new ChatsMessage();
                                        last_message.id = Integer.parseInt(chats_last_message_ids.get(i));
                                        last_message.sender = chats_last_message_senders.get(i);
                                        last_message.chat = chats_ids.get(i);
                                        last_message.message = chats_last_messages.get(i);
                                        last_message.timestamp = chats_last_message_timestamps.get(i);
                                        last_message.status = "read";

                                        if (!dbHelper.checkChatsMessage(last_message.id)) {
                                            dbHelper.createChatsMessage(last_message);
                                        }


                                        if (Long.parseLong(chats_news.get(i)) > 0) {
                                            new_message = true;

                                            for (int j = 0; j < Integer.parseInt(chats_news.get(i)) - 1; j++) {
                                                ChatsMessage new_message = new ChatsMessage();
                                                new_message.id = Integer.parseInt(chats_last_message_ids.get(i)) - j - 1;
                                                new_message.chat = chats_ids.get(i);
                                                new_message.message = "null";
                                                new_message.timestamp = chats_last_message_timestamps.get(i);
                                                new_message.status = "not-read";
                                                if (!dbHelper.checkChatsMessage(new_message.id)) {
                                                    dbHelper.createChatsMessage(new_message);
                                                }
                                            }

                                            last_message = new ChatsMessage();
                                            last_message.id = Integer.parseInt(chats_last_message_ids.get(i));
                                            last_message.sender = chats_last_message_senders.get(i);
                                            last_message.chat = chats_ids.get(i);
                                            last_message.message = chats_last_messages.get(i);
                                            last_message.timestamp = chats_last_message_timestamps.get(i);
                                            last_message.status = "not-read";

                                            if (!dbHelper.checkChatsMessage(last_message.id)) {
                                                dbHelper.createChatsMessage(last_message);
                                            } else {
                                                dbHelper.updateChatsMessage(last_message);
                                            }
                                        }
                                    }


                                    Functions.putLong(getApplicationContext(), "last_chats_check_timestamp", System.currentTimeMillis());
                                }


                            } catch (Exception e)

                            {
                                e.printStackTrace();
                            }


                        }
                    }

                            , new Response.ErrorListener()

                    {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            //NetworkResponse networkResponse = error.networkResponse;
                            //Log.e("debug", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                            //Toast.makeText(getApplicationContext(), "Volley error: " + error.networkResponse.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    )

                    {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user", Functions.getString(getApplicationContext(), "id", ""));
                            params.put("timestamp", String.valueOf(Functions.getLong(getApplicationContext(), "last_chats_check_timestamp", 0)));

                            return params;
                        }
                    };


                    //Adding request to request queue
                    MyApplication.getInstance().addToRequestQueue(strReq);
                }
            });
        }

        CustomFunctions.setBadge(getApplicationContext(), dbHelper);

        if (new_message) {
            if (tabLayout.getTabAt(0).isSelected()) {
                //tabLayout.getTabAt(3).setIcon(R.mipmap.ic_tab_history_new);
                actionRightButton.setImageResource(R.mipmap.ic_action_chat_new);

                Tooltip.make(MainActivity.this, new Tooltip.Builder(101).anchor(actionRightButton, Tooltip.Gravity.LEFT).closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000).activateDelay(800).showDelay(300).text("پیام جدید").maxWidth(500).withArrow(true).withOverlay(true)
                        //.typeface(mYourCustomFont)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT).build()).show();
            } else {
                final ViewGroup root2 = (ViewGroup) tabLayout.getChildAt(0);
                final View tab2 = root2.getChildAt(0);
                Tooltip.make(MainActivity.this, new Tooltip.Builder(101).anchor(tab2, Tooltip.Gravity.TOP).closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000).activateDelay(800).showDelay(300).text("پیام جدید").maxWidth(500).withArrow(true).withOverlay(true)
                        //.typeface(mYourCustomFont)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT).build()).show();
            }
        }


    }

    private void setupHelper() {
        // enable debug logging (for a production application, you should set this to false).


        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // Important: Dynamically register for broadcast messages about updated purchases.
                // We register the receiver here instead of as a <receiver> in the Manifest
                // because we always call getPurchases() at startup, so therefore we can ignore
                // any broadcasts sent while the app isn't running.
                // Note: registering this listener in an Activity is a bad idea, but is done here
                // because this is a SAMPLE. Regardless, the receiver must be registered after
                // IabHelper is setup, but before first call to getPurchases().
                mBillingBroadcastReceiver = new IabBroadcastReceiver(new IabBroadcastReceiver.IabBroadcastListener() {
                    @Override
                    public void receivedBroadcast() {
                        mHelper.queryInventoryAsync(mGotInventoryListener);
                    }
                });
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                activity.registerReceiver(mBillingBroadcastReceiver, broadcastFilter);

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }


    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "خانه");
        adapter.addFragment(new SearchFragment(), "جستجو");
        adapter.addFragment(new ComposeFragment(), "افزودن مطلب");
        adapter.addFragment(new HistoryFragment(), "تاریخچه");
        adapter.addFragment(new ProfileFragment(), "نمایه");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);
            return null;
        }

        private int currentPage;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    private void intentControl() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            String action = intent.getStringExtra("action");

            if (action != null) {
                if (action.equals("open_profile")) {
                    intent.putExtra("action", "");
                    viewPager.setCurrentItem(4, true);
                    tabLayout.setScrollPosition(4, 0, true);
                } else if (action.equals("show_history")) {
                    intent.putExtra("action", "");
                    viewPager.setCurrentItem(3, true);
                    tabLayout.setScrollPosition(3, 0, true);
                    HistoryFragment.refreshHistory();
                } else if (action.equals("show_chats")) {
                    intent.putExtra("action", "");
                    startActivity(new Intent(MainActivity.this, ChatsActivity.class));
                }
            }
        }
    }


    boolean doubleBackToExitPressedOnce = false;
    public static boolean toRefresh = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();

            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "یک بار دیگر کلیک کنید تا خارج شوید.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


        viewPager.setCurrentItem(0, true);
        tabLayout.setScrollPosition(0, 0, true);
        if (toRefresh) {
            HomeFragment.refreshPosts();
            toRefresh = false;
        }
    }

    /**
     * Verifies the developer payload of a purchase.
     */
    public static boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    // Callback for when a purchase is finished
    public static IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE).setTitleText("").setContentText("خطا در ثبت اطلاعات").showCancelButton(false).setConfirmText("باشه").setConfirmClickListener(null).show();
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE).setTitleText("").setContentText("خطا در ثبت اطلاعات").showCancelButton(false).setConfirmText("باشه").setConfirmClickListener(null).show();
                return;
            }

            mHelper.consumeAsync(purchase, mConsumeFinishedListener);


        }
    };

    // Called when consumption is complete
    public static IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                final String sku = purchase.getSku();


                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE).setTitleText("").setContentText("حساب شما با موفقیت شارژ شد").showCancelButton(false).setConfirmText("باشه").setConfirmClickListener(null).show();
                String amount = Functions.findValueFromVariable(skuIDs_iranapps, skuAmounts_iranapps, sku);
                String balance = String.valueOf(Integer.parseInt(Functions.getString(activity, "balance", "0")) + Integer.parseInt(amount));
                Functions.putString(activity, "balance", balance);

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        Fetcher.FetchURL(APIManager.inAppCharge(Functions.getString(activity, "id", ""), sku));
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        ProfileFragment.swipeRefreshLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                ProfileFragment.refreshAccount();
                            }
                        });
                        super.onPostExecute(aVoid);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            } else {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE).setTitleText("").setContentText("خطا در ثبت اطلاعات").showCancelButton(false).setConfirmText("باشه").setConfirmClickListener(null).show();
            }

        }
    };

    // Listener that's called when we finish querying the items and subscriptions we own
    private IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            // Have we been disposed of in the meantime? If so, quit.
            if (MainActivity.mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                return;
            }


            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            for (String skuID : skuIDs_iranapps) {

                // Check for gas delivery -- if we own gas, we should fill up the tank immediately
                Purchase gasPurchase = inventory.getPurchase(skuID);
                if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                    mHelper.consumeAsync(inventory.getPurchase(skuID), mConsumeFinishedListener);
                    break;
                }
            }


        }
    };

    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            // very important:
            if (mBillingBroadcastReceiver != null) {
                unregisterReceiver(mBillingBroadcastReceiver);
            }

            // very important:
            if (mHelper != null) {
                mHelper.dispose();
                mHelper = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        intentControl();

        if (toRefresh) {
            HomeFragment.refreshPosts();
            toRefresh = false;
        }

        if (new_activity) {
            if (!tabLayout.getTabAt(3).isSelected()) {
                tabLayout.getTabAt(3).setIcon(R.mipmap.ic_tab_history_new);
            }

            HistoryFragment.refreshHistory();
        }

        /*new_message = false;
        if (tabLayout.getTabAt(0).isSelected()) {
            actionRightButton.setImageResource(R.drawable.ic_action_chat);
        }*/
        fetchChats();

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.INTENT_PUSH_NOTIFICATION));

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.INTENT_REGISTRATION_COMPLETE));

        super.onResume();
    }

}