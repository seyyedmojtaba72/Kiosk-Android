package com.kiosk.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.adapter.ChatsAdapter;
import com.kiosk.android.adapter.UsersAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;
import com.kiosk.android.info.EndPoints;
import com.kiosk.android.info.MyApplication;
import com.kiosk.android.model.Chat;
import com.kiosk.android.model.ChatsMessage;
import com.kiosk.android.util.DatabaseHelper;
import com.kiosk.android.util.NotificationUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ChatsActivity extends AppCompatActivity {
    public static DatabaseHelper dbHelper;
    public static List<Chat> chats = new ArrayList<Chat>();

    public static ChatsAdapter chats_adapter;
    public static ListView list;

    public static String intent_action = "", intent_data = "", intent_extras = "";
    public static Uri intent_uri = null;

    public static TextViewPlus titleText;


    /*
    private List<String> chats_ids = new ArrayList<String>();
    private List<String> chats_creators = new ArrayList<String>();
    private List<String> chats_types = new ArrayList<String>();
    private List<String> chats_participants = new ArrayList<String>();
    private List<String> chats_names = new ArrayList<String>();
    private List<String> chats_images = new ArrayList<String>();
    private List<String> chats_timestamps = new ArrayList<String>();

    private ProgressBar loading;

    private int start = 0, amount = Config.DEFAULT_CHATS_BUFFER_ITEMS;

    private boolean isProcessing = false;
    */

    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        intent_action = "";
        intent_data = "";
        intent_extras = "";
        intent_uri = null;

        chats.clear();


        // INTENT SHARE

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.get(Intent.EXTRA_STREAM) != null) {
                intent_uri = (Uri) bundle.get(Intent.EXTRA_STREAM);
                Log.d("tag", "uri: " + intent_uri);
                intent_action = "image";
                getIntent().removeExtra(Intent.EXTRA_STREAM);
            }
        }


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.INTENT_PUSH_NOTIFICATION)) {
                    // new push notification is received
                    handlePushNotification(intent);
                }
            }
        };

        //amount = Functions.getInt(getApplicationContext(), "CHATS_BUFFER_ITEMS", Config.DEFAULT_CHATS_BUFFER_ITEMS);

        dbHelper = new DatabaseHelper(getApplicationContext());

        list = (ListView) findViewById(R.id.list);


        chats_adapter = new ChatsAdapter(ChatsActivity.this, null);
        chats_adapter.chats = chats;
        list.setAdapter(chats_adapter);
        list.setSmoothScrollbarEnabled(true);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ChatsActivity.this, ChatActivity.class);
                intent.putExtra("id", String.valueOf(chats.get(i).id));
                intent.putExtra("action", intent_action);
                intent.putExtra("data", intent_data);
                intent.putExtra("extras", intent_extras);
                intent.putExtra("uri", intent_uri);
                startActivity(intent);
            }
        });


        /*
        adapter.ids = chats_ids;
        adapter.types = chats_types;
        adapter.names = chats_names;
        adapter.images = chats_images;


        loading = (ProgressBar) findViewById(R.id.loading);

        loadChats();*/


        // ACTIONBAR

        titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("گفتگوها");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setImageResource(R.drawable.ic_action_back);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton actionRightButton = (ImageButton) findViewById(R.id.btn_right);
        actionRightButton.setImageResource(R.drawable.ic_action_send_now);
        actionRightButton.setVisibility(View.VISIBLE);
        actionRightButton.setOnClickListener(new View.OnClickListener() {

            private String method, argument;

            private UsersAdapter adapter;
            private ListView list;
            AutoCompleteTextView userText;
            private EditText nameText;
            private TextView participantsText;
            private ImageButton selectButton, createButton;

            private List<String> users_ids = new ArrayList<String>();
            private List<String> users_usernames = new ArrayList<String>();
            private List<String> users_names = new ArrayList<String>();
            private List<String> users_profile_images = new ArrayList<String>();
            private List<String> users_timestamps = new ArrayList<String>();
            private List<String> users_follows = new ArrayList<String>();

            private int start = 0, amount = Config.DEFAULT_USERS_BUFFER_ITEMS;

            private ProgressBar loading;

            private boolean isProcessing = false;

            private List<String> participants = new ArrayList<String>();
            private int item = -1;

            @Override
            public void onClick(View view) {

                AlertDialog.Builder localBuilder = new AlertDialog.Builder(ChatsActivity.this);
                ArrayList<String> localArrayList = new ArrayList<String>();
                localArrayList.add("گفتگوی خصوصی");
                localArrayList.add("ساخت گروه");


                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                        switch (paramAnonymous2Int) {
                            default:
                                return;
                            case 0:

                                AlertDialog.Builder localBuilder = new AlertDialog.Builder(ChatsActivity.this);
                                View dialogView = getLayoutInflater().inflate(R.layout.dialog_select_user, (ViewGroup) findViewById(R.id.dialog_select_user));
                                localBuilder.setView(dialogView);

                                list = (ListView) dialogView.findViewById(R.id.list);
                                userText = (AutoCompleteTextView) dialogView.findViewById(R.id.user);
                                selectButton = (ImageButton) dialogView.findViewById(R.id.select);


                                amount = Functions.getInt(getApplicationContext(), "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);
                                loading = (ProgressBar) dialogView.findViewById(R.id.loading);

                                if ((!Functions.isOnline(getApplicationContext()))) {
                                    Toast.makeText(ChatsActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                method = "followed";
                                argument = Functions.getString(getApplicationContext(), "id", "");


                                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        userText.setText(users_usernames.get(i));
                                    }

                                });

                                final AlertDialog dialog = localBuilder.create();
                                dialog.show();

                                selectButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (userText.getText().toString().isEmpty()) {
                                            return;
                                        }

                                        final SweetAlertDialog sDialog = new SweetAlertDialog(ChatsActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);
                                        new AsyncTask<String, String, String>() {
                                            String username = userText.getText().toString();

                                            @Override
                                            protected void onPreExecute() {
                                                sDialog.show();
                                                sDialog.setCancelable(false);

                                                super.onPreExecute();
                                            }

                                            @Override
                                            protected String doInBackground(String... strings) {
                                                return Fetcher.FetchURL(APIManager.startChat(Functions.getString(getApplicationContext(), "id", ""), username + "!new_user!", "", ""));
                                            }

                                            @Override
                                            protected void onPostExecute(String result) {
                                                if (result.contains("success")) {
                                                    Intent intent = new Intent(ChatsActivity.this, ChatActivity.class);
                                                    intent.putExtra("id", result.replaceFirst("success;", ""));

                                                    Chat chat = new Chat();

                                                    chat.id = Integer.parseInt(result.replaceFirst("success;", ""));
                                                    chat.creator = Functions.getString(getApplicationContext(), "id", "");
                                                    chat.type = "single";

                                                    chat.name = userText.getText().toString();
                                                    chat.image = "";
                                                    chat.about = "";


                                                    chat.participants = "";
                                                    chat.timestamp = String.valueOf(System.currentTimeMillis());
                                                    chat.last_timestamp = String.valueOf(System.currentTimeMillis()).substring(0, 10);

                                                    if (dbHelper.checkChat(chat.id)) {
                                                        dbHelper.updateChat(chat);
                                                    } else {
                                                        dbHelper.createChat(chat);
                                                    }

                                                    chats.add(chat);
                                                    sortItems();
                                                    chats_adapter.chats = chats;
                                                    chats_adapter.notifyDataSetChanged();


                                                    startActivity(intent);
                                                    dialog.dismiss();
                                                    sDialog.dismiss();
                                                } else if (result.equals("no-user")) {
                                                    sDialog.setTitleText("").setContentText("چنین کاربری وجود ندارد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                } else {
                                                    sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                }

                                                super.onPostExecute(result);
                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }
                                });


                                adapter = new UsersAdapter(ChatsActivity.this, null, false);

                                adapter.users = users_ids;
                                adapter.user_profile_images = users_profile_images;
                                adapter.user_names = users_names;
                                adapter.user_timestamps = users_timestamps;
                                adapter.follows = users_follows;

                                list.setAdapter(adapter);
                                list.setSmoothScrollbarEnabled(true);

                                ArrayAdapter<String> userAdapter = new ArrayAdapter<String>(ChatsActivity.this, android.R.layout.simple_dropdown_item_1line, users_names);
                                userText.setAdapter(userAdapter);

                                loadUsers(true);

                                break;
                            case 1:

                                AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(ChatsActivity.this);
                                View dialogView2 = getLayoutInflater().inflate(R.layout.dialog_create_group, (ViewGroup) findViewById(R.id.dialog_create_group));
                                localBuilder2.setView(dialogView2);

                                list = (ListView) dialogView2.findViewById(R.id.list);
                                nameText = (EditText) dialogView2.findViewById(R.id.name);
                                participantsText = (TextView) dialogView2.findViewById(R.id.participants);
                                createButton = (ImageButton) dialogView2.findViewById(R.id.create);


                                amount = Functions.getInt(getApplicationContext(), "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);
                                loading = (ProgressBar) dialogView2.findViewById(R.id.loading);

                                if ((!Functions.isOnline(getApplicationContext()))) {
                                    Toast.makeText(ChatsActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                method = "followed";
                                argument = Functions.getString(getApplicationContext(), "id", "");


                                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        if (participants.indexOf(users_usernames.get(i)) > -1) {
                                            participants.remove(participants.indexOf(users_usernames.get(i)));
                                            participantsText.setText(participantsText.getText().toString().replace(users_usernames.get(i) + "   ", ""));
                                        } else {
                                            participants.add(users_usernames.get(i));
                                            participantsText.setText(participantsText.getText().toString() + users_usernames.get(i) + "   ");
                                        }

                                    }

                                });

                                final AlertDialog dialog2 = localBuilder2.create();
                                dialog2.show();

                                createButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final String name = nameText.getText().toString();

                                        if (participants.size() == 0) {
                                            return;
                                        }

                                        if (name.isEmpty()) {
                                            Toast.makeText(ChatsActivity.this, "لطفاً نام گروه را وارد کنید.", Toast.LENGTH_SHORT).show();
                                            return;
                                        }


                                        final SweetAlertDialog sDialog = new SweetAlertDialog(ChatsActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);
                                        new AsyncTask<String, String, String>() {

                                            @Override
                                            protected void onPreExecute() {
                                                sDialog.show();
                                                sDialog.setCancelable(false);

                                                super.onPreExecute();
                                            }

                                            @Override
                                            protected String doInBackground(String... strings) {
                                                String participants_text = "";
                                                for (String participant : participants) {
                                                    participants_text += participant + "!new_user!";
                                                }
                                                return Fetcher.FetchURL(APIManager.startChat(Functions.getString(getApplicationContext(), "id", ""), participants_text, name, ""));
                                            }

                                            @Override
                                            protected void onPostExecute(String result) {
                                                if (result.contains("success")) {
                                                    Intent intent = new Intent(ChatsActivity.this, ChatActivity.class);
                                                    intent.putExtra("id", result.replaceFirst("success;", ""));

                                                    Chat chat = new Chat();

                                                    chat.id = Integer.parseInt(result.replaceFirst("success;", ""));
                                                    chat.creator = Functions.getString(getApplicationContext(), "id", "");

                                                    chat.type = "group";
                                                    chat.name = name;
                                                    chat.image = "";
                                                    chat.about = "";

                                                    chat.participants = "";
                                                    chat.timestamp = String.valueOf(System.currentTimeMillis());
                                                    chat.last_timestamp = String.valueOf(System.currentTimeMillis()).substring(0, 10);

                                                    dbHelper.createChat(chat);

                                                    chats.add(chat);
                                                    sortItems();
                                                    chats_adapter.chats = chats;
                                                    chats_adapter.notifyDataSetChanged();


                                                    startActivity(intent);
                                                    dialog2.dismiss();
                                                    sDialog.dismiss();
                                                } else if (result.equals("no-user")) {
                                                    sDialog.setTitleText("").setContentText("چنین کاربری وجود ندارد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                } else {
                                                    sDialog.setTitleText("").setContentText("خطایی رخ داده. لطفاً دوباره سعی کنید...").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                }

                                                super.onPostExecute(result);
                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }
                                });


                                adapter = new UsersAdapter(ChatsActivity.this, null, false);

                                adapter.users = users_ids;
                                adapter.user_profile_images = users_profile_images;
                                adapter.user_timestamps = users_timestamps;
                                adapter.user_names = users_names;
                                adapter.follows = users_follows;

                                list.setAdapter(adapter);
                                list.setSmoothScrollbarEnabled(true);


                                loadUsers(false);


                                break;
                        }
                    }
                });

                final AlertDialog dialog = localBuilder.create();
                dialog.show();


            }


            void loadUsers(final boolean single) {

                if (isProcessing) {
                    return;
                }

                isProcessing = true;


                new AsyncTask<String, String, String>() {

                    @Override
                    protected void onPreExecute() {
                        loading.setVisibility(View.VISIBLE);

                        super.onPreExecute();
                    }

                    @Override
                    protected String doInBackground(String... strings) {
                        return Fetcher.FetchURL(APIManager.getUsers(method, argument, start, amount, Functions.getString(getApplicationContext(), "id", "")));
                    }

                    @Override
                    protected void onPostExecute(String result) {

                        loading.setVisibility(View.INVISIBLE);

                        if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "users").equals("-1")) {

                            users_ids.addAll(JSONParser.getListFromString(result, "users", "id"));
                            users_usernames.addAll(JSONParser.getListFromString(result, "users", "username"));
                            users_profile_images.addAll(JSONParser.getListFromString(result, "users", "profile_image"));
                            users_timestamps.addAll(JSONParser.getListFromString(result, "users", "timestamp"));
                            users_names.addAll(JSONParser.getListFromString(result, "users", "name"));
                            users_follows.addAll(JSONParser.getListFromString(result, "users", "follow"));

                            start += amount;

                            adapter.users = users_ids;
                            adapter.user_profile_images = users_profile_images;
                            adapter.user_timestamps = users_timestamps;
                            adapter.user_names = users_names;
                            adapter.follows = users_follows;

                            if (single) {
                                ArrayAdapter<String> userAdapter = new ArrayAdapter<String>(ChatsActivity.this, android.R.layout.simple_dropdown_item_1line, users_names);
                                userText.setAdapter(userAdapter);
                            }

                            if (start == amount) { // IF IS FIRST LOAD

                                list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                    public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                        if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == users_ids.size()))
                                            loadUsers(single);
                                    }

                                    public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                    }
                                });
                            }

                            adapter.notifyDataSetChanged();

                        } else {
                            list.setOnScrollListener(null);
                        }

                        isProcessing = false;

                        super.onPostExecute(result);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


            }


        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    /*
    void loadChats() {
        if (!Functions.isOnline(getApplicationContext())) {
            if (isProcessing) {
                return;
            }

            isProcessing = true;

            List<Chat> chats = dbHelper.searchChats(null, "id", start, amount, true, true);


            if (chats.size() == 1) {
                list.setOnScrollListener(null);
            }


            for (Chat chat : chats) {

                chats_ids.add(String.valueOf(chat.id));
                chats_creators.add(chat.creator);
                chats_types.add(chat.type);
                chats_participants.add(chat.participants);
                chats_names.add(chat.name);
                chats_images.add(chat.image);
                chats_timestamps.add(chat.timestamp);
            }


            start += amount;

            adapter.ids = chats_ids;
            adapter.types = chats_types;
            adapter.names = chats_names;
            adapter.images = chats_images;


            if (start == amount) { // IF IS FIRST LOAD

                list.setAdapter(adapter);
                list.setSmoothScrollbarEnabled(true);

                list.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                        if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 >= start)) {
                            //loadChats();
                        }
                    }

                    public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                    }
                });
            }


            adapter.notifyDataSetChanged();

            isProcessing = false;
            loading.setVisibility(View.INVISIBLE);

        } else {
            if (isProcessing) {
                return;
            }

            isProcessing = true;

            new AsyncTask<String, String, String>() {

                @Override
                protected void onPreExecute() {
                    loading.setVisibility(View.VISIBLE);

                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(String... strings) {
                    return Fetcher.FetchURL(APIManager.getChats(Functions.getString(getApplicationContext(), "id", ""), String.valueOf(start), String.valueOf(amount)));
                }

                @Override
                protected void onPostExecute(String result) {

                    if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "chats").equals("-1")) {
                        int offset = chats_ids.size();

                        chats_ids.addAll(JSONParser.getListFromString(result, "chats", "id"));
                        chats_creators.addAll(JSONParser.getListFromString(result, "chats", "creator"));
                        chats_types.addAll(JSONParser.getListFromString(result, "chats", "type"));
                        chats_participants.addAll(JSONParser.getListFromString(result, "chats", "participants"));
                        chats_names.addAll(JSONParser.getListFromString(result, "chats", "name"));
                        chats_images.addAll(JSONParser.getListFromString(result, "chats", "image"));
                        chats_timestamps.addAll(JSONParser.getListFromString(result, "chats", "timestamp"));


                        for (int i = offset; i < chats_ids.size(); i++) {
                            Chat chat;
                            if (dbHelper.checkChat(Integer.parseInt(chats_ids.get(i)))) {
                                chat = dbHelper.getChat(Integer.parseInt(chats_ids.get(i)));
                            } else {
                                chat = new Chat();
                            }

                            chat.id = Integer.parseInt(chats_ids.get(i));
                            chat.creator = chats_creators.get(i);
                            chat.type = chats_types.get(i);
                            chat.participants = chats_participants.get(i);
                            chat.name = EmojiMapUtil.replaceCheatSheetEmojis(chats_names.get(i));
                            chat.image = chats_images.get(i);
                            chat.timestamp = chats_timestamps.get(i);


                            if (dbHelper.checkChat(Integer.parseInt(chats_ids.get(i)))) {
                                dbHelper.updateChat(chat);
                            } else {
                                dbHelper.createChat(chat);
                            }
                        }

                        start += amount;

                        adapter.ids = chats_ids;
                        adapter.types = chats_types;
                        adapter.names = chats_names;
                        adapter.images = chats_images;

                        if (start == amount) { // IF IS FIRST LOAD

                            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                    if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 >= start)) {
                                        //loadChats();
                                    }
                                }

                                public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                                }
                            });
                        }

                        adapter.notifyDataSetChanged();

                    } else {
                        list.setOnScrollListener(null);
                    }

                    isProcessing = false;
                    loading.setVisibility(View.INVISIBLE);

                    super.onPostExecute(result);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }*/


    /**
     * Handles new push notification
     */
    private void handlePushNotification(Intent intent) {
        int type = intent.getIntExtra("type", -1);

        // if the push is of chat room message
        // simply update the UI unread messages count
        if (type == Config.FLAG_CHAT) {
            ChatsMessage chats_message = (ChatsMessage) intent.getSerializableExtra("chats_message");

            if (chats_message != null) {
                updateRow(chats_message);
            }
        }


    }

    /**
     * Updates the chat list unread count and the last message
     */
    private void updateRow(ChatsMessage chats_message) {
        for (Chat chat : chats) {
            if (chat.id == Integer.parseInt(chats_message.chat)) {
                int index = chats.indexOf(chat);
                chats.remove(index);
                chats.add(index, chat);
                break;
            }
        }

        loadOffline();
    }


    /**
     * fetching the chat rooms by making http call
     */
    private void fetchChats() {
        if (Functions.isOnline(getApplicationContext())) {

            loadOffline();

            AsyncTask.execute(new Runnable() {
                                  @Override
                                  public void run() {


                                      String endPoint = EndPoints.CHATS_GET;

                                      StringRequest strReq = new StringRequest(Request.Method.POST, endPoint, new Response.Listener<String>() {

                                          @Override
                                          public void onResponse(String response) {

                                              try {

                                                  if (!response.isEmpty() && !response.equals("-1") && !JSONParser.getFromString(response, "chats").equals("-1")) {


                                                      chats.clear();
                                                      chats_adapter.chats = chats;


                                                      List<String> chats_ids = JSONParser.getListFromString(response, "chats", "id");
                                                      List<String> chats_creators = JSONParser.getListFromString(response, "chats", "creator");
                                                      List<String> chats_types = JSONParser.getListFromString(response, "chats", "type");
                                                      List<String> chats_participants = JSONParser.getListFromString(response, "chats", "participants");
                                                      List<String> chats_names = JSONParser.getListFromString(response, "chats", "name");
                                                      List<String> chats_images = JSONParser.getListFromString(response, "chats", "image");
                                                      List<String> chats_abouts = JSONParser.getListFromString(response, "chats", "about");
                                                      List<String> chats_timestamps = JSONParser.getListFromString(response, "chats", "timestamp");
                                                      List<String> chats_last_timestamps = JSONParser.getListFromString(response, "chats", "last_timestamp");
                                                      List<String> chats_news = JSONParser.getListFromString(response, "chats", "new");
                                                      List<String> chats_last_messages = JSONParser.getListFromString(response, "chats", "last_message");
                                                      List<String> chats_last_message_ids = JSONParser.getListFromString(response, "chats", "last_message_id");
                                                      List<String> chats_last_message_senders = JSONParser.getListFromString(response, "chats", "last_message_sender");
                                                      List<String> chats_last_message_timestamps = JSONParser.getListFromString(response, "chats", "last_message_timestamp");

                                                      // DELETE DELETED CHATS!
                                                      List<Chat> all_chats = dbHelper.getAllChats();
                                                      for (Chat chat : all_chats) {
                                                          if (chats_ids.indexOf(String.valueOf(chat.id)) < 0) {
                                                              dbHelper.deleteChat(chat.id);
                                                          }
                                                      }


                                                      for (int i = 0; i < chats_ids.size(); i++) {
                                                          if (dbHelper.checkChat(Integer.parseInt(chats_ids.get(i)))) {
                                                              if (dbHelper.getChat(Integer.parseInt(chats_ids.get(i))).type.equals("deleted")) {
                                                                  continue;
                                                              }
                                                          }


                                                          Chat chat = new Chat();
                                                          chat.id = Integer.parseInt(chats_ids.get(i));
                                                          chat.creator = chats_creators.get(i);
                                                          chat.type = chats_types.get(i);
                                                          chat.participants = chats_participants.get(i);
                                                          chat.name = chats_names.get(i);
                                                          chat.image = chats_images.get(i);
                                                          chat.about = chats_abouts.get(i);
                                                          chat.timestamp = chats_timestamps.get(i);
                                                          chat.last_timestamp = chats_last_timestamps.get(i);
                                                          chats.add(chat);

                                                          if (dbHelper.checkChat(chat.id)) {
                                                              dbHelper.updateChat(chat);
                                                          } else {
                                                              dbHelper.createChat(chat);
                                                          }


                                                          ChatsMessage last_message = new ChatsMessage();
                                                          last_message.id = Integer.parseInt(chats_last_message_ids.get(i));
                                                          last_message.sender = chats_last_message_senders.get(i);
                                                          last_message.chat = chats_ids.get(i);
                                                          last_message.message = chats_last_messages.get(i);
                                                          last_message.timestamp = chats_last_message_timestamps.get(i);
                                                          last_message.status = "read";

                                                          if (!dbHelper.checkChatsMessage(last_message.id)) {
                                                              dbHelper.createChatsMessage(last_message);
                                                          }


                                                          if (Long.parseLong(chats_news.get(i)) > 0) {

                                                              for (int j = 0; j < Integer.parseInt(chats_news.get(i)) - 1; j++) {
                                                                  ChatsMessage new_message = new ChatsMessage();
                                                                  new_message.id = Integer.parseInt(chats_last_message_ids.get(i)) - j - 1;
                                                                  new_message.chat = chats_ids.get(i);
                                                                  new_message.message = "null";
                                                                  new_message.timestamp = chats_last_message_timestamps.get(i);
                                                                  new_message.status = "not-read";
                                                                  if (!dbHelper.checkChatsMessage(new_message.id)) {
                                                                      dbHelper.createChatsMessage(new_message);
                                                                  }
                                                              }

                                                              last_message = new ChatsMessage();
                                                              last_message.id = Integer.parseInt(chats_last_message_ids.get(i));
                                                              last_message.sender = chats_last_message_senders.get(i);
                                                              last_message.chat = chats_ids.get(i);
                                                              last_message.message = chats_last_messages.get(i);
                                                              last_message.timestamp = chats_last_message_timestamps.get(i);
                                                              last_message.status = "not-read";

                                                              if (!dbHelper.checkChatsMessage(last_message.id)) {
                                                                  dbHelper.createChatsMessage(last_message);
                                                              } else {
                                                                  dbHelper.updateChatsMessage(last_message);
                                                              }
                                                          }
                                                      }

                                                      Functions.putLong(getApplicationContext(), "last_chats_check_timestamp", System.currentTimeMillis());

                                                      sortItems();

                                                      chats_adapter.chats = chats;

                                                      chats_adapter.notifyDataSetChanged();
                                                  }

                                              } catch (
                                                      Exception e
                                                      )

                                              {
                                                  e.printStackTrace();
                                              }


                                          }
                                      }

                                              , new Response.ErrorListener()

                                      {


                                          @Override
                                          public void onErrorResponse(VolleyError error) {
                                              error.printStackTrace();
                                              //NetworkResponse networkResponse = error.networkResponse;
                                              //Log.e("debug", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                                              //Toast.makeText(getApplicationContext(), "Volley error: " + error.networkResponse.toString(), Toast.LENGTH_SHORT).show();
                                          }
                                      }

                                      )

                                      {

                                          @Override
                                          protected Map<String, String> getParams() {
                                              Map<String, String> params = new HashMap<String, String>();
                                              params.put("user", Functions.getString(getApplicationContext(), "id", ""));
                                              params.put("timestamp", String.valueOf(Functions.getLong(getApplicationContext(), "last_chats_check_timestamp", 0)));

                                              return params;
                                          }
                                      };


                                      //Adding request to request queue
                                      MyApplication.getInstance().

                                              addToRequestQueue(strReq);
                                  }
                              }

            );
        } else {
            loadOffline();
        }


    }

    void loadOffline() {
        ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> filter = new HashMap<String, String>();
        filter.put("name", "type");
        filter.put("method", "not-equal");
        filter.put("value", "deleted");
        filters.add(filter);
        chats = dbHelper.searchChats(filters, "id", 0, 0, true, true);


        sortItems();

        chats_adapter.chats = chats;
        chats_adapter.notifyDataSetChanged();
    }


    public static void sortItems() {

        Collections.sort(chats, new Comparator<Chat>() {
            @Override
            public int compare(Chat chat1, Chat chat2) {

                ArrayList<HashMap<String, String>> filters = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> filter = new HashMap<String, String>();
                filter.put("name", "chat");
                filter.put("method", "equal");
                filter.put("value", String.valueOf(chat1.id));
                filters.add(filter);

                List<ChatsMessage> messages1 = dbHelper.searchChatsMessages(filters, "id", 0, 1, true, true);

                filters.clear();
                filter = new HashMap<String, String>();
                filter.put("name", "chat");
                filter.put("method", "equal");
                filter.put("value", String.valueOf(chat2.id));
                filters.add(filter);

                List<ChatsMessage> messages2 = dbHelper.searchChatsMessages(filters, "id", 0, 1, true, true);

                if (messages1.size() == 0) {
                    return +1;
                }

                if (messages2.size() == 0) {
                    return -1;
                }

                return Functions.normalLength(String.valueOf(messages2.get(0).timestamp), 15, "0", true).
                        compareTo(Functions.normalLength(String.valueOf(messages1.get(0).timestamp), 15, "0", true));

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();


        if (!intent_action.isEmpty()) {
            titleText.setText("انتخاب گفتگو");
        } else {
            titleText.setText("گفتگوها");
        }


        fetchChats();

        sortItems();
        chats_adapter.chats = chats;
        chats_adapter.notifyDataSetChanged();

        if (Functions.isOnline(getApplicationContext())) {
            Functions.putLong(getApplicationContext(), "last_chats_check_timestamp", System.currentTimeMillis());
        }

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.INTENT_PUSH_NOTIFICATION));

        // clearing the notification tray
        NotificationUtils.clearNotifications();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}