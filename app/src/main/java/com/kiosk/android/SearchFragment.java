package com.kiosk.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.kiosk.android.adapter.PostsGridAdapter;
import com.kiosk.android.adapter.UsersAdapter;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Config;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by SeyyedMojtaba on 2/25/2016.
 */

@SuppressLint("NewApi")
public class SearchFragment extends Fragment {
    public static Activity activity;
    public static Context context;
    public static View view;

    public static int users_start = 0, users_amount = Config.DEFAULT_USERS_BUFFER_ITEMS;
    public static int posts_start = 0, posts_amount = Config.DEFAULT_POSTS_BUFFER_ITEMS;

    public static UsersAdapter users_adapter;
    public static PostsGridAdapter posts_adapter;
    public static ListView list;
    public static GridView grid;

    public static ImageButton users, posts;

    public static String searchMethod = "user";

    public static List<String> users_ids = new ArrayList<String>();
    public static List<String> users_names = new ArrayList<String>();
    public static List<String> users_profile_images = new ArrayList<String>();
    public static List<String> users_timestamps = new ArrayList<String>();
    public static List<String> users_follows = new ArrayList<String>();

    public static List<String> posts_ids = new ArrayList<String>();
    public static List<String> posts_images = new ArrayList<String>();
    public static List<String> posts_prices = new ArrayList<String>();

    public static boolean isProcessing = false;

    public static EditText fieldText;

    public static ProgressBar loading;

    public static boolean isLoaded = false;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        this.view = rootView;

        users_amount = Functions.getInt(context, "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);
        posts_amount = Functions.getInt(context, "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

        list = (ListView) rootView.findViewById(R.id.list);
        grid = (GridView) rootView.findViewById(R.id.grid);
        users_adapter = new UsersAdapter(activity, null, true);
        posts_adapter = new PostsGridAdapter(activity, null);

        users = (ImageButton) rootView.findViewById(R.id.users);
        posts = (ImageButton) rootView.findViewById(R.id.posts);

        users_adapter.users = users_ids;
        users_adapter.user_profile_images = users_profile_images;
        users_adapter.user_timestamps = users_timestamps;
        users_adapter.user_names = users_names;
        users_adapter.follows = users_follows;

        list.setAdapter(users_adapter);
        list.setSmoothScrollbarEnabled(true);

        posts_adapter.ids = posts_ids;
        posts_adapter.images = posts_images;
        posts_adapter.prices = posts_prices;

        grid.setAdapter(posts_adapter);
        grid.setSmoothScrollbarEnabled(true);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (users_ids.get(i).equals(Functions.getString(activity, "id", ""))) {
                    Intent intent = new Intent(activity, MainActivity.class);
                    intent.putExtra("action", "open_profile");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                } else {
                    Intent localIntent = new Intent(activity, ProfileActivity.class);
                    localIntent.putExtra("id", users_ids.get(i));
                    activity.startActivity(localIntent);
                }
            }

        });

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent localIntent = new Intent(activity, PostActivity.class);
                localIntent.putExtra("id", posts_ids.get(i));
                startActivity(localIntent);
            }

        });

        loading = (ProgressBar) rootView.findViewById(R.id.loading);


        users.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                searchMethod = "user";
                posts.setBackgroundColor(context.getResources().getColor(R.color.basicSecondary));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    users.setBackground(getResources().getDrawable(R.drawable.selectable_item_background_general, context.getTheme()));
                } else {
                    users.setBackground(getResources().getDrawable(R.drawable.selectable_item_background_general));
                }
                grid.setVisibility(View.GONE);
                list.setVisibility(View.VISIBLE);
            }
        });

        posts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchMethod = "post";
                users.setBackgroundColor(context.getResources().getColor(R.color.basicSecondary));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    posts.setBackground(getResources().getDrawable(R.drawable.selectable_item_background_general, context.getTheme()));
                } else {
                    posts.setBackground(getResources().getDrawable(R.drawable.selectable_item_background_general));
                }
                grid.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
            }
        });

        fieldText = (EditText) rootView.findViewById(R.id.field);
        ImageButton searchButton = (ImageButton) rootView.findViewById(R.id.search);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((!Functions.isOnline(context))) {
                    Toast.makeText(activity, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
                    return;
                }


                /*if (fieldText.getText().toString().isEmpty()) {
                    return;
                }*/

                if (searchMethod.equals("user")) {

                    users_start = 0;
                    users_amount = Functions.getInt(context, "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);

                    users_ids.clear();
                    users_names.clear();
                    users_profile_images.clear();
                    users_timestamps.clear();
                    users_follows.clear();


                    users_adapter.users = users_ids;
                    users_adapter.user_profile_images = users_profile_images;
                    users_adapter.user_timestamps = users_timestamps;
                    users_adapter.user_names = users_names;
                    users_adapter.follows = users_follows;

                    //users_adapter.notifyDataSetChanged();

                    list.setOnScrollListener(null);

                    loadUsers();
                } else if (searchMethod.equals("post")) {

                    posts_start = 0;
                    posts_amount = Functions.getInt(context, "POSTS_BUFFER_ITEMS", Config.DEFAULT_POSTS_BUFFER_ITEMS);

                    posts_ids.clear();
                    posts_images.clear();
                    posts_prices.clear();

                    posts_adapter.ids = posts_ids;
                    posts_adapter.images = posts_images;
                    posts_adapter.prices = posts_prices;

                    //posts_adapter.notifyDataSetChanged();

                    grid.setOnScrollListener(null);

                    loadPosts();
                }
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public static void loadUsers() {
        if (isProcessing) {
            return;
        }

        isProcessing = true;
        final String field = fieldText.getText().toString();
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                //loading.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in));
                loading.setVisibility(View.VISIBLE);

                //InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                //imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.searchUsers(field, users_start, users_amount, Functions.getString(context, "id", "")));
            }

            @Override
            protected void onPostExecute(String result) {


                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "users").equals("-1")) {

                    users_ids.addAll(JSONParser.getListFromString(result, "users", "id"));
                    users_profile_images.addAll(JSONParser.getListFromString(result, "users", "profile_image"));
                    users_timestamps.addAll(JSONParser.getListFromString(result, "users", "timestamp"));
                    users_names.addAll(JSONParser.getListFromString(result, "users", "name"));
                    users_follows.addAll(JSONParser.getListFromString(result, "users", "follow"));

                    users_start += users_amount;

                    users_adapter.users = users_ids;
                    users_adapter.user_profile_images = users_profile_images;
                    users_adapter.user_timestamps = users_timestamps;
                    users_adapter.user_names = users_names;
                    users_adapter.follows = users_follows;

                    if (users_start == users_amount) { // IF IS FIRST LOAD


                        list.setOnScrollListener(new AbsListView.OnScrollListener() {
                            public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == users_ids.size()))
                                    loadUsers();
                            }

                            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                            }
                        });
                    }


                } else {
                    list.setOnScrollListener(null);
                }

                users_adapter.notifyDataSetChanged();
                isProcessing = false;
                //loading.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_out));
                loading.setVisibility(View.INVISIBLE);

                super.onPostExecute(result);
            }


        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }


    public static void loadPosts() {


        if (isProcessing) {
            return;
        }

        isProcessing = true;
        final String field = fieldText.getText().toString();

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                //loading.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in));
                loading.setVisibility(View.VISIBLE);

                //InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.searchPosts(field, posts_start, posts_amount, Functions.getString(context, "id", "")));
            }

            @Override
            protected void onPostExecute(String result) {
                String posts_json = result;

                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "posts").equals("-1")) {
                    posts_ids.addAll(JSONParser.getListFromString(posts_json, "posts", "id"));
                    posts_images.addAll(JSONParser.getListFromString(posts_json, "posts", "image"));
                    posts_prices.addAll(JSONParser.getListFromString(posts_json, "posts", "price"));

                    posts_start += posts_amount;

                    posts_adapter.ids = posts_ids;
                    posts_adapter.images = posts_images;
                    posts_adapter.prices = posts_prices;


                    if (posts_start == posts_amount) { // IF IS FIRST LOAD

                        grid.setOnScrollListener(new AbsListView.OnScrollListener() {
                            @Override
                            public void onScroll(AbsListView listView, int paramInt1, int paramInt2, int paramInt3) {
                                //Log.d("tag", "if ((" + paramInt1 + " + " + paramInt2 + " == " + paramInt3 + ") && (" + paramInt3 + " == " + posts_ids.size() + "))");
                                if ((paramInt1 + paramInt2 == paramInt3) && (paramInt3 == posts_ids.size()))
                                    loadPosts();
                            }

                            @Override
                            public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {
                            }
                        });
                    }


                } else {
                    grid.setOnScrollListener(null);
                }

                posts_adapter.notifyDataSetChanged();
                isProcessing = false;
                loading.setVisibility(View.INVISIBLE);

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    public static void searchTopUsers() {
        searchMethod = "user";

        posts.setBackgroundColor(context.getResources().getColor(R.color.basicSecondary));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            users.setBackground(context.getResources().getDrawable(R.drawable.selectable_item_background_general, context.getTheme()));
        } else {
            users.setBackground(context.getResources().getDrawable(R.drawable.selectable_item_background_general));
        }
        grid.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);


        users_start = 0;
        users_amount = Functions.getInt(context, "USERS_BUFFER_ITEMS", Config.DEFAULT_USERS_BUFFER_ITEMS);

        users_ids.clear();
        users_names.clear();
        users_profile_images.clear();
        users_timestamps.clear();
        users_follows.clear();


        users_adapter.users = users_ids;
        users_adapter.user_profile_images = users_profile_images;
        users_adapter.user_timestamps = users_timestamps;
        users_adapter.user_names = users_names;
        users_adapter.follows = users_follows;


        users_adapter.notifyDataSetChanged();

        list.setOnScrollListener(null);

        loadUsers();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        } else {
            this.context = context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}