package com.kiosk.android;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.LinkManager;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class PostActivity extends AppCompatActivity {
    private DatabaseHelper dbHelper;
    private Post post;
    private DownloadManager.DownloadTask downloadTask;
    //private CircularProgressBar progress;

    private String id;

    private ImageButton likeButton;
    private TextView likesText;
    private ImageView likeBig;

    private boolean isDownloading = false;

    private ProgressBar loading;

    private String download_link = "";

    //ImageLoader imageLoader = ImageLoader.getInstance();
    //DisplayImageOptions options = new DisplayImageOptions.Builder().delayBeforeLoading(0).cacheInMemory(true).cacheOnDisk(true).build();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        if ((!Functions.isOnline(getApplicationContext()))) {
            Toast.makeText(PostActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        // LOGIN FIRST!
        if (!Functions.getBoolean(getApplicationContext(), "logged in", false)) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }

        //imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
        dbHelper = new DatabaseHelper(getApplicationContext());
        loading = (ProgressBar) findViewById(R.id.loading);
        final LinearLayout layout = (LinearLayout) findViewById(R.id.layout);

        if (getIntent().getStringExtra("id") != null) {
            id = getIntent().getStringExtra("id");
        } else {
            Uri data = getIntent().getData();
            List<String> params = data.getQueryParameters("id");

            id = params.get(0);
        }

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.getPost(id, Functions.getString(getApplicationContext(), "id", "")));
            }

            @Override
            protected void onPostExecute(String result) {

                loading.setVisibility(View.INVISIBLE);

                layout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
                layout.setVisibility(View.VISIBLE);

                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "post").equals("-1")) {

                    post = new Post();
                    post.id = Integer.parseInt(JSONParser.getListFromString(result, "post", "id").get(0));
                    post.author = JSONParser.getListFromString(result, "post", "author").get(0);
                    post.author_name = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "post", "author_name").get(0));
                    post.author_profile_image = JSONParser.getListFromString(result, "post", "author_profile_image").get(0);
                    post.author_timestamp = JSONParser.getListFromString(result, "post", "author_timestamp").get(0);
                    post.name = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "post", "name").get(0));
                    post.description = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "post", "description").get(0));
                    post.image = JSONParser.getListFromString(result, "post", "image").get(0);
                    post.price = JSONParser.getListFromString(result, "post", "price").get(0);
                    post.download_count = JSONParser.getListFromString(result, "post", "download_count").get(0);
                    post.timestamp = JSONParser.getListFromString(result, "post", "timestamp").get(0);
                    post.likes = JSONParser.getListFromString(result, "post", "likes").get(0);
                    post.user_like = JSONParser.getListFromString(result, "post", "user_like").get(0);
                    post.comments = JSONParser.getListFromString(result, "post", "comments").get(0);
                    post.downloadable = JSONParser.getListFromString(result, "post", "downloadable").get(0);

                    if (dbHelper.checkPost(post.id)) {
                        dbHelper.updatePost(post);
                    } else {
                        dbHelper.createPost(post);
                    }

                    loadPost();
                } else {
                    finish();
                }

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // ACTIONBAR BUTTONS

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void loadPost() {
        CircleImageView profileImageView = (CircleImageView) findViewById(R.id.profile_image);
        TextView authorText = (TextView) findViewById(R.id.author);
        TextView timeText = (TextView) findViewById(R.id.time);
        final ImageView imageView = (ImageView) findViewById(R.id.image);
        imageView.setVisibility(View.VISIBLE);
        TextView priceText = (TextView) findViewById(R.id.price);
        likeBig = (ImageView) findViewById(R.id.likeBig);
        //progress = (CircularProgressBar) findViewById(R.id.progress);
        likeButton = (ImageButton) findViewById(R.id.like);
        ImageButton commentButton = (ImageButton) findViewById(R.id.comment);
        ImageButton shareButton = (ImageButton) findViewById(R.id.share);
        ImageButton moreButton = (ImageButton) findViewById(R.id.more);
        likesText = (TextView) findViewById(R.id.likes);
        TextView nameText = (TextView) findViewById(R.id.name);
        TextView descriptionText = (TextView) findViewById(R.id.description);
        final TextView allCommentsText = (TextView) findViewById(R.id.all_comments);
        final TextView comment1Text = (TextView) findViewById(R.id.comment1);
        final TextView comment2Text = (TextView) findViewById(R.id.comment2);
        final TextView comment3Text = (TextView) findViewById(R.id.comment3);

        final Button downloadLinkButton = (Button) findViewById(R.id.download_link);

        downloadLinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(Information.BASE_NAME, download_link);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(PostActivity.this, "لینک دانلود به حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
            }
        });


        if (!post.author_profile_image.isEmpty()) {
            Picasso.with(PostActivity.this).load(Functions.getString(getApplicationContext(), "PROFILE_IMAGES_URL", Information.DEFAULT_PROFILE_IMAGES_URL) + post.author_profile_image + "_small.jpg" + "?" + post.author_timestamp).fit().centerCrop().error(R.drawable.ic_profile).into(profileImageView);
        } else {
            Picasso.with(PostActivity.this).load(R.drawable.ic_profile).into(profileImageView);
        }

        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.author.equals(Functions.getString(PostActivity.this, "id", ""))) {
                    Intent intent = new Intent(PostActivity.this, MainActivity.class);
                    intent.putExtra("action", "open_profile");
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    PostActivity.this.startActivity(intent);
                    finish();
                } else {
                    Intent localIntent = new Intent(PostActivity.this, ProfileActivity.class);
                    localIntent.putExtra("id", post.author);
                    PostActivity.this.startActivity(localIntent);
                }
            }
        });

        authorText.setText(post.author_name);
        authorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.author.equals(Functions.getString(PostActivity.this, "id", ""))) {
                    Intent intent = new Intent(PostActivity.this, MainActivity.class);
                    intent.putExtra("action", "open_profile");
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    PostActivity.this.startActivity(intent);
                    finish();
                } else {
                    Intent localIntent = new Intent(PostActivity.this, ProfileActivity.class);
                    localIntent.putExtra("id", post.author);
                    PostActivity.this.startActivity(localIntent);
                }
            }
        });

        timeText.setText(Functions.normalPastTime(PersianDate.getShamsidate(Locale.getDefault(), Functions.getDateFromTimestamp(TimeZone.getDefault(), Long.parseLong(post.timestamp))), Functions.getDateTimeFromTimestamp(TimeZone.getDefault(), Long.parseLong(post.timestamp), "hh:mm:ss")));

/*
        imageLoader.displayImage(post.image, imageView, options, new SimpleImageLoadingListener() {


            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progress.setProgress(5);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imageView.setImageResource(R.drawable.ic_no_image);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                imageView.setImageResource(R.drawable.ic_no_image);
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                progress.setProgress(current / total * 100);
            }
        });

        */

        if (!post.image.isEmpty()) {
            Picasso.with(getApplicationContext()).load(post.image).error(R.drawable.ic_no_image).into(imageView);
        } else {
            Picasso.with(getApplicationContext()).load(R.drawable.ic_no_image).into(imageView);
        }


        final GestureDetector gdt = new GestureDetector(new GestureListener());
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gdt.onTouchEvent(motionEvent);
            }
        });

        if (post.price.equals("0")) {
            priceText.setVisibility(View.INVISIBLE);
        } else {
            priceText.setVisibility(View.VISIBLE);
            priceText.setText(Functions.normalPrice(post.price, ".") + " ریال");
        }

        if (post.user_like.equals("1")) {
            likeButton.setBackgroundResource(R.drawable.selector_button_unlike);
        } else {
            likeButton.setBackgroundResource(R.drawable.selector_button_like);
        }

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.user_like.equals("1")) {
                    likeButton.setBackgroundResource(R.drawable.selector_button_like);
                    post.likes = String.valueOf(Integer.parseInt(post.likes) - 1);
                    post.user_like = "0";
                    if (dbHelper.checkPost(post.id)) {
                        dbHelper.updatePost(post);
                    }

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            Fetcher.FetchURL(APIManager.likePost(Functions.getString(PostActivity.this, "id", ""), id));
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String likesDesc = "";
                                    if (Integer.parseInt(post.likes) == 0) {
                                        likesDesc = "بدون پسند";
                                    } else if (Integer.parseInt(post.likes) == 1) {
                                        likesDesc = "1 نفر پسندید";
                                    } else {
                                        likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
                                    }
                                    likesText.setText(likesDesc);
                                }
                            });
                        }
                    });

                } else {
                    likeButton.setBackgroundResource(R.drawable.selector_button_unlike);
                    post.user_like = "1";
                    post.likes = String.valueOf(Integer.parseInt(post.likes) + 1);
                    if (dbHelper.checkPost(post.id)) {
                        dbHelper.updatePost(post);
                    }

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            Fetcher.FetchURL(APIManager.likePost(Functions.getString(PostActivity.this, "id", ""), id));
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String likesDesc = "";
                                    if (Integer.parseInt(post.likes) == 0) {
                                        likesDesc = "بدون پسند";
                                    } else if (Integer.parseInt(post.likes) == 1) {
                                        likesDesc = "1 نفر پسندید";
                                    } else {
                                        likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
                                    }
                                    likesText.setText(likesDesc);
                                }
                            });
                        }
                    });

                }
            }
        });


        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent localIntent = new Intent(PostActivity.this, PostCommentsActivity.class);
                localIntent.putExtra("id", id);
                startActivity(localIntent);
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.shareTextViaIntent(PostActivity.this, getString(R.string.share), Functions.getString(getApplicationContext(), "USERS_BUFFER_ITEMS", Information.DEFAULT_FOOTER_DESCRIPTION), LinkManager.viewPost(post.id));
            }
        });


        String likesDesc = "";
        if (Integer.parseInt(post.likes) == 0) {
            likesDesc = "بدون پسند";
        } else if (Integer.parseInt(post.likes) == 1) {
            likesDesc = "1 نفر پسندید";
        } else {
            likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
        }
        likesText.setText(likesDesc);
        likesText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PostActivity.this, UsersActivity.class);
                intent.putExtra("method", "liked");
                intent.putExtra("argument", id);
                startActivity(intent);
            }
        });


        nameText.setText(post.name);

        if (!post.description.isEmpty()) {
            descriptionText.setVisibility(View.VISIBLE);
            descriptionText.setText(CustomFunctions.getHtml(getApplicationContext(), post.description));
            descriptionText.setMovementMethod(LinkMovementMethod.getInstance());
            Functions.stripUnderlines(descriptionText);

        } else {
            descriptionText.setVisibility(View.GONE);
        }

        LinearLayout getLayout = (LinearLayout) findViewById(R.id.get_layout);
        if (!post.downloadable.equals("1")) {
            getLayout.setVisibility(View.GONE);
        }

        final TextView downloadCountText = (TextView) findViewById(R.id.download_count);
        downloadCountText.setText(Functions.normalPrice(post.download_count, ",") + " دانلود");

        final ActionProcessButton getButton = (ActionProcessButton) findViewById(R.id.get);
        getButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDownloading) return;
                new AsyncTask<String, String, String>() {

                    @Override
                    protected void onPreExecute() {
                        getButton.setMode(ActionProcessButton.Mode.ENDLESS);
                        getButton.setProgress(1);
                        getButton.setText("در حال دریافت...");

                        super.onPreExecute();
                    }

                    @Override
                    protected String doInBackground(String... strings) {
                        return Fetcher.FetchURL(APIManager.purchasePost(Functions.getString(getApplicationContext(), "id", ""), id));
                    }

                    @Override
                    protected void onPostExecute(final String result) {
                        if (result.equals("invalid-post")) {
                            getButton.setText("مطلب نامعتبر است!");
                            getButton.setBackgroundColor(Color.RED);
                            getButton.setProgress(100);
                        } else if (result.equals("not-enough-balance")) {
                            getButton.setText("موجودی کافی نیست!");
                            getButton.setBackgroundColor(Color.RED);
                            getButton.setProgress(100);
                        } else if (result.contains("success;")) {
                            downloadLinkButton.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
                            downloadLinkButton.setVisibility(View.VISIBLE);
                            CustomFunctions.Login(PostActivity.this);
                            download_link = result.replaceFirst("success;", "");
                            post.download_count = String.valueOf(Integer.parseInt(post.download_count) + 1);
                            if (dbHelper.checkPost(post.id)) {
                                dbHelper.updatePost(post);
                            }
                            downloadCountText.setText(Functions.normalPrice(post.download_count, ",") + " دانلود");
                            isDownloading = true;
                            if (new File(Information.DOWNLOADS_PATH + "/" + Functions.getFileName(download_link)).exists()) {
                                isDownloading = false;
                                getButton.setText("باز کردن");
                                getButton.setProgress(100);
                                Functions.openFileViaIntent(getApplicationContext(), getString(R.string.open), Information.DOWNLOADS_PATH + "/" + Functions.getFileName(download_link));
                            } else {
                                getButton.setMode(ActionProcessButton.Mode.PROGRESS);
                                getButton.setText("در حال دریافت...");
                                downloadTask = new DownloadManager.DownloadTask();
                                downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                    @Override
                                    public void onProgressUpdate(int progress) {
                                        getButton.setProgress(progress);
                                    }

                                    @Override
                                    public void onPostExecute(boolean result) {
                                        isDownloading = false;
                                        if (result) {
                                            getButton.setText("باز کردن");
                                        }
                                        getButton.setProgress(100);
                                    }
                                });

                                downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{download_link, Information.DOWNLOADS_PATH, Functions.getFileName(download_link)});
                            }

                        } else {
                            getButton.setText("خطای غیرمنتظره ای پیش آمده است!");
                            getButton.setBackgroundColor(Color.RED);
                            getButton.setProgress(100);
                        }


                        super.onPostExecute(result);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });


        moreButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                AlertDialog.Builder localBuilder = new AlertDialog.Builder(PostActivity.this);
                ArrayList<String> localArrayList = new ArrayList<String>();
                localArrayList.add("گزارش");
                localArrayList.add("دانلود تصویر");
                localArrayList.add("کپی لینک اشتراک");
                if (post.reading.equals("1")) {
                    localArrayList.add("حذف از لیست خواندن");
                } else {
                    localArrayList.add("افزودن به لیست خواندن");
                }
                if (post.author.equals(Functions.getString(getApplicationContext(), "id", ""))) {
                    localArrayList.add("ویرایش");
                    localArrayList.add("حذف");
                }

                String[] arrayOfString = (String[]) localArrayList.toArray(new String[localArrayList.size()]);
                localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                        switch (paramAnonymous2Int) {
                            default:
                                return;
                            case 0:
                                try {
                                    final String[] arrayOfString = {"محتوای نامناسب", "نقض قانون کپی رایت", "خرابی لینک دانلود", "قیمت نامناسب"};
                                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(PostActivity.this);
                                    localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, final int i) {
                                            final SweetAlertDialog sDialog = new SweetAlertDialog(PostActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

                                            new AsyncTask<String, String, String>() {

                                                @Override
                                                protected void onPreExecute() {
                                                    sDialog.show();
                                                    sDialog.setCancelable(false);

                                                    super.onPreExecute();
                                                }

                                                @Override
                                                protected String doInBackground(String... arg0) {
                                                    return Fetcher.FetchURL(APIManager.setPostMyReport(id, Functions.getString(getApplicationContext(), "id", ""), arrayOfString[i]));
                                                }

                                                protected void onPostExecute(final String result) {
                                                    runOnUiThread(new Runnable() {
                                                        public void run() {

                                                            if (result.contains("success")) {
                                                                sDialog.setTitleText("").setContentText("گزارش شما ثبت شد.").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                                            } else {
                                                                sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                            }
                                                        }
                                                    });

                                                }
                                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                            sDialog.show();
                                        }
                                    });

                                    localBuilder.create().show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 1:
                                if (!new File(Information.IMAGES_PATH + post.id + ".jpg").exists()) {
                                    downloadTask = new DownloadManager.DownloadTask();
                                    downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{post.image, Information.IMAGES_PATH, post.id + ".jpg"});
                                    downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                                        @Override
                                        public void onProgressUpdate(int progress) {

                                        }

                                        @Override
                                        public void onPostExecute(boolean result) {
                                            if (result) {
                                                Toast.makeText(PostActivity.this, "تصویر مطلب درون حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(PostActivity.this, "تصویر مطلب درون حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                }

                                break;
                            case 2:
                                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText(Information.BASE_NAME, LinkManager.viewPost(post.id));
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(PostActivity.this, "لینک اشتراک مطلب به حافظه دستگاه کپی شد.", Toast.LENGTH_SHORT).show();
                                break;
                            case 3:
                                try {
                                    if (post.reading.equals("1")) {
                                        post.reading = "0";
                                        if (dbHelper.checkPost(post.id)) {
                                            dbHelper.deletePost(post.id);
                                        }
                                        Toast.makeText(PostActivity.this, "از لیست خواندن حذف شد.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        post.reading = "1";
                                        if (!dbHelper.checkPost(post.id)) {
                                            dbHelper.createPost(post);
                                        } else {
                                            dbHelper.updatePost(post);
                                        }
                                        Toast.makeText(PostActivity.this, "به لیست خواندن اضافه شد.", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 4:
                                Intent localIntent = new Intent(PostActivity.this, EditPostActivity.class);
                                localIntent.putExtra("id", id);
                                startActivity(localIntent);
                                break;
                            case 5:
                                new SweetAlertDialog(PostActivity.this, SweetAlertDialog.WARNING_TYPE).setTitleText("حذف مطلب").setContentText("آیا مطمئنید میخواهید این مطلب را حذف کنید؟").setConfirmText("بله").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(final SweetAlertDialog sDialog) {
                                        new AsyncTask<String, String, String>() {

                                            @Override
                                            protected void onPreExecute() {
                                                sDialog.setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false).changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                                sDialog.show();
                                                sDialog.setCancelable(false);

                                                super.onPreExecute();
                                            }

                                            @Override
                                            protected String doInBackground(String... arg0) {
                                                if (Functions.isOnline(getApplicationContext())) {
                                                    return Fetcher.FetchURL(APIManager.deleteMyPost(String.valueOf(post.id), Functions.getString(getApplicationContext(), "id", "")));
                                                }
                                                return "fail";
                                            }

                                            protected void onPostExecute(final String result) {
                                                runOnUiThread(new Runnable() {
                                                    public void run() {

                                                        if (result.contains("success")) {
                                                            if (dbHelper.checkPost(post.id)) {
                                                                dbHelper.deletePost(post.id);
                                                            }
                                                            sDialog.setTitleText("").setContentText("مطلب شما حذف شد!").setConfirmText("باشه").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                    finish();
                                                                }
                                                            }).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                                        } else {
                                                            sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                        }
                                                    }
                                                });

                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }


                                }).showCancelButton(true).setCancelText("انصراف").setCancelClickListener(null).show();


                                break;
                        }

                    }
                });
                localBuilder.create().show();
            }
        });


        String[] comments_array = post.comments.split("%end_row%");


        if (Integer.parseInt(comments_array[0]) > 3) {
            allCommentsText.setVisibility(View.VISIBLE);
            allCommentsText.setText("مشاهده همه " + Functions.normalPrice(comments_array[0], ",") + " نظر");
        } else {
            allCommentsText.setVisibility(View.GONE);
        }
        allCommentsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent localIntent = new Intent(PostActivity.this, PostCommentsActivity.class);
                localIntent.putExtra("id", id);
                startActivity(localIntent);
            }
        });

        if (Integer.parseInt(comments_array[0]) > 0) {
            comment1Text.setVisibility(View.VISIBLE);
            comment1Text.setText(CustomFunctions.getCommentHtml(getApplicationContext(), comments_array[1].split("%end_record%")[0], EmojiMapUtil.replaceCheatSheetEmojis(comments_array[1].split("%end_record%")[1]), EmojiMapUtil.replaceCheatSheetEmojis(comments_array[1].split("%end_record%")[2])));
            comment1Text.setMovementMethod(LinkMovementMethod.getInstance());
            Functions.stripUnderlines(comment1Text);
        } else {
            comment1Text.setVisibility(View.GONE);
        }

        if (Integer.parseInt(comments_array[0]) > 1) {
            comment2Text.setVisibility(View.VISIBLE);
            comment2Text.setText(CustomFunctions.getCommentHtml(getApplicationContext(), comments_array[2].split("%end_record%")[0], EmojiMapUtil.replaceCheatSheetEmojis(comments_array[2].split("%end_record%")[1]), EmojiMapUtil.replaceCheatSheetEmojis(comments_array[2].split("%end_record%")[2])));
            comment2Text.setMovementMethod(LinkMovementMethod.getInstance());
            Functions.stripUnderlines(comment2Text);
        } else {
            comment2Text.setVisibility(View.GONE);
        }

        if (Integer.parseInt(comments_array[0]) > 2) {
            comment3Text.setVisibility(View.VISIBLE);
            comment3Text.setText(CustomFunctions.getCommentHtml(getApplicationContext(), comments_array[3].split("%end_record%")[0], EmojiMapUtil.replaceCheatSheetEmojis(comments_array[3].split("%end_record%")[1]), EmojiMapUtil.replaceCheatSheetEmojis(comments_array[3].split("%end_record%")[2])));
            comment3Text.setMovementMethod(LinkMovementMethod.getInstance());
            Functions.stripUnderlines(comment3Text);
        } else {
            comment3Text.setVisibility(View.GONE);
        }
    }


    private class GestureListener implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {

            if (!new File(Information.IMAGES_PATH + post.id + ".jpg").exists()) {
                downloadTask = new DownloadManager.DownloadTask();
                downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{post.image, Information.IMAGES_PATH, post.id + ".jpg"});
                downloadTask.setOnEventListener(new DownloadManager.OnEventListener() {
                    @Override
                    public void onProgressUpdate(int progress) {

                    }

                    @Override
                    public void onPostExecute(boolean result) {
                        if (result) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(android.content.Intent.CATEGORY_DEFAULT);
                            intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + post.id + ".jpg")), "image/*");
                            startActivity(intent);
                        }
                    }
                });
            } else {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(android.content.Intent.CATEGORY_DEFAULT);
                intent.setDataAndType(Uri.fromFile(new File(Information.IMAGES_PATH + post.id + ".jpg")), "image/*");
                startActivity(intent);
            }
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent motionEvent) {
            likeBig.startAnimation(AnimationUtils.loadAnimation(PostActivity.this, R.anim.instagram_like));
            if (!post.user_like.equals("1")) {
                likeButton.setBackgroundResource(R.drawable.selector_button_unlike);
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        Fetcher.FetchURL(APIManager.likePost(Functions.getString(PostActivity.this, "id", ""), id));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                post.user_like = "1";
                                post.likes = String.valueOf(Integer.parseInt(post.likes) + 1);
                                String likesDesc = "";
                                if (Integer.parseInt(post.likes) == 0) {
                                    likesDesc = "بدون پسند";
                                } else if (Integer.parseInt(post.likes) == 1) {
                                    likesDesc = "1 نفر پسندید";
                                } else {
                                    likesDesc = Functions.normalPrice(post.likes, ",") + " نفر پسندیدند";
                                }
                                likesText.setText(likesDesc);
                                if (dbHelper.checkPost(post.id)) {
                                    dbHelper.updatePost(post);
                                }
                            }
                        });
                    }
                });
            }
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onDown(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
            return false;
        }
    }


}