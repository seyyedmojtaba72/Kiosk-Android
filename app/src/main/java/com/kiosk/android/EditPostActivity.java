package com.kiosk.android;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.github.seyyedmojtaba72.android_utils.DownloadManager;
import com.github.seyyedmojtaba72.android_utils.EmojiMapUtil;
import com.github.seyyedmojtaba72.android_utils.Fetcher;
import com.github.seyyedmojtaba72.android_utils.Functions;
import com.github.seyyedmojtaba72.android_utils.JSONParser;
import com.github.seyyedmojtaba72.android_utils.PersianDate;
import com.github.seyyedmojtaba72.android_utils.widget.TextViewPlus;
import com.kiosk.android.info.APIManager;
import com.kiosk.android.info.Information;
import com.kiosk.android.info.LinkManager;
import com.kiosk.android.model.Post;
import com.kiosk.android.util.CustomFunctions;
import com.kiosk.android.util.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class EditPostActivity extends AppCompatActivity {
    private Post post;
    //private CircularProgressBar progress;

    private String id;

    private ProgressBar loading;

    EditText nameText, descriptionText;

    ActionProcessButton submitButton;
    private boolean isProcessing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        if ((!Functions.isOnline(getApplicationContext()))) {
            Toast.makeText(EditPostActivity.this, "لطفاً به اینترنت متصل شوید.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        nameText = (EditText) findViewById(R.id.name);
        descriptionText = (EditText) findViewById(R.id.description);

        submitButton = (ActionProcessButton) findViewById(R.id.submit);
        submitButton.setMode(ActionProcessButton.Mode.ENDLESS);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitPost();
            }
        });

        //imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
        loading = (ProgressBar) findViewById(R.id.loading);
        final LinearLayout layout = (LinearLayout) findViewById(R.id.layout);

        if (getIntent().getStringExtra("id") != null) {
            id = getIntent().getStringExtra("id");
        }

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                return Fetcher.FetchURL(APIManager.getPost(id, Functions.getString(getApplicationContext(), "id", "")));
            }

            @Override
            protected void onPostExecute(String result) {

                loading.setVisibility(View.INVISIBLE);

                layout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
                layout.setVisibility(View.VISIBLE);

                if (!result.isEmpty() && !result.equals("-1") && !JSONParser.getFromString(result, "post").equals("-1")) {

                    post = new Post();
                    post.id = Integer.parseInt(JSONParser.getListFromString(result, "post", "id").get(0));
                    post.author = JSONParser.getListFromString(result, "post", "author").get(0);
                    post.author_name = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "post", "author_name").get(0));
                    post.author_profile_image = JSONParser.getListFromString(result, "post", "author_profile_image").get(0);
                    post.author_timestamp = JSONParser.getListFromString(result, "post", "author_timestamp").get(0);
                    post.name = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "post", "name").get(0));
                    post.description = EmojiMapUtil.replaceCheatSheetEmojis(JSONParser.getListFromString(result, "post", "description").get(0));
                    post.image = JSONParser.getListFromString(result, "post", "image").get(0);
                    post.price = JSONParser.getListFromString(result, "post", "price").get(0);
                    post.download_count = JSONParser.getListFromString(result, "post", "download_count").get(0);
                    post.timestamp = JSONParser.getListFromString(result, "post", "timestamp").get(0);
                    post.likes = JSONParser.getListFromString(result, "post", "likes").get(0);
                    post.user_like = JSONParser.getListFromString(result, "post", "user_like").get(0);
                    post.comments = JSONParser.getListFromString(result, "post", "comments").get(0);
                    post.downloadable = JSONParser.getListFromString(result, "post", "downloadable").get(0);

                    nameText.setText(post.name);
                    descriptionText.setText(post.description);



                    /*if (dbHelper.checkPost(post.id)) {
                        dbHelper.updatePost(post);
                    } else {
                        dbHelper.createPost(post);
                    }*/

                } else {
                    finish();
                }

                super.onPostExecute(result);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // ACTIONBAR

        TextViewPlus titleText = (TextViewPlus) findViewById(R.id.title);
        titleText.setText("ویرایش مطلب");

        ImageButton actionLeftButton = (ImageButton) findViewById(R.id.btn_left);
        actionLeftButton.setVisibility(View.VISIBLE);
        actionLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void submitPost() {
        if (isProcessing) {
            return;
        }

        isProcessing = true;

        final SweetAlertDialog sDialog = new SweetAlertDialog(EditPostActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("لطفاً کمی صبر کنید...").setContentText("").showCancelButton(false);

        new AsyncTask<String, String, String>() {
            String name = "", description = "";


            @Override
            protected void onPreExecute() {
                submitButton.setProgress(50);

                name = nameText.getText().toString();
                description = descriptionText.getText().toString();

                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... arg0) {
                return Fetcher.FetchURL(APIManager.editPost(id, name, description));
            }

            protected void onPostExecute(final String result) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (result.equals("success")) {
                            nameText.setText("");
                            descriptionText.setText("");

                            sDialog.setTitleText("").setContentText("مطلب شما ویرایش شد.").setConfirmText("باشه").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                    sDialog.hide();
                                }
                            }).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        } else {
                            sDialog.setTitleText("").setContentText("خطا در ثبت اطلاعات!").setConfirmText("باشه").setConfirmClickListener(null).changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        }

                        submitButton.setProgress(0);
                        isProcessing = false;
                    }
                });

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        sDialog.show();

    }


}